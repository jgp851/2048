#pragma once
#include <vector>
#include <allegro5\allegro.h>

struct AnimationFrame
{
	ALLEGRO_BITMAP *image;
	unsigned endTime;
	AnimationFrame(ALLEGRO_BITMAP *image, unsigned endTime);
};

class Animation
{
private:
	std::vector<AnimationFrame*> *frames;
	unsigned currentFrame;
	unsigned animationDuration;
	unsigned currentTime;
	unsigned endTime;
	float ratio;
	float dw;
	float dh;
	float maxSize;
public:
	Animation();
	~Animation();

	int getWidth();
	int getHeight();
	int getScaledWidth();
	int getScaledHeight();
	
	unsigned getDuration();
	unsigned getEndTime();

	void addFrame(ALLEGRO_BITMAP *bitmap, unsigned duration);
	void copyAnimation(Animation *source);
	void draw(int x, int y, int transOffset);
	void drawTinted(int x, int y, ALLEGRO_COLOR color);
	void reset();
	void setCurrentTime(unsigned currentTime);
	void setEndTime(unsigned endTime);
	void setRatio(float maxSize);
	void update(unsigned elapsedTime);
	Animation* clone();
	Animation* cloneReserved();
};