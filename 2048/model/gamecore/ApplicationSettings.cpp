#include "ApplicationSettings.h"
#include "GameCore.h"

ApplicationSettings::ApplicationSettings(GameCore *gameCore, std::string pathConfigFile)
{
	this->gameCore = gameCore;
	this->pathConfigFile = pathConfigFile;
	read(pathConfigFile);
}

ApplicationSettings::~ApplicationSettings()
{
}

GameState ApplicationSettings::getStateAfterRestart()
{
	return restartToGameState;
}

void ApplicationSettings::setStateToRestart(GameState state)
{
	restartToGameState = state;
}

bool ApplicationSettings::getFullScreen()
{
	return fullScreen;
}
int ApplicationSettings::getScreenWidth()
{
	return screenWidth;
}
int ApplicationSettings::getScreenHeight()
{
	return screenHeight;
}

void ApplicationSettings::setScreenWidth(int value)
{
	screenWidth = value;
}
void ApplicationSettings::setScreenHeight(int value)
{
	screenHeight = value;
}
void ApplicationSettings::setFullScreen(bool state)
{
	fullScreen = state;
}

void ApplicationSettings::read(const string& path)
{
	bool restartApplication;
	ifstream file(path.c_str(), ios::in | ios::binary);
	file.read((char*)&screenWidth, sizeof(int));
	file.read((char*)&screenHeight, sizeof(int));
	file.read((char*)&fullScreen, sizeof(bool));

	file.read((char*)&restartApplication, sizeof(bool));
	gameCore->setRestartAppSetting(restartApplication);

	file.read((char*)&restartToGameState, sizeof(GameState));
	file.close();
}

void ApplicationSettings::write(const string& path)
{
	bool restartApplication;
	ofstream file(path.c_str(), ios::out | ios::binary | ios::trunc);
	file.write((char*)&screenWidth, sizeof(int));
	file.write((char*)&screenHeight, sizeof(int));
	file.write((char*)&fullScreen, sizeof(bool));

	restartApplication = gameCore->getRestartAppSetting();
	file.write((char*)&restartApplication, sizeof(bool));

	file.write((char*)&restartToGameState, sizeof(GameState));
	file.close();
}

void ApplicationSettings::saveSettings()
{
	write(pathConfigFile);
}