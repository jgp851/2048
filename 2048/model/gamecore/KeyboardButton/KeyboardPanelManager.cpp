#include "KeyboardPanelManager.h"
#include "..\..\sources2048\Game2048Core.h"

KeyboardPanelManager::KeyboardPanelManager(Game2048Core *gameCore, std::vector<KeyboardButtonPanel*> *buttonPanels, int x, int y, int align)
{
	this->gameCore = gameCore;
	this->buttonPanels = buttonPanels;
	this->x = x;
	this->y = y;
	this->align = align;
	calculatePanelManager();
	setCoordinatesOfPanelManager(x, y);
}

KeyboardPanelManager::~KeyboardPanelManager()
{
}

void KeyboardPanelManager::calculatePanelManager()
{
	this->width = 0;
	for(unsigned i = 0; i < buttonPanels->size(); i++)
		if((*buttonPanels)[i]->isDrawing())
			this->width += (*buttonPanels)[i]->getWidth();
}

void KeyboardPanelManager::draw()
{
	for(unsigned i = 0; i < buttonPanels->size(); i++)
		(*buttonPanels)[i]->draw();
};

void KeyboardPanelManager::setCoordinatesOfButtonPanels()
{
	int locX = primX;
	for(unsigned i = 0; i < buttonPanels->size(); i++)
	{
		if((*buttonPanels)[i]->isDrawing())
		{
			(*buttonPanels)[i]->setPanelCoordinates(locX, primY);
			locX += (*buttonPanels)[i]->getWidth();
		}
	}
}

void KeyboardPanelManager::setCoordinatesOfPanelManager(int x, int y)
{
	switch(this->align)
	{
	case ALLEGRO_ALIGN_CENTER:
		this->primX = x - this->width / 2;
		break;
	case ALLEGRO_ALIGN_LEFT:
		this->primX = x;
		break;
	case ALLEGRO_ALIGN_RIGHT:
		this->primX = x - this->width;
		break;
	default: this->primX = x; break;
	}
	this->primY = y;
	setCoordinatesOfButtonPanels();
}

void KeyboardPanelManager::setDrawingState(bool state, unsigned n)
{
	if(n < buttonPanels->size())
	{
		(*buttonPanels)[n]->setDrawingState(state);
		calculatePanelManager();
		setCoordinatesOfPanelManager(x, y);
	}
}

void KeyboardPanelManager::setY(int y)
{
	this->y = y;
	for(unsigned i = 0; i < buttonPanels->size(); i++)
		(*buttonPanels)[i]->setY(y);
	setCoordinatesOfButtonPanels();
}

void KeyboardPanelManager::update()
{
	for(unsigned i = 0; i < buttonPanels->size(); i++)
		(*buttonPanels)[i]->update();
}