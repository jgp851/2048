#include "KeyboardButton.h"
#include "..\..\sources2048\Game2048Core.h"

KeyboardButton::KeyboardButton(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->buttonSize = gameCore->appSettings->getScreenWidth() * 0.05;
	this->highlight = false;
	this->push = false;
}

bool KeyboardButton::mouseIsOverButton()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() <= x + buttonSize &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() <= y + buttonSize
		)
		return true;
	else
		return false;
}

float KeyboardButton::getButtonSize()
{
	return buttonSize;
}

int KeyboardButton::getX()
{
	return x;
}
int KeyboardButton::getY()
{
	return y;
}

void KeyboardButton::drawHighlight()
{
	al_draw_filled_rectangle(x, y, x + buttonSize * 0.8363, y + buttonSize * 0.8363, al_map_rgba(60, 60, 60, 50));
}

void KeyboardButton::setSize(float size)
{
	this->buttonSize = size;
}

void KeyboardButton::setX(int x)
{
	this->x = x;
}

void KeyboardButton::setY(int y)
{
	this->y = y;
}

void KeyboardButton::update()
{
	if(mouseIsOverButton())
		highlight = true;
	else
		highlight = false;
	
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER) && gameCore->keyManager->is_EVENT_KEY_DOWN()) || (mouseIsOverButton() && gameCore->mouseManager->isLeftPressed()))
		push = true;
	else
		push = false;
}