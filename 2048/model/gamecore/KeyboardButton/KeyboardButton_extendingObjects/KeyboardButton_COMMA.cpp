#include "KeyboardButton_COMMA.h"
#include "..\..\..\sources2048\Game2048Core.h"

KeyboardButton_COMMA::KeyboardButton_COMMA(Game2048Core *gameCore) : KeyboardButton(gameCore)
{
	this->buttonIcon = gameCore->mediaManager->PNG_buttonKbComma;
	this->buttonIconPressed = gameCore->mediaManager->PNG_buttonKbCommaPressed;
}

KeyboardButton_COMMA::~KeyboardButton_COMMA()
{
}

bool KeyboardButton_COMMA::isPressed()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_COMMA, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
		return true;
	return false;
}

void KeyboardButton_COMMA::draw()
{
	if(!push)
		al_draw_scaled_bitmap(buttonIcon, 0, 0, al_get_bitmap_width(buttonIcon), al_get_bitmap_height(buttonIcon),
			x, y, buttonSize, buttonSize, 0);
	else
		al_draw_scaled_bitmap(buttonIconPressed, 0, 0, al_get_bitmap_width(buttonIconPressed), al_get_bitmap_height(buttonIconPressed),
			x, y, buttonSize, buttonSize, 0);
	if(highlight)
		drawHighlight();
}