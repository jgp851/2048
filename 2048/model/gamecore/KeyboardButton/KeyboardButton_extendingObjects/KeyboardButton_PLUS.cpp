#include "KeyboardButton_PLUS.h"
#include "..\..\..\sources2048\Game2048Core.h"

KeyboardButton_PLUS::KeyboardButton_PLUS(Game2048Core *gameCore) : KeyboardButton(gameCore)
{
	this->buttonIcon = gameCore->mediaManager->PNG_buttonKbPlus;
	this->buttonIconPressed = gameCore->mediaManager->PNG_buttonKbPlusPressed;
}

KeyboardButton_PLUS::~KeyboardButton_PLUS()
{
}

bool KeyboardButton_PLUS::isPressed()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_EQUALS, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_PLUS, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
		return true;
	return false;
}

void KeyboardButton_PLUS::draw()
{
	if(!push)
		al_draw_scaled_bitmap(buttonIcon, 0, 0, al_get_bitmap_width(buttonIcon), al_get_bitmap_height(buttonIcon),
			x, y, buttonSize, buttonSize, 0);
	else
		al_draw_scaled_bitmap(buttonIconPressed, 0, 0, al_get_bitmap_width(buttonIconPressed), al_get_bitmap_height(buttonIconPressed),
			x, y, buttonSize, buttonSize, 0);
	if(highlight)
		drawHighlight();
}