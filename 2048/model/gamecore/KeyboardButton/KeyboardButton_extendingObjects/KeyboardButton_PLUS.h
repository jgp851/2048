#pragma once
#include <allegro5\allegro.h>
#include "..\KeyboardButton.h"

class Game2048Core;

class KeyboardButton_PLUS : public KeyboardButton
{
private:
	ALLEGRO_BITMAP *buttonIcon;
	ALLEGRO_BITMAP *buttonIconPressed;
public:
	KeyboardButton_PLUS(Game2048Core *gameCore);
	~KeyboardButton_PLUS();
	
	bool isPressed();
	void draw();
};