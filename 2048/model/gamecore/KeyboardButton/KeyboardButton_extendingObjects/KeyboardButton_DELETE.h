#pragma once
#include <allegro5\allegro.h>
#include "..\KeyboardButton.h"

class Game2048Core;

class KeyboardButton_DELETE : public KeyboardButton
{
private:
	ALLEGRO_BITMAP *buttonIcon;
	ALLEGRO_BITMAP *buttonIconPressed;
public:
	KeyboardButton_DELETE(Game2048Core *gameCore);
	~KeyboardButton_DELETE();
	
	bool isPressed();
	void draw();
};