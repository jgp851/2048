#include "KeyboardButton_MINUS.h"
#include "..\..\..\sources2048\Game2048Core.h"

KeyboardButton_MINUS::KeyboardButton_MINUS(Game2048Core *gameCore) : KeyboardButton(gameCore)
{
	this->buttonIcon = gameCore->mediaManager->PNG_buttonKbMinus;
	this->buttonIconPressed = gameCore->mediaManager->PNG_buttonKbMinusPressed;
}

KeyboardButton_MINUS::~KeyboardButton_MINUS()
{
}

bool KeyboardButton_MINUS::isPressed()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_MINUS, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_MINUS, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
		return true;
	return false;
}

void KeyboardButton_MINUS::draw()
{
	if(!push)
		al_draw_scaled_bitmap(buttonIcon, 0, 0, al_get_bitmap_width(buttonIcon), al_get_bitmap_height(buttonIcon),
			x, y, buttonSize, buttonSize, 0);
	else
		al_draw_scaled_bitmap(buttonIconPressed, 0, 0, al_get_bitmap_width(buttonIconPressed), al_get_bitmap_height(buttonIconPressed),
			x, y, buttonSize, buttonSize, 0);
	if(highlight)
		drawHighlight();
}