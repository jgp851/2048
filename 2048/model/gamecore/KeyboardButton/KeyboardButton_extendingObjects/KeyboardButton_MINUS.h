#pragma once
#include <allegro5\allegro.h>
#include "..\KeyboardButton.h"

class Game2048Core;

class KeyboardButton_MINUS : public KeyboardButton
{
private:
	ALLEGRO_BITMAP *buttonIcon;
	ALLEGRO_BITMAP *buttonIconPressed;
public:
	KeyboardButton_MINUS(Game2048Core *gameCore);
	~KeyboardButton_MINUS();
	
	bool isPressed();
	void draw();
};