#include "KeyboardButton_FULLSTOP.h"
#include "..\..\..\sources2048\Game2048Core.h"

KeyboardButton_FULLSTOP::KeyboardButton_FULLSTOP(Game2048Core *gameCore) : KeyboardButton(gameCore)
{
	this->buttonIcon = gameCore->mediaManager->PNG_buttonKbFullstop;
	this->buttonIconPressed = gameCore->mediaManager->PNG_buttonKbFullstopPressed;
}

KeyboardButton_FULLSTOP::~KeyboardButton_FULLSTOP()
{
}

bool KeyboardButton_FULLSTOP::isPressed()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_FULLSTOP, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
		return true;
	return false;
}

void KeyboardButton_FULLSTOP::draw()
{
	if(!push)
		al_draw_scaled_bitmap(buttonIcon, 0, 0, al_get_bitmap_width(buttonIcon), al_get_bitmap_height(buttonIcon),
			x, y, buttonSize, buttonSize, 0);
	else
		al_draw_scaled_bitmap(buttonIconPressed, 0, 0, al_get_bitmap_width(buttonIconPressed), al_get_bitmap_height(buttonIconPressed),
			x, y, buttonSize, buttonSize, 0);
	if(highlight)
		drawHighlight();
}