#pragma once
#include <allegro5\allegro.h>
#include "..\KeyboardButton.h"

class Game2048Core;

class KeyboardButton_COMMA : public KeyboardButton
{
private:
	ALLEGRO_BITMAP *buttonIcon;
	ALLEGRO_BITMAP *buttonIconPressed;
public:
	KeyboardButton_COMMA(Game2048Core *gameCore);
	~KeyboardButton_COMMA();
	
	bool isPressed();
	void draw();
};