#pragma once
#include <allegro5\allegro_font.h>
#include <string>
#include <vector>
#include "KeyboardButton.h"

#define KB_BTN_TEXT_SIDE_LEFT 0
#define KB_BTN_TEXT_SIDE_RIGHT 1
#define KB_BTN_ALIGN_LEFT 0
#define KB_BTN_ALIGN_CENTER 1
#define KB_BTN_ALIGN_RIGHT 2
#define KB_BTN_TEXT_SIDE_LEFT 0
#define KB_BTN_TEXT_SIDE_RIGHT 1

enum KbBtnType
{
	KB_BTN_TYP_DELETE,
	KB_BTN_TYP_LEFT,
	KB_BTN_TYP_RIGHT,
	KB_BTN_TYP_COMMA,
	KB_BTN_TYP_FULLSTOP
};

class Game2048Core;

class KeyboardButtonPanel
{
private:
	Game2048Core *gameCore;
	ALLEGRO_FONT *font;

	bool drawing;
	char align;
	int x;
	int y;
	int width;
	float buttonSize;
	float spaceBetween;
	int fontHeight;

	char textSide;
	int textWidth;
	int textX;
	int textY;
	std::string text;

	void drawButtons();
	void drawText();
	void setButtonsCoordinates(int *x, int *y);
	void setTextCoordinates(int *x, int *y);
public:
	KeyboardButtonPanel(Game2048Core *gameCore, std::vector<KbBtnType> buttonsTypes, int x, int y, char align, std::string text, char textSide);
	~KeyboardButtonPanel();
	
	std::vector<KeyboardButton*> buttons;
	bool isDrawing();
	bool isPressed(int id = 0);
	int getWidth();
	void draw();
	void setDrawingState(bool state);
	void setPanelCoordinates(int x, int y);
	void setY(int y);
	void update();
};