#include "KeyboardButtonPanel.h"
#include "KeyboardButton_extendingObjects\KeyboardButton_COMMA.h"
#include "KeyboardButton_extendingObjects\KeyboardButton_DELETE.h"
#include "KeyboardButton_extendingObjects\KeyboardButton_FULLSTOP.h"
#include "..\..\sources2048\Game2048Core.h"
#include "..\..\sources2048\Game2048FontManager.h"

KeyboardButtonPanel::KeyboardButtonPanel(Game2048Core *gameCore, std::vector<KbBtnType> buttonsTypes, int x, int y, char align, std::string text, char textSide)
{
	this->gameCore = gameCore;
	this->align = align;
	this->y = y;
	this->drawing = true;
	this->text = text;
	this->textSide = textSide;

	font = gameCore->fontManager->font_buttonsInfo->font;
	fontHeight = al_get_font_line_height(font);
	buttonSize = gameCore->appSettings->getScreenWidth() * 0.05;
	spaceBetween = gameCore->appSettings->getScreenWidth() * 0.01375;
	textWidth = al_get_text_width(font, text.c_str());
	width = buttonsTypes.size() * (buttonSize + spaceBetween) + spaceBetween + textWidth;

	switch(align)
	{
	case KB_BTN_ALIGN_LEFT: this->x = x; break;
	case KB_BTN_ALIGN_CENTER: this->x = x - (width / 2); break;
	case KB_BTN_ALIGN_RIGHT: this->x = x - width; break;
	}

	for(unsigned i = 0; i < buttonsTypes.size(); i++)
		switch(buttonsTypes[i])
		{
		case KB_BTN_TYP_DELETE: buttons.push_back(new KeyboardButton_DELETE(gameCore)); break;
		case KB_BTN_TYP_COMMA: buttons.push_back(new KeyboardButton_COMMA(gameCore)); break;
		case KB_BTN_TYP_FULLSTOP: buttons.push_back(new KeyboardButton_FULLSTOP(gameCore)); break;
		default: buttons.push_back(NULL); break;
		}
	setPanelCoordinates(x, y);
}

KeyboardButtonPanel::~KeyboardButtonPanel()
{
}

bool KeyboardButtonPanel::isDrawing()
{
	return drawing;
}

bool KeyboardButtonPanel::isPressed(int id)
{
	if(drawing)
		if(buttons[id]->isPressed())
			return true;
	return false;
}

int KeyboardButtonPanel::getWidth()
{
	return width;
}

void KeyboardButtonPanel::draw()
{
	if(drawing)
	{
		drawButtons();
		drawText();
	}
}

void KeyboardButtonPanel::drawButtons()
{
	for(unsigned i = 0; i < buttons.size(); i++)
		buttons[i]->draw();
}

void KeyboardButtonPanel::drawText()
{
	al_draw_text(font, al_map_rgb(255, 255, 255), textX, textY, ALLEGRO_ALIGN_LEFT, text.c_str());
}

void KeyboardButtonPanel::setDrawingState(bool state)
{
	drawing = state;
}

void KeyboardButtonPanel::setPanelCoordinates(int x, int y)
{
	switch(textSide)
	{
	case KB_BTN_TEXT_SIDE_LEFT: setTextCoordinates(&x, &y); setButtonsCoordinates(&x, &y); break;
	case KB_BTN_TEXT_SIDE_RIGHT: setButtonsCoordinates(&x, &y); setTextCoordinates(&x, &y); break;
	}
}

void KeyboardButtonPanel::setButtonsCoordinates(int *x, int *y)
{
	for(unsigned i = 0; i < buttons.size(); i++)
	{
		*x += spaceBetween;
		buttons[i]->setX(*x);
		buttons[i]->setY(*y);
		*x += buttonSize;
	}
}

void KeyboardButtonPanel::setTextCoordinates(int *x, int *y)
{
	*x += spaceBetween;
	textX = *x;
	*x += textWidth;
	textY = *y + buttonSize / 2 - fontHeight / 2;
}

void KeyboardButtonPanel::setY(int y)
{
	this->y = y;
	for(unsigned i = 0; i < buttons.size(); i++)
		buttons[i]->setY(y);
	textY = y + buttonSize / 2 - fontHeight / 2;
}

void KeyboardButtonPanel::update()
{
	for(unsigned i = 0; i < buttons.size(); i++)
		buttons[i]->update();
}