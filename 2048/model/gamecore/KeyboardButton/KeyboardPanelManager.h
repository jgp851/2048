#pragma once
#include "KeyboardButtonPanel.h"
#include <vector>

class Game2048Core;

class KeyboardPanelManager
{
private:
	Game2048Core *gameCore;
	std::vector<KeyboardButtonPanel*> *buttonPanels;

	int x;
	int y;
	int primX;
	int primY;
	int width;
	int align;

	void calculatePanelManager();
	void setCoordinatesOfButtonPanels();
public:
	KeyboardPanelManager(Game2048Core *gameCore, std::vector<KeyboardButtonPanel*> *buttonPanels, int x, int y, int align);
	~KeyboardPanelManager();

	void draw();
	void setCoordinatesOfPanelManager(int x, int y);
	void setDrawingState(bool state, unsigned n);
	void setY(int y);
	void update();
};