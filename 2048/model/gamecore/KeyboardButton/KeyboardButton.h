#pragma once
#include <allegro5\allegro.h>

class Game2048Core;

class KeyboardButton
{
protected:
	Game2048Core *gameCore;
	bool highlight;
	bool push;
	float buttonSize;
	int x;
	int y;
public:
	KeyboardButton(Game2048Core *gameCore);
	
	bool mouseIsOverButton();
	float getButtonSize();
	int getX();
	int getY();
	void drawHighlight();
	void setSize(float size);
	void setX(int x);
	void setY(int y);
	void update();

	virtual bool isPressed() = 0;
	virtual void draw() = 0;
};