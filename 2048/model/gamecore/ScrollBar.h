#pragma once
#include <allegro5\allegro.h>

class Game2048Core;
class LoaderWindow;

struct ScrollBar
{
private:
	Game2048Core *gameCore;
	
	ALLEGRO_BITMAP *buttonArrowheadDown;
	ALLEGRO_BITMAP *buttonArrowheadUp;
	ALLEGRO_BITMAP *buttonUp;
	ALLEGRO_BITMAP *buttonDown;
	ALLEGRO_BITMAP *bar;
	ALLEGRO_BITMAP *barTop;
	ALLEGRO_BITMAP *barBottom;
	ALLEGRO_BITMAP *barMiddle;

	ALLEGRO_COLOR barColor;
		ALLEGRO_COLOR barColorStd;
		ALLEGRO_COLOR barColorMouseOver;
		ALLEGRO_COLOR barColorPressed;
	ALLEGRO_COLOR buttonDownColor;
	ALLEGRO_COLOR buttonUpColor;
		ALLEGRO_COLOR buttonsColorStd;
		ALLEGRO_COLOR buttonsColorMouseOver;
		ALLEGRO_COLOR buttonsColorPressed;

	bool slideDown;
	int x;
	int y;
	int width;
	int height;

	int barAreaY;
	float barAreaHeight;
	float barY;
	float barH;
	float mouseY;
	float mouseMovement;
	float previousMouseMovement;

	int *position;
	int previousPosition;
	unsigned totalElements;

	bool mouseIsOverBar();
	bool mouseIsOverButtonDown();
	bool mouseIsOverButtonUp();
	bool mouseIsOverEmptyBarArea();
	void allureToPosition();
	void allureSoftToPosition();
	void drawBackground();
	void drawBar();
	void drawButtons();
	void moveCyclicallyBar();
	void slideBar();
	void coloringElements();
public:
	ScrollBar(Game2048Core *gameCore, int x, int y, int height);
	
	bool isSlideBar();
	bool mouseIsOverScrollBar();
	int getActualPosition();
	int getPreviousPosition();
	unsigned getTotalElements();

	void draw();
	void setPositionVariable(int *newPosition);
	void setTotalElements(unsigned totalElements);
	void update();
};