#include <iostream>
#include <ctime>
#include "GameCore.h"

GameCore::GameCore(int tps, std::string title) : gameTitle(title)
{
	this->tps = tps;
	alManager = new AllegroManager(this);
	appSettings = NULL;
	bitmapLoader = NULL;
	fontLoader = NULL;
	keyManager = NULL;
	mouseManager = NULL;
	soundLoader = NULL;
	trans = NULL;
	
	eventDisplayClose = false;
	isRunning = true;
	restartApplication = false;
	redraw = false;
	loopLength = 0;
}

GameCore::~GameCore()
{
	if(alManager != NULL)
		delete alManager;
	if(appSettings != NULL)
		delete appSettings;
	if(bitmapLoader != NULL)
		delete bitmapLoader;
	if(fontLoader != NULL)
		delete fontLoader;
	if(keyManager != NULL)
		delete keyManager;
	if(mouseManager != NULL)
		delete mouseManager;
	if(soundLoader != NULL)
		delete soundLoader;
	if(trans != NULL)
		delete trans;
}

/**
 * Funkcja przygotowuje obiekt do dzialania. Tworzy managery allegro, klawiszy i wczytywania grafiki,
 * z ktorych bedzie mozna korzystac wewnatrz tej klasy i klas po niej dziedziczacych.
 *
 * Funkcje ta takze mozna i nalezy przeslaniac w klasie dziedziczacej, gdzie w ramach przygotowania do dzialania
 * bedzie takze np wczytywanie konkretnych grafik, tworzenie obiektow gry, czyli np glowna postac, przeciwnicy itp.
 * Jedyna roznica jest taka, ze przy dziedziczeniu init w klasie dziedziczacej nalezy wywolac takze init z klasy bazowej
 * aby wykonal sie ponizszy kod.
 */
bool GameCore::init()
{
	try
	{
		appSettings = new ApplicationSettings(this, "applicationSettings.ini");
		bitmapLoader = new BitmapLoader();
		fontLoader = new FontLoader();
		keyManager = new KeyManager();
		mouseManager = new MouseManager();
		soundLoader = new SoundLoader();
		trans = new Transform();
		alManager->init(tps);
		return true;
	}
	catch(char *text)
	{
		std::cerr << text << std::endl;
		return false;
	}
}

bool GameCore::getRestartAppSetting()
{
	return restartApplication;
}

std::string GameCore::getCurrentTime(std::string format)
{
	time_t rawtime;
	struct tm timeinfo;
	char buffer [80];
	
	time (&rawtime);
	localtime_s(&timeinfo, &rawtime);
	
	strftime(buffer, 80, format.c_str(), &timeinfo);
	
	std::string path = buffer;

	return path;
}

bool GameCore::copyFile(std::string sourcePath, std::string destinationPath)
{
	ifstream sourceFile(sourcePath.c_str(), ios::binary);
	ofstream destinationFile(destinationPath.c_str(), ios::binary);

	if(!sourceFile || !destinationFile)
		return false;

	sourceFile.seekg(0, ios::end);
	unsigned fileLength = sourceFile.tellg();
	char *buffer = new char[fileLength + 1];
	sourceFile.seekg(0, ios::beg);
	sourceFile.read(buffer, fileLength);
	sourceFile.close();
	
	destinationFile.write(buffer, fileLength);
	destinationFile.close();

	delete buffer;
	return true;
}

/**
 * Glowna petla gry. Funkcje nalezy wywolac po funkcji init()
 */
void GameCore::mainLoop()
{
	alManager->startTimer();
	unsigned previousLoopTime = clock();

	while(isRunning)
	{
		ALLEGRO_EVENT event;
		//Tutaj z klasy managera allegro wywolywana jest funkcja pobierajaca zdarzenie z kolejki zdarzen.
		//Jesli zadnego nie ma, kod stoi w tym miejscu az jakies sie pojawi.
		alManager->waitForEvent(&event);
		//debugEvents(&event);

		//obs�uga zamkni�cia okna programu
		if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			eventDisplayClose = true;

		//obs�uga klawiatury
		if(event.type == ALLEGRO_EVENT_KEY_DOWN || event.type == ALLEGRO_EVENT_KEY_UP)
			keyManager->processEvent(&event);

		//obs�uga myszy
		if(event.type == ALLEGRO_EVENT_MOUSE_AXES || event.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY || event.type == ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY || event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN || event.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
			mouseManager->processEvent(&event);
		else
			mouseManager->inactiveTimeUpdate(clock() - previousLoopTime);

		//akcje wykonywane przy ka�dym kolejnym cyklu timera
		if(event.type == ALLEGRO_EVENT_TIMER)
		{
			//ustawienie redraw na true przy kazdzym cyklu informuje petle, ze mozna podjac probe wyswietlenia kolejnej klatki na ekranie
			redraw = true;
			//obliczanie czasu, ktory minal od poprzedniego cyklu petli
			//zmienna loopLength bedzie przechowywac czas w milisekundach, ktory minal od poprzedniej aktualizacji
			//czas ten bedzie przekazywany jako parametr do funkcji update() w obiekcie zarzadzajacym swiatem gry.
			//W ten spsob obiekt ten bedzie wiedzial o ile czasu nalezy zaktualizowac swiat gry.
			loopLength = clock() - previousLoopTime;
			previousLoopTime += loopLength;
			update(loopLength);			
		}

		//ponowne narysowanie ekranu gry i podmiana buforow
		//wyswietlenie klatki odbywa sie tylko wtedy, gdy:
		//- redraw==true - ustawiane w cyklu petli, czyli rendering nie wystapi czesciej niz raz na kazdy cykl timera
		//- alManager->isQueueEmpty() - kolejka zdarzen jest pusta. Jest to swego rodzaju zabezpieczenie przed zbyt slabym komputerem,
		// ktory nie nadarza z wykonywaniem petli i wyswietlaniem grafiki. Jesli kolejka nie jest pusta, oznacza to, ze nadal sa zdarzenia (event)
		// do obsluzenia. Moze to byc np wcisniecie klawisza, ktore duzym problemem nie jest. Wtedy rendering sie nie wykona, w kolejnym cyklu petli
		// klawisz zostanie obsluzony i gdy kolejka bedzie pusta, wejdzie w rendering.
		// Wiekszym problemem jest, gdy np w kolejce juz czeka kilka zdarzen od timera. Wtedy aktualna petla pobrala jeden z nich, ale kolejka nie jest
		// pusta i juz kolejny cykl timera czeka w kolejce. To oznacza, ze petla wykonuje sie zbyt wolno i swiat gry nie jest aktualizowany
		// z taka czestotliwoscia jak powinien.
		// Wtedy rendering jest pomijany az petla zdarzy obsluzyc wszystkie zdarzenia i oproznic kolejke.
		if(redraw && alManager->isQueueEmpty())
		{
			redraw = false;
			alManager->clearDisplay(0, 0, 0);
			draw();
			alManager->flipBuffers();
		}
	}
}

void GameCore::setRestartAppSetting(bool state)
{
	 restartApplication = state;
}

/**
 * Funkcja pozwalaj�ca podglada� obs�ugiwane przez g��wn� p�tle zdarzenia
 */
/*
void GameCore::debugEvents(ALLEGRO_EVENT *event)
{
	std::cout << clock() << " - ";
	switch(event->type)
	{
	case ALLEGRO_EVENT_DISPLAY_CLOSE:
		std::cout << "zamkniecie okna";
		break;
	case ALLEGRO_EVENT_KEY_DOWN:
		std::cout << "wcisnieto klawisz (kod: " << event->keyboard.keycode << ")";
		break;
	case ALLEGRO_EVENT_KEY_UP:
		std::cout << "puszczono klawisz (kod: " << event->keyboard.keycode << ")";
		break;
	case ALLEGRO_EVENT_MOUSE_AXES:
		std::cout << "ruch myszy (pozycja: [" << event->mouse.x << ";" << event->mouse.y << "])";
		break;
	case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
		std::cout << "wcisnieto przycisk myszy (numer: " << event->mouse.button << ")";
		break;
	case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
		std::cout << "puszczono przycisk myszy (numer: " << event->mouse.button << ")";
		break;
	case ALLEGRO_EVENT_TIMER:
		std::cout << "timer";
		break;
	default: std::cout << "inne zdarzenie";
		break;
	}
	std::cout << std::endl;
}*/