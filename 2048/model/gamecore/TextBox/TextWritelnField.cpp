#include "TextWritelnField.h"
#include "TextBox.h"
#include "..\..\sources2048\Game2048FontManager.h"

TextWritelnField::TextWritelnField(unsigned IDfield, Fonts *font, int width, int height, ALLEGRO_COLOR fontColor, std::string text, int align)
{
	this->textBox = NULL;
	
	this->IDfield = IDfield;
	this->visible = true;
	this->fontColor = fontColor;
	this->font = font;
	this->fontHeight = al_get_font_line_height(font->font);
	this->text = text;
	this->align = align;

	if(width < al_get_text_width(font->font, text.c_str()))
		this->width = al_get_text_width(font->font, text.c_str());
	else
		this->width = width;

	if(height < fontHeight)
		this->height = fontHeight;
	else
		this->height = height;
}

TextWritelnField::~TextWritelnField()
{
}

std::string TextWritelnField::getText()
{
	return text;
}

void TextWritelnField::draw()
{
	al_draw_text(this->font->font, this->fontColor, this->textX, this->textY, this->align, this->text.c_str());
}

void TextWritelnField::setText(std::string newText)
{
	this->text = newText;

	if(this->width < al_get_text_width(font->font, text.c_str()))
		this->width = al_get_text_width(font->font, text.c_str());
	
	// je�li TextBox jest w�szy ni� field w nim zawarty, rozszerzenie go.
	// Po zako�czonych zmianach we wszystkich polach koniecznie trzeba wywo�a� metod� TextBox::setCoordinatesOfTextBox(),
	//	kt�ra odpowiednio ustawi TextBox, wzgl�dem ewentualnej nowej szeroko�ci bior�c pod uwag� align.
	if(textBox->width < this->width)
	{
		textBox->width = this->width;
		textBox->updateDimension();
	}
}

void TextWritelnField::setTextBoxAddress(TextBox *textBox)
{
	this->textBox = textBox;
}