#pragma once
#include <vector>
#include "TextWritelnField.h"
#include "TextReadlnField.h"

#define TRANSPARENCY_SIZE 20

#define BOX_STANDARD 0
#define BOX_GAME_INFO 1

#define BOX_HORIZONTAL 0
#define BOX_VERTICAL 1

#define BOX_TOP 0
#define BOX_LEFT 0
#define BOX_MIDDLE 1
#define BOX_BOTTOM 2
#define BOX_RIGHT 2

class Game2048Core;

/** TextBox - 2014-09-09 - klasa bazowa dla obiekt�w obs�uguj�cych stringi
 *
 * - int x, y         - wsp�rz�dne x, y.
 * - int primX, primY - wsp�rz�dne okre�laj�ce docelowe po�o�enie, przydatne przy zmianie rozmiar�w TextBox. Je�li zmieni� si� wymiary TextBox, z primX, primY przywracane s� odpowiednie warto�ci x i y. Ustalaj� one docelowe po�o�enie uwzgl�dniaj�c align TextBox'a.
 * - int align        - wycentrowanie TextBox'a na ekranie, wzgl�dem podanych wsp�rz�dnych.
 * - int orientation  - orientacja pozioma lub pionowa.
 * - int type         - typ TextBox'a.
 *
 * - ALLEGRO_COLOR
 *   a) bgColor     - kolor t�a jednolity dla ca�ego TextBox
 *   b) bgColorEven - kolor dla p�l parzystych
 *   c) bgColorOdd  - kolor dla p�l nieparzystych
 *
 * - setWritelnVisible() - funkcja ustawiaj�ca widoczno�� pola. Przesuwa w odpowiedni� stron� te kt�re pozosta�y widoczne.
 * - sizeUpdate() - funkcja wewn�trzna, przesuwaj�ca pola widzialne na miejsce nie widocznych - uruchamiana warunkowo w setWritelnVisible().
 * - updateDimension() - funkcja aktualizuj�ca wsp�rz�dne, po zmianie rozmiar�w z uwzgl�dnieniem align TextBox'a.
 *
 *   Zale�nie od potrzeb do TextBox'a doda� mo�na pola typu TextWritelnField, TextReadlnField, lub oba na raz. W ka�dym przypadku wykorzystywany jest do tego odpowiedni konstruktor.
 *   W zwi�zku z powy�sz� opcj� nale�y pami�ta� o odpowiednim numerowaniu obiekt�w (IDfield). TextBox na podstawie tego identyfikatora rysuje pola w odpowiedniej kolejno�ci, nadaje im swoje wsp�rz�dne, itd.
 * - std::vector<TextWritelnField*> *writelnFields
 *     Wektor przechowuj�cy wska�niki do obiekt�w typu TextWritelnField.
 * - std::vector<TextReadlnField*> readlnFields
 *     Wektor przechowuj�cy wska�niki do obiekt�w typu TextReadlnField.
 *
 * - getWidth() oraz getHeight() zwracaj� odpowiednio szeroko�� i wysoko�� TextBox'a
 * - mo�liwo�� zmiany wsp�rz�dnych TextBox na dwa sposoby:
 *		a) bezpo�rednio - setWidth(int newWidth), setHeight(int newHeight), lecz bez uwzgl�dnienia align, ustawionego przy utworzeniu obiektu.
 *		b) wy�rodkowanie wzgl�dem dw�ch punkt�w na osi X lub Y - setHeightBetweenTwoPoints(), setWidthBetweenTwoPoints.
 * - setCoordinatesOfText() ustawia wsp�rz�dne tekstu pola.
 *
 * - obramowanie pola:
 *		a) gradientowo-przezroczystego - TRANSPARENCY_SIZE to warto�� okre�laj�ca rozmiar, obramowania t�a. Obramowanie to wychodzi poza obszar TextBox'a.
 *		b) zwyk�e - warto�� "border" ustala szeroko�� obramowania. Obramowanie znajduje si� w obszarze TextBox'a. Na podstawie szeroko�ci obramowania, tekst jest rysowany z odpowiednim odst�pem od kraw�dzi.
 *		   Szeroko�� ustalana jest na podstawie typu TextBox'a.
 */

class TextBox
{
private:
	Game2048Core *gameCore;
	std::vector<TextWritelnField*> *writelnFields;
	std::vector<TextReadlnField*> *readlnFields;

	
	ALLEGRO_BITMAP *textBgBottom;
	ALLEGRO_BITMAP *textBgLeft;
	ALLEGRO_BITMAP *textBgMiddle;
	ALLEGRO_BITMAP *textBgRight;
	ALLEGRO_BITMAP *textBgTop;
	ALLEGRO_BITMAP *textBgCorner_ne;
	ALLEGRO_BITMAP *textBgCorner_nw;
	ALLEGRO_BITMAP *textBgCorner_se;
	ALLEGRO_BITMAP *textBgCorner_sw;

	ALLEGRO_COLOR bgColor;
	ALLEGRO_COLOR bgColorEven;
	ALLEGRO_COLOR bgColorOdd;
	unsigned totalFields;
	
	int x;
	int y;
	int primX;
	int primY;
	int width;
	int height;
	int align;
	int orientation;
	int type;
	int border;

	void drawBackground();
	void sizeUpdate();
	void updateDimension();
public:
	TextBox(Game2048Core *gameCore, int x, int y, int align, std::vector<TextWritelnField*> *writelnFields, int orientation, int type);
	TextBox(Game2048Core *gameCore, int x, int y, int align, std::vector<TextReadlnField*> *readlnFields, int orientation, int type);
	TextBox(Game2048Core *gameCore, int x, int y, int align, std::vector<TextWritelnField*> *writelnFields, std::vector<TextReadlnField*> *readlnFields, int orientation, int type);
	~TextBox();
	
	int getX();
	int getY();
	int getWidth();
	int getHeight();
	void draw();

	void setCoordinatesOfText();
	void setCoordinatesOfTextBox(int x, int y);
	void setHeight(int newHeight);
	void setWidth(int newWidth);
	void setHeightBetweenTwoPoints(int first, int second, int align);
	void setWidthBetweenTwoPoints(int first, int second, int align);
	void setX(int x);
	void setY(int y);

	void setType(int newType);
	void setWritelnVisible(int fieldID, bool state);

	friend class TextReadlnField;
	friend class TextWritelnField;
};