#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <string>

#define DRAWING_CURSOR_TIME 500

// states
#define ESCAPE 0
#define ENTER 1
#define READLN 2

struct Fonts;
class TextBox;
class Game2048Core;

/** TextReadlnField - 2014-09-12 - sk�adowa klasa TextBox odpowiadzialna za wpisywanie string�w, tj. �cie�ka pliku, nazwa.
 *
 * - unsigned IDfield - jest to unikalny identyfikator informuj�cy TextBox o kolejno�ci ka�dego obiektu w nim zawartego. Wliczaj�c w to pola typu TextWritelnField.
 * - Fonts *font      - czcionka, kt�r� b�dzie wy�wietlany tekst.
 * - int width        - szeroko�� pola - je�li width == 0, wtedy szeroko�� obliczana jest na podstawie d�ugo�ci stringu o liczbie znak�w podanych w shownCharacters (dla czcionek o sta�ej szeroko�ci).
 * - int height       - wysoko�� pola - je�li height == 0, wtedy wysoko�� obliczana jest na podstawie wysoko�ci czcionki.
 * - int textX        - okre�la X dla tekstu z uwzgl�dnieniem wsp�rz�dnych TextBox'a i obramowania.
 * - int textY        - okre�la Y dla tekstu z uwzgl�dnieniem wsp�rz�dnych TextBox'a i obramowania.
 * - unsigned shownCharacters
 *                    - liczba znak�w maksymalnie wy�wietlanych w polu.
 * - unsigned typingCharacters
 *                    - oznacza maksymaln� mo�liw� liczb� znak�w do wpisania, niesko�czona je�li 0
 * - AL_COL fontColor - kolor czcionki tekstu.
 * - AL_COL cursorColor
 *                    - kolor kursora, ustawiany wg. koloru tekstu.
 *
 * - isFieldTextEmpty() - sprawdza czy string jest pusty.
 * - getFieldText() - zwraca string.
 * - getCursorPosition() - zwraca pozycj� kursora.
 * - getState() - zwraca state pola. Rozr�niane s� trzy:
 *                a) ESCAPE - wci�ni�to klawisz escape, czyli anulowano wpisywany tekst. Konieczno�� odpowiedniego obs�u�enia state,
 *                b) ENTER  - wci�ni�to klawisz enter, czyli potwierdzono wpisany tekst. Konieczno�� odpowiedniego obs�u�enia wpisanego tekstu ( getFieldText() ),
 *                c) READLN - wpisywanie, edycja tekstu.
 * - reset() - przed ka�dym ponownym u�yciem pola, wymagany jest jego reset.
 * - setAddresses(Game2048Core *gameCore, TextBox *textBox) - funkcja, kt�r� TextBox wykorzystuje w swoim konstruktorze do przes�ania adresu gameCore gry i swojego.
 * - setReadlnFieldState(int newState) - zmiana state pola zewn�trz.
 * - setText(std::string newText) - ustawienie tekstu pola.
 * - update(unsigned elapsedTime) - pole trzeba za ka�dym cyklem timer'a koniecznie aktualizowa�.
 */

class TextReadlnField
{
private:
	Game2048Core *gameCore;
	TextBox *textBox;
	int fieldState;

	unsigned IDfield;
	ALLEGRO_COLOR fontColor;
	ALLEGRO_COLOR cursorColor;
	ALLEGRO_COLOR selectionColor;
	Fonts *font;
	int align;
	int x;
	int y;
	int width;
	int height;
	int textX;
	int textY;
	int fontHeight;
	
	bool drawCursor;
	unsigned lastDraw;
	unsigned cursor;
	unsigned cursorShowPos;
	unsigned lastCursorPos;
	unsigned lastTextSize;
	unsigned shownCharacters;
	unsigned typingCharacters;
	std::string text;

	// zaznaczenie
	bool selection;
	unsigned selBegin;
	unsigned selEnd;

	void addCursors();
	void copyToClipboard();
	void cursorLeft();
	void cursorRight();
	void pasteFromClipboard();
	void selectionErase();
	void selectionReset();
public:
	TextReadlnField(unsigned IDfield, Fonts *font, int width, int height, unsigned shownCharacters, unsigned typingCharacters, ALLEGRO_COLOR fontColor);
	~TextReadlnField();
	
	bool isFieldTextEmpty();

	std::string getFieldText();
	
	unsigned getCursorPosition();
	int getState();

	void draw();
	void reset();
	void selectAll();
	void setAddresses(Game2048Core *gameCore, TextBox *textBox);
	void setReadlnFieldState(int newState);
	void setText(std::string newText);
	void update(unsigned elapsedTime);

	friend class TextBox;
};