#include "TextBox.h"
#include "..\..\sources2048\Game2048Core.h"
#include "..\..\sources2048\Game2048FontManager.h"

TextBox::TextBox(Game2048Core *gameCore, int x, int y, int align, std::vector<TextWritelnField*> *writelnFields, int orientation, int type)
{
	this->gameCore = gameCore;
	this->writelnFields = writelnFields;
	this->readlnFields = NULL;
	this->x = x;
	this->y = y;
	this->primX = x;
	this->primY = y;

	textBgBottom = gameCore->mediaManager->PNG_textBgBottom;
	textBgLeft = gameCore->mediaManager->PNG_textBgLeft;
	textBgMiddle = gameCore->mediaManager->PNG_textBgMiddle;
	textBgRight = gameCore->mediaManager->PNG_textBgRight;
	textBgTop = gameCore->mediaManager->PNG_textBgTop;
	textBgCorner_ne = gameCore->mediaManager->PNG_textBgCorner_ne;
	textBgCorner_nw = gameCore->mediaManager->PNG_textBgCorner_nw;
	textBgCorner_se = gameCore->mediaManager->PNG_textBgCorner_se;
	textBgCorner_sw = gameCore->mediaManager->PNG_textBgCorner_sw;

	totalFields = writelnFields->size();
	this->align = align;
	this->orientation = orientation;
	this->setType(type);

	for(unsigned i = 0; i < writelnFields->size(); i++)
		(*writelnFields)[i]->setTextBoxAddress(this);
	
	switch(orientation)
	{
	case BOX_HORIZONTAL:
		{
			// obliczanie wymiar�w textBox.

			int widthFields = 0;
			for(unsigned i = 0; i < writelnFields->size(); i++)
				widthFields += (*writelnFields)[i]->width;
			this->width = widthFields;
			
			for(unsigned i = 0; i < writelnFields->size(); i++)
				if((*writelnFields)[i]->height > this->height)
					this->height = (*writelnFields)[i]->height;
			
			// ustawienie jednakowej wysoko�ci dla wszystkich p�l

			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->height = this->height;

			// nadawanie wsp�rz�dnych element�w textBox.

			int x = 0;
			// wzgl�dem osi X
			for(unsigned i = 0; i < writelnFields->size(); i++)
			{
				(*writelnFields)[i]->x = x;
				x += (*writelnFields)[i]->width;
			}
			// wzgl�dem osi Y
			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->y = 0;
		}
		break;
	case BOX_VERTICAL:
		{
			// obliczanie wymiar�w textBox.

			int heightFields = 0;
			for(unsigned i = 0; i < writelnFields->size(); i++)
				heightFields += (*writelnFields)[i]->height;
			this->height = heightFields;
			
			for(unsigned i = 0; i < writelnFields->size(); i++)
				if((*writelnFields)[i]->width > this->width)
					this->width = (*writelnFields)[i]->width;
			
			// ustawienie odpowiedniej szeroko�ci dla wszystkich p�l

			for(unsigned i = 0; i < writelnFields->size(); i++)
			{
				Fonts *font = (*writelnFields)[i]->font;
				(*writelnFields)[i]->width = al_get_text_width(font->font, (*writelnFields)[i]->text.c_str());
			}

			// nadawanie wsp�rz�dnych element�w textBox.

			int y = 0;
			// wzgl�dem osi Y
			for(unsigned i = 0; i < writelnFields->size(); i++)
			{
				(*writelnFields)[i]->y = y;
				y += (*writelnFields)[i]->height;
			}
			// wzgl�dem osi X
			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->x = 0;
		}
		break;
	}

	// na podstawie obliczonych wymiar�w textBox, ustalanie jego wsp�rz�dnych.
	this->setCoordinatesOfTextBox(x, y);

	// wsp�rz�dne tekstu
	this->setCoordinatesOfText();
}
TextBox::TextBox(Game2048Core *gameCore, int x, int y, int align, std::vector<TextReadlnField*> *readlnFields, int orientation, int type)
{
	this->gameCore = gameCore;
	this->readlnFields = readlnFields;
	this->writelnFields = NULL;
	this->x = x;
	this->y = y;
	this->primX = x;
	this->primY = y;
	
	textBgBottom = gameCore->mediaManager->PNG_textBgBottom;
	textBgLeft = gameCore->mediaManager->PNG_textBgLeft;
	textBgMiddle = gameCore->mediaManager->PNG_textBgMiddle;
	textBgRight = gameCore->mediaManager->PNG_textBgRight;
	textBgTop = gameCore->mediaManager->PNG_textBgTop;
	textBgCorner_ne = gameCore->mediaManager->PNG_textBgCorner_ne;
	textBgCorner_nw = gameCore->mediaManager->PNG_textBgCorner_nw;
	textBgCorner_se = gameCore->mediaManager->PNG_textBgCorner_se;
	textBgCorner_sw = gameCore->mediaManager->PNG_textBgCorner_sw;

	totalFields = readlnFields->size();
	this->align = align;
	this->orientation = orientation;
	this->setType(type);

	for(unsigned i = 0; i < readlnFields->size(); i++)
		(*readlnFields)[i]->setAddresses(gameCore, this);
	
	switch(orientation)
	{
	case BOX_HORIZONTAL:
		{
			// obliczanie wymiar�w textBox.

			int widthFields = 0;
			for(unsigned i = 0; i < readlnFields->size(); i++)
				widthFields += (*readlnFields)[i]->width;
			this->width = widthFields;
			
			for(unsigned i = 0; i < readlnFields->size(); i++)
				if((*readlnFields)[i]->height > this->height)
					this->height = (*readlnFields)[i]->height;
			
			// ustawienie jednakowej wysoko�ci dla wszystkich p�l

			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->height = this->height;

			// nadawanie wsp�rz�dnych element�w textBox.

			int x = 0;
			// wzgl�dem osi X
			for(unsigned i = 0; i < readlnFields->size(); i++)
			{
				(*readlnFields)[i]->x = x;
				x += (*readlnFields)[i]->width;
			}
			// wzgl�dem osi Y
			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->y = 0;
		}
		break;
	case BOX_VERTICAL:
		{
			// obliczanie wymiar�w textBox.

			int heightFields = 0;
			for(unsigned i = 0; i < readlnFields->size(); i++)
				heightFields += (*readlnFields)[i]->height;
			this->height = heightFields;
			
			for(unsigned i = 0; i < readlnFields->size(); i++)
				if((*readlnFields)[i]->width > this->width)
					this->width = (*readlnFields)[i]->width;
			
			// ustawienie jednakowej szeroko�ci dla wszystkich p�l

			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->width = this->width;

			// nadawanie wsp�rz�dnych element�w textBox.

			int y = 0;
			// wzgl�dem osi Y
			for(unsigned i = 0; i < readlnFields->size(); i++)
			{
				(*readlnFields)[i]->y = y;
				y += (*readlnFields)[i]->height;
			}
			// wzgl�dem osi X
			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->x = 0;
		}
		break;
	}
	
	// na podstawie obliczonych wymiar�w textBox, ustalanie jego wsp�rz�dnych.
	this->setCoordinatesOfTextBox(x, y);
	
	// wsp�rz�dne tekstu
	setCoordinatesOfText();
}
TextBox::TextBox(Game2048Core *gameCore, int x, int y, int align, std::vector<TextWritelnField*> *writelnFields, std::vector<TextReadlnField*> *readlnFields, int orientation, int type)
{
	this->gameCore = gameCore;
	this->writelnFields = writelnFields;
	this->readlnFields = readlnFields;
	this->x = x;
	this->y = y;
	this->primX = x;
	this->primY = y;
	
	textBgBottom = gameCore->mediaManager->PNG_textBgBottom;
	textBgLeft = gameCore->mediaManager->PNG_textBgLeft;
	textBgMiddle = gameCore->mediaManager->PNG_textBgMiddle;
	textBgRight = gameCore->mediaManager->PNG_textBgRight;
	textBgTop = gameCore->mediaManager->PNG_textBgTop;
	textBgCorner_ne = gameCore->mediaManager->PNG_textBgCorner_ne;
	textBgCorner_nw = gameCore->mediaManager->PNG_textBgCorner_nw;
	textBgCorner_se = gameCore->mediaManager->PNG_textBgCorner_se;
	textBgCorner_sw = gameCore->mediaManager->PNG_textBgCorner_sw;

	totalFields = writelnFields->size() + readlnFields->size();
	this->align = align;
	this->orientation = orientation;
	this->setType(type);

	for(unsigned i = 0; i < writelnFields->size(); i++)
		(*writelnFields)[i]->setTextBoxAddress(this);
	for(unsigned i = 0; i < readlnFields->size(); i++)
		(*readlnFields)[i]->setAddresses(gameCore, this);
	
	switch(orientation)
	{
	case BOX_HORIZONTAL:
		{
			// obliczanie wymiar�w textBox.

			int widthFields = 0;
			for(unsigned i = 0; i < writelnFields->size(); i++)
				widthFields += (*writelnFields)[i]->width;

			for(unsigned i = 0; i < readlnFields->size(); i++)
				widthFields += (*readlnFields)[i]->width;
			this->width = widthFields;
			
			for(unsigned i = 0; i < writelnFields->size(); i++)
				if((*writelnFields)[i]->height > this->height)
					this->height = (*writelnFields)[i]->height;

			for(unsigned i = 0; i < readlnFields->size(); i++)
				if((*readlnFields)[i]->height > this->height)
					this->height = (*readlnFields)[i]->height;
			
			// ustawienie jednakowej wysoko�ci dla wszystkich p�l

			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->height = this->height;

			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->height = this->height;

			// nadawanie wsp�rz�dnych element�w textBox.

			int x = 0;
			unsigned writeVec = 0, readVec = 0;;
			// wzgl�dem osi X
			for(unsigned i = 0; i < totalFields; i++)
			{
				if(writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i)
				{
					(*writelnFields)[writeVec]->x = x;
					x += (*writelnFields)[writeVec]->width;
					writeVec++;
				}
				if(readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
				{
					(*readlnFields)[readVec]->x = x;
					x += (*readlnFields)[readVec]->width;
					readVec++;
				}
			}
			// wzgl�dem osi Y
			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->y = 0;
			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->y = 0;
		}
		break;
	case BOX_VERTICAL:
		{
			// obliczanie wymiar�w textBox.

			int heightFields = 0;
			for(unsigned i = 0; i < writelnFields->size(); i++)
				heightFields += (*writelnFields)[i]->height;

			for(unsigned i = 0; i < readlnFields->size(); i++)
				heightFields += (*readlnFields)[i]->height;
			this->height = heightFields;
			
			for(unsigned i = 0; i < writelnFields->size(); i++)
				if((*writelnFields)[i]->width > this->width)
					this->width = (*writelnFields)[i]->width;

			for(unsigned i = 0; i < readlnFields->size(); i++)
				if((*readlnFields)[i]->width > this->width)
					this->width = (*readlnFields)[i]->width;
			
			// ustawienie jednakowej szeroko�ci dla wszystkich p�l

			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->width = this->width;

			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->width = this->width;

			// nadawanie wsp�rz�dnych element�w textBox.
			
			int y = 0;
			unsigned writeVec = 0, readVec = 0;;
			// wzgl�dem osi Y
			for(unsigned i = 0; i < totalFields; i++)
			{
				if(writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i)
				{
					(*writelnFields)[writeVec]->y = y;
					y += (*writelnFields)[writeVec]->height;
					writeVec++;
				}
				if(readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
				{
					(*readlnFields)[readVec]->y = y;
					y += (*readlnFields)[readVec]->height;
					readVec++;
				}
			}
			// wzgl�dem osi X
			for(unsigned i = 0; i < writelnFields->size(); i++)
				(*writelnFields)[i]->x = 0;
			for(unsigned i = 0; i < readlnFields->size(); i++)
				(*readlnFields)[i]->x = 0;
		}
		break;
	}

	// na podstawie obliczonych wymiar�w textBox, ustalanie jego wsp�rz�dnych.
	this->setCoordinatesOfTextBox(x, y);

	// wsp�rz�dne tekstu
	this->setCoordinatesOfText();
}
TextBox::~TextBox()
{
	if(writelnFields)
		for(std::vector<TextWritelnField*>::iterator it = writelnFields->begin(); it != writelnFields->end(); it++)
			delete *it;
	if(readlnFields)
		for(std::vector<TextReadlnField*>::iterator it = readlnFields->begin(); it != readlnFields->end(); it++)
			delete *it;
}

int TextBox::getX()
{
	return x;
}

int TextBox::getY()
{
	return y;
}

int TextBox::getWidth()
{
	return width + TRANSPARENCY_SIZE * 2;
}

int TextBox::getHeight()
{
	return height + (TRANSPARENCY_SIZE - 2) * 2;
}

void TextBox::draw()
{
	this->drawBackground();

	// text fields
	unsigned writeVec = 0, readVec = 0;;
	for(unsigned i = 0; i < totalFields; i++)
	{
		if(writelnFields && writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i)
		{
			if((*writelnFields)[writeVec]->visible)
				(*writelnFields)[writeVec]->draw();
			writeVec++;
		}
		if(readlnFields && readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
		{
			(*readlnFields)[readVec]->draw();
			readVec++;
		}
	}
}

void TextBox::drawBackground()
{
	switch(type)
	{
	case BOX_STANDARD:
		al_draw_tinted_scaled_bitmap(textBgLeft, bgColor, 0, 0, al_get_bitmap_width(textBgLeft), al_get_bitmap_height(textBgLeft),
			x - TRANSPARENCY_SIZE,
			y,
			TRANSPARENCY_SIZE,
			height, 0);
		al_draw_tinted_scaled_bitmap(textBgRight, bgColor, 0, 0, al_get_bitmap_width(textBgRight), al_get_bitmap_height(textBgRight),
			x + width,
			y,
			TRANSPARENCY_SIZE,
			height, 0);
		al_draw_tinted_scaled_bitmap(textBgTop, bgColor, 0, 0, al_get_bitmap_width(textBgTop), al_get_bitmap_height(textBgTop),
			x,
			y - TRANSPARENCY_SIZE,
			width,
			TRANSPARENCY_SIZE, 0);
		al_draw_tinted_scaled_bitmap(textBgBottom, bgColor, 0, 0, al_get_bitmap_width(textBgBottom), al_get_bitmap_height(textBgBottom),
			x,
			y + height,
			width,
			TRANSPARENCY_SIZE, 0);
		al_draw_tinted_scaled_bitmap(textBgMiddle, bgColor, 0, 0, al_get_bitmap_width(textBgMiddle), al_get_bitmap_height(textBgMiddle),
			x,
			y,
			width,
			height, 0);

		al_draw_tinted_scaled_bitmap(textBgCorner_nw, bgColor, 0, 0, al_get_bitmap_width(textBgCorner_nw), al_get_bitmap_height(textBgCorner_nw),
			x - TRANSPARENCY_SIZE,
			y - TRANSPARENCY_SIZE,
			TRANSPARENCY_SIZE, TRANSPARENCY_SIZE, 0);
		al_draw_tinted_scaled_bitmap(textBgCorner_ne, bgColor, 0, 0, al_get_bitmap_width(textBgCorner_ne), al_get_bitmap_height(textBgCorner_ne),
			x + width,
			y - TRANSPARENCY_SIZE,
			TRANSPARENCY_SIZE, TRANSPARENCY_SIZE, 0);
		al_draw_tinted_scaled_bitmap(textBgCorner_sw, bgColor, 0, 0, al_get_bitmap_width(textBgCorner_sw), al_get_bitmap_height(textBgCorner_sw),
			x - TRANSPARENCY_SIZE, y + height,
			TRANSPARENCY_SIZE, TRANSPARENCY_SIZE, 0);
		al_draw_tinted_scaled_bitmap(textBgCorner_se, bgColor, 0, 0, al_get_bitmap_width(textBgCorner_se), al_get_bitmap_height(textBgCorner_se),
			x + width, y + height,
			TRANSPARENCY_SIZE, TRANSPARENCY_SIZE, 0);
		break;
	case BOX_GAME_INFO:
		unsigned writeVec = 0, readVec = 0;
		for(unsigned i = 0, j = 0; i < totalFields; i++)
		{
			if(writelnFields && writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i)
			{
				if((*writelnFields)[writeVec]->visible)
				{
					al_draw_filled_rectangle(
						this->x + (*writelnFields)[writeVec]->x,
						this->y + (*writelnFields)[writeVec]->y,
						this->x + (*writelnFields)[writeVec]->x + this->width + border * 2, 
						this->y + (*writelnFields)[writeVec]->y + (*writelnFields)[writeVec]->height,
						j % 2 == 0 ? bgColorEven : bgColorOdd);
					j++;
				}
				writeVec++;
			}
			if(readlnFields && readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
			{
				al_draw_filled_rectangle(
					this->x + (*readlnFields)[readVec]->x,
					this->y + (*readlnFields)[readVec]->y,
					this->x + (*readlnFields)[readVec]->x + this->width + border * 2,
					this->y + (*readlnFields)[readVec]->y + (*readlnFields)[readVec]->height,
					j % 2 == 0 ? bgColorEven : bgColorOdd);
				j++;
				readVec++;
			}
		}
		break;
	}
}

void TextBox::setCoordinatesOfText()
{
	switch(orientation)
	{
	case BOX_VERTICAL:
		if(writelnFields)
		{
			for(unsigned i = 0; i < writelnFields->size(); i++)
			{
				switch((*writelnFields)[i]->align)
				{
				case ALLEGRO_ALIGN_CENTER:
					(*writelnFields)[i]->textX = x + (*writelnFields)[i]->x + width / 2;
					(*writelnFields)[i]->textY = y + (*writelnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_LEFT:
					(*writelnFields)[i]->textX = x + (*writelnFields)[i]->x + border;
					(*writelnFields)[i]->textY = y + (*writelnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_RIGHT:
					(*writelnFields)[i]->textX = x + (*writelnFields)[i]->x + width - border;
					(*writelnFields)[i]->textY = y + (*writelnFields)[i]->y + height + border;
					break;
				}
			}
		}
		if(readlnFields)
		{
			for(unsigned i = 0; i < readlnFields->size(); i++)
			{
				switch((*readlnFields)[i]->align)
				{
				case ALLEGRO_ALIGN_CENTER:
					(*readlnFields)[i]->textX = x + (*readlnFields)[i]->x + width / 2;
					(*readlnFields)[i]->textY = y + (*readlnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_LEFT:
					(*readlnFields)[i]->textX = x + (*readlnFields)[i]->x + border;
					(*readlnFields)[i]->textY = y + (*readlnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_RIGHT:
					(*readlnFields)[i]->textX = x + (*readlnFields)[i]->x + width - border;
					(*readlnFields)[i]->textY = y + (*readlnFields)[i]->y + height + border;
					break;
				}
			}
		}
		break;
	case BOX_HORIZONTAL:
		if(writelnFields)
		{
			for(unsigned i = 0; i < writelnFields->size(); i++)
			{
				switch((*writelnFields)[i]->align)
				{
				case ALLEGRO_ALIGN_CENTER:
					(*writelnFields)[i]->textX = x + (*writelnFields)[i]->x + (*writelnFields)[i]->width / 2;
					(*writelnFields)[i]->textY = y + (*writelnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_LEFT:
					(*writelnFields)[i]->textX = x + (*writelnFields)[i]->x + border;
					(*writelnFields)[i]->textY = y + (*writelnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_RIGHT:
					(*writelnFields)[i]->textX = x + (*writelnFields)[i]->x + (*writelnFields)[i]->width - border;
					(*writelnFields)[i]->textY = y + (*writelnFields)[i]->y + (*writelnFields)[i]->height + border;
					break;
				}
			}
		}
		if(readlnFields)
		{
			for(unsigned i = 0; i < readlnFields->size(); i++)
			{
				switch((*readlnFields)[i]->align)
				{
				case ALLEGRO_ALIGN_CENTER:
					(*readlnFields)[i]->textX = x + (*readlnFields)[i]->x + (*readlnFields)[i]->width / 2;
					(*readlnFields)[i]->textY = y + (*readlnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_LEFT:
					(*readlnFields)[i]->textX = x + (*readlnFields)[i]->x + border;
					(*readlnFields)[i]->textY = y + (*readlnFields)[i]->y + border;
					break;
				case ALLEGRO_ALIGN_RIGHT:
					(*readlnFields)[i]->textX = x + (*readlnFields)[i]->x + (*readlnFields)[i]->width - border;
					(*readlnFields)[i]->textY = y + (*readlnFields)[i]->y + (*readlnFields)[i]->height + border;
					break;
				}
			}
		}
		break;
	}
}

void TextBox::setCoordinatesOfTextBox(int x, int y)
{
	setX(x);
	setY(y);
}

void TextBox::setWidth(int newWidth)
{
	this->x = newWidth;
	this->primX = newWidth;
	setCoordinatesOfText();
}

void TextBox::setHeight(int newHeight)
{
	this->y = newHeight;
	this->primY = newHeight;
	setCoordinatesOfText();
}

void TextBox::setWidthBetweenTwoPoints(int first, int second, int align)
{
	switch(align)
	{
	case BOX_LEFT:
		if(border == 0)
			this->x = first + (second - first) / 2 - TRANSPARENCY_SIZE / 2;
		else
			this->x = first + (second - first) / 2;
		break;
	case BOX_MIDDLE:
		if(border == 0)
			this->x = first + (second - first) / 2 - TRANSPARENCY_SIZE / 2 + width / 2;
		else
			this->x = first + (second - first) / 2 - width / 2;
		break;
	case BOX_RIGHT:
		if(border == 0)
			this->x = first + (second - first) / 2 - TRANSPARENCY_SIZE / 2 + width;
		else
			this->x = first + (second - first) / 2 - width;
		break;
	}
	setCoordinatesOfText();
}

void TextBox::setHeightBetweenTwoPoints(int first, int second, int align)
{
	switch(align)
	{
	case BOX_TOP:
		if(border == 0)
			this->y = first + (second - first) / 2 - TRANSPARENCY_SIZE / 2;
		else
			this->y = first + (second - first) / 2;
		break;
	case BOX_MIDDLE:
		if(border == 0)
			this->y = first + (second - first) / 2 - TRANSPARENCY_SIZE / 2 - height / 2;
		else
			this->y = first + (second - first) / 2 - height / 2;
		break;
	case BOX_BOTTOM:
		if(border == 0)
			this->y = first + (second - first) / 2 - TRANSPARENCY_SIZE / 2 - height;
		else
			this->y = first + (second - first) / 2 - height;
		break;
	}
	setCoordinatesOfText();
}

void TextBox::setX(int x)
{
	switch(this->align)
	{
	case ALLEGRO_ALIGN_CENTER:
		this->x = x - this->width / 2;
		break;
	case ALLEGRO_ALIGN_LEFT:
		this->x = x;
		break;
	case ALLEGRO_ALIGN_RIGHT:
		this->x = x - this->width;
		break;
	default: this->x = x; break;
	}
	setCoordinatesOfText();
}

void TextBox::setY(int y)
{
	this->y = y;
	setCoordinatesOfText();
}

void TextBox::setType(int newType)
{
	this->type = newType;
	
	switch(newType)
	{
	case BOX_STANDARD:
		bgColor = al_map_rgb(124, 68, 13);
		border = 0;
		break;
	case BOX_GAME_INFO:
		{
			bgColorOdd = al_map_rgba(50, 50, 50, 150);
			bgColorEven = al_map_rgba(30, 30, 30, 130);

			if(writelnFields)
				for(unsigned i = 0; i < writelnFields->size(); i++)
					(*writelnFields)[i]->fontColor = al_map_rgb(210, 210, 210);
			if(readlnFields)
				for(unsigned i = 0; i < readlnFields->size(); i++)
				{
					(*readlnFields)[i]->fontColor = al_map_rgb(210, 210, 210);
					(*readlnFields)[i]->cursorColor = al_map_rgb(210, 210, 210);
				}

			border = 8;
		}
		break;
	default:
		bgColor = al_map_rgb(0, 0, 0);
		break;
	}

	// ustawienie szeroko�ci obramowania (nie dotyczy np. typu STANDARD, kt�ry obramowanie ma rysowane poza obszarem - w takim przypadku border == 0)
	width += (border * 2);
	height += (border * 2);
	
	unsigned writeVec = 0, readVec = 0;
	for(unsigned i = 0; i < totalFields; i++)
	{
		if(writelnFields && writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i)
		{
			(*writelnFields)[writeVec]->width += (border * 2);
			(*writelnFields)[writeVec]->height += (border * 2);
			writeVec++;
		}
		if(readlnFields && readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
		{
			(*readlnFields)[readVec]->width += (border * 2);
			(*readlnFields)[readVec]->height += (border * 2);
			readVec++;
		}
	}
}

void TextBox::setWritelnVisible(int fieldID, bool state)
{
	(*writelnFields)[fieldID]->visible = state;
	sizeUpdate();
}

void TextBox::sizeUpdate()
{
	unsigned writeVec = 0, readVec = 0;

	switch(this->orientation)
	{
	case BOX_HORIZONTAL:
		this->width = 0;
		this->height = 0;
		for(unsigned i = 0; i < totalFields; i++)
		{
			if(writelnFields && writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i && (*writelnFields)[writeVec]->visible)
			{
				(*writelnFields)[writeVec]->x = this->width;
				this->width += (*writelnFields)[writeVec]->width;
				if(this->height < (*writelnFields)[writeVec]->height)
					this->height = (*writelnFields)[writeVec]->height;
			}
			if(readlnFields && readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
			{
				(*readlnFields)[readVec]->x = this->width;
				this->width += (*readlnFields)[readVec]->width;
				if(this->height < (*readlnFields)[readVec]->height)
					this->height = (*readlnFields)[readVec]->height;
			}
			if(writelnFields && (*writelnFields)[writeVec]->IDfield == i)
				writeVec++;
			if(readlnFields && (*readlnFields)[readVec]->IDfield == i)
				readVec++;
		}
		break;
	case BOX_VERTICAL:
		this->width = 0;
		this->height = 0;
		for(unsigned i = 0; i < totalFields; i++)
		{
			if(writelnFields && writelnFields->size() > writeVec && (*writelnFields)[writeVec]->IDfield == i && (*writelnFields)[writeVec]->visible)
			{
				(*writelnFields)[writeVec]->y = this->height;
				this->height += (*writelnFields)[writeVec]->height;
				if(this->width < (*writelnFields)[writeVec]->width)
					this->width = (*writelnFields)[writeVec]->width;
			}
			if(readlnFields && readlnFields->size() > readVec && (*readlnFields)[readVec]->IDfield == i)
			{
				(*readlnFields)[readVec]->y = this->height;
				this->height += (*readlnFields)[readVec]->height;
				if(this->width < (*readlnFields)[readVec]->width)
					this->width = (*readlnFields)[readVec]->width;
			}
			if(writelnFields && (*writelnFields)[writeVec]->IDfield == i)
				writeVec++;
			if(readlnFields && (*readlnFields)[readVec]->IDfield == i)
				readVec++;
		}
		break;
	}
	updateDimension();
	setCoordinatesOfText();
}

void TextBox::updateDimension()
{
	switch(this->align)
	{
	case ALLEGRO_ALIGN_CENTER:
		this->x = this->primX - this->width / 2;
		break;
	case ALLEGRO_ALIGN_RIGHT:
		this->x = this->primX - this->width;
		break;
	}
}