#include "TextReadlnField.h"
#include "TextBox.h"
#include "..\..\gamecore\GameCore.h"
#include "..\..\sources2048\Game2048Core.h"
#include "..\..\sources2048\Game2048FontManager.h"

TextReadlnField::TextReadlnField(unsigned IDfield, Fonts *font, int width, int height, unsigned shownCharacters, unsigned typingCharacters, ALLEGRO_COLOR fontColor)
{
	this->gameCore = NULL;
	this->textBox = NULL;

	this->IDfield = IDfield;
	this->align = ALLEGRO_ALIGN_LEFT;

	//obliczanie szeroko�ci pola na podstawie - sprawdza si� dobrze je�li czcionka jest o sta�ej szeroko�ci. Kod do optymalizacji
	if(width == 0)
	{
		std::string tmp;
		if(!typingCharacters || (typingCharacters > shownCharacters))
			tmp.assign(shownCharacters, 'W');
		else
			tmp.assign(typingCharacters, 'W');
		this->width = al_get_text_width(font->font, tmp.c_str());
	}

	this->height = height;
	this->shownCharacters = shownCharacters;
	this->typingCharacters = typingCharacters;
	this->fontColor = fontColor;
	this->cursorColor = fontColor;
	this->selectionColor = al_map_rgb(51, 153, 255);
	this->font = font;
	this->fontHeight = al_get_font_line_height(font->font);
	
	reset();
}

TextReadlnField::~TextReadlnField()
{
}

bool TextReadlnField::isFieldTextEmpty()
{
	if(text.empty())
		return true;
	else
		return false;
}

std::string TextReadlnField::getFieldText()
{
	return text;
}

unsigned TextReadlnField::getCursorPosition()
{
	return cursor;
}

int TextReadlnField::getState()
{
	return fieldState;
}

void TextReadlnField::draw()
{
	std::string tmp;
	std::string showText;

	showText.assign(text, text.size() < shownCharacters ? 0 : cursor - cursorShowPos, text.size() < shownCharacters ? text.size() : shownCharacters);
	
	if(selection)
	{
		for(unsigned i = 0; i < showText.size(); i++)
		{
			tmp = showText.substr(i, 1);
			if(cursor - cursorShowPos + i >= selBegin && cursor - cursorShowPos + i < selEnd)
				for(int j = 0; j < al_get_text_width(font->font, tmp.c_str()); j++)
					al_draw_line(
						this->textX + i * al_get_text_width(font->font, tmp.c_str()) + j,
						this->textY,
						this->textX + i * al_get_text_width(font->font, tmp.c_str()) + j,
						this->textY + fontHeight,
						selectionColor, 0);
		}
	}

	al_draw_text(font->font, fontColor,  this->textX, this->textY, this->align, showText.c_str());
	
	tmp = showText.substr(0, cursorShowPos);

	if(drawCursor)
		al_draw_line(
			this->textX + al_get_text_width(font->font, tmp.c_str()),
			this->textY,
			this->textX + al_get_text_width(font->font, tmp.c_str()),
			this->textY + fontHeight,
			this->cursorColor, 0);
}

void TextReadlnField::reset()
{
	fieldState = READLN;
	drawCursor = true;
	cursor = 0;
	cursorShowPos = 0;
	lastCursorPos = 0;
	lastDraw = 0;
	lastTextSize = 0;
	text.clear();
	selectionReset();
}

void TextReadlnField::selectAll()
{
	selection = true;
	selBegin = 0;
	selEnd = text.size();
	cursor = text.size();
	cursorShowPos = text.size() > 29 ? 29 : text.size();
}

void TextReadlnField::setAddresses(Game2048Core *gameCore, TextBox *textBox)
{
	this->gameCore = gameCore;
	this->textBox = textBox;
}

void TextReadlnField::setReadlnFieldState(int newState)
{
	fieldState = newState;
}

void TextReadlnField::addCursors()
{
	cursor++;
	if(cursor == text.size() && cursorShowPos < shownCharacters)
		cursorShowPos++;
	else if((cursor > text.size() - shownCharacters / 2 - 1 || cursorShowPos < shownCharacters / 2) && cursorShowPos < shownCharacters)
		cursorShowPos++;
	else if(lastTextSize != text.size() && cursorShowPos < shownCharacters)
		cursorShowPos++;
}

void TextReadlnField::copyToClipboard()
{
	if (selection)
		al_set_clipboard_text(gameCore->alManager->getDisplay(), text.substr(selBegin, selEnd - selBegin).c_str());
}

void TextReadlnField::cursorLeft()
{
	if(cursor > 0)
		cursor--;
	if((cursor < shownCharacters / 2 || cursorShowPos > shownCharacters / 2) && cursorShowPos > 0)
		cursorShowPos--;
}

void TextReadlnField::cursorRight()
{
	if(cursor < text.size())
		addCursors();
}

void TextReadlnField::pasteFromClipboard()
{
	char *cText = al_get_clipboard_text(gameCore->alManager->getDisplay());
	int cTextLen = strlen(cText);
	if(cTextLen > 0)
	{
		selectionErase();

		text.insert(cursor, cText);
		cursor += cTextLen;
		cursorShowPos = cursorShowPos + cTextLen < 30 ? cursorShowPos + cTextLen : 29;
	}
}

void TextReadlnField::selectionErase()
{
	if(selection)
	{
		text.erase(text.begin() + selBegin, text.begin() + selEnd);
		cursor = selBegin;
		cursorShowPos = selBegin > 29 ? 29 : selBegin;
		selectionReset();
	}
}

void TextReadlnField::selectionReset()
{
	selection = false;
	selBegin = 0;
	selEnd = 0;
}

void TextReadlnField::setText(std::string newText)
{
	this->text = newText;
}

void TextReadlnField::update(unsigned elapsedTime)
{
	if(lastCursorPos != cursor || lastTextSize != text.size())
	{
		drawCursor = true;
		lastDraw = 0;
		lastCursorPos = cursor;
		lastTextSize = text.size();
	}
	else
	{
		lastDraw += elapsedTime;

		if(lastDraw > DRAWING_CURSOR_TIME)
		{
			lastDraw = lastDraw % DRAWING_CURSOR_TIME;
			if(drawCursor)
				drawCursor = false;
			else
				drawCursor = true;
		}
	}
	
	// klawiszologia zaznaczania tekstu
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LSHIFT) || gameCore->keyManager->isPressed(ALLEGRO_KEY_RSHIFT))
	{
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, true))
		{
			if(!selection)
			{
				selection = true;
				selEnd = cursor;
				cursorLeft();
				selBegin = cursor;
			}
			else
			{
				if(cursor == selBegin) // po lewej stronie kursora
				{
					cursorLeft();
					if(selBegin > 0)
						selBegin--;
				}
				else if(cursor == selEnd) // po prawej stronie kursora
				{
					cursorLeft();
					if(selEnd > selBegin)
						selEnd--;
				}
				if(selBegin == selEnd)
					selection = false;
			}
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, true))
		{
			if(!selection)
			{
				selection = true;
				selBegin = cursor;
				cursorRight();
				selEnd = cursor;
			}
			else
			{
				if(cursor == selBegin) // po lewej stronie kursora
				{
					cursorRight();
					if(selBegin < selEnd)
						selBegin++;
				}
				else if(cursor == selEnd) // po prawej stronie kursora
				{
					cursorRight();
					if(selEnd < text.size())
						selEnd++;
				}
				if(selBegin == selEnd)
					selection = false;
			}
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_HOME, true))
		{
			if(!selection)
			{
				selection = true;
				selEnd = cursor;
			}
			selBegin = 0;
			cursor = 0;
			cursorShowPos = 0;
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_END, true))
		{
			if(!selection)
			{
				selection = true;
				selBegin = cursor;
			}
			selEnd = text.size();
			cursor = text.size();
			cursorShowPos = text.size() > 29 ? 29 : text.size();
		}
	}
	// pozosta�e klawisze specjalne
	else if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL) || gameCore->keyManager->isPressed(ALLEGRO_KEY_RCTRL))
	{
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_A, true))
			selectAll();
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_X, true))
		{
			copyToClipboard();
			selectionErase();
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_C, true))
			copyToClipboard();
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_V, true))
			pasteFromClipboard();
	}
	else
	{
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, true))
		{
			if(!selection)
				cursorLeft();
			selectionReset();
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, true))
		{
			if(!selection)
				cursorRight();
			selectionReset();
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_HOME, true))
		{
			selectionReset();
			cursor = 0;
			cursorShowPos = 0;
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_END, true))
		{
			selectionReset();

			cursor = text.size();
			if(text.size() > shownCharacters)
				cursorShowPos = shownCharacters;
			else
				cursorShowPos = cursor;
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_BACKSPACE, true))
		{
			if(selection)
				selectionErase();
			else if(cursor > 0)
			{
				text.erase(cursor - 1, 1);
				cursor--;
				if(cursor < shownCharacters && cursorShowPos > 0)
					cursorShowPos--;
			}
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DELETE, true))
		{
			if(selection)
				selectionErase();
			else if(cursor < text.size())
				text.erase(cursor, 1);
		}
	}
	// klawisze alfanumeryczne + spacja
	if(!typingCharacters || (text.size() < typingCharacters))
	{
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LSHIFT) || gameCore->keyManager->isPressed(ALLEGRO_KEY_RSHIFT))
		{
			if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ALTGR))
			{
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_A, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_C, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_E, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_L, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_N, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_O, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_X, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Z, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
			}
			else
			{
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_A, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'A');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_B, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'B');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_C, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'C');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_D, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'D');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_E, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'E');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_F, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'F');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_G, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'G');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_H, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'H');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_I, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'I');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_J, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'J');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_K, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'K');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_L, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'L');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_M, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'M');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_N, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'N');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_O, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'O');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_P, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'P');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Q, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'Q');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_R, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'R');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_S, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'S');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_T, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'T');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_U, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'U');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_V, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'V');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_W, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'W');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_X, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'X');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Y, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'Y');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Z, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'Z');
					addCursors();
				}
				
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_1, true))
				{
					selectionErase();
					text.insert(cursor, 1, '!');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_2, true))
				{
					selectionErase();
					text.insert(cursor, 1, '@');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_3, true))
				{
					selectionErase();
					text.insert(cursor, 1, '#');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_4, true))
				{
					selectionErase();
					text.insert(cursor, 1, '$');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_5, true))
				{
					selectionErase();
					text.insert(cursor, 1, '%');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_6, true))
				{
					selectionErase();
					text.insert(cursor, 1, '^');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_7, true))
				{
					selectionErase();
					text.insert(cursor, 1, '&');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_9, true))
				{
					selectionErase();
					text.insert(cursor, 1, '(');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_0, true))
				{
					selectionErase();
					text.insert(cursor, 1, ')');
					addCursors();
				}
				
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_SEMICOLON, true))
				{
					selectionErase();
					text.insert(cursor, 1, ':');
					addCursors();
				}
				
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_TILDE, true))
				{
					selectionErase();
					text.insert(cursor, 1, '~');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_MINUS, true))
				{
					selectionErase();
					text.insert(cursor, 1, '_');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_EQUALS, true))
				{
					selectionErase();
					text.insert(cursor, 1, '+');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_OPENBRACE, true))
				{
					selectionErase();
					text.insert(cursor, 1, '{');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_CLOSEBRACE, true))
				{
					selectionErase();
					text.insert(cursor, 1, '}');
					addCursors();
				}
			}
		}
		else
		{
			if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ALTGR))
			{
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_A, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_C, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_E, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_L, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_N, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_O, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_X, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Z, true))
				{
					selectionErase();
					text.insert(cursor, 1, '�');
					addCursors();
				}
			}
			else
			{
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_A, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'a');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_B, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'b');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_C, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'c');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_D, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'd');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_E, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'e');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_F, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'f');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_G, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'g');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_H, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'h');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_I, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'i');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_J, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'j');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_K, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'k');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_L, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'l');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_M, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'm');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_N, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'n');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_O, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'o');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_P, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'p');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Q, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'q');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_R, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'r');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_S, true))
				{
					selectionErase();
					text.insert(cursor, 1, 's');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_T, true))
				{
					selectionErase();
					text.insert(cursor, 1, 't');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_U, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'u');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_V, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'v');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_W, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'w');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_X, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'x');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Y, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'y');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_Z, true))
				{
					selectionErase();
					text.insert(cursor, 1, 'z');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_0, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_0, true))
				{
					selectionErase();
					text.insert(cursor, 1, '0');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_1, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_1, true))
				{
					selectionErase();
					text.insert(cursor, 1, '1');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_2, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_2, true))
				{
					selectionErase();
					text.insert(cursor, 1, '2');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_3, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_3, true))
				{
					selectionErase();
					text.insert(cursor, 1, '3');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_4, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_4, true))
				{
					selectionErase();
					text.insert(cursor, 1, '4');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_5, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_5, true))
				{
					selectionErase();
					text.insert(cursor, 1, '5');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_6, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_6, true))
				{
					selectionErase();
					text.insert(cursor, 1, '6');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_7, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_7, true))
				{
					selectionErase();
					text.insert(cursor, 1, '7');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_8, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_8, true))
				{
					selectionErase();
					text.insert(cursor, 1, '8');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_9, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_9, true))
				{
					selectionErase();
					text.insert(cursor, 1, '9');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_SPACE, true))
				{
					selectionErase();
					text.insert(cursor, 1, ' ');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_SLASH, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_SLASH, true))
				{
					selectionErase();
					text.insert(cursor, 1, '/');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_BACKSLASH, true))
				{
					selectionErase();
					text.insert(cursor, 1, '\\');
					addCursors();
				}
				
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_TILDE, true))
				{
					selectionErase();
					text.insert(cursor, 1, '`');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_MINUS, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_MINUS, true))
				{
					selectionErase();
					text.insert(cursor, 1, '-');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_EQUALS, true))
				{
					selectionErase();
					text.insert(cursor, 1, '=');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_PAD_PLUS, true))
				{
					selectionErase();
					text.insert(cursor, 1, '+');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_OPENBRACE, true))
				{
					selectionErase();
					text.insert(cursor, 1, '[');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_CLOSEBRACE, true))
				{
					selectionErase();
					text.insert(cursor, 1, ']');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_SEMICOLON, true))
				{
					selectionErase();
					text.insert(cursor, 1, ';');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_QUOTE, true))
				{
					selectionErase();
					text.insert(cursor, 1, '\'');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_COMMA, true))
				{
					selectionErase();
					text.insert(cursor, 1, ',');
					addCursors();
				}
				if(gameCore->keyManager->isPressed(ALLEGRO_KEY_FULLSTOP, true))
				{
					selectionErase();
					text.insert(cursor, 1, '.');
					addCursors();
				}
			}
		}
	}
	
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true))
		this->setReadlnFieldState(ENTER);

	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
		this->setReadlnFieldState(ESCAPE);
}