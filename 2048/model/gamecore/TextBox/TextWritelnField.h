#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <string>

struct Fonts;
class TextBox;

/** TextWritelnField - 2014-09-09 - sk�adowa klasa TextBox zawieraj�ca stringi
 *
 * - unsigned IDfield - jest to unikalny identyfikator informuj�cy TextBox o kolejno�ci ka�dego obiektu w nim zawartego. Wliczaj�c w to pola typu TextReadlnField.
 * - bool visible     - parametr okre�laj�cy widoczno�� pola.
 * - Fonts *font      - czcionka, kt�r� b�dzie wy�wietlany tekst.
 * - int width        - szeroko�� pola - je�li width == 0, wtedy szeroko�� obliczana jest na podstawie d�ugo�ci stringu.
 * - int height       - wysoko�� pola - je�li height == 0, wtedy wysoko�� obliczana jest na podstawie wysoko�ci czcionki.
 * - int textX        - okre�la X dla tekstu z uwzgl�dnieniem wsp�rz�dnych TextBox'a i obramowania.
 * - int textY        - okre�la Y dla tekstu z uwzgl�dnieniem wsp�rz�dnych TextBox'a i obramowania.
 * - AL_COL fontColor - kolor czcionki tekstu.
 * - std::string text - string. Mo�na go zmieni� po utworzeniu obiektu za pomoc� funkcji setText(std::string newText).
 * - int align        - centrowanie tekstu w obr�bie TextWritelnField.
 *
 * - setText(std::string newText) - ustawienie tekstu pola.
 * - setTextBoxAddress(TextBox *textBox) - funkcja, kt�r� TextBox wykorzystuje w swoim konstruktorze do przes�ania swojego adresu.
 */

class TextWritelnField
{
private:
	TextBox *textBox;
	unsigned IDfield;
	bool visible;

	ALLEGRO_COLOR fontColor;
	
	Fonts *font;
	int align;
	int x;
	int y;
	int width;
	int height;
	int textX;
	int textY;
	int fontHeight;
	std::string text;
public:
	TextWritelnField(unsigned IDfield, Fonts *font, int width, int height, ALLEGRO_COLOR fontColor, std::string text = "", int align = ALLEGRO_ALIGN_CENTER);
	~TextWritelnField();
	
	std::string getText();
	void draw();
	void setText(std::string newText);
	void setTextBoxAddress(TextBox *textBox);

	friend class TextBox;
};