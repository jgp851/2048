#pragma once
#include "..\allegro\AllegroManager.h"
#include "..\allegro\BitmapLoader.h"
#include "..\allegro\FontLoader.h"
#include "..\allegro\KeyManager.h"
#include "..\allegro\MouseManager.h"
#include "..\allegro\SoundLoader.h"
#include "..\allegro\Transform.h"
#include "ApplicationSettings.h"
#include <string>

class GameCore
{
private:
	bool redraw;
	unsigned loopLength;
	//void debugEvents(ALLEGRO_EVENT *event);
protected:
	bool eventDisplayClose;
	bool isRunning;
	bool restartApplication;
	int tps;

	/**
	* Funkcje draw() i update() nic nie robi� i docelowo powinny byc abstrakcyjne.
	* Oznaczac to bedzie, ze nie da sie wtedy utworzyc obiektu klasy GameCore,
	* Nalezy natomiast utworzyc wlasna klase juz dla konkretnej gry, ktora bedzie
	* rozszerzac klase GameCore i w niej nalezy zaimplementowac funkcje update().
	*/
	GameCore(int tps, std::string gameTitle);
	virtual void draw() = 0;
	virtual void update(unsigned elapsedTime) = 0;
public:
	~GameCore();
	
	AllegroManager *alManager;
	ApplicationSettings *appSettings;
	BitmapLoader *bitmapLoader;
	FontLoader *fontLoader;
	MouseManager *mouseManager;
	KeyManager *keyManager;
	SoundLoader *soundLoader;
	Transform *trans;

	const std::string gameTitle;

	bool getRestartAppSetting();
	std::string getCurrentTime(std::string format);
	bool copyFile(std::string sourcePath, std::string destinationPath);
	void mainLoop();
	void setRestartAppSetting(bool state);
	
	virtual bool init() = 0;
};