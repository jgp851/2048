#pragma once
#include <allegro5\allegro.h>

class ButtonSide
{
protected:
	ALLEGRO_COLOR bgColor;
	int size;
	int x;
	int y;
public:
	ButtonSide(){};
	~ButtonSide(){};
	
	ALLEGRO_COLOR getMouseOverButtonColor();
	ALLEGRO_COLOR getMouseOutsideButtonColor();
	
	int getSize();
	void setSize(int size);
	virtual bool isPressed() = 0;
	virtual bool mouseIsOverButton() = 0;
	virtual void draw() = 0;
	virtual void setCoordinates(int x, int y) = 0;
	virtual void update() = 0;
};