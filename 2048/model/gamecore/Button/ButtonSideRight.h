#pragma once
#include <allegro5\allegro.h>
#include "ButtonSide.h"

class Game2048Core;

class ButtonSideRight : public ButtonSide
{
private:
	Game2048Core *gameCore;
	
	ALLEGRO_BITMAP *buttonRight;
	ALLEGRO_BITMAP *buttonRightPressed;
	ALLEGRO_BITMAP *buttonRightIcon;
public:
	ButtonSideRight(Game2048Core *gameCore, int size);
	~ButtonSideRight();
	
	bool isPressed();
	bool mouseIsOverButton();
	void draw();
	void setCoordinates(int x, int y);
	void update();
};