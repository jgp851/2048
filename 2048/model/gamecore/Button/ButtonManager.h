#pragma once
#include <vector>
#include "Button.h"

class Game2048Core;

struct ButtonCoordinates
{
	int x;
	int y;
	ButtonCoordinates(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

class ButtonManager
{
private:
	Game2048Core *gameCore;
	std::vector<Button*> *buttons;
	std::vector<ButtonCoordinates*> coordinatesBtnList;

	bool isDrawing;
	bool isUpdating;
	bool keyboardActive;
	int selectedButton;
	
	void getBtnCoordinates();
	void processInput();
	void resetState();
public:
	ButtonManager(Game2048Core *gameCore, std::vector<Button*> *buttons);
	~ButtonManager();

	bool isKeyEnterPressed();
	bool isLeftMousePressed();
	bool isPressed();
	int getSelectedButton();
	unsigned int size();

	void draw();
	void selectButton(int number);
	void setDrawing(bool state);
	void switchButton(unsigned int id, bool state);
	void update();
	void updateWithoutInputProcess();
};