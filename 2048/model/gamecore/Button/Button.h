#pragma once
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <string>

enum BtnType
{
	BTN_TYP_AVAILABLE,
	BTN_TYP_UNAVAILABLE,
	BTN_TYP_DEFAULT_BUTTON,
	BTN_TYP_INFO_BUTTON,
	BTN_TYP_INFO_BUTTON_BAD,
	BTN_TYP_INFO_BUTTON_GOOD,
	BTN_TYP_MINI_NOTE
};
enum BtnState
{
	BTN_ST_UNSELECTED,
	BTN_ST_SELECTED
};

class ButtonSide;
class Game2048Core;

class Button
{
private:
	Game2048Core *gameCore;
	ALLEGRO_BITMAP *buttonMainMiddle;
	ALLEGRO_BITMAP *buttonMainLeft;
	ALLEGRO_BITMAP *buttonMainRight;
	ALLEGRO_BITMAP *buttonMainMiddlePressed;
	ALLEGRO_BITMAP *buttonMainLeftPressed;
	ALLEGRO_BITMAP *buttonMainRightPressed;
	ALLEGRO_BITMAP *mainLeft;
	ALLEGRO_BITMAP *mainMiddle;
	ALLEGRO_BITMAP *mainRight;
	ALLEGRO_COLOR bgColor;
	ALLEGRO_COLOR fontColor;
	ALLEGRO_FONT *font;
	ButtonSide *sideButtonLeft;
	ButtonSide *sideButtonRight;
	
	BtnType type;
	BtnState state;

	bool drawing;
	bool sideButtons;
	float width;
	float height;
	int textWidth;
	int x;
	int y;
	std::string text;
	int btnWidthWithLeft;
	int btnWidthWithRight;
	int fontHeight;
	
	void calculateButtonWidth();
	void calculateButtonsSideCoordinates();
	void calculateButtonsSideSize();
	void calculateTextWidth();
	void drawButtonBackground();
	void setBgStandardColor();
	void setBgMouseOverColor();
	void setFont();
	void setFontColor();
	void setStateStandardColor();
	void setStateMouseOverColor();
	void setStateFontColor();
public:
	Button(Game2048Core *gameCore, int x, int y, bool sideButtons = false, std::string strText = "", BtnType type = BTN_TYP_AVAILABLE);
	~Button();
	
	bool mouseIsOverButton();
	bool isDrawing();
	bool isPressed();
	bool isKeyEnterPressed();
	bool isLeftMousePressed();
	bool isLeftPressed();
	bool isRightPressed();
	bool isSideButtons();
	BtnType getType();

	float getWidth();

	int getX();
	int getY();

	void draw();
	void drawSideButtons();
	void resetState();
	void setHeight(int newHeight);
	void setDrawing(bool state);
	void setText(std::string newText);
	void setState(BtnState buttonState);
	void setType(BtnType buttonType);
	void setX(int x);
	void setY(int y);
	void update();
	void updateSideButtons();
};