#include "ButtonSideRight.h"
#include "..\..\sources2048\Game2048Core.h"

ButtonSideRight::ButtonSideRight(Game2048Core *gameCore, int size)
{
	this->gameCore = gameCore;
	this->size = size;
	buttonRight = gameCore->mediaManager->PNG_buttonRight;
	buttonRightPressed = gameCore->mediaManager->PNG_buttonRightPressed;
	buttonRightIcon = gameCore->mediaManager->PNG_buttonRightIcon;
}

ButtonSideRight::~ButtonSideRight()
{
}

bool ButtonSideRight::isPressed()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
	{
		gameCore->soundPlayer->playSwitchMenu();
		return true;
	}
	else
		return false;
}

bool ButtonSideRight::mouseIsOverButton()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + size &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + size
		)
		return true;
	else
		return false;
}

void ButtonSideRight::draw()
{
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, false) && gameCore->keyManager->is_EVENT_KEY_DOWN()) || (mouseIsOverButton() && gameCore->mouseManager->isLeftPressed()))
	{
		al_draw_tinted_scaled_bitmap(buttonRightPressed, bgColor, 0, 0, al_get_bitmap_width(buttonRightPressed), al_get_bitmap_height(buttonRightPressed), x, y, size, size, 0);
		al_draw_scaled_bitmap(buttonRightIcon, 0, 0, al_get_bitmap_width(buttonRightIcon), al_get_bitmap_height(buttonRightIcon),
			x + size * 0.2049, y + size * 0.3512,
			size * 0.4195, size * 0.3415, 0);
	}
	else
	{
		al_draw_tinted_scaled_bitmap(buttonRight, bgColor, 0, 0, al_get_bitmap_width(buttonRight), al_get_bitmap_height(buttonRight), x, y, size, size, 0);
		al_draw_scaled_bitmap(buttonRightIcon, 0, 0, al_get_bitmap_width(buttonRightIcon), al_get_bitmap_height(buttonRightIcon),
			x + size * 0.1317, y + size * 0.3171,
			size * 0.5122, size * 0.4146, 0);
	}
}

void ButtonSideRight::setCoordinates(int x, int y)
{
	this->x = x;
	this->y = y;
}

void ButtonSideRight::update()
{
	if(mouseIsOverButton())
		bgColor = getMouseOverButtonColor();
	else
		bgColor = getMouseOutsideButtonColor();
}