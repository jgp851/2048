#pragma once
#include <allegro5\allegro.h>
#include "ButtonSide.h"

class Game2048Core;

class ButtonSideLeft : public ButtonSide
{
private:
	Game2048Core *gameCore;
	
	ALLEGRO_BITMAP *buttonLeft;
	ALLEGRO_BITMAP *buttonLeftPressed;
	ALLEGRO_BITMAP *buttonLeftIcon;
public:
	ButtonSideLeft(Game2048Core *gameCore, int size);
	~ButtonSideLeft();
	
	bool isPressed();
	bool mouseIsOverButton();
	void draw();
	void setCoordinates(int x, int y);
	void update();
};