#include "Button.h"
#include "ButtonSideLeft.h"
#include "ButtonSideRight.h"
#include "..\..\sources2048\Game2048Core.h"
#include "..\..\sources2048\Game2048FontManager.h"

Button::Button(Game2048Core *gameCore, int x, int y, bool sideButtons, std::string text, BtnType type)
{
	this->gameCore = gameCore;
	buttonMainLeft = gameCore->mediaManager->PNG_buttonMainLeft;
	buttonMainMiddle = gameCore->mediaManager->PNG_buttonMainMiddle;
	buttonMainRight = gameCore->mediaManager->PNG_buttonMainRight;
	buttonMainLeftPressed = gameCore->mediaManager->PNG_buttonMainLeftPressed;
	buttonMainMiddlePressed = gameCore->mediaManager->PNG_buttonMainMiddlePressed;
	buttonMainRightPressed = gameCore->mediaManager->PNG_buttonMainRightPressed;
	
	this->drawing = true;
	this->sideButtons = sideButtons;
	this->text = text;
	this->x = x;
	this->y = y;
	this->setType(type);
	this->setState(BTN_ST_UNSELECTED);
	this->font = gameCore->fontLoader->loadFont("gfx/kirsty.ttf", fontHeight, 0);
	calculateTextWidth();
	calculateButtonWidth();

	if(sideButtons)
	{
		sideButtonLeft = new ButtonSideLeft(gameCore, this->height);
		sideButtonRight = new ButtonSideRight(gameCore, this->height);
		calculateButtonsSideCoordinates();
		calculateButtonsSideSize();
	}
	else
	{
		sideButtonLeft = NULL;
		sideButtonRight = NULL;
		btnWidthWithLeft = 0;
		btnWidthWithRight = 0;
	}
}

Button::~Button()
{
}

bool Button::isKeyEnterPressed()
{
	if(drawing)
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true))
		{
			gameCore->soundPlayer->playGoIn();
			return true;
		}
	return false;
}

bool Button::isDrawing()
{
	return drawing;
}

bool Button::isLeftMousePressed()
{
	if(drawing)
		if(mouseIsOverButton() && gameCore->mouseManager->isLeftReleased())
			return true;
	return false;
}

bool Button::isPressed()
{
	if(drawing)
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
		{
			gameCore->soundPlayer->playGoIn();
			return true;
		}
	return false;
}

bool Button::isLeftPressed()
{
	if(drawing && sideButtonLeft->isPressed())
		return true;
	return false;
}

bool Button::isRightPressed()
{
	if(drawing && sideButtonRight->isPressed())
			return true;
	return false;
}

bool Button::isSideButtons()
{
	return sideButtons;
}

bool Button::mouseIsOverButton()
{
	if(drawing && (gameCore->mouseManager->getX() >= x - width / 2 - btnWidthWithLeft &&
		gameCore->mouseManager->getX() <= x + width / 2 + btnWidthWithRight &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() <= y + height
		))
		return true;
	else
		return false;
}

BtnType Button::getType()
{
	return type;
}

float Button::getWidth()
{
	return width + btnWidthWithLeft + btnWidthWithRight;
}

int Button::getX()
{
	return x;
}

int Button::getY()
{
	return y;
}

void Button::calculateButtonWidth()
{
	width = (x + textWidth / 2 + 1 + 0.5317 * height) - ((x - textWidth / 2) - 0.5317 * height);
}

void Button::calculateButtonsSideCoordinates()
{
	if(sideButtons)
	{
		sideButtonLeft->setCoordinates(x - width / 2 - sideButtonLeft->getSize() * 1.2, y);
		sideButtonRight->setCoordinates(x + width / 2 + sideButtonRight->getSize() * 0.2 + 2, y);
	}
}

void Button::calculateButtonsSideSize()
{
	btnWidthWithLeft = sideButtonLeft->getSize() * 1.2;
	btnWidthWithRight = sideButtonRight->getSize() * 1.2;
}

void Button::calculateTextWidth()
{
	this->textWidth = al_get_text_width(font, text.c_str());
}

void Button::draw()
{
	if(drawing)
	{
		this->drawButtonBackground();
		al_draw_text(font, fontColor, x, y, ALLEGRO_ALIGN_CENTER, text.c_str());
	}
}

void Button::drawSideButtons()
{
	if(drawing && sideButtons)
	{
		sideButtonLeft->draw();
		sideButtonRight->draw();
	}
}

void Button::drawButtonBackground()
{
	if(type != BTN_TYP_UNAVAILABLE && state == BTN_ST_SELECTED &&
		((gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER) && gameCore->keyManager->is_EVENT_KEY_DOWN()) || (mouseIsOverButton() && gameCore->mouseManager->isLeftPressed())) )
	{
		mainLeft = buttonMainLeftPressed;
		mainMiddle = buttonMainMiddlePressed;
		mainRight = buttonMainRightPressed;
	}
	else
	{
		mainLeft = buttonMainLeft;
		mainMiddle = buttonMainMiddle;
		mainRight = buttonMainRight;
	}

	al_draw_tinted_scaled_bitmap(mainLeft, bgColor, 0, 0, al_get_bitmap_width(mainLeft), al_get_bitmap_height(mainLeft), (x - textWidth / 2) - 0.5317 * height, y, 0.5317 * height, height, 0);
	al_draw_tinted_scaled_bitmap(mainRight, bgColor, 0, 0, al_get_bitmap_width(mainRight), al_get_bitmap_height(mainRight), x + textWidth / 2 + 1, y, 0.5317 * height, height, 0);
	for(int i = x - (textWidth / 2); i <= x + (textWidth / 2); i++)
		al_draw_tinted_scaled_bitmap(mainMiddle, bgColor, 0, 0, al_get_bitmap_width(mainMiddle), al_get_bitmap_height(mainMiddle), i, y, 1, height, 0);
}

void Button::resetState()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE: setState(BTN_ST_UNSELECTED); break;
	}
}

void Button::setDrawing(bool state)
{
	drawing = state;
}

void Button::setText(std::string newText)
{
	this->text = newText;
	calculateTextWidth();
	calculateButtonWidth();
	calculateButtonsSideCoordinates();
}

void Button::setState(BtnState buttonState)
{
	this->state = buttonState;
	setStateStandardColor();
	setStateFontColor();
}

void Button::setType(BtnType buttonType)
{
	this->type = buttonType;
	setBgStandardColor();
	setFontColor();
	setFont();
}

void Button::setX(int x)
{
	this->x = x;
}

void Button::setY(int y)
{
	this->y = y;
}

void Button::setBgStandardColor()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE: setStateStandardColor(); break; // dostępne
	case BTN_TYP_UNAVAILABLE: bgColor = al_map_rgb(127, 127, 127); break; // niedostępne
	case BTN_TYP_DEFAULT_BUTTON: bgColor = al_map_rgb(0, 126, 255); break; // podzakładka, klawisz domyślny; jedyny; potwierdzający
	case BTN_TYP_INFO_BUTTON: bgColor = al_map_rgb(46, 33, 251); break; //informacyjne, nazwa menu
	case BTN_TYP_INFO_BUTTON_BAD: bgColor = al_map_rgb(255, 30, 10); break;
	case BTN_TYP_INFO_BUTTON_GOOD: bgColor = al_map_rgb(0, 131, 55); break;
	case BTN_TYP_MINI_NOTE: bgColor = al_map_rgb(16, 80, 121); break;
	default: bgColor = al_map_rgb(241, 241, 241); break;
	}
}

void Button::setBgMouseOverColor()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE: setStateMouseOverColor(); break; // dostępne
	case BTN_TYP_UNAVAILABLE: bgColor = al_map_rgb(139, 139, 139); break; // niedostępne
	case BTN_TYP_DEFAULT_BUTTON: bgColor = al_map_rgb(50, 139, 255); break; // podzakładka, klawisz domyślny; jedyny; potwierdzający
	case BTN_TYP_INFO_BUTTON: bgColor = al_map_rgb(71, 47, 255); break; //informacyjne, nazwa menu
	case BTN_TYP_INFO_BUTTON_BAD: bgColor = al_map_rgb(255, 53, 24); break;
	case BTN_TYP_INFO_BUTTON_GOOD: bgColor = al_map_rgb(33, 145, 67); break;
	default: bgColor = al_map_rgb(255, 255, 255); break;
	}
}

void Button::setHeight(int newHeight)
{
	if(font)
		al_destroy_font(font);
	font = gameCore->fontLoader->loadFont("gfx/kirsty.ttf", newHeight, 0);
	this->fontHeight = newHeight;
	this->height = fontHeight;
	calculateTextWidth();
	calculateButtonWidth();
	if(sideButtons)
	{
		sideButtonLeft->setSize(height);
		sideButtonRight->setSize(height);
		calculateButtonsSideCoordinates();
		calculateButtonsSideSize();
	}
}

void Button::setFont()
{
	switch(type)
	{
	case BTN_TYP_MINI_NOTE: fontHeight = gameCore->appSettings->getScreenHeight() * 0.025; break;
	default: fontHeight = gameCore->appSettings->getScreenHeight() * 0.0533; break;
	}
	this->height = fontHeight;
}

void Button::setFontColor()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE: setStateFontColor(); break; // dostępne
	case BTN_TYP_UNAVAILABLE: fontColor = al_map_rgb(0, 0, 0); break; // niedostępne
	case BTN_TYP_DEFAULT_BUTTON: fontColor = al_map_rgb(255, 255, 255); break; // podzakładka, klawisz domyślny; jedyny; potwierdzający
	case BTN_TYP_INFO_BUTTON: fontColor = al_map_rgb(255, 255, 255); break; //informacyjne
	case BTN_TYP_INFO_BUTTON_BAD: fontColor = al_map_rgb(217, 179, 0); break;
	case BTN_TYP_INFO_BUTTON_GOOD: fontColor = al_map_rgb(29, 233, 2); break;
	case BTN_TYP_MINI_NOTE: fontColor = al_map_rgb(255, 255, 255); break;
	default: fontColor = al_map_rgb(0, 0, 0); break;
	}
}

void Button::setStateStandardColor()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE:
		switch(state)
		{
		case BTN_ST_UNSELECTED: bgColor = al_map_rgb(2, 233, 149); break; // nie zaznaczone
		case BTN_ST_SELECTED: bgColor = al_map_rgb(0, 198, 70); break; // zaznaczone
		}
		break;
	}
}

void Button::setStateMouseOverColor()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE:
		switch(state)
		{
		case BTN_ST_SELECTED: bgColor = al_map_rgb(39, 212, 83); break; // zaznaczone
		}
		break;
	}
}

void Button::setStateFontColor()
{
	switch(type)
	{
	case BTN_TYP_AVAILABLE:
		switch(state)
		{
		case BTN_ST_UNSELECTED: fontColor = al_map_rgb(0, 0, 0); break; // nie zaznaczone
		case BTN_ST_SELECTED: fontColor = al_map_rgb(255, 255, 0); break; // zaznaczone
		}
		break;
	}
}

void Button::update()
{
	if(mouseIsOverButton())
		setBgMouseOverColor();
	else
		setBgStandardColor();
}

void Button::updateSideButtons()
{
	if(drawing && sideButtons)
	{
		sideButtonLeft->update();
		sideButtonRight->update();
	}
}