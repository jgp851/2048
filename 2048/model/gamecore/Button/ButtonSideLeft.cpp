#include "ButtonSideLeft.h"
#include "..\..\sources2048\Game2048Core.h"

ButtonSideLeft::ButtonSideLeft(Game2048Core *gameCore, int size)
{
	this->gameCore = gameCore;
	this->size = size;
	buttonLeft = gameCore->mediaManager->PNG_buttonLeft;
	buttonLeftPressed = gameCore->mediaManager->PNG_buttonLeftPressed;
	buttonLeftIcon = gameCore->mediaManager->PNG_buttonLeftIcon;
}

ButtonSideLeft::~ButtonSideLeft()
{
}

bool ButtonSideLeft::isPressed()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, true) || (mouseIsOverButton() && gameCore->mouseManager->isLeftReleased()))
	{
		gameCore->soundPlayer->playSwitchMenu();
		return true;
	}
	else
		return false;
}

bool ButtonSideLeft::mouseIsOverButton()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + size &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + size
		)
		return true;
	else
		return false;
}

void ButtonSideLeft::draw()
{
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, false) && gameCore->keyManager->is_EVENT_KEY_DOWN()) || (mouseIsOverButton() && gameCore->mouseManager->isLeftPressed()))
	{
		al_draw_tinted_scaled_bitmap(buttonLeftPressed, bgColor, 0, 0, al_get_bitmap_width(buttonLeftPressed), al_get_bitmap_height(buttonLeftPressed), x, y, size, size, 0);
		al_draw_scaled_bitmap(buttonLeftIcon, 0, 0, al_get_bitmap_width(buttonLeftIcon), al_get_bitmap_height(buttonLeftIcon),
			x + size * 0.4049, y + size * 0.3512,
			size * 0.4195, size * 0.3415, 0);
	}
	else
	{
		al_draw_tinted_scaled_bitmap(buttonLeft, bgColor, 0, 0, al_get_bitmap_width(buttonLeft), al_get_bitmap_height(buttonLeft), x, y, size, size, 0);
		al_draw_scaled_bitmap(buttonLeftIcon, 0, 0, al_get_bitmap_width(buttonLeftIcon), al_get_bitmap_height(buttonLeftIcon),
			x + size * 0.3561, y + size * 0.3171,
			size * 0.5122, size * 0.4146, 0);
	}
}

void ButtonSideLeft::setCoordinates(int x, int y)
{
	this->x = x;
	this->y = y;
}

void ButtonSideLeft::update()
{
	if(mouseIsOverButton())
		bgColor = getMouseOverButtonColor();
	else
		bgColor = getMouseOutsideButtonColor();
}