#include "ButtonManager.h"
#include "..\..\sources2048\Game2048Core.h"

ButtonManager::ButtonManager(Game2048Core *gameCore, std::vector<Button*> *buttons)
{
	this->gameCore = gameCore;
	this->buttons = buttons;
	this->selectedButton = 0;
	this->isDrawing = true;
	getBtnCoordinates();
}

ButtonManager::~ButtonManager()
{
	for(std::vector<Button*>::iterator it = buttons->begin(); it != buttons->end(); it++)
		delete *it;
	for(std::vector<ButtonCoordinates*>::iterator it = coordinatesBtnList.begin(); it != coordinatesBtnList.end(); it++)
		delete *it;
}

bool ButtonManager::isKeyEnterPressed()
{
	if((*buttons)[selectedButton]->isKeyEnterPressed())
		return true;
	else
		return false;
}

bool ButtonManager::isLeftMousePressed()
{
	if((*buttons)[selectedButton]->isLeftMousePressed())
		return true;
	else
		return false;
}

bool ButtonManager::isPressed()
{
	if((*buttons)[selectedButton]->isPressed())
		return true;
	else
		return false;
}

int ButtonManager::getSelectedButton()
{
	return selectedButton;
}

unsigned int ButtonManager::size()
{
	return buttons->size();
}

void ButtonManager::draw()
{
	if(isDrawing)
		for(unsigned i = 0; i < buttons->size(); i++)
		{
			(*buttons)[i]->draw();
			if(i == selectedButton && isUpdating)
				(*buttons)[i]->drawSideButtons();
		}
}

void ButtonManager::getBtnCoordinates()
{
	for(unsigned i = 0; i < buttons->size(); i++)
		coordinatesBtnList.push_back(new ButtonCoordinates((*buttons)[i]->getX(), (*buttons)[i]->getY()));
}

void ButtonManager::processInput()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true) || gameCore->mouseManager->isWheelDOWN())
	{
		gameCore->soundPlayer->playSwitchButtons();
		do {
			selectedButton++;
			if(selectedButton == buttons->size())
				selectedButton = 0;
		} while((*buttons)[selectedButton]->getType() == BTN_TYP_INFO_BUTTON || (*buttons)[selectedButton]->getType() == BTN_TYP_UNAVAILABLE || !(*buttons)[selectedButton]->isDrawing());
		keyboardActive = true;
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true) || gameCore->mouseManager->isWheelUP())
	{
		gameCore->soundPlayer->playSwitchButtons();
		do {
			selectedButton--;
			if(selectedButton < 0)
				selectedButton = buttons->size() - 1;
		} while((*buttons)[selectedButton]->getType() == BTN_TYP_INFO_BUTTON || (*buttons)[selectedButton]->getType() == BTN_TYP_UNAVAILABLE || !(*buttons)[selectedButton]->isDrawing());
		keyboardActive = true;
	}
	if(gameCore->mouseManager->isMouseEvent() && !gameCore->mouseManager->isMouseEventWheel())
		keyboardActive = false;
}

void ButtonManager::resetState()
{
	keyboardActive = true;
	for(unsigned i = 0; i < buttons->size(); i++)
		(*buttons)[i]->resetState();
}

void ButtonManager::selectButton(int number)
{
	selectedButton = number;
}

void ButtonManager::setDrawing(bool state)
{
	isDrawing = state;
}

void ButtonManager::switchButton(unsigned int id, bool state)
{
	switch(state)
	{
	case true:
		if(id < buttons->size())
			(*buttons)[id]->setDrawing(true);
		for(unsigned int i = id, btnId = id; i < buttons->size(); i++)
			if((*buttons)[i]->isDrawing())
			{
				(*buttons)[i]->setX(coordinatesBtnList[btnId]->x);
				(*buttons)[i]->setY(coordinatesBtnList[btnId]->y);
				btnId++;
			}
		break;
	case false:
		if(id < buttons->size())
			(*buttons)[id]->setDrawing(false);
		for(unsigned int i = id + 1, btnId = id; i < buttons->size(); i++)
			if((*buttons)[i]->isDrawing())
			{
				(*buttons)[i]->setX(coordinatesBtnList[btnId]->x);
				(*buttons)[i]->setY(coordinatesBtnList[btnId]->y);
				btnId++;
			}
		break;
	}
}

void ButtonManager::update()
{
	isUpdating = true;
	if(isDrawing)
	{
		resetState();
		processInput();

		for(unsigned i = 0; i < buttons->size(); i++)
		{
			if(!keyboardActive && (*buttons)[i]->mouseIsOverButton() && selectedButton != i)
			{
				selectedButton = i;
				if( (*buttons)[i]->getType() == BTN_TYP_AVAILABLE ||
					((*buttons)[i]->getType() == BTN_TYP_DEFAULT_BUTTON && (*buttons)[i]->isSideButtons()) &&
					(*buttons)[i]->getType() != BTN_TYP_INFO_BUTTON)
					gameCore->soundPlayer->playSwitchButtons();
			}
			if(selectedButton == i)
				(*buttons)[i]->setState(BTN_ST_SELECTED);
			(*buttons)[i]->update();
			if(i == selectedButton)
				(*buttons)[i]->updateSideButtons();
		}
	}
}

void ButtonManager::updateWithoutInputProcess()
{
	isUpdating = false;
	resetState();
	if(isDrawing)
	{
		for(unsigned i = 0; i < buttons->size(); i++)
			(*buttons)[i]->update();
	}
}