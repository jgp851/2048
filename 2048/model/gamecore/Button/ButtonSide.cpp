#include "ButtonSide.h"

ALLEGRO_COLOR ButtonSide::getMouseOverButtonColor()
{
	return al_map_rgb(130, 130, 130);
}
ALLEGRO_COLOR ButtonSide::getMouseOutsideButtonColor()
{
	return al_map_rgb(80, 80, 80);
}

int ButtonSide::getSize()
{
	return size;
}

void ButtonSide::setSize(int size)
{
	this->size = size;
}