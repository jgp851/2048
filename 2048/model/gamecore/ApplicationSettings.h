#pragma once
#include <fstream>
#include <string>

using namespace std;

enum GameState;
class GameCore;

class ApplicationSettings
{
private:
	GameCore *gameCore;
	string pathConfigFile;

	bool fullScreen;
	int screenWidth;
	int screenHeight;
	GameState restartToGameState;
	
	void read(const string& path);
	void write(const string& path);
public:
	ApplicationSettings(GameCore *gameCore, std::string pathConfigFile);
	~ApplicationSettings();
	
	GameState getStateAfterRestart();
	void setStateToRestart(GameState state);

	bool getFullScreen();
	int getScreenWidth();
	int getScreenHeight();
	
	void setScreenWidth(int value);
	void setScreenHeight(int value);
	void setFullScreen(bool state);
	
	void saveSettings();
};