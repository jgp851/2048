#include "Sprite.h"

Sprite::Sprite(bool isTinted)
{
	this->isTinted = isTinted;
	x = 0;
	y = 0;
	dx = 0;
	dy = 0;
	activeAnimation = 0;
	currentTime = 0;
	spriteDuration = 0;
	transOffset = 0;
	transInProgress = 0;
	transOutProgress = 0;
	transparencyInDuration = 0;
	transparencyOutDuration = 0;
	animations = new std::vector<Animation*>();
	setSpriteType(SPRITE_LOOPED);
}
Sprite::Sprite(Animation *animation, bool isTinted)
{
	this->isTinted = isTinted;
	x = 0;
	y = 0;
	dx = 0;
	dy = 0;
	currentTime = 0;
	transOffset = 0;
	transInProgress = 0;
	transOutProgress = 0;
	transparencyInDuration = 0;
	transparencyOutDuration = 0;
	animations = new std::vector<Animation*>();
	spriteDuration = animation->getDuration();
	addAnimation(animation);
	setActiveAnimation(0);
	setSpriteType(SPRITE_LOOPED);
}
Sprite::~Sprite()
{
	for(std::vector<Animation*>::iterator it = animations->begin(); it != animations->end(); it++)
		delete *it;
	delete animations;
}

float Sprite::getX()
{
	return x;
}
float Sprite::getY()
{
	return y;
}
float Sprite::getDX()
{
	return dx;
}
float Sprite::getDY()
{
	return dy;
}

int Sprite::getWidth()
{
	return (*animations)[activeAnimation]->getWidth();
}
int Sprite::getHeight()
{
	return (*animations)[activeAnimation]->getHeight();
}
int Sprite::getScaledWidth()
{
	if((*animations)[activeAnimation]->getScaledWidth())
		return (*animations)[activeAnimation]->getScaledWidth();
	else
		return (*animations)[activeAnimation]->getWidth();
}
int Sprite::getScaledHeight()
{
	if((*animations)[activeAnimation]->getScaledHeight())
		return (*animations)[activeAnimation]->getScaledHeight();
	else
		return (*animations)[activeAnimation]->getHeight();
}
int Sprite::getTransOffset()
{
	return transOffset;
}

unsigned Sprite::getAnimationDuration(unsigned animation)
{
	return (*animations)[animation]->getDuration();
}
unsigned Sprite::getDuration()
{
	return spriteDuration;
}
unsigned Sprite::getTransInProgress()
{
	return transInProgress;
}
unsigned Sprite::getTransOutProgress()
{
	return transOutProgress;
}

void Sprite::addAnimation(Animation *animation)
{
	unsigned endTime = 0;
	for(unsigned i = 0; i < animations->size(); i++)
		endTime += (*animations)[i]->getDuration();
	animation->setEndTime(endTime + animation->getDuration());

	spriteDuration += animation->getDuration();

	animations->push_back(animation);
}
void Sprite::clearAnimations()
{
	for(std::vector<Animation*>::iterator it = animations->begin(); it != animations->end(); it++)
		delete *it;
	delete animations;
	animations = new std::vector<Animation*>();
}
void Sprite::copySprite(Sprite *source)
{
	this->isTinted = source->isTinted;

	// kopiowanie animacji
		while(source->animations->size() > this->animations->size())
			this->animations->push_back(new Animation());
		std::vector<Animation*> *sourceAnimations = source->animations;
		for(unsigned i = 0; i < source->animations->size(); i++)
			(*animations)[i]->copyAnimation((*sourceAnimations)[i]);
		
	this->x = source->x;
	this->y = source->y;
	this->dx = source->dx;
	this->dy = source->dy;
	this->tintColor = source->tintColor;
	this->activeAnimation = source->activeAnimation;
	this->currentTime = source->currentTime;
	this->spriteDuration = source->spriteDuration;
	this->spriteType = source->spriteType;
	this->transOffset = source->transOffset;
	this->transInProgress = source->transInProgress;
	this->transOutProgress = source->transOutProgress;
	this->transparencyInDuration = source->transparencyInDuration;
	this->transparencyOutDuration = source->transparencyOutDuration;
}
void Sprite::draw()
{
	switch(isTinted)
	{
	case false:
		(*animations)[activeAnimation]->draw(x, y, transOffset);
		break;
	case true:
		(*animations)[activeAnimation]->drawTinted(x, y, tintColor);
		break;
	}
}
void Sprite::randomTintColor()
{
	if(isTinted)
	{
		unsigned char r, g, b;
		r = static_cast<unsigned char>(rand() % 256);
		g = static_cast<unsigned char>(rand() % 256);
		b = static_cast<unsigned char>(rand() % 256);
		tintColorDest.r = r;
		tintColorDest.g = g;
		tintColorDest.b = b;
		tintColorDest.a = 255;
		tintColor = tintColorDest;
	}
}
void Sprite::reset()
{
	currentTime = 0;
	(*animations)[activeAnimation]->reset();
}
void Sprite::setCurrentTime(unsigned currentTime)
{
	this->currentTime = currentTime;
	if(currentTime < transparencyInDuration)
		transInProgress = currentTime;
	if(currentTime >= spriteDuration - transparencyOutDuration)
		transOutProgress = currentTime - (spriteDuration - transparencyOutDuration);
}
void Sprite::setDuration(unsigned duration)
{
	spriteDuration = duration;
}
void Sprite::setSpriteType(SpriteType newType)
{
	spriteType = newType;
}
void Sprite::setTintColor(ALLEGRO_COLOR color)
{
	tintColor = color;
}
void Sprite::setTintColorDest(ALLEGRO_COLOR colorDest)
{
	tintColorDest = colorDest;
}
void Sprite::setX(float value)
{
	x = value;
}
void Sprite::setY(float value)
{
	y = value;
}
void Sprite::setDX(float value)
{
	dx = value;
}
void Sprite::setDY(float value)
{
	dy = value;
}
void Sprite::setActiveAnimation(unsigned key)
{
	if(activeAnimation != key)
	{
		activeAnimation = key;
		(*animations)[key]->reset();
	}
}
void Sprite::setPosition(float valueX, float valueY)
{
	x = valueX;
	y = valueY;
}
void Sprite::setTransOffset(int transOffset)
{
	this->transOffset = transOffset;
}
void Sprite::setTransInProgress(unsigned transInProgress)
{
	this->transInProgress = transInProgress;
}
void Sprite::setTransOutProgress(unsigned transOutProgress)
{
	this->transOutProgress = transOutProgress;
}
void Sprite::setTransparencyInDuration(unsigned duration)
{
	transparencyInDuration = duration;
	setSpriteType(SPRITE_TRANSPARENCY_LOOPED);
}
void Sprite::setTransparencyOutDuration(unsigned duration)
{
	transparencyOutDuration = duration;
	setSpriteType(SPRITE_TRANSPARENCY_LOOPED);
}
void Sprite::update(unsigned elapsedTime)
{
	currentTime += elapsedTime;
	switch(spriteType)
	{
	case SPRITE_TRANSPARENCY_LOOPED:
		if(currentTime < transparencyInDuration)
		{
			transInProgress += elapsedTime;
			transOffset = 255 * (transInProgress / static_cast<float>(transparencyInDuration));
			if(transOffset > 255)
				transOffset = 255;
			if(isTinted)
			{
				tintColor.r = tintColorDest.r * (transInProgress / static_cast<float>(transparencyInDuration));
				if(tintColor.r > 255)
					tintColor.r = 255;
				tintColor.g = tintColorDest.g * (transInProgress / static_cast<float>(transparencyInDuration));
				if(tintColor.g > 255)
					tintColor.g = 255;
				tintColor.b = tintColorDest.b * (transInProgress / static_cast<float>(transparencyInDuration));
				if(tintColor.b > 255)
					tintColor.b = 255;
				tintColor.a = transOffset;
			}
		}
		if(currentTime >= spriteDuration - transparencyOutDuration)
		{
			transOutProgress += elapsedTime;
			transOffset = 255 * (transOutProgress / static_cast<float>(transparencyOutDuration));
			transOffset = 255 - transOffset;
			if(transOffset < 0)
				transOffset = 0;
			if(isTinted)
			{
				tintColor.r = tintColorDest.r * (transOutProgress / static_cast<float>(transparencyOutDuration));
				tintColor.r = tintColorDest.r - tintColor.r;
				if(tintColor.r < 0)
					tintColor.r = 0;
				tintColor.g = tintColorDest.g * (transOutProgress / static_cast<float>(transparencyOutDuration));
				tintColor.g = tintColorDest.g - tintColor.g;
				if(tintColor.g < 0)
					tintColor.g = 0;
				tintColor.b = tintColorDest.b * (transOutProgress / static_cast<float>(transparencyOutDuration));
				tintColor.b = tintColorDest.b - tintColor.b;
				if(tintColor.b < 0)
					tintColor.b = 0;
				tintColor.a = transOffset;
			}
		}
		if(currentTime > spriteDuration)
		{
			transInProgress = 0;
			transOutProgress = 0;
		}
	case SPRITE_LOOPED:
		if(currentTime > spriteDuration)
		{
			currentTime = currentTime % spriteDuration;
			activeAnimation = 0;
			(*animations)[activeAnimation]->reset();
			(*animations)[activeAnimation]->setCurrentTime(currentTime - elapsedTime);
		}

		if(animations->size() > 1)
		{
			unsigned lastActiveAnimation = activeAnimation;
			while((*animations)[activeAnimation]->getEndTime() < currentTime)
				activeAnimation++;
			if(lastActiveAnimation != activeAnimation)
			{
				(*animations)[activeAnimation]->reset();
				(*animations)[activeAnimation]->setCurrentTime(currentTime - (*animations)[activeAnimation - 1]->getEndTime() - elapsedTime);
			}
		}
		break;
	}

	x += dx * elapsedTime;
	y += dy * elapsedTime;

	(*animations)[activeAnimation]->update(elapsedTime);
}
ALLEGRO_COLOR Sprite::getTintColor()
{
	return tintColor;
}
ALLEGRO_COLOR Sprite::getTintColorDest()
{
	return tintColorDest;
}
Sprite* Sprite::clone()
{
	Sprite *clone = new Sprite();
	clone->animations = this->animations;
	clone->isTinted = this->isTinted;
	clone->x = this->x;
	clone->y = this->y;
	clone->dx = this->dx;
	clone->dy = this->dy;
	clone->tintColor = this->tintColor;
	clone->activeAnimation = this->activeAnimation;
	clone->currentTime = this->currentTime;
	clone->spriteDuration = this->spriteDuration;
	clone->spriteType = this->spriteType;
	clone->transOffset = this->transOffset;
	clone->transInProgress = this->transInProgress;
	clone->transOutProgress = this->transOutProgress;
	clone->transparencyInDuration = this->transparencyInDuration;
	clone->transparencyOutDuration = this->transparencyOutDuration;
	return clone;
}