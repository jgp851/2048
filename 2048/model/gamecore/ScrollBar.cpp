#include "ScrollBar.h"
#include "..\sources2048\Game2048Core.h"

ScrollBar::ScrollBar(Game2048Core *gameCore, int x, int y, int height)
{
	this->gameCore = gameCore;
	
	this->bar = gameCore->mediaManager->PNG_scrollBar_bar;
	this->barBottom = gameCore->mediaManager->PNG_scrollBar_barBottom;
	this->barMiddle = gameCore->mediaManager->PNG_scrollBar_barMiddle;
	this->barTop = gameCore->mediaManager->PNG_scrollBar_barTop;
	this->buttonArrowheadDown = gameCore->mediaManager->PNG_scrollBar_buttonArrowheadDown;
	this->buttonArrowheadUp = gameCore->mediaManager->PNG_scrollBar_buttonArrowheadUp;
	this->buttonDown = gameCore->mediaManager->PNG_scrollBar_buttonDown;
	this->buttonUp = gameCore->mediaManager->PNG_scrollBar_buttonUp;
	this->position = NULL;
	this->slideDown = false;

	barColorStd = al_map_rgb(250, 250, 250);
	barColorMouseOver = al_map_rgb(220, 220, 220);
	barColorPressed = al_map_rgb(190, 190, 190);
	barColor = barColorStd;
	
	buttonsColorStd = al_map_rgb(250, 250, 250);
	buttonsColorMouseOver = al_map_rgb(220, 220, 220);
	buttonsColorPressed = al_map_rgb(190, 190, 190);
	buttonDownColor = buttonsColorStd;
	buttonUpColor = buttonsColorStd;

	this->x = x;
	this->y = y;
	this->width = 16;
	this->height = height;
	
	this->barAreaY = this->y + 16;
	this->barAreaHeight = this->height - 32;
	this->mouseMovement = 0;
	this->previousPosition = 0;
}

bool ScrollBar::mouseIsOverScrollBar()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + width &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + height
		)
		return true;
	else
		return false;
}

bool ScrollBar::mouseIsOverBar()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + width &&
		gameCore->mouseManager->getY() >= barY + mouseMovement &&
		gameCore->mouseManager->getY() < barY + mouseMovement + barH
		)
		return true;
	else
		return false;
}

bool ScrollBar::mouseIsOverButtonDown()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + al_get_bitmap_width(buttonDown) &&
		gameCore->mouseManager->getY() >= y + height - al_get_bitmap_height(buttonDown) &&
		gameCore->mouseManager->getY() < y + height
		)
		return true;
	else
		return false;
}

bool ScrollBar::mouseIsOverButtonUp()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + al_get_bitmap_width(buttonUp) &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + al_get_bitmap_height(buttonUp)
		)
		return true;
	else
		return false;
}

bool ScrollBar::isSlideBar()
{
	return slideDown;
}

bool ScrollBar::mouseIsOverEmptyBarArea()
{
	if(mouseIsOverScrollBar() && !mouseIsOverButtonDown() && !mouseIsOverButtonUp() && !mouseIsOverBar())
		return true;
	return false;
}

int ScrollBar::getActualPosition()
{
	return *position;
}

int ScrollBar::getPreviousPosition()
{
	return previousPosition;
}

unsigned ScrollBar::getTotalElements()
{
	return totalElements;
}

void ScrollBar::allureToPosition()
{
	*position = (barY - barAreaY + mouseMovement) / barH;
}

void ScrollBar::allureSoftToPosition()
{
	allureToPosition();
	if((static_cast<int>(barY) - barAreaY + static_cast<int>(mouseMovement)) % static_cast<int>(barH) > barH / 2)
		(*position)++;
	if(mouseMovement > previousMouseMovement)
		previousPosition = *position - 1;
	else if(mouseMovement != previousMouseMovement)
		previousPosition = *position + 1;
}

void ScrollBar::coloringElements()
{
	barColor = barColorStd;
	buttonDownColor = buttonsColorStd;
	buttonUpColor = buttonsColorStd;
	if(mouseIsOverBar())
		barColor = barColorMouseOver;
	if(slideDown)
		barColor = barColorPressed;
	else if(mouseIsOverButtonDown())
	{
		buttonDownColor = buttonsColorMouseOver;
		if(gameCore->mouseManager->isLeftPressed())
			buttonDownColor = buttonsColorPressed;
	}
	else if(mouseIsOverButtonUp())
	{
		buttonUpColor = buttonsColorMouseOver;
		if(gameCore->mouseManager->isLeftPressed())
			buttonUpColor = buttonsColorPressed;
	}
}

void ScrollBar::draw()
{
	drawBackground();
	drawButtons();
	drawBar();
}

void ScrollBar::drawBackground()
{
	al_draw_filled_rectangle(x, y, x + width, y + height, al_map_rgb(185, 185, 185));
}

void ScrollBar::drawBar()
{
	al_draw_tinted_bitmap(barTop, barColor, x, barY + mouseMovement, 0);
	al_draw_tinted_bitmap(barBottom, barColor, x, barY + barH - 3 + mouseMovement, 0);
	al_draw_tinted_scaled_bitmap(bar, barColor,
		0, 0,
		al_get_bitmap_width(bar),
		al_get_bitmap_height(bar),
		x,
		barY + 3 + mouseMovement,
		width,
		barH - 6,
		0);
	if(barH > 30)
		al_draw_bitmap(barMiddle, x, barY + barH / 2 - 10 + mouseMovement, 0);
}

void ScrollBar::drawButtons()
{
	if(mouseIsOverScrollBar() || slideDown)
	{
		al_draw_tinted_bitmap(buttonUp, buttonUpColor, x, y, 0);
		al_draw_tinted_bitmap(buttonDown, buttonDownColor, x, y + height - al_get_bitmap_height(buttonDown), 0);
	}
	al_draw_bitmap(buttonArrowheadUp, x, y, 0);
	al_draw_bitmap(buttonArrowheadDown, x, y + height - al_get_bitmap_height(buttonArrowheadDown), 0);
}

void ScrollBar::moveCyclicallyBar()
{
	if(!slideDown)
	{
		if(*position > 0 && (gameCore->mouseManager->isWheelUP() || (mouseIsOverButtonUp() && gameCore->mouseManager->isLeftReleased())))
		{
			previousPosition = *position;
			(*position)--;
		}
		if(*position < static_cast<int>(totalElements) - 1 && (gameCore->mouseManager->isWheelDOWN() || (mouseIsOverButtonDown() && gameCore->mouseManager->isLeftReleased())))
		{
			previousPosition = *position;
			(*position)++;
		}
		if(barAreaHeight / totalElements > 6)
			barH = barAreaHeight / totalElements;
		else
			barH = 6;
		barY = barAreaY + barAreaHeight / totalElements * *position;
	}
}

void ScrollBar::setPositionVariable(int *newPosition)
{
	this->position = newPosition;
	this->previousPosition = *newPosition;
}

void ScrollBar::setTotalElements(unsigned totalElements)
{
	this->totalElements = totalElements;
}

void ScrollBar::slideBar()
{
	if(!slideDown && mouseIsOverBar() && gameCore->mouseManager->isLeftPressed())
	{
		slideDown = true;
		mouseY = gameCore->mouseManager->getY();
	}
	else if(!slideDown && mouseIsOverEmptyBarArea() && gameCore->mouseManager->isLeftPressed())
	{
		previousMouseMovement = mouseMovement;
		mouseMovement = gameCore->mouseManager->getY() - barY;
		allureToPosition();
		mouseMovement = 0;
	}
	else if(slideDown && gameCore->mouseManager->isLeftPressed())
	{
		previousMouseMovement = mouseMovement;
		mouseMovement = gameCore->mouseManager->getY() - mouseY;
		while(barY + mouseMovement < barAreaY)
			mouseMovement++;
		while(barY + mouseMovement > barAreaY + barAreaHeight - barH)
			mouseMovement--;
		allureSoftToPosition();
	}
	if(slideDown && !gameCore->mouseManager->isLeftPressed())
	{
		slideDown = false;
		mouseMovement = 0;
		previousMouseMovement = 0;
	}
}

void ScrollBar::update()
{
	coloringElements();
	slideBar();
	moveCyclicallyBar();
}