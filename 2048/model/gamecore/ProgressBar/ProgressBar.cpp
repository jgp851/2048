#include "ProgressBar.h"
#include "Light.h"
#include "..\Button\Button.h"
#include "..\KeyboardButton\KeyboardButton_extendingObjects\KeyboardButton_MINUS.h"
#include "..\KeyboardButton\KeyboardButton_extendingObjects\KeyboardButton_PLUS.h"
#include "..\..\sources2048\Game2048Core.h"

ProgressBar::ProgressBar(Game2048Core *gameCore, int x, int y, int barWidth, float minValue, float maxValue, float *value)
{
	this->gameCore = gameCore;
	this->x = x;
	this->y = y;
	this->height = gameCore->appSettings->getScreenHeight() * 0.036;
	this->buttonSkip = 0.1f;
	this->barWidth = barWidth;
	this->maxValue = maxValue;
	this->minValue = minValue;
	this->value = value;
	this->scale = maxValue - minValue;
	this->sample = NULL;
	this->slide = false;
	this->changedValue = false;
	this->drawBarHighlight = false;
	this->drawInterfaceAlways = false;
	this->mouseMovement = 0;
	this->lightAddon = false;
	this->light = NULL;
	kbMinus = new KeyboardButton_MINUS(gameCore);
	kbPlus = new KeyboardButton_PLUS(gameCore);
	nameBar = NULL;
	
	buttonSize = gameCore->appSettings->getScreenWidth() * 0.02;
	kbMinus->setSize(buttonSize);
	kbPlus->setSize(buttonSize);
	spaceBetween = gameCore->appSettings->getScreenWidth() * 0.015;
	setCoordinates();
}

ProgressBar::ProgressBar(Game2048Core *gameCore, int x, int y, int barWidth, float minValue, float maxValue, float *value, std::string nameStr)
{
	this->gameCore = gameCore;
	this->x = x;
	this->y = y;
	this->height = gameCore->appSettings->getScreenHeight() * 0.036;
	this->buttonSkip = 0.1f;
	this->barWidth = barWidth;
	this->maxValue = maxValue;
	this->minValue = minValue;
	this->value = value;
	this->scale = maxValue - minValue;
	this->sample = NULL;
	this->slide = false;
	this->changedValue = false;
	this->drawBarHighlight = false;
	this->drawInterfaceAlways = false;
	this->mouseMovement = 0;
	this->lightAddon = false;
	this->light = NULL;
	kbMinus = new KeyboardButton_MINUS(gameCore);
	kbPlus = new KeyboardButton_PLUS(gameCore);
	nameBar = new Button(gameCore, 0, 0, false, nameStr, BTN_TYP_MINI_NOTE);
	nameBar->setHeight(height);
	
	buttonSize = gameCore->appSettings->getScreenWidth() * 0.02;
	kbMinus->setSize(buttonSize);
	kbPlus->setSize(buttonSize);
	spaceBetween = gameCore->appSettings->getScreenWidth() * 0.015;
	setCoordinates();
}

ProgressBar::~ProgressBar()
{
	if(nameBar)
		delete nameBar;
	if(light)
		delete light;
}

bool ProgressBar::isActive()
{
	if(mouseIsOverBarArea() || slide)
		return true;
	return false;
}

bool ProgressBar::valueIsChanging()
{
	return slide;
}

bool ProgressBar::valueWasChanged()
{
	return changedValue;
}

bool ProgressBar::mouseIsOverBar()
{
	if(gameCore->mouseManager->getX() >= barX &&
		gameCore->mouseManager->getX() < barX + barWidth &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + height)
		return true;
	return false;
}

bool ProgressBar::mouseIsOverBarArea()
{
	if(gameCore->mouseManager->getX() >= kbMinus->getX() &&
		gameCore->mouseManager->getX() < kbPlus->getX() + kbPlus->getButtonSize() &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + height)
		return true;
	return false;
}

int ProgressBar::getHeight()
{
	return height;
}

void ProgressBar::adjustProgressToFirstClick()
{
	progressLength = mouseX - barX;
}

void ProgressBar::detectProgressBeyondScope()
{
	if(progressLength < 0)
		progressLength = 0;
	if(progressLength > barWidth)
		progressLength = barWidth;
}

void ProgressBar::draw()
{
	if(nameBar)
		nameBar->draw();
	if(lightAddon)
		light->draw();
	if(isActive())
	{
		kbMinus->draw();
		kbPlus->draw();
	}
	if(drawInterfaceAlways || drawBarHighlight)
		al_draw_filled_rectangle(barX - 5, barY, barX + barWidth + 5, barY + 10, al_map_rgb(127, 127, 127));
	al_draw_filled_rectangle(barX, barY, barX + (progressLength + mouseMovement), barY + 5, al_map_rgb(255, 0, 0));
}

void ProgressBar::moveBar()
{
	if(mouseIsOverBar())
	{
		if(gameCore->mouseManager->isWheelDOWN())
			skipForward();
		if(gameCore->mouseManager->isWheelUP())
			skipBackward();
	}
	if(kbMinus->isPressed())
		skipBackward();
	if(kbPlus->isPressed())
		skipForward();
}

void ProgressBar::processInput()
{
	changedValue = false;
	kbMinus->update();
	kbPlus->update();
	slideBar();
	moveBar();
	detectProgressBeyondScope();
	*value = (progressLength + mouseMovement) * scale / barWidth;
	if(changedValue && sample)
	{
		if(lightAddon)
		{
			if(*value == minValue)
				light->setValueState(false);
			else
				light->setValueState(true);
		}
		gameCore->soundPlayer->playSampleForced(sample, *value);
	}
}

void ProgressBar::reset()
{
	progressLength = barWidth * (*value / scale);
	if(lightAddon)
		light->reset();
}

void ProgressBar::setButtonSkip(float percent)
{
	buttonSkip = percent / 100;
}

void ProgressBar::setCoordinates()
{
	int lightWidth = 0;
	totalWidth = barWidth;
	if(nameBar)
		totalWidth += nameBar->getWidth();
	totalWidth += buttonSize * 2 + spaceBetween * 3;
	if(lightAddon)
		totalWidth += buttonSize + spaceBetween;
	primX = x - totalWidth / 2;

	if(nameBar)
	{
		nameBar->setX(primX + nameBar->getWidth() / 2);
		nameBar->setY(y);
	}
	if(lightAddon)
	{
		light->setCoordinates(nameBar->getX() + nameBar->getWidth() / 2 + spaceBetween, y + (height - buttonSize) / 2 + 2);
		light->setSize(buttonSize);
		lightWidth = buttonSize + spaceBetween;
	}
	if(nameBar)
		kbMinus->setX(nameBar->getX() + nameBar->getWidth() / 2 + lightWidth + spaceBetween);
	else
		kbMinus->setX(primX);
	kbMinus->setY(y + (height - buttonSize) / 2 + 2);
	barX = kbMinus->getX() + buttonSize + spaceBetween;
	barY = y + (height - 5) / 2;
	kbPlus->setX(barX + barWidth + spaceBetween);
	kbPlus->setY(y + (height - buttonSize) / 2 + 2);
}

void ProgressBar::setCoordinates(int x, int y)
{
	this->x = x;
	this->y = y;
	setCoordinates();
}

void ProgressBar::setDrawingHighlightAlways(bool state)
{
	drawInterfaceAlways = state;
}

void ProgressBar::setLightAddon(bool state, bool *value)
{
	lightAddon = state;
	if(lightAddon)
	{
		light = new Light(gameCore);
		light->setValueAddress(value);
		light->reset();
	}
	if(!lightAddon && light)
	{
		delete light;
		light = NULL;
	}
	setCoordinates();
}

void ProgressBar::setSampleAddress(ALLEGRO_SAMPLE *sample)
{
	this->sample = sample;
}

void ProgressBar::setValue(float newValue)
{
	*value = newValue;
	progressLength = barWidth * (*value / scale);
}

void ProgressBar::setY(int y)
{
	this->y = y;
	setCoordinates();
}

void ProgressBar::skipBackward()
{
	progressLength -= barWidth * buttonSkip;
	changedValue = true;
}

void ProgressBar::skipForward()
{
	progressLength += barWidth * buttonSkip;
	changedValue = true;
}

void ProgressBar::slideBar()
{
	if(!slide && mouseIsOverBar() && gameCore->mouseManager->isLeftPressed())
	{
		slide = true;
		mouseX = gameCore->mouseManager->getX();
		adjustProgressToFirstClick();
	}
	if(slide && gameCore->mouseManager->isLeftPressed())
	{
		mouseMovement = gameCore->mouseManager->getX() - mouseX;
		while(progressLength + mouseMovement < 0)
			mouseMovement++;
		while(progressLength + mouseMovement > barWidth)
			mouseMovement--;
	}
	if(slide && !gameCore->mouseManager->isLeftPressed())
	{
		slide = false;
		changedValue = true;
		progressLength += mouseMovement;
		mouseMovement = 0;
	}
}

void ProgressBar::update()
{
	if(lightAddon)
	{
		light->update();
		if(!slide && *value == minValue && light->getState())
		{
			setValue(scale * 0.7);
			gameCore->soundPlayer->playSampleForced(sample, *value);
		}
	}

	if(isActive() || progressLength < barWidth * 0.05)
		drawBarHighlight = true;
	else
		drawBarHighlight = false;
}