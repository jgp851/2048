#pragma once
#include <allegro5\allegro.h>

class Game2048Core;

class Light
{
private:
	Game2048Core *gameCore;

	ALLEGRO_BITMAP *light;
	ALLEGRO_BITMAP *lightOff;
	ALLEGRO_BITMAP *lightOn;
	
	bool lightTurned;
	bool *value;

	int x;
	int y;
	int size;

	bool mouseIsOverLight();
	void setBitmap();
	void setLight();
	void setValue();
public:
	Light(Game2048Core *gameCore);
	~Light();

	bool getState();

	void draw();
	void reset();
	void setCoordinates(int x, int y);
	void setSize(int size);
	void setValueAddress(bool *value);
	void setValueState(bool state);
	void update();
};