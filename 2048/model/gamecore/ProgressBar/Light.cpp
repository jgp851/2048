#include "Light.h"
#include "..\..\sources2048\Game2048Core.h"

Light::Light(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->lightTurned = false;
	this->size = gameCore->appSettings->getScreenWidth() * 0.02;
	this->value = NULL;
	light = NULL;
	lightOff = gameCore->mediaManager->PNG_lightOff;
	lightOn = gameCore->mediaManager->PNG_lightOn;
}

Light::~Light()
{
}

bool Light::getState()
{
	return lightTurned;
}

bool Light::mouseIsOverLight()
{
	if(gameCore->mouseManager->getX() >= x &&
		gameCore->mouseManager->getX() < x + size &&
		gameCore->mouseManager->getY() >= y &&
		gameCore->mouseManager->getY() < y + size)
		return true;
	return false;
}

void Light::draw()
{
	al_draw_scaled_bitmap(light, 0, 0, al_get_bitmap_width(lightOn), al_get_bitmap_height(lightOn),
		x, y,
		size, size, 0);
}

void Light::reset()
{
	setLight();
	setBitmap();
}

void Light::setCoordinates(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Light::setBitmap()
{
	if(lightTurned)
		light = lightOn;
	else
		light = lightOff;
}

void Light::setLight()
{
	if(*value)
		lightTurned = true;
	else
		lightTurned = false;
}

void Light::setSize(int size)
{
	this->size = size;
}

void Light::setValue()
{
	if(lightTurned)
		*value = true;
	else
		*value = false;
}

void Light::setValueAddress(bool *value)
{
	this->value = value;
}

void Light::setValueState(bool state)
{
	*value = state;
	setLight();
	setBitmap();
}

void Light::update()
{
	if(mouseIsOverLight() && gameCore->mouseManager->isLeftReleased())
	{
		if(!lightTurned)
			gameCore->soundPlayer->playSwitchMenu(true);
		else
			gameCore->soundPlayer->playSwitchMenu(false);
		lightTurned = !lightTurned;
		setBitmap();
		setValue();
	}
}