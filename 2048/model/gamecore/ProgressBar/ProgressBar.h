#pragma once
#include <string>
#include <allegro5/allegro_audio.h>

class Button;
class Game2048Core;
class KeyboardButton;
class Light;

class ProgressBar
{
private:
	Game2048Core *gameCore;
	Button *nameBar;
	Light *light;
	KeyboardButton *kbMinus;
	KeyboardButton *kbPlus;
	ALLEGRO_SAMPLE *sample;

	bool lightAddon;
	bool changedValue;
	bool drawBarHighlight;
	bool drawInterfaceAlways;
	bool slide;
	int x;
	int y;
	int height;
	int primX;
	int totalWidth;
	int barWidth;
	int buttonSize;
	int spaceBetween;
	float buttonSkip;
	float maxValue;
	float minValue;
	float mouseX;
	float mouseMovement;
	float scale;
	float *value;
	float progressLength;

	// wsp�rz�dne poszczeg�lnych element�w
	int barX;
	int barY;
	
	bool mouseIsOverBar();
	bool mouseIsOverBarArea();
	void adjustProgressToFirstClick();
	void detectProgressBeyondScope();
	void skipBackward();
	void skipForward();
	void setCoordinates();
	void moveBar();
	void slideBar();
public:
	ProgressBar(Game2048Core *gameCore, int x, int y, int width, float minValue, float maxValue, float *value);
	ProgressBar(Game2048Core *gameCore, int x, int y, int width, float minValue, float maxValue, float *value, std::string nameStr);
	~ProgressBar();
	
	bool isActive();
	bool valueIsChanging();
	bool valueWasChanged();
	int getHeight();
	void draw();
	void processInput();
	void reset();
	void setButtonSkip(float percent);
	void setCoordinates(int x, int y);
	void setDrawingHighlightAlways(bool state);
	void setLightAddon(bool state, bool *value);
	void setSampleAddress(ALLEGRO_SAMPLE *sample);
	void setValue(float newValue);
	void setY(int y);
	void update();
};