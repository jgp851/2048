#include "Animation.h"

AnimationFrame::AnimationFrame(ALLEGRO_BITMAP *image, unsigned endTime)
{
	this->image = image;
	this->endTime = endTime;
}

Animation::Animation()
{
	animationDuration = 0;
	currentFrame = 0;
	currentTime = 0;
	ratio = 0;
	dw = 0;
	dh = 0;
	maxSize = 0;
	frames = new std::vector<AnimationFrame*>();
}
Animation::~Animation()
{
	for(std::vector<AnimationFrame*>::iterator it = frames->begin(); it != frames->end(); it++)
		delete *it;
	delete frames;
}

int Animation::getWidth()
{
	return al_get_bitmap_width((*frames)[currentFrame]->image);
}
int Animation::getHeight()
{
	return al_get_bitmap_height((*frames)[currentFrame]->image);
}
int Animation::getScaledWidth()
{
	return dw;
}
int Animation::getScaledHeight()
{
	return dh;
}
unsigned Animation::getDuration()
{
	return animationDuration;
}
unsigned Animation::getEndTime()
{
	return endTime;
}

void Animation::addFrame(ALLEGRO_BITMAP *bitmap, unsigned duration)
{
	animationDuration += duration;
	AnimationFrame *frame = new AnimationFrame(bitmap, animationDuration);
	frames->push_back(frame);
}
void Animation::copyAnimation(Animation *source)
{
	this->frames = source->frames;
	this->currentFrame = source->currentFrame;
	this->animationDuration = source->animationDuration;
	this->currentTime = source->currentTime;
	this->endTime = source->endTime;
	this->ratio = source->ratio;
	this->dw = source->dw;
	this->dh = source->dh;
	this->maxSize = source->maxSize;
}
void Animation::draw(int x, int y, int transOffset)
{
	if(ratio)
		al_draw_tinted_scaled_bitmap((*frames)[currentFrame]->image,
			al_map_rgba(transOffset, transOffset, transOffset, transOffset),
			0, 0,
			al_get_bitmap_width((*frames)[currentFrame]->image),
			al_get_bitmap_height((*frames)[currentFrame]->image),
			x, y,
			dw,
			dh,
			0);
	else
		al_draw_tinted_bitmap((*frames)[currentFrame]->image, al_map_rgba(transOffset, transOffset, transOffset, transOffset), x, y, 0);
}
void Animation::drawTinted(int x, int y, ALLEGRO_COLOR color)
{
	if(ratio)
		al_draw_tinted_scaled_bitmap((*frames)[currentFrame]->image, al_map_rgba(color.r, color.g, color.b, color.a),
			0, 0,
			al_get_bitmap_width((*frames)[currentFrame]->image),
			al_get_bitmap_height((*frames)[currentFrame]->image),
			x, y,
			dw,
			dh,
			0);
	else
		al_draw_tinted_bitmap((*frames)[currentFrame]->image, al_map_rgba(color.r, color.g, color.b, color.a), x, y, 0);
}
void Animation::reset()
{
	currentFrame = 0;
	currentTime = 0;
}
void Animation::setCurrentTime(unsigned currentTime)
{
	this->currentTime = currentTime;
}
void Animation::setEndTime(unsigned endTime)
{
	this->endTime = endTime;
}
void Animation::setRatio(float maxSize)
{
	if(al_get_bitmap_width((*frames)[0]->image) > maxSize || al_get_bitmap_height((*frames)[0]->image) > maxSize)
		ratio = maxSize / al_get_bitmap_width((*frames)[0]->image);
	dw = al_get_bitmap_width((*frames)[0]->image) * ratio;
	dh = al_get_bitmap_height((*frames)[0]->image) * ratio;
}
void Animation::update(unsigned elapsedTime)
{
	currentTime += elapsedTime;
	if(currentTime > animationDuration)
	{
		currentTime = currentTime % animationDuration;
		currentFrame = 0;
	}
	if(frames->size() > 1)
		while((*frames)[currentFrame]->endTime < currentTime)
			currentFrame++;
}
Animation* Animation::clone()
{
	Animation *clone = new Animation();

	clone->frames = this->frames;
	clone->animationDuration = this->animationDuration;
	clone->endTime = this->endTime;
	clone->ratio = this->ratio;
	clone->dw = this->dw;
	clone->dh = this->dh;
	clone->maxSize = this->maxSize;
	return clone;
}
Animation* Animation::cloneReserved()
{
	Animation *clone = new Animation();
	unsigned counter = 0;
	for(std::vector<AnimationFrame*>::iterator itF = frames->end() - 1, itT = frames->begin(); true; itF--, itT++)
	{
		clone->addFrame((*itF)->image, (*itT)->endTime - counter);
		counter = (*itT)->endTime;
		if(itF == frames->begin())
			break;
	}
	clone->animationDuration = this->animationDuration;
	clone->endTime = this->endTime;
	clone->ratio = this->ratio;
	clone->dw = this->dw;
	clone->dh = this->dh;
	clone->maxSize = this->maxSize;
	return clone;
}