#pragma once
#include <vector>
#include "Animation.h"
#include <allegro5\allegro.h>

enum SpriteType
{
	SPRITE_LOOPED,
	SPRITE_MANUAL,
	SPRITE_TRANSPARENCY_LOOPED
};

class Sprite
{
private:
	std::vector<Animation*> *animations;
	SpriteType spriteType;

	ALLEGRO_COLOR tintColor;
	ALLEGRO_COLOR tintColorDest;

	bool isTinted;
	float x;
	float y;
	float dx;
	float dy;
	unsigned activeAnimation;
	unsigned currentTime;
	unsigned spriteDuration;
	int transOffset;
	unsigned transInProgress;
	unsigned transOutProgress;
	unsigned transparencyInDuration;
	unsigned transparencyOutDuration;
public:
	Sprite(bool isTinted = false);
	Sprite(Animation *animation, bool isTinted = false);
	~Sprite();

	float getX();
	float getY();
	float getDX();
	float getDY();

	int getWidth();
	int getHeight();
	int getScaledWidth();
	int getScaledHeight();
	int getTransOffset();

	unsigned getAnimationDuration(unsigned animation);
	unsigned getDuration();
	unsigned getTransInProgress();
	unsigned getTransOutProgress();

	void addAnimation(Animation *animation);
	void clearAnimations();
	void copySprite(Sprite *source);
	void draw();
	void drawTint();
	void randomTintColor();
	void reset();
	void setCurrentTime(unsigned currentTime);
	void setDuration(unsigned duration);
	void setSpriteType(SpriteType newType);
	void setTintColor(ALLEGRO_COLOR color);
	void setTintColorDest(ALLEGRO_COLOR colorDest);
	void setX(float value);
	void setY(float value);
	void setDX(float value);
	void setDY(float value);
	void setActiveAnimation(unsigned key);
	void setPosition(float valueX, float valueY);
	void setTransOffset(int transOffset);
	void setTransInProgress(unsigned transInProgress);
	void setTransOutProgress(unsigned transOutProgress);
	void setTransparencyInDuration(unsigned duration);
	void setTransparencyOutDuration(unsigned duration);
	void update(unsigned elapsedTime);
	ALLEGRO_COLOR getTintColor();
	ALLEGRO_COLOR getTintColorDest();
	Sprite* clone();
};