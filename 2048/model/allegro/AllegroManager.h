#pragma once
#include <string>
#include <utility>
#include <vector>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>

using namespace std;

class GameCore;
class MonitorTester;

class AllegroManager
{
private:
	bool audioAddonFlag;
	bool audioStreamFlag;
	bool displayEventSourceFlag;
	bool fontAddonFlag;
	bool imageAddonFlag;
	bool keyboardSupportFlag;
	bool mouseSupportFlag;
	bool primitivesAddonFlag;

	int audioSamplesCount;
	unsigned int frequency;
	
	ALLEGRO_AUDIO_DEPTH mixerDepth;
	ALLEGRO_AUDIO_DEPTH voiceDepth;
	ALLEGRO_CHANNEL_CONF channelConf;
	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT *eventPnt;
	ALLEGRO_EVENT_QUEUE *eventQueue;
	ALLEGRO_MIXER *mixer;
	ALLEGRO_TIMER *timer;
	ALLEGRO_VOICE *voice;
	
	GameCore *gameCore;

	ALLEGRO_BITMAP* resizeBitmap(ALLEGRO_BITMAP *source, int destW, int destH, bool resizeIfSourceIsTooSmall);

	void createDisplay();
	void createEventQueue();
	void createTimer(int tps);
	void registerDisplayEventSource();
	void registerKeyboardEventSource();
	void registerMouseEventSource();
	void registerTimerEventSource();
public:
	AllegroManager(GameCore *gameCore);
	~AllegroManager();
	
	MonitorTester *monitorTester;

	ALLEGRO_BITMAP* getResizedBitmap(ALLEGRO_BITMAP *source, int destW, int destH, bool resizeIfSourceIsTooSmall = false);
	ALLEGRO_BITMAP* getResizedBitmap(const char *filename, int destW, int destH, bool resizeIfSourceIsTooSmall = false);
	ALLEGRO_DISPLAY* getDisplay();
	ALLEGRO_EVENT_QUEUE* getAllegroEventQueue();
	ALLEGRO_MIXER* getAllegroMixer();
	ALLEGRO_VOICE* getAllegroVoice();
	
	bool isEventAudioStreamFinished();
	bool isQueueEmpty();

	void clearDisplay(int r, int g, int b);
	void enableAudioAddon(bool state, int reservedSamplesCount);
	void enableAudioStream(bool state, unsigned int freq, ALLEGRO_AUDIO_DEPTH mixerDepth, ALLEGRO_AUDIO_DEPTH voiceDepth, ALLEGRO_CHANNEL_CONF channelConf);
	void enableDisplayEventSource(bool state);
	void enableFontAddon(bool state);
	void enableImageAddon(bool state);
	void enableKeyboard(bool state);
	void enableMouse(bool state);
	void enablePrimitivesAddon(bool state);
	void disablePrimitivesAddon(bool state);
	void flipBuffers();
	void init(int tps);
	void startTimer();
	void waitForEvent(ALLEGRO_EVENT *event);
};