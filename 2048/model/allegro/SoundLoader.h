#pragma once
#include <vector>
#include <allegro5\allegro_audio.h>

class SoundLoader
{
private:
	std::vector<ALLEGRO_SAMPLE*> *samples;
public:
	SoundLoader();
	~SoundLoader();
	ALLEGRO_SAMPLE* loadSound(char *filename);
};