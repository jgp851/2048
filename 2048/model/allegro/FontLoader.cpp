#include "FontLoader.h"
#include <allegro5/allegro_ttf.h>

FontLoader::FontLoader()
{
	fonts = new std::vector<ALLEGRO_FONT*>();
}
FontLoader::~FontLoader()
{
	for(std::vector<ALLEGRO_FONT*>::iterator it = fonts->begin(); it != fonts->end(); it++)
		al_destroy_font(*it);
	delete(fonts);
}

ALLEGRO_FONT* FontLoader::loadFont(char *filename, int size, int flags)
{
	ALLEGRO_FONT *font = al_load_ttf_font(filename, size, flags);
	if(!font)
	{
		std::string message = "Failed to load font: ";
		message += filename;
		throw message.c_str();
	}
	fonts->push_back(font);
	return font;
}