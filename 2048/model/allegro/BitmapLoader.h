#pragma once
#include <vector>
#include "allegro5/allegro.h"

class BitmapLoader
{
private:
	std::vector<ALLEGRO_BITMAP*> *bitmaps;
public:
	BitmapLoader();
	~BitmapLoader();

	ALLEGRO_BITMAP *loadBitmap(char *filename);
	ALLEGRO_BITMAP *loadBitmap_f(char *buffer, int fileLength, std::string fileType);
};