#pragma once
#include <vector>

class GameCore;

struct Resolution
{
	bool supported;
	int width;
	int height;
	Resolution(bool supported, int width, int height)
	{
		this->supported = supported;
		this->width = width;
		this->height = height;
	}
};

class MonitorTester
{
private:
	GameCore *gameCore;
	std::vector<Resolution*> resolutions;

	int supportedW;
	int supportedH;
	
	void initResolutions();
	void setSupportedResolution();
public:
	MonitorTester(GameCore *gameCore);
	~MonitorTester();
	
	int getWidth(int id);
	int getHeight(int id);
	int getSupportedAfter(int id);
	int getSupportedPrevious(int id);
	int numberOfResolution();
	void resolutionValidation();
};