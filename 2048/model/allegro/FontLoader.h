#pragma once
#include <vector>
#include <allegro5/allegro_font.h>

class FontLoader
{
private:
	std::vector<ALLEGRO_FONT*> *fonts;
public:
	FontLoader();
	~FontLoader();
	ALLEGRO_FONT* loadFont(char *filename, int size, int flags);
};