#include <string>
#include <allegro5\allegro_memfile.h>
#include "BitmapLoader.h"

BitmapLoader::BitmapLoader()
{
	bitmaps = new std::vector<ALLEGRO_BITMAP*>();
}

BitmapLoader::~BitmapLoader()
{
	for(std::vector<ALLEGRO_BITMAP*>::iterator it = bitmaps->begin(); it != bitmaps->end(); it++)
		al_destroy_bitmap(*it);

	delete bitmaps;
}

ALLEGRO_BITMAP* BitmapLoader::loadBitmap(char *filename)
{
	ALLEGRO_BITMAP *image = al_load_bitmap(filename);
	if(!image)
	{
		std::string message = "Failed to load image: ";
		message += filename;
		throw message.c_str();
	}
	bitmaps->push_back(image);
	return image;
}

ALLEGRO_BITMAP* BitmapLoader::loadBitmap_f(char *buffer, int fileLength, std::string fileType)
{
	ALLEGRO_FILE *alFile = al_open_memfile((void*)buffer, fileLength, "rb");
	ALLEGRO_BITMAP *image = al_load_bitmap_f(alFile, fileType.c_str());
	if(!image)
	{
		std::string message = "Failed to load image of type: ";
		message += fileType;
		throw message.c_str();
	}
	bitmaps->push_back(image);
	al_fclose(alFile);
	return image;
}