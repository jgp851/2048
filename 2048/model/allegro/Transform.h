#pragma once
#include <allegro5\allegro_font.h>
#include <allegro5\transformations.h>
#include <string>

class Transform
{
private:
	ALLEGRO_TRANSFORM t;
public:
	Transform();
	~Transform();

	void drawRotatedText(const std::string text, ALLEGRO_FONT *font, ALLEGRO_COLOR color, int x, int y, float deg, int align);
	void drawRotatedScaledText(const std::string text, ALLEGRO_FONT *font, ALLEGRO_COLOR color, int x, int y, float deg, int align, float percentHeight);
	void reset();
};