#include "MouseManager.h"
#include "AllegroManager.h"

MouseManager::MouseManager()
{
	x = 0;
	y = 0;
	z = 0;
	wheelUP = false;
	wheelDOWN = false;
	elapsedTimeToHide = 0;
	hidingTime = 0;
	display = NULL;
	isActive = false;
	isHidding = false;
	noActiveTime = 0;
}

bool MouseManager::isMouseEvent()
{
	if(isActive)
	{
		isActive = false;
		return true;
	}
	return false;
}

bool MouseManager::isMouseEventWheel()
{
	return mouseEventWheel;
}

bool MouseManager::isLeftPressed()
{
	return buttons[1].pressed;
}

bool MouseManager::isLeftReleased()
{
	if(buttons[1].released)
	{
		buttons[1].released = false;
		return true;
	}
	return false;
}

bool MouseManager::isMiddlePressed()
{
	return buttons[2].pressed;
}

bool MouseManager::isMiddleReleased()
{
	if(buttons[2].released)
	{
		buttons[2].released = false;
		return true;
	}
	return false;
}

bool MouseManager::isRightPressed()
{
	return buttons[3].pressed;
}

bool MouseManager::isRightReleased()
{
	if(buttons[3].released)
	{
		buttons[3].released = false;
		return true;
	}
	return false;
}

bool MouseManager::isWheelUP()
{
	if(wheelUP)
	{
		wheelUP = false;
		wheelDOWN = false;
		return true;
	}
	return wheelUP;
}

bool MouseManager::isWheelDOWN()
{
	if(wheelDOWN)
	{
		wheelDOWN = false;
		wheelUP = false;
		return true;
	}
	return wheelDOWN;
}

bool MouseManager::setMouseCursor(ALLEGRO_DISPLAY *display, ALLEGRO_SYSTEM_MOUSE_CURSOR cursor_id)
{
	return al_set_system_mouse_cursor(display, cursor_id);
}

int MouseManager::getX()
{
	return x;
}

int MouseManager::getY()
{
	return y;
}

void MouseManager::inactiveTimeUpdate(unsigned int elapsedTime)
{
	noActiveTime += elapsedTime;
	if(noActiveTime >= 300)
		isActive = false;
}

void MouseManager::processEvent(ALLEGRO_EVENT *event)
{
	resetStatus();
	noActiveTime = 0;
	isActive = true;
	
	if(event->type == ALLEGRO_EVENT_MOUSE_AXES || event->type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
	{
		update_Z_AXES(event);
		x = event->mouse.x;
		y = event->mouse.y;
		return;
	}

	if(event->type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN || event->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
	{
		if(event->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
			buttons[event->mouse.button].released = true;

		bool pressed = (event->type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN);
		buttons[event->mouse.button].pressed = pressed;
		if(!pressed)
			buttons[event->mouse.button].checked = false;
	}
}

void MouseManager::resetStatus()
{
	mouseEventWheel = false;
	for(std::map<int, ButtonStatus>::iterator it = buttons.begin(); it != buttons.end(); it++)
		it->second.released = false;
}

void MouseManager::setHidingTime(ALLEGRO_DISPLAY *display, unsigned int hidingTime)
{
	this->display = display;
	this->hidingTime = hidingTime;
	elapsedTimeToHide = 0;
	isHidding = true;
}

void MouseManager::stopHiding()
{
	al_show_mouse_cursor(display);
	isHidding = false;
}

void MouseManager::update(unsigned int elapsedTime)
{
	// update ukrywania kursora w określonym uprzednio czasie
	if(isHidding)
	{
		if(!isActive)
		{
			elapsedTimeToHide += elapsedTime;
			if(elapsedTimeToHide >= hidingTime)
				al_hide_mouse_cursor(display);
		}
		else
		{
			elapsedTimeToHide = 0;
			al_show_mouse_cursor(display);
		}
	}
}

void MouseManager::update_Z_AXES(ALLEGRO_EVENT *event)
{
	if(z != event->mouse.z)
	{
		mouseEventWheel = true;
		if(z < event->mouse.z)
			wheelUP = true;
		else
			wheelDOWN = true;
		z = event->mouse.z;
	}
}