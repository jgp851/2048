#pragma once
#include <map>
#include <allegro5\allegro.h>

struct ButtonStatus
{
	bool checked;
	bool pressed;
	bool released;

	ButtonStatus():pressed(false), checked(false), released(false){}
};

class MouseManager
{
private:
	ALLEGRO_DISPLAY *display;
	bool isActive; // czy mysz jest aktywna
	bool isHidding; // oznacza aktywowanie opcji ukrywania kursora
	bool mouseEventWheel;
	bool wheelUP;
	bool wheelDOWN;
	int x;
	int y;
	int z;
	unsigned int hidingTime; // czas po kt�rym ukrywany jest kursor
	unsigned int elapsedTimeToHide; // czas kt�ry up�yn�� od czasu bezczynno�ci myszy
	unsigned int noActiveTime; // czas bezczynno�ci myszy
	std::map<int, ButtonStatus> buttons;
	void resetStatus();
	void update_Z_AXES(ALLEGRO_EVENT *event);
public:
	MouseManager();

	bool isMouseEvent();
	bool isMouseEventWheel();
	bool isLeftPressed();
	bool isLeftReleased();
	bool isMiddlePressed();
	bool isMiddleReleased();
	bool isRightPressed();
	bool isRightReleased();
	bool isWheelUP();
	bool isWheelDOWN();
	bool setMouseCursor(ALLEGRO_DISPLAY *display, ALLEGRO_SYSTEM_MOUSE_CURSOR cursor_id);

	int getX();
	int getY();

	void processEvent(ALLEGRO_EVENT *event);
	void inactiveTimeUpdate(unsigned int elapsedTime); // w stanie bezczynno�ci myszy jest liczony czas jej bezczynno�ci. Je�li przekroczy okre�lony limit oczekiwania to flaga isActive = false
	void setHidingTime(ALLEGRO_DISPLAY *display, unsigned int hidingTime);
	void stopHiding();
	void update(unsigned int elapsedTime);
};