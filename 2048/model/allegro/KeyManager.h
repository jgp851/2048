#pragma once
#include <map>
#include "allegro5/allegro.h"

struct KeyStatus
{
	bool pressed;
	bool checked;
	KeyStatus()
	{
		checked = false;
		pressed = false;
	}
};

class KeyManager
{
private:
	ALLEGRO_EVENT_TYPE eventType;
	std::map<int, KeyStatus> keys;
public:
	KeyManager();
	~KeyManager();
	bool is_EVENT_KEY_DOWN();
	bool is_EVENT_KEY_UP();
	bool isPressed(int keyCode);
	bool isPressed(int keyCode, bool singleCheck);
	void processEvent(ALLEGRO_EVENT *event);
};