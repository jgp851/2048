#include "Transform.h"
#define _USE_MATH_DEFINES
#include <math.h>

Transform::Transform()
{
	al_identity_transform(&t);
}
Transform::~Transform() {}

void Transform::drawRotatedText(const std::string text, ALLEGRO_FONT *font, ALLEGRO_COLOR color, int x, int y, float deg, int align)
{
	al_rotate_transform(&t, deg * 0.01745329);
	al_translate_transform(&t, x, y);

	al_use_transform(&t);
	al_draw_text(font, color, 0, 0, align, text.c_str());
	reset();
}

void Transform::drawRotatedScaledText(const std::string text, ALLEGRO_FONT *font, ALLEGRO_COLOR color, int x, int y, float deg, int align, float percentHeight)
{
	al_scale_transform(&t, 1, percentHeight);
	al_rotate_transform(&t, deg * 0.01745329);
	al_translate_transform(&t, x, y);
	al_use_transform(&t);
	al_draw_text(font, color, 0, 0, align, text.c_str());
	reset();
}

void Transform::reset()
{
	al_identity_transform(&t);
	al_use_transform(&t);
}