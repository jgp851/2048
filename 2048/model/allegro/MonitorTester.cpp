#include "MonitorTester.h"
#include "..\gamecore\ApplicationSettings.h"
#include "..\gamecore\GameCore.h"
#include <fstream>

MonitorTester::MonitorTester(GameCore *gameCore)
{
	this->gameCore = gameCore;
	ALLEGRO_MONITOR_INFO monitorInfo;
	al_get_monitor_info(0, &monitorInfo);
	supportedW = monitorInfo.x2;
	supportedH = monitorInfo.y2;
	initResolutions();
}
MonitorTester::~MonitorTester()
{
}

int MonitorTester::getWidth(int id)
{
	return resolutions[id]->width;
}

int MonitorTester::getHeight(int id)
{
	return resolutions[id]->height;
}

int MonitorTester::getSupportedAfter(int id)
{
	while(true)
	{
		if(id < (int)resolutions.size() - 1)
			id++;
		else
			id = 0;
		if(resolutions[id]->supported)
			return id;
	}
}

int MonitorTester::getSupportedPrevious(int id)
{
	while(true)
	{
		if(id > 0)
			id--;
		else
			id = (int)resolutions.size() - 1;
		if(resolutions[id]->supported)
			return id;
	}
}

int MonitorTester::numberOfResolution()
{
	return resolutions.size();
}

void MonitorTester::initResolutions()
{
	resolutions.push_back(new Resolution(false, 640, 480));
	resolutions.push_back(new Resolution(false, 800, 600));
	resolutions.push_back(new Resolution(false, 1024, 768));
	resolutions.push_back(new Resolution(false, 1280, 800));
	resolutions.push_back(new Resolution(false, 1280, 960));
	resolutions.push_back(new Resolution(false, 1280, 1024));
	resolutions.push_back(new Resolution(false, 1366, 768));
	resolutions.push_back(new Resolution(false, 1600, 900));
	resolutions.push_back(new Resolution(false, 1920, 1080));
}

void MonitorTester::resolutionValidation()
{
	//przy pierwszym uruchomieniu ustawia domy�lnie najwy�sz� mo�liw� rozdzielczo�� z trybem fullScreen
	if(gameCore->appSettings->getScreenWidth() == 0 || gameCore->appSettings->getScreenHeight() == 0)
	{
		setSupportedResolution();
		gameCore->appSettings->setFullScreen(true);
		gameCore->appSettings->setScreenWidth(supportedW);
		gameCore->appSettings->setScreenHeight(supportedH);
		gameCore->appSettings->saveSettings();
	}
	else // przy ka�dym kolejnym uruchomieniu sprawdza si� kt�re z rozdzielczo�ci s� obs�ugiwane przez dany monitor
		setSupportedResolution();
}

void MonitorTester::setSupportedResolution()
{
	for(unsigned int i = 0; i < resolutions.size(); i++)
		if(supportedW >= resolutions[i]->width && supportedH >= resolutions[i]->height)
			resolutions[i]->supported = true;
	// je�li zapisana rozdzielczo�� jest niepoprawna ustawia si� domy�ln� 800x600 lub 640x480
	if(gameCore->appSettings->getScreenWidth() > supportedW || gameCore->appSettings->getScreenHeight() > supportedH)
	{
		if(supportedW >= 800 && supportedH >= 600)
		{
			gameCore->appSettings->setScreenWidth(800);
			gameCore->appSettings->setScreenHeight(600);
		}
		else
		{
			gameCore->appSettings->setScreenWidth(640);
			gameCore->appSettings->setScreenHeight(480);
		}
		gameCore->appSettings->saveSettings();
	}
}