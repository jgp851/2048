#include <string>
#include "SoundLoader.h"

SoundLoader::SoundLoader()
{
	samples = new std::vector<ALLEGRO_SAMPLE*>();
}

SoundLoader::~SoundLoader()
{
	for(std::vector<ALLEGRO_SAMPLE*>::iterator it = samples->begin(); it != samples->end(); it++)
		al_destroy_sample(*it);
	delete(samples);
}

ALLEGRO_SAMPLE* SoundLoader::loadSound(char *filename)
{
	ALLEGRO_SAMPLE *sample = al_load_sample(filename);
	if(!sample)
	{
		std::string message = "Failed to load sample: ";
		message += filename;
		throw message.c_str();
	}
	samples->push_back(sample);
	return sample;
}