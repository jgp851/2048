#include "AllegroManager.h"
#include "MonitorTester.h"
#include "../gamecore/GameCore.h"

/**
 * Konstruktor wywolywany przy tworzeniu nowego obiektu bedacego instancja klasy.
 */
AllegroManager::AllegroManager(GameCore *gameCore)
{
	this->gameCore = gameCore;

	display = NULL;
	eventQueue = NULL;
	mixer = NULL;
	timer = NULL;
	voice = NULL;
	audioAddonFlag = false;
	audioStreamFlag = false;
	displayEventSourceFlag = false;
	fontAddonFlag = false;
	imageAddonFlag = false;
	keyboardSupportFlag = false;
	mouseSupportFlag = false;
	primitivesAddonFlag = false;
	audioSamplesCount = 1;
}

/**
 * Destruktor wywolywany przy usuwaniu obiektu.
 * Glowne zadanie to porzadki w pamieci - usuwanie utworzonych na potrzeby allegro obiektow.
 */
AllegroManager::~AllegroManager()
{
	if(display != NULL)
		al_destroy_display(display);
	if(eventQueue != NULL)
		al_destroy_event_queue(eventQueue);
	if(mixer != NULL)
		al_destroy_mixer(mixer);
	if(voice != NULL)
		al_destroy_voice(voice);
	if(timer != NULL)
		al_destroy_timer(timer);
}

ALLEGRO_BITMAP* AllegroManager::resizeBitmap(ALLEGRO_BITMAP *source, int destW, int destH, bool resizeIfSourceIsTooSmall)
{
	ALLEGRO_BITMAP *resized_bmp, *loaded_bmp = source, *prev_target;
	
	// jeśli resizeIfSourceIsTooSmall == false, czyli nie mamy skalować bitmapy gdy jest mniejsza od podanych wymiarów
	if(!resizeIfSourceIsTooSmall && al_get_bitmap_width(loaded_bmp) < destW && al_get_bitmap_height(loaded_bmp) < destH)
		return loaded_bmp;

	// 1. create a temporary bitmap of size we want
	resized_bmp = al_create_bitmap(destW, destH);
	if (!resized_bmp)
		return NULL;
		
	// 3. set the target bitmap to the resized bmp
	prev_target = al_get_target_bitmap();
	al_set_target_bitmap(resized_bmp);
	
	// 4. copy the loaded bitmap to the resized bmp
	al_draw_scaled_bitmap(loaded_bmp,
	   0, 0,                              // source origin
	   al_get_bitmap_width(loaded_bmp),   // source width
	   al_get_bitmap_height(loaded_bmp),  // source height
	   0, 0,                              // target origin
	   destW, destH,                      // target dimensions
	   0                                  // flags
	);
	
	// 5. restore the previous target and clean up
	al_set_target_bitmap(prev_target);
	al_destroy_bitmap(loaded_bmp);

	return resized_bmp;
}

ALLEGRO_BITMAP* AllegroManager::getResizedBitmap(ALLEGRO_BITMAP *source, int destW, int destH, bool resizeIfSourceIsTooSmall)
{
	return resizeBitmap(source, destW, destH, resizeIfSourceIsTooSmall);
}

ALLEGRO_BITMAP* AllegroManager::getResizedBitmap(const char *filename, int destW, int destH, bool resizeIfSourceIsTooSmall)
{
	ALLEGRO_BITMAP *loaded_bmp;
	// 2. load the bitmap at the original size
	loaded_bmp = al_load_bitmap(filename);
	if (!loaded_bmp)
	   return NULL;
	return resizeBitmap(loaded_bmp, destW, destH, resizeIfSourceIsTooSmall);
}

ALLEGRO_DISPLAY* AllegroManager::getDisplay()
{
	return display;
}

ALLEGRO_EVENT_QUEUE* AllegroManager::getAllegroEventQueue()
{
	return eventQueue;
}

ALLEGRO_MIXER* AllegroManager::getAllegroMixer()
{
	return mixer;
}

ALLEGRO_VOICE* AllegroManager::getAllegroVoice()
{
	return voice;
}

bool AllegroManager::isEventAudioStreamFinished()
{
	if(eventPnt->type == ALLEGRO_EVENT_AUDIO_STREAM_FINISHED)
		return true;
	return false;
}

/**
 * Pozwala sprawdzic, czy w kolejce wciaz oczekuja zadania do obsluzenia.
 */
bool AllegroManager::isQueueEmpty()
{
	return al_is_event_queue_empty(eventQueue);
}

/**
 * Wyczyszczenie zawartosci okna danym kolorem.
 */
void AllegroManager::clearDisplay(int r, int g, int b)
{
	al_clear_to_color(al_map_rgb(r, g, b));
}

/**
 * Utworzenie glownego okna programu.
 */
void AllegroManager::createDisplay()
{
	monitorTester->resolutionValidation(); // walidacja dostępnych rozdzielczości dla obecnego monitora. Obsługiwane ustawia na true, nieobsługiwane na false
	
	if(gameCore->appSettings->getFullScreen())
		al_set_new_display_flags(ALLEGRO_FULLSCREEN);
	else
		al_set_new_display_flags(ALLEGRO_WINDOWED);

	display = al_create_display(gameCore->appSettings->getScreenWidth(), gameCore->appSettings->getScreenHeight());
    al_set_window_title(display, gameCore->gameTitle.c_str());
	if(!display)
		throw "Failed to create display!";

	if(displayEventSourceFlag)
		registerDisplayEventSource();
}

/**
 * Utworzenie kolejki zdarzen.
 * Kolejka ta jest wykorzystywana przez allegro do wrzucania do niej zdarzen, dla ktorych zostanie zarejestrowana,
 * np. zamkniecie okna, kolejny cykl timera, wcisniecie klawisza itp.
 * Z kolejki takiej w glownej petli programu mozna pobierac zdarzenia, jakie zostaly do niej dodane i odpowiednio na nie reagowac.
 */
void AllegroManager::createEventQueue()
{
	eventQueue = al_create_event_queue();
	if(!eventQueue)
		throw "Failed to create event queue!";
}

/**
 * Utworzenie timera o okreslonej czestotliwosci.
 * Timer ten po pozniejszym jego zarejestrowaniu jako zrodla zdarzen pozwoli na regularny cykl wykonywania glownej petli programu.
 * TPS, to ticks per second, czyli parametr ten okresla, ile sygnalow na sekunde timer ma wysylac do kolejki zdarzen.
 * Z taka sama czestotliwoscia bedzie wykonywac sie glowna petla gry.
 */
void AllegroManager::createTimer(int tps)
{
	timer = al_create_timer(1.0 / tps);
	if(!timer)
		throw "Failed to create timer!";
}

void AllegroManager::enableAudioAddon(bool state, int reservedSamplesCount)
{
	audioAddonFlag = state;
	audioSamplesCount =	reservedSamplesCount;
}

void AllegroManager::enableAudioStream(bool state, unsigned int freq, ALLEGRO_AUDIO_DEPTH mixerDepth, ALLEGRO_AUDIO_DEPTH voiceDepth, ALLEGRO_CHANNEL_CONF channelConf)
{
	audioStreamFlag = state;
	frequency = freq;
	this->mixerDepth = mixerDepth;
	this->voiceDepth = voiceDepth;
	this->channelConf = channelConf;
}

void AllegroManager::enableDisplayEventSource(bool state)
{
	displayEventSourceFlag = state;
}

void AllegroManager::enableFontAddon(bool state)
{
	fontAddonFlag = state;
}

void AllegroManager::enableImageAddon(bool state)
{
	imageAddonFlag = state;
}

void AllegroManager::enableKeyboard(bool state)
{
	keyboardSupportFlag = state;
}

void AllegroManager::enableMouse(bool state)
{
	mouseSupportFlag = state;
}

void AllegroManager::enablePrimitivesAddon(bool state)
{
	primitivesAddonFlag = state;
}

void AllegroManager::disablePrimitivesAddon(bool state)
{
	if(primitivesAddonFlag && state)
		al_shutdown_primitives_addon();
}

/**
 * Podmiana buforow ekranu.
 */
void AllegroManager::flipBuffers()
{
	al_flip_display();
}

/**
 * Inicjacja biblioteki allegro.
 */
void AllegroManager::init(int tps)
{
	if(!al_init())
		throw "Failed to initialize Allegro library!";
	if(audioAddonFlag && !al_install_audio())
		throw "Failed to initialize audio!";
	if(audioAddonFlag && !al_init_acodec_addon())
		throw "Failed to initialize audio codecs addon!";
	if(audioAddonFlag && !al_reserve_samples(audioSamplesCount))
		throw "Failed to initialize reserve samples!";
	if(audioAddonFlag && audioStreamFlag)
	{
		voice = al_create_voice(frequency, voiceDepth, channelConf);
		if(!voice)
			throw "Could not create ALLEGRO_VOICE!";
		mixer = al_create_mixer(frequency, mixerDepth, channelConf);
		if(!mixer)
			throw "Could not create ALLEGRO_MIXER!";
		if(!al_attach_mixer_to_voice(mixer, voice))
			throw "al_attach_mixer_to_voice failed!";
	}
	if(imageAddonFlag && !al_init_image_addon())
		throw "Failed to initialize Allegro image addon!";
	if(keyboardSupportFlag && !al_install_keyboard())
		throw "Failed to initialize keyboard!";
	if(mouseSupportFlag && !al_install_mouse())
		throw "Failed to initialize mouse!";
	if(primitivesAddonFlag && !al_init_primitives_addon())
		throw "Failed to initialize primitives!";
	if(fontAddonFlag)
		al_init_font_addon();
	if(fontAddonFlag & !al_init_ttf_addon())
		throw "Failed to initialize ttf addon!";

	createEventQueue();
	if(keyboardSupportFlag)
		registerKeyboardEventSource();
	if(mouseSupportFlag)
		registerMouseEventSource();
	createTimer(tps);
	registerTimerEventSource();
	monitorTester = new MonitorTester(gameCore);
	createDisplay();
}

/**
 * Rejestracja okna, jako zrodla zdarzen dla kolejki.
 * Dzieki tej rejestracji zdarzenia okna (takie jak np jego zamkniecie) beda dodawane do kolejki zdarzen, co pozwoli na ich obsluge.
 */
void AllegroManager::registerDisplayEventSource()
{
	al_register_event_source(eventQueue, al_get_display_event_source(display));
}

/**
 * Rejestracja klawiatury, jako zrodla zdarzen dla kolejki.
 */
void AllegroManager::registerKeyboardEventSource()
{
	al_register_event_source(eventQueue, al_get_keyboard_event_source());
}

void AllegroManager::registerMouseEventSource()
{
	al_register_event_source(eventQueue, al_get_mouse_event_source());
}

/**
 * Rejestracja timera, jako zrodla zdarzen dla kolejki.
 */
void AllegroManager::registerTimerEventSource()
{
	al_register_event_source(eventQueue, al_get_timer_event_source(timer));
}

/**
 * Uruchomienie zdefiniowanego timera.
 * Rozpoczyna wysylanie zdarzen do kolejki.
 */
void AllegroManager::startTimer()
{
	al_start_timer(timer);
}

/**
 * Metoda oczekuje na nowe zdarzenie w kolejce i po wystapieniu zapisuje je do obiektu przekazanego w parametrze.
 */
void AllegroManager::waitForEvent(ALLEGRO_EVENT *event)
{
	eventPnt = event;
	al_wait_for_event(eventQueue, event);
}