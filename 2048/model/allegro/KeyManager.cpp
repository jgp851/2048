#include "KeyManager.h"

KeyManager::KeyManager()
{
	eventType = 0;
}
KeyManager::~KeyManager()
{
}

/**
 * Dwie poni�sze funkcje zwracaj� informacj� czy mia�o miejsce naci�ni�cie lub puszczenie klawisza.
 */
bool KeyManager::is_EVENT_KEY_DOWN()
{
	if(eventType == ALLEGRO_EVENT_KEY_DOWN)
		return true;
	else
		return false;
}
bool KeyManager::is_EVENT_KEY_UP()
{
	if(eventType == ALLEGRO_EVENT_KEY_UP)
		return true;
	else
		return false;
}

/**
 * Zwraca informacje, czy klawisz o podanym kodzie jest aktualnie wcisniety.
 */
bool KeyManager::isPressed(int keyCode)
{
	return isPressed(keyCode, false);
}

bool KeyManager::isPressed(int keyCode, bool singleCheck)
{
	if(singleCheck && keys[keyCode].checked == true)
		return false;
	if(keys[keyCode].pressed == true)
		keys[keyCode].checked = true;
	return keys[keyCode].pressed;
}

/**
 * Ze zdarzenia przekazanego w parametrze pobiera i zapamietuje informacje na temat stanu klawisza (wcisniety / puszczony),
 * ktory wywolal zdarzenie.
 */
void KeyManager::processEvent(ALLEGRO_EVENT *event)
{
	eventType = event->type;

	if(event->type != ALLEGRO_EVENT_KEY_DOWN && event->type != ALLEGRO_EVENT_KEY_UP)
		return;

	bool pressed = (event->type == ALLEGRO_EVENT_KEY_DOWN);
	int keyCode = event->keyboard.keycode;

	if(pressed)
		keys[keyCode].pressed = true;
	else
	{
		keys[keyCode].checked = false;
		keys[keyCode].pressed = false;
	}
}