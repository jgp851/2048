#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;

enum MainMenuState
{
	MAIN,
	ANIMATION_IN,
	ANIMATION_OUT
};

class Game2048MainMenu
{
private:
	Game2048Core *gameCore;
	MainMenuState menuState;
	
	std::vector<Button*> *buttons;
	ALLEGRO_BITMAP *title;
	ButtonManager *buttonManager;

	bool *btnDirectories; // przechowuje wylosowane kierunki (randBtnDirectories()) animacji button�w. false = left, true = right
	int *btnX; // przechowuje aktualne wsp�rz�dne X, obliczone w animacji
	int buttonsXDest;
	int titleY;
	int titleXDest;
	int titleYDest;
	int titleH;
	unsigned animTitleOffset;
	unsigned animScrollProgress;
	unsigned IN_DURATION;
	unsigned OUT_DURATION;

	void randBtnDirectories();
public:
	Game2048MainMenu(Game2048Core *gameCore);
	~Game2048MainMenu();
	void draw();
	void reset();
	/*
	 * reset nie przewiduje animacji ANIMATION_IN bez uprzedniego wykonania ANIMATON_OUT
	 * wsp�rz�dne obiekt�w obliczane s� w�a�nie w ANIMATION_OUT, aby w ANIMATION_IN powr�ci� do pierwotnych pozycji
	 * nie przewiduj� potrzeby wykonywania ANIM_IN bez ANIM_OUT
	 */
	void setState(MainMenuState newState);
	void update(unsigned elapsedTime);
};