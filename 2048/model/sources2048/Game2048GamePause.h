#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;

enum GamePauseState
{
	PAUSE_MENU,
	SAVE_GAME
};

class Game2048GamePause
{
private:
	Game2048Core *gameCore;
	GamePauseState gamePauseState;

	std::vector<Button*> *buttons;
	ButtonManager *buttonManager;
	ALLEGRO_BITMAP *background;

	void reset();
public:
	Game2048GamePause(Game2048Core *gameCore);
	~Game2048GamePause();

	void draw();
	void setGamePauseState(GamePauseState newState);
	void update(unsigned elapsedTime);
};