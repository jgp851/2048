#include "Game2048StartScreen.h"
#include "Game2048Core.h"
#include "Game2048FontManager.h"
#include "MusicPlayer\MusicPlayerCore.h"
#include "..\gamecore\Button\Button.h"

Game2048StartScreen::Game2048StartScreen(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	cpyRightPanel = new CpyRightPanel(gameCore);
	
	title = gameCore->mediaManager->PNG_gameTitle;
	buttonAnyKey = new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.66, false, (*gameCore->LNGmanager)[LNG_PRESS_ANY_KEY], BTN_TYP_DEFAULT_BUTTON);
	buttonAnyKey->setState(BTN_ST_SELECTED);

	logoEndPosition = gameCore->appSettings->getScreenHeight() * 0.08;
	logoScrollProgress = 0;
	logoStartPosition = (gameCore->appSettings->getScreenHeight() - al_get_bitmap_height(title)) / 2;
}

void Game2048StartScreen::draw()
{
	float logoOffset = (logoEndPosition - logoStartPosition) * (logoScrollProgress / static_cast<float>(START_SCREEN_TITLE_SCROLL_TIME));
	al_draw_scaled_bitmap(title, 0, 0, al_get_bitmap_width(title), al_get_bitmap_height(title),
		(gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.64) / 2,
		logoStartPosition + logoOffset,
		gameCore->appSettings->getScreenWidth() * 0.64,
		gameCore->appSettings->getScreenHeight() * 0.35, 0);
	if(cpyRightPanel->isMusicStarted() && logoScrollProgress == 0)
	{
		buttonAnyKey->draw();
		cpyRightPanel->draw();
	}
}

void Game2048StartScreen::update(unsigned elapsedTime)
{
	cpyRightPanel->update(elapsedTime);
	if(cpyRightPanel->isMusicStarted())
		buttonAnyKey->update();
	if(cpyRightPanel->isTimeToStartMusic())
	{
		cpyRightPanel->wasStartedMusic = true;
		gameCore->musicPlayer->playPlaylist();
	}
	if(logoScrollProgress > 0)
		logoScrollProgress += elapsedTime;
	if(logoScrollProgress > START_SCREEN_TITLE_SCROLL_TIME)
		gameCore->setGameState(MAIN_MENU);
	if(cpyRightPanel->isMusicStarted() && logoScrollProgress == 0 && (gameCore->keyManager->is_EVENT_KEY_UP() || (buttonAnyKey->mouseIsOverButton() && gameCore->mouseManager->isLeftReleased())))
	{
		logoScrollProgress += elapsedTime;
		gameCore->soundPlayer->playGoIn();
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE))
		gameCore->setGameState(SHUTDOWN);
}

/*
 * CpyRightPanel
 */

CpyRightPanel::CpyRightPanel(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	wasStartedMusic = false;
	destTimeToPlayMusic = (rand() % 2 + 1) * 1000;
	elapsedTimeToPlayMusic = 0;

	copyright = "Copyright © 2016 Jerzy Gubała. All rights reserved";
	randCpyColorUpSection();
	colorToUp = true;
	cpyColorValue = 0;
}

bool CpyRightPanel::isTimeToStartMusic()
{
	if(!wasStartedMusic && elapsedTimeToPlayMusic >= destTimeToPlayMusic)
		return true;
	return false;
}

bool CpyRightPanel::isMusicStarted()
{
	return wasStartedMusic;
}

void CpyRightPanel::draw()
{
	al_draw_text(gameCore->fontManager->font_number28->font, cpyColor, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.74, ALLEGRO_ALIGN_CENTER, copyright.c_str());
}

void CpyRightPanel::randCpyColorUpSection()
{
	colorUpSection = rand() % 50 + 95;
}

void CpyRightPanel::update(unsigned elapsedTime)
{
	elapsedTimeToPlayMusic += elapsedTime;
	if(isMusicStarted())
		updateCopyrightColor(elapsedTime);
}

void CpyRightPanel::updateCopyrightColor(unsigned elapsedTime)
{
	int offset = COLOR_TRANS_OFFSET * elapsedTime / 1000;
	if(!colorToUp)
		offset *= -1;
	cpyColorValue += offset;
	
	if(cpyColorValue <= 20)
	{
		cpyColorValue = 20;
		colorToUp = true;
		randCpyColorUpSection();
	}
	else if(cpyColorValue >= colorUpSection)
		colorToUp = false;
	cpyColor.r = cpyColorValue;
	cpyColor.g = cpyColorValue;
	cpyColor.b = cpyColorValue;
	cpyColor.a = cpyColorValue;
}
