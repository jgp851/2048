#include <iostream>
#include "backgrounds\GameBackground\GameBackground.h"
#include "backgrounds\MainMenuBackground\MainMenuBackground.h"
#include "GameplayValuesManage\Loader\LoaderGame.h"
#include "GameplayValuesManage\NewGameInitializer\NewGameInitializer.h"
#include "GameplayValuesManage\Saver\SaveGame.h"
#include "GameOptions\GameOptions.h"
#include "Gameplay\GameplayBackwardsOnHexBoard.h"
#include "Gameplay\GameplayBackwardsOnStdBoard.h"
#include "Gameplay\GameplayFibonacciOnHexBoard.h"
#include "Gameplay\GameplayFibonacciOnStdBoard.h"
#include "Gameplay\GameplayStandardOnHexBoard.h"
#include "Gameplay\GameplayStandardOnStdBoard.h"
#include "MusicPlayer\MusicPlayerCore.h"
#include "Game2048Core.h"
#include "Game2048FontManager.h"
#include "Game2048GameEnd.h"
#include "Game2048GamePause.h"
#include "Game2048GameWin.h"
#include "Game2048MainMenu.h"
#include "Game2048StartScreen.h"
#include "..\gamecore\TextBox\TextBox.h"
#include "..\..\..\..\JGP.iostream\JGP.iostream\jgp.iostream.h" // DLL JGP.iostream

Game2048Core::Game2048Core() : GameCore(TPS, "2048")
{
	alManager->enableAudioAddon(true, 8);
	alManager->enableAudioStream(true, 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2);
	alManager->enableDisplayEventSource(true);
	alManager->enableFontAddon(true);
	alManager->enableImageAddon(true);
	alManager->enableKeyboard(true);
	alManager->enableMouse(true);
	alManager->enablePrimitivesAddon(true);
}
Game2048Core::~Game2048Core()
{
	delete fontManager;
	if(game != NULL)
		delete game;
	delete gameBackground;
	delete gameEnd;
	delete gameOptions;
	delete gamePause;
	delete gameSettings;
	delete gameWin;
	delete LNGmanager;
	delete loaderGame;
	delete mainMenu;
	delete mainMenuBackground;
	delete mediaManager;
	delete musicPlayer;
	delete newGameInitializer;
	delete saveGame;
	delete soundPlayer;
	delete startScreen;
}

GameState Game2048Core::getGameState()
{
	return gameState;
}

bool Game2048Core::init()
{
	bool success = GameCore::init();
	if(!success)
		return false;

	try
	{
		srand(static_cast<unsigned>(time(NULL)));
		
		LNGmanager = new LNGfileManager("lang\\");
		gameSettings = new GameSettings(this);
		mediaManager = new Game2048MultimediaManager(this);
		fontManager = new Game2048FontManager(this);
		soundPlayer = new Game2048SoundPlayer(this);
		musicPlayer = new MusicPlayerCore(this);
		
		if(GameCore::restartApplication)
		{
			gameState = appSettings->getStateAfterRestart();
			GameCore::restartApplication = false;
			musicPlayer->playPlaylist();
		}
		else
			gameState = START_SCREEN;
		
		gameBackground = new GameBackground(this);
		gameEnd = new Game2048GameEnd(this);
		gameOptions = new GameOptions(this);
		gamePause = new Game2048GamePause(this);
		gameWin = new Game2048GameWin(this);
		loaderGame = new LoaderGame(this);
		mainMenu = new Game2048MainMenu(this);
		mainMenuBackground = new MainMenuBackground(this);
		newGameInitializer = new NewGameInitializer(this);
		saveGame = new SaveGame(this);
		startScreen = new Game2048StartScreen(this);
		
		return true;
	}
	catch(char *text)
	{
		std::cerr << text << std::endl;
		return false;
	}
}

unsigned int Game2048Core::fibonacci(unsigned int n)
{
	if(n == 0)
		return 0;
	if(n == 1)
		return 1;
	unsigned fPrim = 0, f = 1;
	for(unsigned i = 2; i <= n; i++)
	{
	    unsigned m  = f + fPrim;
	    fPrim = f;
		f = m;
	}
	return f;
}

void Game2048Core::createNewGameInstance(int boardType, int gameMode)
{
	switch(boardType)
	{
	case 0: // StandardBoard
		switch(gameMode)
		{
		case GAME_MODE_STD: game = new GameplayStandardOnStdBoard(this); break;
		case GAME_MODE_BACK: game = new GameplayBackwardsOnStdBoard(this); break;
		case GAME_MODE_FIB: game = new GameplayFibonacciOnStdBoard(this); break;
		}
		break;
	case 1: // HexagonBoard
		switch(gameMode)
		{
		case GAME_MODE_STD: game = new GameplayStandardOnHexBoard(this); break;
		case GAME_MODE_BACK: game = new GameplayBackwardsOnHexBoard(this); break;
		case GAME_MODE_FIB: game = new GameplayFibonacciOnHexBoard(this); break;
		}
		break;
	}
}

void Game2048Core::draw()
{
	switch(gameState)
	{
		// MAIN_MENU
	case START_SCREEN:
		mainMenuBackground->draw();
		startScreen->draw();
		break;
	case MAIN_MENU:
		mainMenuBackground->draw();
		mainMenu->draw();
		break;
	case NEW_GAME:
		mainMenuBackground->draw();
		newGameInitializer->draw();
		break;
	case LOAD_GAME:
		mainMenuBackground->draw();
		loaderGame->draw();
		break;
	case GAME_OPTIONS:
		mainMenuBackground->draw();
		gameOptions->draw();
		break;

		// GAME
	case GAME:
		gameBackground->draw();
		game->draw();
		break;
	case GAME_PAUSE:
		gameBackground->draw();
		game->draw();
		gamePause->draw();
		break;
	case GAME_END:
		gameBackground->draw();
		game->draw();
		gameEnd->draw();
		break;
	case GAME_WIN:
		gameBackground->draw();
		game->draw();
		gameWin->draw();
		break;
	}
	musicPlayer->draw();
}

void Game2048Core::clearTmpFolder()
{
	std::vector<const char*> *tmpFiles = jgp_iostr_fileListing("temp\\", true);
	for(unsigned i = 0; i < tmpFiles->size(); i++)
		remove((*tmpFiles)[i]);
}

void Game2048Core::restartToGameState()
{
	appSettings->setStateToRestart(gameState);
}

void Game2048Core::setGameState(GameState newState)
{
	gameState = newState;
}

void Game2048Core::update(unsigned elapsedTime)
{
	if(GameCore::eventDisplayClose)
		setGameState(SHUTDOWN);

	switch(gameState)
	{
		// MAIN_MENU
	case START_SCREEN:
		mainMenuBackground->update(elapsedTime);
		startScreen->update(elapsedTime);
		break;
	case MAIN_MENU:
		mainMenuBackground->update(elapsedTime);
		mainMenu->update(elapsedTime);
		break;
	case NEW_GAME:
		mainMenuBackground->update(elapsedTime);
		newGameInitializer->update(elapsedTime);
		break;
	case LOAD_GAME:
		mainMenuBackground->update(elapsedTime);
		loaderGame->update(elapsedTime);
		break;
	case GAME_OPTIONS:
		mainMenuBackground->update(elapsedTime);
		gameOptions->update(elapsedTime);
		break;

		// GAME
	case GAME:
		gameBackground->update(elapsedTime);
		game->update(elapsedTime);
		game->processInput(elapsedTime);
		break;
	case GAME_PAUSE:
		gamePause->update(elapsedTime);
		break;
	case GAME_END:
		gameEnd->update(elapsedTime);
		break;
	case GAME_WIN:
		gameWin->update(elapsedTime);
		break;

		// SHUTDOWN APP
	case SHUTDOWN:
		appSettings->saveSettings();
		gameSettings->saveSettings();
		isRunning = false;
		clearTmpFolder();
		break;
	case RESTART:
		GameCore::restartApplication = true;
		setGameState(SHUTDOWN);
		break;
	}
	musicPlayer->update(elapsedTime);
}