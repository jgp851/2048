#pragma once
#include <vector>
#include <allegro5\allegro.h>
#include <allegro5\allegro_audio.h>

class Game2048Core;

class Game2048MultimediaManager
{
private:
	Game2048Core *gameCore;
	void createTexturesTemplates();
public:
	Game2048MultimediaManager(Game2048Core *gameCore);
	~Game2048MultimediaManager();

	void loadMultimedia();
	
	ALLEGRO_BITMAP *PNG_buttonPlay;
	ALLEGRO_BITMAP *PNG_gameTitle;
	ALLEGRO_BITMAP *PNG_lightOff;
	ALLEGRO_BITMAP *PNG_lightOn;
	ALLEGRO_BITMAP *PNG_pauseBg;
	// Button
	ALLEGRO_BITMAP *PNG_buttonMainLeft;
	ALLEGRO_BITMAP *PNG_buttonMainMiddle;
	ALLEGRO_BITMAP *PNG_buttonMainRight;
	ALLEGRO_BITMAP *PNG_buttonMainLeftPressed;
	ALLEGRO_BITMAP *PNG_buttonMainMiddlePressed;
	ALLEGRO_BITMAP *PNG_buttonMainRightPressed;
	ALLEGRO_BITMAP *PNG_buttonLeft;
	ALLEGRO_BITMAP *PNG_buttonLeftPressed;
	ALLEGRO_BITMAP *PNG_buttonLeftIcon;
	ALLEGRO_BITMAP *PNG_buttonRight;
	ALLEGRO_BITMAP *PNG_buttonRightPressed;
	ALLEGRO_BITMAP *PNG_buttonRightIcon;
	// GameScrollingBackground
	ALLEGRO_BITMAP *PNG_scrollingBgBackground;
	// MainMenuRotatingBackground
	ALLEGRO_BITMAP *PNG_mainMenuRotatingBgBackground;
	// MusicPlayer
	ALLEGRO_BITMAP *PNG_playerBg;
	// KeyboardButton
	ALLEGRO_BITMAP *PNG_buttonKbDelete;
	ALLEGRO_BITMAP *PNG_buttonKbComma;
	ALLEGRO_BITMAP *PNG_buttonKbFullstop;
	ALLEGRO_BITMAP *PNG_buttonKbMinus;
	ALLEGRO_BITMAP *PNG_buttonKbPlus;
	ALLEGRO_BITMAP *PNG_buttonKbDeletePressed;
	ALLEGRO_BITMAP *PNG_buttonKbCommaPressed;
	ALLEGRO_BITMAP *PNG_buttonKbFullstopPressed;
	ALLEGRO_BITMAP *PNG_buttonKbMinusPressed;
	ALLEGRO_BITMAP *PNG_buttonKbPlusPressed;
	// ScrollBar
	ALLEGRO_BITMAP *PNG_scrollBar_bar;
	ALLEGRO_BITMAP *PNG_scrollBar_barBottom;
	ALLEGRO_BITMAP *PNG_scrollBar_barMiddle;
	ALLEGRO_BITMAP *PNG_scrollBar_barTop;
	ALLEGRO_BITMAP *PNG_scrollBar_buttonArrowheadDown;
	ALLEGRO_BITMAP *PNG_scrollBar_buttonArrowheadUp;
	ALLEGRO_BITMAP *PNG_scrollBar_buttonDown;
	ALLEGRO_BITMAP *PNG_scrollBar_buttonUp;
	// TextBox
	ALLEGRO_BITMAP *PNG_textBgBottom;
	ALLEGRO_BITMAP *PNG_textBgLeft;
	ALLEGRO_BITMAP *PNG_textBgMiddle;
	ALLEGRO_BITMAP *PNG_textBgRight;
	ALLEGRO_BITMAP *PNG_textBgTop;
	ALLEGRO_BITMAP *PNG_textBgCorner_ne;
	ALLEGRO_BITMAP *PNG_textBgCorner_nw;
	ALLEGRO_BITMAP *PNG_textBgCorner_se;
	ALLEGRO_BITMAP *PNG_textBgCorner_sw;
	// tekstury
	ALLEGRO_BITMAP *PNG_textDef1;
	ALLEGRO_BITMAP *PNG_textDef2;
	ALLEGRO_BITMAP *PNG_textDef3;
	ALLEGRO_BITMAP *PNG_textDef4;
	ALLEGRO_BITMAP *PNG_textDef5;
	ALLEGRO_BITMAP *PNG_textDef6;
	ALLEGRO_BITMAP *PNG_textDef7;
	ALLEGRO_BITMAP *PNG_textHexDef1;
	ALLEGRO_BITMAP *PNG_textHexDef2;
	ALLEGRO_BITMAP *PNG_textHexDef3;
	ALLEGRO_BITMAP *PNG_textHexDef4;
	ALLEGRO_BITMAP *PNG_textHexDef5;
	ALLEGRO_BITMAP *PNG_textureHighlightHex;
	// TraxInfoPanel
	ALLEGRO_BITMAP *PNG_traxPanelCircle;

	ALLEGRO_SAMPLE *SMP_goIn;
	ALLEGRO_SAMPLE *SMP_goOut;
	ALLEGRO_SAMPLE *SMP_switchButtons;
	ALLEGRO_SAMPLE *SMP_switchMenu;
	
	std::vector<ALLEGRO_BITMAP*> texturesCustom;
	std::vector<std::string> customTexturesDirectories;
	std::vector<ALLEGRO_BITMAP*> texturesHexCustom;
	std::vector<std::string> customTexturesHexDirectories;
	std::vector<ALLEGRO_BITMAP*> texturesDefault;
	std::vector<ALLEGRO_BITMAP*> texturesHexDefault;
};