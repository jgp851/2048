#include "Game2048GamePause.h"
#include "Game2048Core.h"
#include "Game2048FontManager.h"
#include "Gameplay\GameplayCore.h"
#include "GamePlayValuesManage\Saver\SaveGame.h"
#include "..\gamecore\Button\ButtonManager.h"

Game2048GamePause::Game2048GamePause(Game2048Core *gameCore)
{
	this->gameCore = gameCore;

	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.33, false, (*gameCore->LNGmanager)[LNG_BACK_TO_GAME]));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.415, false, (*gameCore->LNGmanager)[LNG_SAVE_GAME]));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.5, false, (*gameCore->LNGmanager)[LNG_BACK_TO_MAIN_MENU]));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.585, false, (*gameCore->LNGmanager)[LNG_EXIT]));
	buttonManager = new ButtonManager(gameCore, buttons);

	background = gameCore->mediaManager->PNG_pauseBg;
	reset();
}
Game2048GamePause::~Game2048GamePause()
{
	delete buttonManager;
}

void Game2048GamePause::draw()
{
	for(int i = 0; i < gameCore->appSettings->getScreenWidth(); i += al_get_bitmap_width(background))
		for(int j = 0; j < gameCore->appSettings->getScreenHeight(); j += al_get_bitmap_height(background))
			al_draw_bitmap(background, i, j, 0);
	
	switch(gamePauseState)
	{
	case PAUSE_MENU: buttonManager->draw(); break;
	case SAVE_GAME: gameCore->saveGame->draw(); break;
	}
}

void Game2048GamePause::reset()
{
	buttonManager->selectButton(0);
	setGamePauseState(PAUSE_MENU);
}

void Game2048GamePause::setGamePauseState(GamePauseState newState)
{
	gamePauseState = newState;
}

void Game2048GamePause::update(unsigned elapsedTime)
{
	switch(gamePauseState)
	{
	case PAUSE_MENU:
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
		{
			gameCore->game->saveThisGame = false;
			gameCore->game->setMouseHiding();
			gameCore->setGameState(GAME);
			reset();
		}
		buttonManager->update();
		if(buttonManager->isPressed())
			switch(buttonManager->getSelectedButton())
			{
			case 0:
				gameCore->game->saveThisGame = false;
				gameCore->game->setMouseHiding();
				gameCore->setGameState(GAME);
				reset();
				break;
			case 1:
				setGamePauseState(SAVE_GAME);
				gameCore->saveGame->reset();
				break;
			case 2:
				gameCore->saveGame->saveLastGame();
				delete gameCore->game;
				gameCore->game = NULL;
				gameCore->setGameState(MAIN_MENU);
				reset();
				break;
			case 3:
				gameCore->saveGame->saveLastGame();
				gameCore->setGameState(SHUTDOWN);
				reset();
				break;
			}
		break;
	case SAVE_GAME: gameCore->saveGame->update(elapsedTime); break;
	}
}