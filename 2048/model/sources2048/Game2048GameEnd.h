#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;

class Game2048GameEnd
{
private:
	Game2048Core *gameCore;

	std::vector<Button*> *buttons;
	ButtonManager *buttonManager;
	ALLEGRO_BITMAP *pauseBg;
	ALLEGRO_BITMAP *title;
	ALLEGRO_FONT *font;
public:
	Game2048GameEnd(Game2048Core *gameCore);
	~Game2048GameEnd();

	void draw();
	void reset();
	void update(unsigned elapsedTime);
};