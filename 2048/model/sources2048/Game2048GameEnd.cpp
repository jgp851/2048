#include "Game2048GameEnd.h"
#include "Game2048Core.h"
#include "Game2048FontManager.h"
#include "Gameplay\GameplayCore.h"
#include "GameplayValuesManage\Saver\SaveGame.h"
#include "..\gamecore\Button\ButtonManager.h"

Game2048GameEnd::Game2048GameEnd(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.5, false, (*gameCore->LNGmanager)[LNG_NO_MOVES_YOU_LOSE], BTN_TYP_INFO_BUTTON));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.585, false, (*gameCore->LNGmanager)[LNG_NO_MOVES_BACK]));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.67, false, (*gameCore->LNGmanager)[LNG_NO_MOVES_EXIT]));
	buttonManager = new ButtonManager(gameCore, buttons);
	pauseBg = gameCore->mediaManager->PNG_pauseBg;
	title = gameCore->mediaManager->PNG_gameTitle;
}

Game2048GameEnd::~Game2048GameEnd()
{
	delete buttonManager;
}

void Game2048GameEnd::draw()
{
	for(int i = 0; i < gameCore->appSettings->getScreenWidth(); i += al_get_bitmap_width(pauseBg))
		for(int j = 0; j < gameCore->appSettings->getScreenHeight(); j += al_get_bitmap_height(pauseBg))
			al_draw_bitmap(pauseBg, i, j, 0);
	al_draw_scaled_bitmap(title, 0, 0, al_get_bitmap_width(title), al_get_bitmap_height(title),
		(gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.64) / 2,
		gameCore->appSettings->getScreenHeight() * 0.08,
		gameCore->appSettings->getScreenWidth() * 0.64,
		gameCore->appSettings->getScreenHeight() * 0.35, 0);
	buttonManager->draw();
}

void Game2048GameEnd::reset()
{
	buttonManager->selectButton(1);
	gameCore->game->reversingMovements = true;
	
	gameCore->game->screenshot_tmp = al_get_backbuffer(gameCore->alManager->getDisplay());
	al_save_bitmap("temp/screenshot.jpg", gameCore->game->screenshot_tmp);
	gameCore->game->screenshot = gameCore->bitmapLoader->loadBitmap("temp/screenshot.jpg");
	gameCore->game->lastModificationTime = gameCore->getCurrentTime("%Y-%m-%d %H:%M:%S");
}

void Game2048GameEnd::update(unsigned elapsedTime)
{
	buttonManager->update();
	if(buttonManager->isPressed())
	{
		switch(buttonManager->getSelectedButton())
		{
		case 1:
			gameCore->setGameState(GAME);
			gameCore->game->setMouseHiding();
			break;
		case 2:
			gameCore->saveGame->saveLastGame();
			delete gameCore->game;
			gameCore->game = NULL;
			gameCore->setGameState(MAIN_MENU);
			break;
		}
	}
}