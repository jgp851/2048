#include <sstream>
#include "GameOptions\GameOptions.h"
#include "GameplayValuesManage\Loader\LoaderGame.h"
#include "GameplayValuesManage\NewGameInitializer\NewGameInitializer.h"
#include "Game2048MainMenu.h"
#include "Game2048Core.h"
#include "Game2048FontManager.h"
#include "..\gamecore\Button\ButtonManager.h"

Game2048MainMenu::Game2048MainMenu(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	
	buttonsXDest = gameCore->appSettings->getScreenWidth() / 2;
	titleXDest = (gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.64) / 2;
	titleYDest = gameCore->appSettings->getScreenHeight() * 0.08;
	titleY = titleYDest;
	titleH = gameCore->appSettings->getScreenHeight() * 0.35;
	animTitleOffset = 0;
	IN_DURATION = 400;
	OUT_DURATION = 400;
	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, buttonsXDest, gameCore->appSettings->getScreenHeight() * 0.5, false, (*gameCore->LNGmanager)[LNG_NEW_GAME]));
	buttons->push_back(new Button(gameCore, buttonsXDest, gameCore->appSettings->getScreenHeight() * 0.585, false, (*gameCore->LNGmanager)[LNG_LOAD_GAME]));
	buttons->push_back(new Button(gameCore, buttonsXDest, gameCore->appSettings->getScreenHeight() * 0.67, false, (*gameCore->LNGmanager)[LNG_OPTIONS]));
	buttons->push_back(new Button(gameCore, buttonsXDest, gameCore->appSettings->getScreenHeight() * 0.755, false, (*gameCore->LNGmanager)[LNG_EXIT]));
	buttonManager = new ButtonManager(gameCore, buttons);

	btnDirectories = new bool[buttons->size()];
	btnX = new int[buttons->size()];
	
	title = gameCore->mediaManager->PNG_gameTitle;
	reset();
}
Game2048MainMenu::~Game2048MainMenu()
{
	delete btnDirectories;
	delete btnX;
	delete buttonManager;
}

void Game2048MainMenu::draw()
{
	al_draw_scaled_bitmap(title, 0, 0, al_get_bitmap_width(title), al_get_bitmap_height(title),
		titleXDest, titleY,
		gameCore->appSettings->getScreenWidth() * 0.64,
		titleH, 0);
	buttonManager->draw();
}

void Game2048MainMenu::randBtnDirectories()
{
	switch(rand() % 4)
	{
	// na przemian poczynaj�c od g�ry - lewo, prawo
	case 0:
		for(unsigned i = 0; i < buttons->size(); i++)
			btnDirectories[i] = i % 2 == 0 ?false:true;
		break;
	// na przemian poczynaj�c od g�ry - prawo, lewo
	case 1:
		for(unsigned i = 0; i < buttons->size(); i++)
			btnDirectories[i] = i % 2 != 0 ?false:true;
		break;
	// pierwsza cz�� na lewo, druga na prawo
	case 2:
		for(unsigned i = 0; i < buttons->size() / 2; i++)
			btnDirectories[i] = false;
		for(unsigned i = buttons->size() / 2; i < buttons->size(); i++)
			btnDirectories[i] = true;
		break;
	// pierwsza cz�� na prawo, druga na lewo
	case 3:
		for(unsigned i = 0; i < buttons->size() / 2; i++)
			btnDirectories[i] = true;
		for(unsigned i = buttons->size() / 2; i < buttons->size(); i++)
			btnDirectories[i] = false;
		break;
	}
}

void Game2048MainMenu::reset()
{
	setState(MAIN);
}

void Game2048MainMenu::setState(MainMenuState newState)
{
	animScrollProgress = 0;
	menuState = newState;
	switch(newState)
	{
	case MAIN:
		titleY = titleYDest;
		for(unsigned i = 0; i < buttons->size(); i++)
			(*buttons)[i]->setX(gameCore->appSettings->getScreenWidth() / 2);
		break;
	case ANIMATION_OUT:
		titleY = titleYDest;
		randBtnDirectories();
		for(unsigned i = 0; i < buttons->size(); i++)
			btnX[i] = 0;
		break;
	}
}

void Game2048MainMenu::update(unsigned elapsedTime)
{
	switch(menuState)
	{
	case MAIN:
		buttonManager->update();

		if(buttonManager->isPressed())
		{
			switch(buttonManager->getSelectedButton())
			{
			case 0:
				gameCore->newGameInitializer->reset();
				gameCore->setGameState(NEW_GAME);
				break;
			case 1:
				gameCore->loaderGame->reset();
				setState(ANIMATION_OUT);
				break;
			case 2:
				gameCore->gameOptions->reset();
				gameCore->setGameState(GAME_OPTIONS);
				break;
			case 3:
				gameCore->setGameState(SHUTDOWN);
				break;
			}
		}
		break;
	case ANIMATION_OUT:
		animScrollProgress += elapsedTime;

		// offset title 2048
		animTitleOffset = (titleH + titleYDest) * (animScrollProgress / static_cast<float>(OUT_DURATION));
		titleY = titleYDest - animTitleOffset;
		// obliczanie offset�w button�w i ustawienie ich z odpowiednim offsetem
		for(unsigned i = 0; i < buttons->size(); i++)
		{
			btnX[i] = ((*buttons)[i]->getWidth() / 2 + gameCore->appSettings->getScreenWidth() / 2) * (animScrollProgress / static_cast<float>(OUT_DURATION)); // offset
			if(btnDirectories[i] == false)
				btnX[i] *= -1;
			(*buttons)[i]->setX(buttonsXDest + btnX[i]);
		}
		if(animScrollProgress >= OUT_DURATION)
			gameCore->setGameState(LOAD_GAME);
		break;
	case ANIMATION_IN:
		animScrollProgress += elapsedTime;
		// offset title 2048
		animTitleOffset = (titleH + titleYDest) * (animScrollProgress / static_cast<float>(IN_DURATION));
		titleY = -titleH + animTitleOffset;
		// obliczanie offset�w button�w i ustawienie ich z odpowiednim offsetem
		for(unsigned i = 0; i < buttons->size(); i++)
		{
			btnX[i] = ((*buttons)[i]->getWidth() / 2 + gameCore->appSettings->getScreenWidth() / 2) * (animScrollProgress / static_cast<float>(IN_DURATION)); // offset
			switch(btnDirectories[i])
			{
			case false:
				(*buttons)[i]->setX((-(*buttons)[i]->getWidth() / 2) + btnX[i]);
				break;
			case true:
				(*buttons)[i]->setX((gameCore->appSettings->getScreenWidth() + (*buttons)[i]->getWidth() / 2) - btnX[i]);
				break;
			}
		}
		if(animScrollProgress >= IN_DURATION)
			reset();
		break;
	}
}