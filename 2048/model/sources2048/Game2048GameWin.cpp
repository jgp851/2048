#include "Game2048GameWin.h"
#include "Game2048Core.h"
#include "Game2048FontManager.h"
#include "Gameplay\GameplayCore.h"
#include "GameplayValuesManage\Saver\SaveGame.h"
#include "..\gamecore\Button\ButtonManager.h"

Game2048GameWin::Game2048GameWin(Game2048Core *gameCore)
{
	this->gameCore = gameCore;

	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.5, false, (*gameCore->LNGmanager)[LNG_YOU_WIN], BTN_TYP_INFO_BUTTON));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.585, false, (*gameCore->LNGmanager)[LNG_GAME_CONTINUE]));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.67, false, (*gameCore->LNGmanager)[LNG_BACK_TO_MAIN_MENU]));
	buttonManager = new ButtonManager(gameCore, buttons);

	pauseBg = gameCore->mediaManager->PNG_pauseBg;
	title = gameCore->mediaManager->PNG_gameTitle;
	continueGameAtEnd = true;
	this->reset();
}

Game2048GameWin::~Game2048GameWin()
{
	delete buttonManager;
}

void Game2048GameWin::draw()
{
	for(int i = 0; i < gameCore->appSettings->getScreenWidth(); i += al_get_bitmap_width(pauseBg))
		for(int j = 0; j < gameCore->appSettings->getScreenHeight(); j += al_get_bitmap_height(pauseBg))
			al_draw_bitmap(pauseBg, i, j, 0);
	al_draw_scaled_bitmap(title, 0, 0, al_get_bitmap_width(title), al_get_bitmap_height(title),
		(gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.64) / 2,
		gameCore->appSettings->getScreenHeight() * 0.08,
		gameCore->appSettings->getScreenWidth() * 0.64,
		gameCore->appSettings->getScreenHeight() * 0.35, 0);
	buttonManager->draw();
}

void Game2048GameWin::reset()
{
	buttonManager->selectButton(1);
}

void Game2048GameWin::setToContinueGameAtEnd(bool state)
{
	continueGameAtEnd = state;
	switch(continueGameAtEnd)
	{
	case true:
		buttonManager->switchButton(1, true);
		buttonManager->selectButton(1);
		break;
	case false:
		buttonManager->switchButton(1, false);
		buttonManager->selectButton(2);
		break;
	}
}

void Game2048GameWin::update(unsigned elapsedTime)
{
	buttonManager->update();
	if(buttonManager->isPressed())
	{
		switch(buttonManager->getSelectedButton())
		{
		case 1: gameCore->setGameState(GAME);
				gameCore->game->setMouseHiding(); break;
		case 2:
			gameCore->saveGame->saveLastGame();
			delete gameCore->game;
			gameCore->game = NULL;
			gameCore->setGameState(MAIN_MENU); break;
		}
		this->reset();
	}
}