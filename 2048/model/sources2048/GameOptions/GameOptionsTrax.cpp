#include <sstream>
#include "GameOptionsTrax.h"
#include "GameOptions.h"
#include "..\Game2048Core.h"
#include "..\MusicPlayer\MusicPlayerCore.h"
#include "..\MusicPlayer\MusicPlayerWindow.h"
#include "..\..\gamecore\Button\ButtonManager.h"

#define BTN_BOOKMARK 0
#define BTN_WAY_OF_PLAY 1
#define BTN_CONFIRM_CHANGES 2

GameOptionsTrax::GameOptionsTrax(Game2048Core *gameCore, GameOptions *gameOptions)
{
	this->gameCore = gameCore;
	this->gameOptions = gameOptions;
	this->musicPlayerWindow = new MusicPlayerWindow(gameCore, gameCore->musicPlayer, gameCore->appSettings->getScreenWidth() * 0.0254, gameCore->appSettings->getScreenHeight() * 0.16, gameCore->appSettings->getScreenWidth() * 0.9492, gameCore->appSettings->getScreenHeight() * 0.7213);
	
	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.0833, true, "2048 TRAX", BTN_TYP_DEFAULT_BUTTON)); // BOOKMARK
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() * 0.85, gameCore->appSettings->getScreenHeight() * 0.065, true, "", BTN_TYP_AVAILABLE)); // WAY_OF_PLAY
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.9, false, (*gameCore->LNGmanager)[LNG_CONFIRM_CHANGES])); // TRAX_CONFIRM_CHANGES
	buttonManager = new ButtonManager(gameCore, buttons);
}

GameOptionsTrax::~GameOptionsTrax()
{
	delete buttonManager;
	delete musicPlayerWindow;
}

void GameOptionsTrax::draw()
{
	buttonManager->draw();
	musicPlayerWindow->draw();
}

void GameOptionsTrax::refreshButtons()
{
	std::stringstream randomizedText;
	if(*musicIsRandomized)
		randomizedText << (*gameCore->LNGmanager)[LNG_RANDOMIZED];
	else
		randomizedText << (*gameCore->LNGmanager)[LNG_SEQUENTIAL];
	(*buttons)[BTN_WAY_OF_PLAY]->setText(randomizedText.str());
}

void GameOptionsTrax::reset()
{
	musicPlayerWindow->reset();
	buttonManager->selectButton(BTN_BOOKMARK);
	musicIsRandomized = &gameCore->gameSettings->musicPlayerRandomized;
	selectedPlaylist = gameCore->musicPlayer->getSelectedPlaylist();
	isWindowPlayerActive = false;
	refreshButtons();
}

void GameOptionsTrax::restore()
{
	gameCore->gameSettings->musicPlayerRandomized = musicIsRandomized;
	if(selectedPlaylist != gameCore->musicPlayer->getSelectedPlaylist())
		gameCore->musicPlayer->playPlaylist(selectedPlaylist);
}

void GameOptionsTrax::save()
{
	gameCore->gameSettings->selectedPlaylist = gameCore->musicPlayer->getSelectedPlaylist();
}

void GameOptionsTrax::update(unsigned elapsedTime)
{
	if(musicPlayerWindow->isActive())
	{
		musicPlayerWindow->processInput();
		buttonManager->updateWithoutInputProcess();
	}
	else
		buttonManager->update();
	musicPlayerWindow->update(elapsedTime);
	
	if(buttonManager->getSelectedButton() == 0)
		gameOptions->setOptionState(SELECTING_BOOKMARK);
	else
		gameOptions->setOptionState(MAIN_OPTION);
	
	switch(buttonManager->getSelectedButton())
	{
	case BTN_BOOKMARK:
		if((*buttons)[BTN_BOOKMARK]->isLeftPressed())
		{
			gameOptions->menuSelectedBookmark--;
			if(gameOptions->menuSelectedBookmark < 0)
				gameOptions->menuSelectedBookmark = gameOptions->totalBookmarks;
		}
		if((*buttons)[BTN_BOOKMARK]->isRightPressed())
		{
			gameOptions->menuSelectedBookmark++;
			if(gameOptions->menuSelectedBookmark > gameOptions->totalBookmarks)
				gameOptions->menuSelectedBookmark = 0;
		}
		break;
	case BTN_WAY_OF_PLAY:
		if((*buttons)[BTN_WAY_OF_PLAY]->isLeftPressed() || (*buttons)[BTN_WAY_OF_PLAY]->isRightPressed())
			*musicIsRandomized = !*musicIsRandomized;
		break;
	case BTN_CONFIRM_CHANGES:
		if((*buttons)[BTN_CONFIRM_CHANGES]->isPressed())
		{
			gameCore->soundPlayer->playGoIn();
			gameOptions->setOptionState(SAVE);
		}
		break;
	}

	refreshButtons();
}