#include <sstream>
#include "GameOptionsGraphics.h"
#include "GameOptions.h"
#include "..\Game2048Core.h"
#include "..\Game2048FontManager.h"
#include "..\Gameplay\GameplayCore.h"
#include "..\..\allegro\MonitorTester.h"
#include "..\..\gamecore\Button\ButtonManager.h"
#include "..\..\gamecore\TextBox\TextBox.h"

GameOptionsGraphics::GameOptionsGraphics(Game2048Core *gameCore, GameOptions *gameOptions)
{
	this->gameCore = gameCore;
	this->fontManager = gameCore->fontManager;
	this->gameOptions = gameOptions;
	monitorTester = gameCore->alManager->monitorTester;
	mustBeRestart = &gameOptions->mustBeRestart;

	buttons = new vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.0833, true, (*gameCore->LNGmanager)[LNG_GRAPHICS], BTN_TYP_DEFAULT_BUTTON)); // GRAPHICS
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // RESOLUTION
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // FULLSCREEN
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // LANGUAGE
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_CONFIRM_CHANGES])); // GRAPHICS_CONFIRM_CHANGES
	buttonManager = new ButtonManager(gameCore, buttons);

	STR_RESOLUTION = (*gameCore->LNGmanager)[LNG_RESOLUTION];
	STR_NO = (*gameCore->LNGmanager)[LNG_NO];
	STR_YES = (*gameCore->LNGmanager)[LNG_YES];
}
GameOptionsGraphics::~GameOptionsGraphics()
{
	delete buttonManager;
}

void GameOptionsGraphics::draw()
{
	buttonManager->draw();
	if(drawNotesAboutRestart)
		gameOptions->notesAboutRestartBox->draw();
	else if(drawWarningAboutRestart)
		gameOptions->warningAboutRestartBox->draw();
}

void GameOptionsGraphics::refreshButtons()
{
	yText = gameCore->appSettings->getScreenHeight() * 0.0833;
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	
	// BUTTON RESOLUTION
	std::stringstream resolutionText;
	resolutionText << STR_RESOLUTION << monitorTester->getWidth(selectedResolution) << "x" << monitorTester->getHeight(selectedResolution);
	(*buttons)[BTN_RESOLUTION]->setText(resolutionText.str());
	(*buttons)[BTN_RESOLUTION]->setY(yText);
	
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	// BUTTON FULLSCREEN
	std::stringstream fullScreenText;
	fullScreenText << (*gameCore->LNGmanager)[LNG_FULL_SCREEN];
	switch(selectedFullScreen)
	{
	case 0: fullScreenText << STR_NO; break;
	case 1: fullScreenText << STR_YES; break;
	}
	(*buttons)[BTN_FULLSCREEN]->setText(fullScreenText.str());
	(*buttons)[BTN_FULLSCREEN]->setY(yText);
	
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	// BUTTON LANGUAGE
	std::stringstream languageText;
	languageText << (*gameCore->LNGmanager)[LNG_LANGUAGE] << gameCore->LNGmanager->getCurrentLanguageLocal();
	(*buttons)[BTN_LANGUAGE]->setText(languageText.str());
	(*buttons)[BTN_LANGUAGE]->setY(yText);

	// BUTTON GRAPHICS_CONFIRM_CHANGES
	yText = gameCore->appSettings->getScreenHeight() * 0.8;
	(*buttons)[BTN_GRAPHICS_CONFIRM_CHANGES]->setY(yText);

	// if mustBeRestart
	if(gameOptions->mustRestart())
		*mustBeRestart = true;
	else
		*mustBeRestart = false;
	
	// notes about restart
	if(buttonManager->getSelectedButton() == BTN_GRAPHICS_CONFIRM_CHANGES && *mustBeRestart)
	{
		yText -= gameOptions->notesAboutRestartBox->getHeight();
		gameOptions->notesAboutRestartBox->setHeight(yText);
		drawNotesAboutRestart = true;
	}
	else
		drawNotesAboutRestart = false;
	// warning about restart
	if((buttonManager->getSelectedButton() == BTN_RESOLUTION && currentResolution != selectedResolution)
		|| (buttonManager->getSelectedButton() == BTN_FULLSCREEN && selectedFullScreen != gameCore->appSettings->getFullScreen())
		|| (buttonManager->getSelectedButton() == BTN_LANGUAGE && currentLanguage != selectedLanguage))
	{
		yText -= gameOptions->warningAboutRestartBox->getHeight();
		gameOptions->warningAboutRestartBox->setHeight(yText);
		drawWarningAboutRestart = true;
	}
	else
		drawWarningAboutRestart = false;
}

void GameOptionsGraphics::reset()
{
	buttonManager->selectButton(0);
	for(int i = 0; i < monitorTester->numberOfResolution(); i++)
		if(monitorTester->getWidth(i) == gameCore->appSettings->getScreenWidth() && monitorTester->getHeight(i) == gameCore->appSettings->getScreenHeight())
		{
			currentResolution = i;
			selectedResolution = i;
			break;
		}
	selectedFullScreen = gameCore->appSettings->getFullScreen();
	currentLanguage = gameCore->LNGmanager->getLanguageCode();
	selectedLanguage = currentLanguage;
	drawNotesAboutRestart = false;
	drawWarningAboutRestart = false;

	refreshButtons();
}

void GameOptionsGraphics::restore()
{
	gameCore->LNGmanager->setLanguageFileByCode(gameCore->gameSettings->languageCode);
}

void GameOptionsGraphics::save()
{
	gameCore->appSettings->setScreenWidth(monitorTester->getWidth(selectedResolution));
	gameCore->appSettings->setScreenHeight(monitorTester->getHeight(selectedResolution));
	gameCore->appSettings->setFullScreen(selectedFullScreen);
	gameCore->gameSettings->languageCode = gameCore->LNGmanager->getLanguageCode();
}

void GameOptionsGraphics::update(unsigned elapsedTime)
{
	buttonManager->update();

	if(buttonManager->getSelectedButton() == 0)
		gameOptions->setOptionState(SELECTING_BOOKMARK);
	else
		gameOptions->setOptionState(MAIN_OPTION);

	switch(buttonManager->getSelectedButton())
	{
	case BTN_GRAPHICS:
		if((*buttons)[BTN_GRAPHICS]->isLeftPressed())
		{
			gameOptions->menuSelectedBookmark--;
			if(gameOptions->menuSelectedBookmark < 0)
				gameOptions->menuSelectedBookmark = gameOptions->totalBookmarks;
		}
		if((*buttons)[BTN_GRAPHICS]->isRightPressed())
		{
			gameOptions->menuSelectedBookmark++;
			if(gameOptions->menuSelectedBookmark > gameOptions->totalBookmarks)
				gameOptions->menuSelectedBookmark = 0;
		}
		break;
	case BTN_RESOLUTION:
		if((*buttons)[BTN_RESOLUTION]->isLeftPressed())
			selectedResolution = monitorTester->getSupportedPrevious(selectedResolution);
		if((*buttons)[BTN_RESOLUTION]->isRightPressed())
			selectedResolution = monitorTester->getSupportedAfter(selectedResolution);
		break;
	case BTN_FULLSCREEN:
		if(((*buttons)[BTN_FULLSCREEN]->isLeftPressed() || (*buttons)[BTN_FULLSCREEN]->isRightPressed()))
			selectedFullScreen = !selectedFullScreen;
		break;
	case BTN_LANGUAGE:
		if((*buttons)[BTN_LANGUAGE]->isLeftPressed())
		{
			gameCore->LNGmanager->setPreviousLanguage();
			currentLanguage = gameCore->LNGmanager->getLanguageCode();
		}
		if((*buttons)[BTN_LANGUAGE]->isRightPressed())
		{
			gameCore->LNGmanager->setNextLanguage();
			currentLanguage = gameCore->LNGmanager->getLanguageCode();
		}
		break;
	}

	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true))
		switch(buttonManager->getSelectedButton())
		{
		case BTN_GRAPHICS_CONFIRM_CHANGES:
			gameCore->soundPlayer->playGoIn();
			gameOptions->setOptionState(SAVE); break;
		default: buttonManager->selectButton(buttons->size() - 1); break;
		}
	if(buttonManager->isLeftMousePressed())
		switch(buttonManager->getSelectedButton())
		{
		case BTN_GRAPHICS_CONFIRM_CHANGES:
			gameCore->soundPlayer->playGoIn();
			gameOptions->setOptionState(SAVE); break;
		}
	
	refreshButtons();
}