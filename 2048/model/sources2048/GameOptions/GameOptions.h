#pragma once
#include <vector>
#include <utility>
#include <allegro5\allegro.h>

#define TXTF_INFO_ABOUT_REBOOTING 0
#define TXTF_INFO_ABOUT_RESTART 1

using namespace std;

enum OptionState
{
	MAIN_OPTION,
	SELECTING_BOOKMARK,
	RESTORE,
	SAVE
};

class Game2048ConfigFileManager;
class Game2048Core;
class GameOptionsGraphics;
class GameOptionsSound;
class GameOptionsTrax;
class Game2048FontManager;
class TextBox;
class TextWritelnField;

class GameOptions
{
private:
	Game2048Core *gameCore;
	GameOptionsGraphics *optionGraphics;
	GameOptionsSound *optionSound;
	GameOptionsTrax *optionTrax;
	OptionState optionState;
	
	TextBox *notesAboutRestartBox;
	vector<TextWritelnField*> notesAboutRestart;
	TextBox *warningAboutRestartBox;
	vector<TextWritelnField*> warningAboutRestart;

	vector< pair<int, int> > resolutions;
	
	ALLEGRO_COLOR menuColor;
	ALLEGRO_COLOR menuColorSelected;
	
	bool mustBeRestart;
	int currentDrawing;
	int menuSelectedBookmark;
	int totalBookmarks;

	bool mustRestart();
public:
	GameOptions(Game2048Core *gameCore);
	~GameOptions();
	void draw();
	void reset();
	void setOptionState(OptionState newState);
	void update(unsigned elapsedTime);

	friend class GameOptionsGraphics;
	friend class GameOptionsSound;
	friend class GameOptionsTrax;
};