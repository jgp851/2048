#include <sstream>
#include "GameOptionsSound.h"
#include "GameOptions.h"
#include "..\MusicPlayer\MusicPlayerCore.h"
#include "..\Game2048Core.h"
#include "..\..\gamecore\Button\ButtonManager.h"
#include "..\..\gamecore\ProgressBar\ProgressBar.h"
#include "..\..\gamecore\TextBox\TextBox.h"

GameOptionsSound::GameOptionsSound(Game2048Core *gameCore, GameOptions *gameOptions)
{
	this->gameCore = gameCore;
	this->gameOptions = gameOptions;
	mustBeRestart = &gameOptions->mustBeRestart;
	
	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.0833, true, (*gameCore->LNGmanager)[LNG_SOUND], BTN_TYP_DEFAULT_BUTTON)); // SOUND
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_CONFIRM_CHANGES])); // SOUND_CONFIRM_CHANGES
	buttonManager = new ButtonManager(gameCore, buttons);
	
	gameMusicVolumeBar = new ProgressBar(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, gameCore->appSettings->getScreenWidth() * 0.3, 0, 0.7, gameCore->musicPlayer->getGameVolumeAddress(), (*gameCore->LNGmanager)[LNG_GAME_MUSIC_VOLUME]);
	gameMusicVolumeBar->setLightAddon(true, &gameCore->gameSettings->gameMusicTurned);
	gameMusicVolumeBar->setSampleAddress(gameCore->mediaManager->SMP_switchMenu);
	menuMusicVolumeBar = new ProgressBar(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, gameCore->appSettings->getScreenWidth() * 0.3, 0, 0.7, gameCore->musicPlayer->getMenuVolumeAddress(), (*gameCore->LNGmanager)[LNG_MENU_MUSIC_VOLUME]);
	menuMusicVolumeBar->setLightAddon(true, &gameCore->gameSettings->menuMusicTurned);
	menuMusicVolumeBar->setSampleAddress(gameCore->mediaManager->SMP_switchMenu);
	soundEffectsVolumeBar = new ProgressBar(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, gameCore->appSettings->getScreenWidth() * 0.3, 0, 1, gameCore->soundPlayer->getVolumeAddress(), (*gameCore->LNGmanager)[LNG_SOUND_EFFECTS_VOLUME]);
	soundEffectsVolumeBar->setLightAddon(true, &gameCore->gameSettings->soundEffectsTurned);
	soundEffectsVolumeBar->setSampleAddress(gameCore->mediaManager->SMP_switchMenu);
}

GameOptionsSound::~GameOptionsSound()
{
	delete buttonManager;
	delete gameMusicVolumeBar;
	delete menuMusicVolumeBar;
	delete soundEffectsVolumeBar;
}

void GameOptionsSound::draw()
{
	buttonManager->draw();
	gameMusicVolumeBar->draw();
	menuMusicVolumeBar->draw();
	soundEffectsVolumeBar->draw();
	if(drawNotesAboutRestart)
		gameOptions->notesAboutRestartBox->draw();
}

void GameOptionsSound::refreshButtons()
{
	yText = gameCore->appSettings->getScreenHeight() * 0.0833;
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	
	gameMusicVolumeBar->setY(yText);
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	menuMusicVolumeBar->setY(yText);
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	soundEffectsVolumeBar->setY(yText);

	// BUTTON SOUND_CONFIRM_CHANGES
	yText = gameCore->appSettings->getScreenHeight() * 0.8;
	(*buttons)[BTN_SOUND_CONFIRM_CHANGES]->setY(yText);
	
	// notes about restart
	if(buttonManager->getSelectedButton() == BTN_SOUND_CONFIRM_CHANGES && *mustBeRestart)
	{
		yText -= gameOptions->notesAboutRestartBox->getHeight();
		gameOptions->notesAboutRestartBox->setHeight(yText);
		drawNotesAboutRestart = true;
	}
	else
		drawNotesAboutRestart = false;
}

void GameOptionsSound::reset()
{
	buttonManager->selectButton(0);
	drawNotesAboutRestart = false;
	gameMusicTurned = gameCore->musicPlayer->getGameTurned();
	gameMusicVolume = gameCore->musicPlayer->getGameVolumeValue();
	gameMusicVolumeBar->reset();
	menuMusicTurned = gameCore->musicPlayer->getMenuTurned();
	menuMusicVolume = gameCore->musicPlayer->getMenuVolumeValue();
	menuMusicVolumeBar->reset();
	soundEffectsTurned = gameCore->soundPlayer->getSoundEffectsTurned();
	soundEffectsVolume = gameCore->soundPlayer->getVolumeValue();
	soundEffectsVolumeBar->reset();
	refreshButtons();
}

void GameOptionsSound::restore()
{
	gameCore->musicPlayer->setGameTurned(gameMusicTurned);
	gameCore->musicPlayer->setMenuTurned(menuMusicTurned);
	gameCore->soundPlayer->setTurned(soundEffectsTurned);
	gameCore->musicPlayer->setGameVolume(gameMusicVolume);
	gameCore->musicPlayer->setMenuVolume(menuMusicVolume);
	gameCore->soundPlayer->setVolume(soundEffectsVolume);
}

void GameOptionsSound::update(unsigned elapsedTime)
{
	updateBars(elapsedTime);

	if(buttonManager->getSelectedButton() == 0)
		gameOptions->setOptionState(SELECTING_BOOKMARK);
	else
		gameOptions->setOptionState(MAIN_OPTION);

	switch(buttonManager->getSelectedButton())
	{
	case BTN_SOUND:
		if((*buttons)[BTN_SOUND]->isLeftPressed())
		{
			gameOptions->menuSelectedBookmark--;
			if(gameOptions->menuSelectedBookmark < 0)
				gameOptions->menuSelectedBookmark = gameOptions->totalBookmarks;
		}
		if((*buttons)[BTN_SOUND]->isRightPressed())
		{
			gameOptions->menuSelectedBookmark++;
			if(gameOptions->menuSelectedBookmark > gameOptions->totalBookmarks)
				gameOptions->menuSelectedBookmark = 0;
		}
		break;
	}

	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true))
		switch(buttonManager->getSelectedButton())
		{
		case BTN_SOUND_CONFIRM_CHANGES:
			gameCore->soundPlayer->playGoIn();
			gameOptions->setOptionState(SAVE); break;
		default: buttonManager->selectButton(buttons->size() - 1); break;
		}
	if(buttonManager->isLeftMousePressed())
		switch(buttonManager->getSelectedButton())
		{
		case BTN_SOUND_CONFIRM_CHANGES:
			gameCore->soundPlayer->playGoIn();
			gameOptions->setOptionState(SAVE); break;
		}

	refreshButtons();
}

void GameOptionsSound::updateBars(unsigned elapsedTime)
{
	if(gameMusicVolumeBar->isActive())
		gameMusicVolumeBar->processInput();
	else
		buttonManager->update();
	gameMusicVolumeBar->update();
	
	if(menuMusicVolumeBar->isActive())
		menuMusicVolumeBar->processInput();
	else
		buttonManager->update();
	menuMusicVolumeBar->update();

	if(soundEffectsVolumeBar->isActive())
		soundEffectsVolumeBar->processInput();
	else
		buttonManager->update();
	soundEffectsVolumeBar->update();
}