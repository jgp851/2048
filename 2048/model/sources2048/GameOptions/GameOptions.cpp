#include <sstream>
#include "GameOptions.h"
#include "GameOptionsGraphics.h"
#include "GameOptionsSound.h"
#include "GameOptionsTrax.h"
#include "..\Game2048Core.h"
#include "..\Game2048FontManager.h"
#include "..\Gameplay\GameplayCore.h"
#include "..\MusicPlayer\MusicPlayerCore.h"
#include "..\..\gamecore\TextBox\TextBox.h"
#include "..\..\gamecore\TextBox\TextWritelnField.h"

GameOptions::GameOptions(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->optionGraphics = new GameOptionsGraphics(gameCore, this);
	this->optionSound = new GameOptionsSound(gameCore, this);
	this->optionTrax = NULL;
	if(gameCore->musicPlayer->isAnyMusicPlaylist())
	{
		this->optionTrax = new GameOptionsTrax(gameCore, this);
		totalBookmarks = 2;
	}
	else
		totalBookmarks = 1;
	
	notesAboutRestart.push_back(new TextWritelnField(0, gameCore->fontManager->font_infoAboutRebooting, 0, 0, al_map_rgb(255, 255, 255), (*gameCore->LNGmanager)[LNG_INFO_ABOUT_RESTART])); // INFO_ABOUT_RESTART
	notesAboutRestartBox = new TextBox(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, ALLEGRO_ALIGN_CENTER, &notesAboutRestart, BOX_HORIZONTAL, BOX_STANDARD);
	warningAboutRestart.push_back(new TextWritelnField(0, gameCore->fontManager->font_infoAboutRebooting, 0, 0, al_map_rgb(255, 255, 255), (*gameCore->LNGmanager)[LNG_INFO_ABOUT_REBOOTING])); // INFO_ABOUT_REBOOTING
	warningAboutRestartBox = new TextBox(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, ALLEGRO_ALIGN_CENTER, &warningAboutRestart, BOX_HORIZONTAL, BOX_STANDARD);
	
	this->reset();

	menuColor = al_map_rgb(0, 0, 0);
	menuColorSelected = al_map_rgb(255, 255, 0);
}
GameOptions::~GameOptions()
{
	delete optionGraphics;
	delete optionSound;
	if(optionTrax)
		delete optionTrax;
}

bool GameOptions::mustRestart()
{
	if(optionGraphics->currentResolution != optionGraphics->selectedResolution || optionGraphics->selectedFullScreen != gameCore->appSettings->getFullScreen() || gameCore->gameSettings->languageCode != gameCore->LNGmanager->getLanguageCode())
		return true;
	return false;
}

void GameOptions::draw()
{
	switch(menuSelectedBookmark)
	{
	case 0: optionGraphics->draw(); break;
	case 1: optionSound->draw(); break;
	case 2: optionTrax->draw(); break;
	}
}

void GameOptions::reset()
{
	setOptionState(SELECTING_BOOKMARK);

	optionGraphics->reset();
	optionSound->reset();
	if(optionTrax)
		optionTrax->reset();
	menuSelectedBookmark = 0;
	mustBeRestart = false;
}

void GameOptions::setOptionState(OptionState newState)
{
	optionState = newState;
}

void GameOptions::update(unsigned elapsedTime)
{
	switch(optionState)
	{
		/*************************************/
	case SELECTING_BOOKMARK:
	case MAIN_OPTION:
		switch(menuSelectedBookmark)
		{
		case 0: optionGraphics->update(elapsedTime); break;
		case 1: optionSound->update(elapsedTime); break;
		case 2: optionTrax->update(elapsedTime); break;
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
			setOptionState(RESTORE);
		break;
		
		/*************************************/
	case RESTORE:
		optionGraphics->restore();
		optionSound->restore();
		if(optionTrax)
			optionTrax->restore();
		gameCore->soundPlayer->playGoOut();
		gameCore->setGameState(MAIN_MENU);
		break;
	case SAVE:
		//zapis ustawień, które wymagają i nie wymagają restartu gry
		if(mustRestart())
		{
			optionGraphics->save();
			if(optionTrax)
				optionTrax->save();
			gameCore->restartToGameState();
			gameCore->setGameState(RESTART);
		}
		else //zapis ustawień, które nie wymagają restartu gry
		{
			if(optionTrax)
				optionTrax->save();
			gameCore->setGameState(MAIN_MENU);
		}
		break;
	}
}