#pragma once
#include <vector>
#include <utility>
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>

#define BTN_GRAPHICS 0
#define BTN_RESOLUTION 1
#define BTN_FULLSCREEN 2
#define BTN_LANGUAGE 3
#define BTN_GRAPHICS_CONFIRM_CHANGES 4

using namespace std;

class Button;
class ButtonManager;
class Game2048ConfigFileManager;
class Game2048Core;
class Game2048FontManager;
class GameOptions;
class MonitorTester;

class GameOptionsGraphics
{
private:
	Game2048Core *gameCore;
	Game2048FontManager *fontManager;
	GameOptions *gameOptions;
	MonitorTester *monitorTester;

	vector<Button*> *buttons;
	ButtonManager *buttonManager;
	
	bool *mustBeRestart;
	bool drawNotesAboutRestart;
	bool drawWarningAboutRestart;
	int currentDrawing;
	int yText;
	
	bool selectedFullScreen;
	int currentResolution;
	int selectedResolution;
	unsigned short int currentLanguage;
	unsigned short int selectedLanguage;

	std::string STR_RESOLUTION;
	std::string STR_NO;
	std::string STR_YES;
	
	void draw();
	void refreshButtons();
	void reset();
	void restore();
	void save();
	void saveWithoutRestart();
	void update(unsigned elapsedTime);
public:
	GameOptionsGraphics(Game2048Core *gameCore, GameOptions *gameOptions);
	~GameOptionsGraphics();

	friend class GameOptions;
};