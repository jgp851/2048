#pragma once
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;
class GameOptions;
class MusicPlayerWindow;

class GameOptionsTrax
{
private:
	Game2048Core *gameCore;
	GameOptions *gameOptions;
	MusicPlayerWindow *musicPlayerWindow;
	
	std::vector<Button*> *buttons;
	ButtonManager *buttonManager;
	
	bool isWindowPlayerActive;
	bool *musicIsRandomized;

	unsigned selectedPlaylist;

	void refreshButtons();
	void restore();
	void save();
public:
	GameOptionsTrax(Game2048Core *gameCore, GameOptions *gameOptions);
	~GameOptionsTrax();

	void draw();
	void reset();
	void update(unsigned elapsedTime);
	
	friend class GameOptions;
};