#pragma once
#include <vector>

#define BTN_SOUND 0
#define BTN_SOUND_CONFIRM_CHANGES 1

class Button;
class ButtonManager;
class Game2048Core;
class GameOptions;
class ProgressBar;

class GameOptionsSound
{
private:
	Game2048Core *gameCore;
	GameOptions *gameOptions;
	
	std::vector<Button*> *buttons;
	ButtonManager *buttonManager;
	ProgressBar *gameMusicVolumeBar;
	ProgressBar *menuMusicVolumeBar;
	ProgressBar *soundEffectsVolumeBar;
	
	bool *mustBeRestart;
	bool drawNotesAboutRestart;
	bool gameMusicTurned;
	bool menuMusicTurned;
	bool soundEffectsTurned;
	float gameMusicVolume;
	float menuMusicVolume;
	float soundEffectsVolume;
	int yText;

	void draw();
	void refreshButtons();
	void reset();
	void restore();
	void update(unsigned elapsedTime);
	void updateBars(unsigned elapsedTime);
public:
	GameOptionsSound(Game2048Core *gameCore, GameOptions *gameOptions);
	~GameOptionsSound();

	friend class GameOptions;
};