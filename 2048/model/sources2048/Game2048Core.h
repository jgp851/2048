#pragma once
#include "..\gamecore\GameCore.h"
#include "..\LNGfileList.h"
#include "GameSettings.h"
#include "Game2048MultimediaManager.h"
#include "Game2048SoundPlayer.h"
#include "..\..\..\..\lng-file-manager\lng-file-manager\LNGfileManager.h"

#define TPS 30

enum GameState
{
	START_SCREEN,
	MAIN_MENU,
	NEW_GAME,
	LOAD_GAME,
	GAME,
	GAME_PAUSE,
	GAME_END,
	SHUTDOWN,
	GAME_WIN,
	GAME_OPTIONS,
	RESTART
};

class GameBackground;
class LoaderGame;
class MainMenuBackground;
class MusicPlayerCore;
class NewGameInitializer;
class Game2048FontManager;
class GameplayCore;
class Game2048GameEnd;
class GameOptions;
class Game2048GamePause;
class Game2048GameWin;
class Game2048MainMenu;
class Game2048StartScreen;
class SaveGame;

class Game2048Core: public GameCore
{
private:
	GameState gameState;

	Game2048StartScreen *startScreen;
	MainMenuBackground *mainMenuBackground;

	void clearTmpFolder();
public:
	Game2048Core();
	~Game2048Core();

	Game2048FontManager *fontManager;
	Game2048GameEnd *gameEnd;
	Game2048GamePause *gamePause;
	Game2048GameWin *gameWin;
	Game2048MainMenu *mainMenu;
	Game2048MultimediaManager *mediaManager;
	Game2048SoundPlayer *soundPlayer;
	GameplayCore *game;
	GameBackground *gameBackground;
	GameOptions *gameOptions;
	GameSettings *gameSettings;
	LNGfileManager *LNGmanager;
	LoaderGame *loaderGame;
	MusicPlayerCore *musicPlayer;
	NewGameInitializer *newGameInitializer;
	SaveGame *saveGame;
	
	GameState getGameState();
	bool init();
	unsigned int fibonacci(unsigned int n);
	void createNewGameInstance(int boardType, int gameMode);
	void draw();
	void restartToGameState();
	void setGameState(GameState newState);
	void update(unsigned elapsedTime);
};