#pragma once
#include <vector>
#include <string>
#include <allegro5\allegro_audio.h>

class Game2048Core;
class MusicPlayerCore;
class JGMfile;
//struct Song;

class Playlist
{
private:
	Game2048Core *gameCore;
	MusicPlayerCore *playerCore;
	JGMfile *jgmFile;
	
	//std::vector<Song*> songs;
	std::vector<unsigned> lastPlayed;
	std::string filePath;
	//std::string playlistTitle;
	float percentLastList;
	unsigned lastPlayedPnt;
	unsigned lastSongPlayedTotalCount;
	unsigned playedSong;
	unsigned songCount;
	
	ALLEGRO_AUDIO_STREAM* createAudioStream(int id);

	bool songIsNotLast(unsigned id);
	void getJGMfileInstance();
public:
	Playlist(Game2048Core *gameCore, MusicPlayerCore *playerCore, std::string fileName);
	~Playlist();

	ALLEGRO_AUDIO_STREAM* getAudioStream();
	ALLEGRO_AUDIO_STREAM* getAudioStream(unsigned song);

	const char* getAlbum();
	const char* getOrigin();
	const char* getPerformer();
	const char* getPlaylistTitle();
	const char* getTitle();
	const char* getAlbumOf(unsigned song);
	const char* getOriginOf(unsigned song);
	const char* getPerformerOf(unsigned song);
	const char* getTitleOf(unsigned song);

	bool haveAlbum();
	bool haveOrigin();
	bool havePerformer();

	unsigned getDuration();
	unsigned getSongCount();
	unsigned getSongID();
	
	void nextSong();
	void playSong(unsigned id);
	void randSong();
	void reset();
};