#include "ButtonPlay.h"
#include "..\Game2048Core.h"

ButtonPlay::ButtonPlay(Game2048Core *gameCore, int size)
{
	this->gameCore = gameCore;
	this->size = size;
	this->x = 0;
	this->y = 0;
	buttonPlay = gameCore->mediaManager->PNG_buttonPlay;
}

ButtonPlay::~ButtonPlay()
{
}

bool ButtonPlay::mouseIsOverButton()
{
	if(gameCore->mouseManager->getX() >= x && gameCore->mouseManager->getX() < x + size &&
		gameCore->mouseManager->getY() >= y && gameCore->mouseManager->getY() < y + size)
		return true;
	return false;
}

int ButtonPlay::getSize()
{
	return size;
}

void ButtonPlay::draw()
{
	al_draw_scaled_bitmap(buttonPlay, 0, 0, al_get_bitmap_width(buttonPlay), al_get_bitmap_height(buttonPlay), x, y, size, size, 0);
}

void ButtonPlay::setCoordinates(int x, int y)
{
	this->x = x;
	this->y = y;
}