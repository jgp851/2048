#include <sstream>
#include "WindowItem.h"
#include "ButtonPlay.h"
#include "MusicPlayerCore.h"
#include "..\Game2048Core.h"
#include "..\..\gamecore\ProgressBar\ProgressBar.h"

WindowItem::WindowItem(Game2048Core *gameCore, MusicPlayerCore *playerCore, const char *title, const char *album, const char *performer, const char *origin, float width, float height, unsigned playlistID, unsigned songID)
{
	this->gameCore = gameCore;
	this->playerCore = playerCore;
	this->title = title;
	this->album = album;
	this->performer = performer;
	this->origin = origin;
	this->width = width;
	this->height = height;
	this->playlistID = playlistID;
	this->songID = songID;
	this->musicProgress = 0;
	this->prevMusicProgress = 0;
	
	idFont = al_load_font("gfx/kirsty.ttf", height * 0.27, 0);
	propertiesFont = al_load_font("gfx/arial.ttf", height * 0.17, 0);
	this->isActive = false;
	std::stringstream idTextSS;
	if(songID < 9)
		idTextSS << 0;
	idTextSS << songID + 1;
	std::string idTextStr = idTextSS.str();
	idText = new char[idTextStr.length() + 1];
	strcpy(idText, idTextStr.c_str());
	this->idTextW = height * 0.6;
	this->idTextX = idTextW / 2;
	this->idTextY = height * 0.1;
	this->originX = width - height * 0.1;
	this->originY = height * 0.75;
	this->propTextX = idTextW;
	this->progBarW = width * 0.3;
	this->progBarX = width - progBarW * 1.3 / 2;
	this->progBarY = height / 2;
	this->timeX = width - height * 0.1;
	this->timeY = height / 4 - al_get_font_line_height(propertiesFont) / 2;
	this->musicProgressBar = new ProgressBar(gameCore, progBarX, 0, progBarW, 0, playerCore->getMusicStreamLength(playlistID, songID), &musicProgress);
	musicProgressBar->setDrawingHighlightAlways(true);
	musicProgressBar->setButtonSkip(2);
	progBarY -= musicProgressBar->getHeight() / 2;
	
	this->btnPlay = new ButtonPlay(gameCore, height * 0.4);
	this->btnPlayX = width - btnPlay->getSize() * 1.5;
	this->btnPlayY = height / 2 - btnPlay->getSize() / 2;
	if(strlen(origin))
		btnPlayY -= al_get_font_line_height(propertiesFont);

	albumColor = al_map_rgb(200, 150, 0);
	idColor = al_map_rgb(255, 255, 255);
	performerColor = al_map_rgb(255, 255, 255);
	standardColor = al_map_rgb(170, 170, 170);
	titleColor = al_map_rgb(210, 210, 210);
	originColor = standardColor;

	this->propTextY = (height - 4 * al_get_font_line_height(propertiesFont)) / 4;
	if(strlen(album) == 0)
		titleColor = albumColor;
}

WindowItem::~WindowItem()
{
	delete [] idText;
	delete btnPlay;
	delete musicProgressBar;
}

bool WindowItem::mouseIsOverWindowItem()
{
	if(gameCore->mouseManager->getX() >= x && gameCore->mouseManager->getX() < x + width &&
		gameCore->mouseManager->getY() >= y && gameCore->mouseManager->getY() < y + height)
		return true;
	return false;
}

bool WindowItem::playButtonPressed()
{
	if(!isPlayed && btnPlay->mouseIsOverButton() && gameCore->mouseManager->isLeftReleased())
		return true;
	return false;
}

void WindowItem::draw()
{
	setAndDrawInterface();
	al_draw_text(idFont, idColor, x + idTextX, y + idTextY, ALLEGRO_ALIGN_CENTER, idText);
	drawProperties();
}

void WindowItem::drawProperties()
{
	int yText = 0;
	al_draw_text(propertiesFont, titleColorDes, x + propTextX, y + propTextY, ALLEGRO_ALIGN_LEFT, title);
	if(strlen(album))
	{
		yText += height / 4;
		al_draw_text(propertiesFont, albumColorDes, x + propTextX, y + propTextY + yText, ALLEGRO_ALIGN_LEFT, album);
	}
	if(strlen(performer))
	{
		yText += height / 4;
		al_draw_text(propertiesFont, performerColorDes, x + propTextX, y + propTextY + yText, ALLEGRO_ALIGN_LEFT, performer);
	}
	if(strlen(origin))
		al_draw_text(propertiesFont, originColor, x + originX, y + originY, ALLEGRO_ALIGN_RIGHT, origin);
}

void WindowItem::reset()
{
	isActive = false;
	isPlayed = false;
	musicProgressBar->reset();
}

void WindowItem::setActive(bool state)
{
	isActive = state;
}

void WindowItem::setPlayed(bool state)
{
	isPlayed = state;
}

void WindowItem::setCoordinates(float x, float y)
{
	this->x = x;
	this->y = y;
	btnPlay->setCoordinates(x + btnPlayX, y + btnPlayY);
}

void WindowItem::setAndDrawInterface()
{
	if(isPlayed)
	{
		albumColorDes = albumColor;
		performerColorDes = performerColor;
		titleColorDes = titleColor;
		al_draw_filled_rectangle(x, y, x + width, y + height, al_map_rgba(60, 60, 60, 40));
		musicProgressBar->draw();
		setAndDrawTime();
	}
	else
	{
		albumColorDes = standardColor;
		performerColorDes = standardColor;
		titleColorDes = standardColor;
		if(mouseIsOverWindowItem())
		{
			al_draw_filled_rectangle(x, y, x + width, y + height, al_map_rgba(50, 50, 50, 30));
			btnPlay->draw();
		}
		else if(isActive)
			al_draw_filled_rectangle(x, y, x + width, y + height, al_map_rgba(30, 30, 30, 10));
	}
}

void WindowItem::setAndDrawTime()
{
	unsigned actualTime = musicProgress;
	unsigned totalTime = playerCore->getDurationCurrentSong() / 1000.0f;
	unsigned actualMinutes = 0, actualSeconds, totalMinutes = 0, totalSeconds;
	actualMinutes = actualTime / 60;
	actualSeconds = actualTime % 60;
	totalMinutes = totalTime / 60;
	totalSeconds = totalTime % 60;
	stringstream time;
	time << actualMinutes << ":";
	if(actualSeconds < 10)
		time << "0";
	time << actualSeconds << "/" << totalMinutes << ":";
	if(totalSeconds < 10)
		time << "0";
	time << totalSeconds;
	al_draw_text(propertiesFont, al_map_rgb(255, 255, 255), x + timeX, y + timeY, ALLEGRO_ALIGN_RIGHT, time.str().c_str());
}

void WindowItem::update()
{
	if(isPlayed)
	{
		if(musicProgressBar->isActive())
			musicProgressBar->processInput();
		musicProgressBar->update();
		if(musicProgressBar->valueWasChanged())
		{
			playerCore->seekMusic(musicProgress);
			prevMusicProgress = musicProgress;
		}
		if(!musicProgressBar->valueIsChanging())
		{
			musicProgress = playerCore->getCurrentTime() / 1000.0f;
			prevMusicProgress = musicProgress;
			musicProgressBar->setValue(musicProgress);
		}
		musicProgressBar->setCoordinates(x + progBarX, y + progBarY);
	}
}