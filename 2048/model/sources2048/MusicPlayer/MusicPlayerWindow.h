#pragma once
#include <allegro5\allegro.h>
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;
class MusicPlayerCore;
struct ScrollBar;
class WindowItem;

class MusicPlayerWindow
{
private:
	Game2048Core *gameCore;
	MusicPlayerCore *playerCore;
	ScrollBar *scrollBar;
	
	std::vector<Button*> *buttons;
	ButtonManager *buttonManager;
	std::vector<std::vector<WindowItem*>> playlists;

	ALLEGRO_BITMAP *bg;

	bool keyboardActive;
	float x;
	float y;
	float width;
	float height;
	int windowX;
	int windowY;
	int windowW;
	int windowH;
	int itemH;

	int currentDrawing;
	int lastScrollItem;
	int previousSongItem;
	int selectedPlaylistItem;
	int selectedSongItem;
	int showItems;

	void resetWindowItem(unsigned id);
	void setCurrentDrawing();
	void updateRightItems();
	void updateWindowItem();
public:
	MusicPlayerWindow(Game2048Core *gameCore, MusicPlayerCore *playerCore, int x, int y, int width, int height);
	~MusicPlayerWindow();
	
	bool isActive();
	bool mouseIsOver();

	void draw();
	void processInput();
	void reset();
	void update(unsigned elapsedTime);
};