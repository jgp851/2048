#pragma once
#include <string>
#include <allegro5\allegro_font.h>

class ButtonPlay;
class Game2048Core;
class MusicPlayerCore;
class ProgressBar;

class WindowItem
{
private:
	Game2048Core *gameCore;
	MusicPlayerCore *playerCore;
	ButtonPlay *btnPlay;
	ProgressBar *musicProgressBar;
	
	ALLEGRO_COLOR idColor;
	ALLEGRO_COLOR albumColor;
	ALLEGRO_COLOR albumColorDes;
	ALLEGRO_COLOR originColor;
	ALLEGRO_COLOR performerColor;
	ALLEGRO_COLOR performerColorDes;
	ALLEGRO_COLOR standardColor;
	ALLEGRO_COLOR titleColor;
	ALLEGRO_COLOR titleColorDes;
	ALLEGRO_FONT *idFont;
	ALLEGRO_FONT *propertiesFont;

	const char *title;
	const char *album;
	const char *performer;
	const char *origin;

	char *idText; // numer porządkowy

	bool isActive;
	bool isPlayed;
	float x;
	float y;
	float width;
	float height;
	float musicProgress;
	float prevMusicProgress;
	int currentDrawing;
	int btnPlayX;
	int btnPlayY;
	int idTextX;
	int idTextY;
	int idTextW;
	int originX;
	int originY;
	int progBarX;
	int progBarY;
	int progBarW;
	int propTextX;
	int propTextY;
	int timeX;
	int timeY;
	unsigned playlistID;
	unsigned songID;

	void drawProperties();
	void setAndDrawInterface();
	void setAndDrawTime();
public:
	WindowItem(Game2048Core *gameCore, MusicPlayerCore *playerCore, const char *title, const char *album, const char *performer, const char *origin, float width, float height, unsigned playlistID, unsigned songID);
	~WindowItem();

	bool mouseIsOverWindowItem();
	bool playButtonPressed();

	void draw();
	void reset();
	void setActive(bool state);
	void setPlayed(bool state);
	void setCoordinates(float x, float y);
	void update();
};