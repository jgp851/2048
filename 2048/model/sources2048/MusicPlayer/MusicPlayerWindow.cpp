#include "MusicPlayerWindow.h"
#include "MusicPlayerCore.h"
#include "WindowItem.h"
#include "..\Game2048Core.h"
#include "..\..\gamecore\ScrollBar.h"
#include "..\..\gamecore\Button\ButtonManager.h"

#define BTN_PLAYLIST 0

MusicPlayerWindow::MusicPlayerWindow(Game2048Core *gameCore, MusicPlayerCore *playerCore, int x, int y, int width, int height)
{
	this->gameCore = gameCore;
	this->playerCore = playerCore;
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->windowX = x;
	this->windowY = y + height * 0.0794;
	this->windowW = width;
	this->itemH = height * 0.23;
	this->windowH = itemH * 4;
	bg = gameCore->mediaManager->PNG_playerBg;
	
	buttons = new std::vector<Button*>();
	if(playerCore->getPlaylistCount() > 1)
	{
		buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, y, true, "", BTN_TYP_AVAILABLE)); // PLAYLIST
		buttonManager = new ButtonManager(gameCore, buttons);
	}
	else
	{
		buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, y, false, "", BTN_TYP_AVAILABLE)); // PLAYLIST
		buttonManager = NULL;
	}
	(*buttons)[BTN_PLAYLIST]->setHeight(gameCore->appSettings->getScreenHeight() * 0.03);
	
	scrollBar = new ScrollBar(gameCore, x + width - 16, windowY, windowH);

	for(unsigned i = 0; i < playerCore->getPlaylistCount(); i++)
	{
		std::vector<WindowItem*> vec;
		for(unsigned j = 0; j < playerCore->getTotalSongPlaylist(i); j++)
			vec.push_back(new WindowItem(gameCore, playerCore, playerCore->getTitleOf(i, j), playerCore->getAlbumOf(i, j), playerCore->getPerformerOf(i, j), playerCore->getOriginOf(i, j), playerCore->getTotalSongPlaylist(i) > 4 ? windowW - 16: windowW, itemH, i, j));
		playlists.push_back(vec);
	}
}

MusicPlayerWindow::~MusicPlayerWindow()
{
	if(buttonManager)
		delete buttonManager;
	else
		for(std::vector<Button*>::iterator it = buttons->begin(); it != buttons->end(); it++)
			delete *it;
}

bool MusicPlayerWindow::isActive()
{
	if(mouseIsOver() || scrollBar->isSlideBar())
		return true;
	return false;
}

bool MusicPlayerWindow::mouseIsOver()
{
	if(gameCore->mouseManager->getX() >= x && gameCore->mouseManager->getX() < x + width &&
		gameCore->mouseManager->getY() >= y && gameCore->mouseManager->getY() < y + height ||
		scrollBar->mouseIsOverScrollBar())
		return true;
	return false;
}

void MusicPlayerWindow::draw()
{
	for(int dx = windowX, j = 0; j < windowW; dx++, j++)
		al_draw_scaled_bitmap(bg, 0, 0, al_get_bitmap_width(bg), al_get_bitmap_height(bg), dx, windowY, 1, windowH, 0);
	if(buttonManager)
		buttonManager->draw();
	else
		(*buttons)[0]->draw();
	if(playerCore->getTotalSongPlaylist(selectedPlaylistItem) > 4)
		scrollBar->draw();

	for(int i = 0, j = currentDrawing; i < showItems; i++, j++)
	{
		std::vector<WindowItem*> *vec = &playlists[playerCore->getSelectedPlaylist()];
		(*vec)[j]->draw();
	}
}

void MusicPlayerWindow::processInput()
{
	scrollBar->update();

	if(buttonManager)
	{
		if((*buttons)[BTN_PLAYLIST]->isLeftPressed())
		{
			playerCore->playPreviousPlaylist();
			reset();
		}
		if((*buttons)[BTN_PLAYLIST]->isRightPressed())
		{
			playerCore->playNextPlaylist();
			reset();
		}
	}
	if(scrollBar->getActualPosition() != lastScrollItem)
		keyboardActive = false;
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true))
	{
		previousSongItem = selectedSongItem;
		if(selectedSongItem < static_cast<int>(playerCore->getTotalSongPlaylist(selectedPlaylistItem)) - 1)
			selectedSongItem++;
		lastScrollItem = scrollBar->getActualPosition();
		keyboardActive = true;
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true))
	{
		previousSongItem = selectedSongItem;
		if(selectedSongItem > 0)
			selectedSongItem--;
		lastScrollItem = scrollBar->getActualPosition();
		keyboardActive = true;
	}
}

void MusicPlayerWindow::reset()
{
	currentDrawing = 0;
	selectedPlaylistItem = playerCore->getSelectedPlaylist();
	previousSongItem = 0;
	selectedSongItem = 0;
	showItems = static_cast<signed>(playerCore->getTotalSongPlaylist(selectedPlaylistItem)) < 4 ? static_cast<signed>(playerCore->getTotalSongPlaylist(selectedPlaylistItem)) : 4;
	scrollBar->setPositionVariable(&selectedSongItem);
	scrollBar->setTotalElements(playerCore->getTotalSongSelectedPlaylist());
	(*buttons)[BTN_PLAYLIST]->setText(playerCore->getTitleSelectedPlaylist());
	for(unsigned i = 0; i < playlists.size(); i++)
		resetWindowItem(i);
}

void MusicPlayerWindow::resetWindowItem(unsigned id)
{
	std::vector<WindowItem*> *vec = &playlists[id];
	for(unsigned i = 0; i < playerCore->getTotalSongPlaylist(id); i++)
		(*vec)[i]->reset();
}

void MusicPlayerWindow::setCurrentDrawing()
{
	if(playerCore->getTotalSongPlaylist(selectedPlaylistItem) > 4)
	{
		if(selectedSongItem == 0 || selectedSongItem == 1)
			currentDrawing = 0;
		else if(selectedSongItem == playerCore->getTotalSongPlaylist(selectedPlaylistItem) - 1)
			currentDrawing = selectedSongItem - 3;
		else
		{
			if(!keyboardActive && scrollBar->getActualPosition() < static_cast<signed>(scrollBar->getTotalElements()) - 2 && scrollBar->getPreviousPosition() > scrollBar->getActualPosition())
				currentDrawing = selectedSongItem - 1;
			else if(keyboardActive && previousSongItem > selectedSongItem && selectedSongItem < static_cast<signed int>(playerCore->getTotalSongPlaylist(selectedPlaylistItem)) - 2)
				currentDrawing = selectedSongItem - 1;
			else
				currentDrawing = selectedSongItem - 2;
		}
	}
	else
		currentDrawing = 0;
}

void MusicPlayerWindow::update(unsigned elapsedTime)
{
	if(buttonManager)
		buttonManager->update();
	updateWindowItem();
}

void MusicPlayerWindow::updateRightItems()
{
	int itemY = windowY;
	for(int i = 0, j = currentDrawing; i < showItems; i++, j++)
	{
		std::vector<WindowItem*> *vec = &playlists[playerCore->getSelectedPlaylist()];
		(*vec)[j]->setCoordinates(windowX, itemY);
		if((*vec)[j]->playButtonPressed())
		{
			playerCore->playSong(j);
			selectedSongItem = j;
		}
		if(playerCore->getPlaySong() == j)
			(*vec)[j]->setPlayed(true);
		else
		{
			(*vec)[j]->setPlayed(false);
			if(selectedSongItem == j)
				(*vec)[j]->setActive(true);
			else
				(*vec)[j]->setActive(false);
		}
		itemY += itemH;
		(*vec)[j]->update();
	}
}

void MusicPlayerWindow::updateWindowItem()
{
	setCurrentDrawing();
	updateRightItems();
}