#pragma once
#include <allegro5\allegro.h>

class Game2048Core;

class ButtonPlay
{
private:
	Game2048Core *gameCore;
	
	ALLEGRO_BITMAP *buttonPlay;
	int x;
	int y;
	int size;
public:
	ButtonPlay(Game2048Core *gameCore, int size);
	~ButtonPlay();

	bool mouseIsOverButton();
	int getSize();
	void draw();
	void setCoordinates(int x, int y);
};