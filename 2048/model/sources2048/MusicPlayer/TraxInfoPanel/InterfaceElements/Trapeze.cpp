#include "Trapeze.h"
#include "..\..\..\Game2048Core.h"
#include "..\..\..\Game2048FontManager.h"
#include <allegro5\allegro_primitives.h>

Trapeze::Trapeze(Game2048Core *gameCore, float height, float widthDest, float y)
{
	this->gameCore = gameCore;
	this->height = height;
	this->widthDest = widthDest;
	this->y = y;
	triangleHeight = height * 0.2706;
	triangleDownY = y + height - triangleHeight;
	rectangleY = y + triangleHeight;
	font = gameCore->fontLoader->loadFont("gfx/FallingSkyCondObl.otf", widthDest * 0.7, 0);
}

Trapeze::~Trapeze()
{
	al_destroy_font(font);
}

float Trapeze::getWidth()
{
	return widthDest;
}

float Trapeze::getWidthDest()
{
	return widthDest;
}

void Trapeze::draw()
{
	al_draw_filled_triangle(x, y, x, y + triangleHeight, x + width, y + triangleHeight, color);
	al_draw_filled_rectangle(x, rectangleY, x + width, triangleDownY, color);
	al_draw_filled_triangle(x, triangleDownY + triangleHeight, x, triangleDownY, x + width, triangleDownY, color);
	gameCore->trans->drawRotatedScaledText("TRAX", font, al_map_rgba(31, 84, 112, color.a), x + (width * al_get_font_line_height(font) / widthDest), y + height / 2, 90, ALLEGRO_ALIGN_CENTER, width / widthDest);
}

void Trapeze::setColor(ALLEGRO_COLOR color)
{
	this->color = color;
}

void Trapeze::setWidth(float width)
{
	this->width = width;
}

void Trapeze::setX(float x)
{
	this->x = x;
}

void Trapeze::update(unsigned int offset, unsigned int duration, float destWidth)
{
	if(destWidth > width)
		width += offset / static_cast<float>(duration) * (destWidth - width);
	else if(destWidth < width)
		width -= offset / static_cast<float>(duration) * (width - destWidth);
	else
		width -= offset / static_cast<float>(duration) * width;
}