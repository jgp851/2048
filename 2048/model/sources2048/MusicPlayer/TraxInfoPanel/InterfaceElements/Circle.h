#pragma once
#include <allegro5\allegro.h>

class Circle
{
protected:
	ALLEGRO_BITMAP *circleBitmap;
	float cx;
	float cy;
	float x;
	float y;
	float size;
public:
	Circle(ALLEGRO_BITMAP *circleBitmap, float cx, float cy);

	float getSize();
	void setSize(float size);
	void update(unsigned int offset, unsigned int duration, float destSize);
	virtual void draw() = 0;
};