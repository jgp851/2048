#pragma once
#include "Circle.h"

class CircleBg : public Circle
{
public:
	CircleBg(ALLEGRO_BITMAP *circleBitmap, float cx, float cy);
	void draw();
};