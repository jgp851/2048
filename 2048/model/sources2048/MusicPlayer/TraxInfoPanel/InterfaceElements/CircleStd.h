#pragma once
#include "Circle.h"

class CircleStd : public Circle
{
public:
	CircleStd(ALLEGRO_BITMAP *circleBitmap, float cx, float cy);
	void draw();
};