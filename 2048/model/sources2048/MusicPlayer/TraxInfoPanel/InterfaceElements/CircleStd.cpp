#include "CircleStd.h"

CircleStd::CircleStd(ALLEGRO_BITMAP *circleBitmap, float cx, float cy) : Circle(circleBitmap, cx, cy) {}

void CircleStd::draw()
{
	al_draw_scaled_bitmap(circleBitmap, 0, 0, al_get_bitmap_width(circleBitmap), al_get_bitmap_height(circleBitmap), x, y, size, size, 0);
}