#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>

class Game2048Core;

class Trapeze
{
private:
	Game2048Core *gameCore;
	ALLEGRO_COLOR color;
	ALLEGRO_FONT *font;
	float x;
	float y;
	float height;
	float width;
	float widthDest;
	float rectangleY;
	float triangleHeight;
	float triangleDownY;
public:
	Trapeze(Game2048Core *gameCore, float height, float widthDest, float y);
	~Trapeze();
	
	float getWidth();
	float getWidthDest();

	void draw();
	void setColor(ALLEGRO_COLOR color);
	void setWidth(float width);
	void setX(float x);
	void update(unsigned int offset, unsigned int duration, float destWidth);
};