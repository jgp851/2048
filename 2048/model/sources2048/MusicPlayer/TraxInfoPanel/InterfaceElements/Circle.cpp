#include "Circle.h"

Circle::Circle(ALLEGRO_BITMAP *circleBitmap, float cx, float cy)
{
	this->circleBitmap = circleBitmap;
	this->cx = cx;
	this->cy = cy;
	this->size = 0;
}

float Circle::getSize()
{
	return size;
}

void Circle::setSize(float size)
{
	this->size = size;
	x = cx - size / 2;
	y = cy - size / 2;
}

void Circle::update(unsigned int offset, unsigned int duration, float destSize)
{
	float newSize = size;
	if(destSize > size)
		newSize += offset / static_cast<float>(duration) * (destSize - size);
	else if(destSize < size)
		newSize -= offset / static_cast<float>(duration) * (size - destSize);
	else
		newSize -= offset / static_cast<float>(duration) * size;
	setSize(newSize);
}