#include "CircleBg.h"

CircleBg::CircleBg(ALLEGRO_BITMAP *circleBitmap, float cx, float cy) : Circle(circleBitmap, cx, cy) {}

void CircleBg::draw()
{
	al_draw_tinted_scaled_bitmap(circleBitmap, al_map_rgba(0, 77, 153, 127), 0, 0, al_get_bitmap_width(circleBitmap), al_get_bitmap_height(circleBitmap), x, y, size, size, 0);
}