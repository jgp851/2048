#include "TraxInfoPanel.h"
#include "InterfaceElements\CircleBg.h"
#include "InterfaceElements\CircleStd.h"
#include "InterfaceElements\Trapeze.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"

TraxInfoPanel::TraxInfoPanel(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	
	originStFlag = false;
	panelX = gameCore->appSettings->getScreenWidth() * 0.035;
	panelY = gameCore->appSettings->getScreenHeight() * 0.7867;
	circleBgSizeDest = gameCore->appSettings->getScreenHeight() * 0.1867;
	circleStdSizeDest = circleBgSizeDest * 0.6786;
	circleStdMaxSizeDest = circleBgSizeDest * 0.7857;
	float circleBgXDest = panelX;
	float circleBgYDest = panelY;
	float circleStdXDest = panelX + (circleBgSizeDest - circleStdSizeDest) / 2;
	float circleStdYDest = panelY + (circleBgSizeDest - circleStdSizeDest) / 2;

	bgWidthDest = gameCore->appSettings->getScreenWidth() * 0.4375;
	bgHeight = circleStdSizeDest * 0.92;
	bgX = circleStdXDest + circleStdSizeDest / 2;
	bgY = circleStdYDest + bgHeight * 0.06;
	bgFrameTransDest = 180;
	bgFrameTransLight = 110;
	bgTransDest = 140;
	bgTransLight = 70;
	bgFrameHeight = bgHeight * 0.0833;
	bgFrameDownY = bgY + bgHeight - bgFrameHeight;
	bgFrameUpY = bgY;
	bgMainY = bgFrameUpY + bgFrameHeight;
	bgMainHeight = bgHeight - 2 * bgFrameHeight;
	
	txtX = circleStdXDest + circleStdSizeDest * 1.1;
	txtPerformerY = bgFrameUpY;
	txtTitleY = bgMainY + bgMainHeight * 0.31;
	txtAlbumY = bgMainY + bgMainHeight * 0.63;
	
	circleBg = new CircleBg(gameCore->mediaManager->PNG_traxPanelCircle, circleBgXDest + circleBgSizeDest / 2, circleBgYDest + circleBgSizeDest / 2);
	circleStd = new CircleStd(gameCore->mediaManager->PNG_traxPanelCircle, circleStdXDest + circleStdSizeDest / 2, circleStdYDest + circleStdSizeDest / 2);
	trapeze = new Trapeze(gameCore, bgHeight * 0.6528, bgHeight * 0.2778, bgY + (bgHeight - bgHeight * 0.6528) / 2);
	
	IN_DURATION = 683;
		ANIMATION_IN_0_DURATION = IN_DURATION * 0.1749;
		ANIMATION_IN_1_DURATION = IN_DURATION * 0.075;
		ANIMATION_IN_2_DURATION = IN_DURATION * 0.1999;
			ANIMATION_IN_2_ZOOM_IN_DURATION = ANIMATION_IN_2_DURATION * 0.5714;
		ANIMATION_IN_3_DURATION = IN_DURATION * 0.1249;
			ANIMATION_IN_3_0_DURATION = ANIMATION_IN_3_DURATION * 0.75;
			ANIMATION_IN_3_1_DURATION = ANIMATION_IN_3_DURATION - ANIMATION_IN_3_0_DURATION;
		ANIMATION_IN_4_DURATION = IN_DURATION * 0.1249;
		ANIMATION_IN_5_DURATION = IN_DURATION * 0.1749;
		ANIMATION_IN_6_DURATION = IN_DURATION * 0.1249;
	MAIN_DURATION = 4350;
	ORIGIN_ST_DURATION = 2000;
		ANIMATION_ORIGIN_ST_0_DURATION = ORIGIN_ST_DURATION * 0.05;
		ANIMATION_ORIGIN_ST_1_DURATION = ORIGIN_ST_DURATION * 0.035;
		ANIMATION_ORIGIN_ST_2_DURATION = ORIGIN_ST_DURATION * 0.05;
		ANIMATION_ORIGIN_ST_3_DURATION = ORIGIN_ST_DURATION * 0.03;
		ANIMATION_ORIGIN_ST_4_DURATION = ORIGIN_ST_DURATION * 0.05;
		ANIMATION_ORIGIN_ST_5_DURATION = ORIGIN_ST_DURATION * 0.03;
		ANIMATION_ORIGIN_ST_6_DURATION = ORIGIN_ST_DURATION * 0.04;
		ANIMATION_ORIGIN_ST_7_DURATION = ORIGIN_ST_DURATION * 0.05;
		ANIMATION_ORIGIN_ST_8_DURATION = ORIGIN_ST_DURATION * 0.665;
	OUT_DURATION = 550;
		ANIMATION_OUT_0_DURATION = OUT_DURATION * 0.1515;
		ANIMATION_OUT_1_DURATION = OUT_DURATION * 0.0909;
		ANIMATION_OUT_2_DURATION = OUT_DURATION * 0.0606;
		ANIMATION_OUT_3_DURATION = OUT_DURATION * 0.4545;
		ANIMATION_OUT_4_DURATION = OUT_DURATION * 0.0909;
		ANIMATION_OUT_5_DURATION = OUT_DURATION * 0.1515;
	PANEL_STD_DURATION = 5583;
	PANEL_WITH_ORIGIN_DURATION = PANEL_STD_DURATION + ORIGIN_ST_DURATION;
	reset();
}

TraxInfoPanel::~TraxInfoPanel()
{
	delete circleBg;
	delete circleStd;
	delete trapeze;
}

void TraxInfoPanel::draw()
{
	if(active)
	{
		drawHighlight();
		drawCircle();
		drawText();
	}
}

void TraxInfoPanel::drawCircle()
{
	bool draw = false;
	switch(panelState)
	{
	case ANIMATION_IN:
		if(stage >= 5 || stage == 3)
			draw = true;
		break;
	case MAIN: draw = true; break;
	case ORIGIN_ST:
		if(stage <= 2 || stage >= 6)
			draw = true;
		break;
	case ANIMATION_OUT:
		if(stage <= 2)
			draw = true;
		break;
	}
	if(draw)
		circleBg->draw();
	circleStd->draw();
}

void TraxInfoPanel::drawHighlight()
{
	bool draw = false;
	switch(panelState)
	{
	case ANIMATION_IN:
		if(stage >= 4)
			draw = true;
		break;
	case MAIN: draw = true; break;
	case ORIGIN_ST:
		if(stage <= 3 || stage >= 6)
			draw = true;
		break;
	case ANIMATION_OUT:
		if(stage <= 3)
			draw = true;
		break;
	}
	if(draw)
	{
		al_draw_filled_rectangle(bgX, bgMainY, bgX + bgWidth, bgMainY + bgMainHeight, al_map_rgba(0, 49, 74, bgTrans));
		al_draw_filled_rectangle(bgX, bgFrameUpY, bgX + bgWidth, bgFrameUpY + bgFrameHeight, al_map_rgba(0, 77, 153, bgFrameTrans));
		al_draw_filled_rectangle(bgX, bgFrameDownY, bgX + bgWidth, bgFrameDownY + bgFrameHeight, al_map_rgba(0, 77, 153, bgFrameTrans));
		trapeze->setColor(al_map_rgba(0, 49, 74, bgTrans));
		trapeze->draw();
	}
}

void TraxInfoPanel::drawText()
{
	bool draw = false;
	switch(panelState)
	{
	case ANIMATION_IN:
		if(stage >= 6)
			draw = true;
		break;
	case MAIN: draw = true; break;
	case ORIGIN_ST:
		if(stage == 0)
			draw = true;
		else if(stage >= 7)
			al_draw_text(gameCore->fontManager->font_fallingSkyCondObl_albumAndTitle->font, al_map_rgba((255*txtTrans)/255, (255*txtTrans)/255, (255*txtTrans)/255, txtTrans), txtX, txtTitleY, 0, origin.c_str());
		break;
	case ANIMATION_OUT:
		if(originStFlag && stage == 0)
			al_draw_text(gameCore->fontManager->font_fallingSkyCondObl_albumAndTitle->font, al_map_rgba((255*txtTrans)/255, (255*txtTrans)/255, (255*txtTrans)/255, txtTrans), txtX, txtTitleY, 0, origin.c_str());
		else if(stage == 0)
			draw = true;
		break;
	}
	if(draw)
	{
		al_draw_text(gameCore->fontManager->font_fallingSkyCondObl_performer->font, al_map_rgba((255*txtTrans)/255, (255*txtTrans)/255, (255*txtTrans)/255, txtTrans), txtX, txtPerformerY, 0, performer.c_str());
		al_draw_text(gameCore->fontManager->font_fallingSkyCondObl_albumAndTitle->font, al_map_rgba((255*txtTrans)/255, (255*txtTrans)/255, (255*txtTrans)/255, txtTrans), txtX, txtTitleY, 0, title.c_str());
		al_draw_text(gameCore->fontManager->font_fallingSkyCondObl_albumAndTitle->font, al_map_rgba((255*txtTrans)/255, (255*txtTrans)/255, (255*txtTrans)/255, txtTrans), txtX, txtAlbumY, 0, album.c_str());
	}
}

void TraxInfoPanel::reset()
{
	active = false;
	bgFrameTrans = bgFrameTransLight;
	bgTrans = bgTransLight;
	bgWidth = bgWidthDest * 0.6514;
	trapeze->setWidth(trapeze->getWidthDest() * 0.7);
	txtTrans = 0;
	stage = 0;
	thirdStage = 0;
	circleBg->setSize(circleStdSizeDest);
	circleStd->setSize(0);
	panelCurrentTime = 0;
	stageOffset = 0;
	stagePointOffset = 0;
	if(originStFlag)
		PANEL_DURATION = PANEL_WITH_ORIGIN_DURATION;
	else
		PANEL_DURATION = PANEL_STD_DURATION;
	setState(ANIMATION_IN);
}

void TraxInfoPanel::setData(std::string album, std::string origin, std::string performer, std::string title)
{
	this->album = album;
	this->origin = origin;
	this->performer = performer;
	this->title = title;
	this->title.insert(this->title.begin(), '"');
	this->title.insert(this->title.end(), '"');
	if(this->origin.empty())
		originStFlag = false;
	else
		originStFlag = true;
}

void TraxInfoPanel::setState(TraxInfoPanelState panelState)
{
	this->panelState = panelState;
	stage = 0;
	stageOffset = 0;
	stateTime = 0;
}

void TraxInfoPanel::startDrawing()
{
	reset();
	active = true;
}

void TraxInfoPanel::update(unsigned elapsedTime)
{
	if(active)
	{
		panelCurrentTime += elapsedTime;
		stateTime += elapsedTime;
		stageOffset += elapsedTime;
		switch(panelState)
		{
		case ANIMATION_IN:
			if(stage == 0)
			{
				circleStd->update(stageOffset, ANIMATION_IN_0_DURATION, circleStdSizeDest); // zoom in
				if(stageOffset >= ANIMATION_IN_0_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_IN_0_DURATION;
				}
			}
			if(stage == 1)
			{
				circleStd->update(stageOffset, ANIMATION_IN_1_DURATION, circleStdSizeDest * 0.5252); // zoom out
				if(stageOffset >= ANIMATION_IN_1_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_IN_1_DURATION;
				}
			}
			if(stage == 2)
			{
				circleStd->update(stageOffset, ANIMATION_IN_2_ZOOM_IN_DURATION, circleStdMaxSizeDest); // zoom in
				if(stageOffset >= ANIMATION_IN_2_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_IN_2_DURATION;
				}
			}
			if(stage == 3)
			{
				stagePointOffset += elapsedTime;
				circleStd->update(stageOffset, ANIMATION_IN_3_DURATION, circleStdSizeDest); // zoom out
				switch(thirdStage)
				{
				case 0:
					circleBg->update(stagePointOffset, ANIMATION_IN_3_0_DURATION, circleBgSizeDest); // zoom in
					if(stageOffset >= ANIMATION_IN_3_0_DURATION)
					{
						thirdStage++;
						stagePointOffset = 0;
					}
					break;
				case 1: circleBg->update(stagePointOffset, ANIMATION_IN_3_1_DURATION, circleStdMaxSizeDest); break; // zoom out
				}
				if(stageOffset >= ANIMATION_IN_3_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_IN_3_DURATION;
					stagePointOffset = 0;
				}
			}
			if(stage == 4)
			{
				bgWidth -= stageOffset / static_cast<float>(ANIMATION_IN_4_DURATION) * (bgWidth - bgWidthDest * 0.2686);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_IN_4_DURATION, trapeze->getWidthDest() * 0.45);
				if(stageOffset >= ANIMATION_IN_4_DURATION * 0.4)
				{
					bgFrameTrans = bgFrameTransDest;
					bgTrans = bgTransDest;
				}
				if(stageOffset >= ANIMATION_IN_4_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_IN_4_DURATION;
					circleBg->setSize(circleStdMaxSizeDest);
				}
			}
			if(stage == 5)
			{
				bgWidth += stageOffset / static_cast<float>(ANIMATION_IN_5_DURATION) * (bgWidthDest - bgWidth);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_IN_5_DURATION, trapeze->getWidthDest());
				if(stageOffset >= ANIMATION_IN_5_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_IN_5_DURATION;
				}
			}
			if(stage == 6)
			{
				unsigned char offset = stageOffset / static_cast<float>(ANIMATION_IN_6_DURATION) * 255;
				if(255 - offset > txtTrans)
					txtTrans += offset;
				else
					txtTrans = 255;
			}
			if(stateTime >= IN_DURATION)
				setState(MAIN);
			break;
		case MAIN:
			if(stateTime >= MAIN_DURATION)
			{
				if(originStFlag)
					setState(ORIGIN_ST);
				else
					setState(ANIMATION_OUT);
			}
			break;
		case ORIGIN_ST:
			if(stage == 0)
			{
				bgWidth = bgWidthDest * 0.8;
				unsigned char offset = stageOffset / static_cast<float>(ANIMATION_ORIGIN_ST_0_DURATION) * 255;
				if(offset < txtTrans)
					txtTrans -= offset;
				else
					txtTrans = 0;
				if(stageOffset >= ANIMATION_ORIGIN_ST_0_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_0_DURATION;
				}
			}
			if(stage == 1)
			{
				bgWidth -= stageOffset / static_cast<float>(ANIMATION_ORIGIN_ST_1_DURATION) * (bgWidth - bgWidthDest * 0.5286);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_ORIGIN_ST_1_DURATION, trapeze->getWidthDest() * 0.5);
				circleBg->update(stageOffset, ANIMATION_ORIGIN_ST_1_DURATION, circleBgSizeDest); // zoom in
				circleStd->update(stageOffset, ANIMATION_ORIGIN_ST_1_DURATION, circleStdMaxSizeDest); // zoom in
				if(stageOffset >= ANIMATION_ORIGIN_ST_1_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_1_DURATION;
				}
			}
			if(stage == 2)
			{
				bgWidth += stageOffset / static_cast<float>(ANIMATION_ORIGIN_ST_2_DURATION) * (bgWidthDest * 0.7086 - bgWidth);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_ORIGIN_ST_2_DURATION, trapeze->getWidthDest() * 0.75);
				circleBg->update(stageOffset, ANIMATION_ORIGIN_ST_2_DURATION, circleStdMaxSizeDest * 0.95); // zoom out
				circleStd->update(stageOffset, ANIMATION_ORIGIN_ST_2_DURATION, circleStdSizeDest * 0.95); // zoom out
				if(stageOffset >= ANIMATION_ORIGIN_ST_2_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_2_DURATION;
				}
			}
			if(stage == 3)
			{
				bgWidth -= stageOffset / static_cast<float>(ANIMATION_ORIGIN_ST_3_DURATION) * bgWidth;
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_ORIGIN_ST_3_DURATION, 0);
				circleStd->update(stageOffset, ANIMATION_ORIGIN_ST_3_DURATION, circleStdMaxSizeDest * 1.0682); // zoom in
				if(stageOffset >= ANIMATION_ORIGIN_ST_3_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_3_DURATION;
				}
			}
			if(stage == 4)
			{
				circleStd->update(stageOffset, ANIMATION_ORIGIN_ST_4_DURATION, 0); // zoom out
				if(stageOffset >= ANIMATION_ORIGIN_ST_4_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_4_DURATION;
				}
			}
			if(stage == 5)
			{
				circleStd->update(stageOffset, ANIMATION_ORIGIN_ST_5_DURATION, circleStdMaxSizeDest); // zoom in
				if(stageOffset >= ANIMATION_ORIGIN_ST_5_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_5_DURATION;
					circleBg->setSize(circleStdMaxSizeDest);
				}
			}
			if(stage == 6)
			{
				bgWidth += stageOffset / static_cast<float>(ANIMATION_ORIGIN_ST_6_DURATION) * (bgWidthDest - bgWidth);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_ORIGIN_ST_6_DURATION, trapeze->getWidthDest());
				circleStd->update(stageOffset, ANIMATION_ORIGIN_ST_6_DURATION, circleStdSizeDest); // zoom out
				if(stageOffset >= ANIMATION_ORIGIN_ST_6_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_6_DURATION;
				}
			}
			if(stage == 7)
			{
				unsigned char offset = stageOffset / static_cast<float>(ANIMATION_ORIGIN_ST_7_DURATION) * 255;
				if(255 - offset > txtTrans)
					txtTrans += offset;
				else
					txtTrans = 255;
				if(stageOffset >= ANIMATION_ORIGIN_ST_7_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_ORIGIN_ST_7_DURATION;
				}
			}
			if(stateTime >= ORIGIN_ST_DURATION)
				setState(ANIMATION_OUT);
			break;
		case ANIMATION_OUT:
			if(stage == 0)
			{
				bgWidth = bgWidthDest * 0.8;
				unsigned char offset = stageOffset / static_cast<float>(ANIMATION_OUT_0_DURATION) * 255;
				if(offset < txtTrans)
					txtTrans -= offset;
				else
					txtTrans = 0;
				if(stageOffset >= ANIMATION_OUT_0_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_OUT_0_DURATION;
				}
			}
			if(stage == 1)
			{
				circleBg->update(stageOffset, ANIMATION_OUT_1_DURATION, circleStdMaxSizeDest * 0.95); // zoom out
				circleStd->update(stageOffset, ANIMATION_OUT_1_DURATION, circleStdSizeDest * 0.95); // zoom out
				bgWidth -= stageOffset / static_cast<float>(ANIMATION_OUT_1_DURATION) * (bgWidth - bgWidthDest * 0.5286);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_OUT_1_DURATION, trapeze->getWidthDest() * 0.5);
				if(stageOffset >= ANIMATION_OUT_1_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_OUT_1_DURATION;
				}
			}
			if(stage == 2)
			{
				circleBg->update(stageOffset, ANIMATION_OUT_2_DURATION, circleStdMaxSizeDest * 1.125); // zoom in
				circleStd->update(stageOffset, ANIMATION_OUT_2_DURATION, circleStdMaxSizeDest * 1.0682); // zoom in
				bgWidth += stageOffset / static_cast<float>(ANIMATION_OUT_2_DURATION) * (bgWidthDest * 0.7086 - bgWidth);
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_OUT_2_DURATION, trapeze->getWidthDest() * 0.75);
				if(stageOffset >= ANIMATION_OUT_2_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_OUT_2_DURATION;
				}
			}
			if(stage == 3)
			{
				if(stageOffset <= ANIMATION_OUT_3_DURATION * 0.0667)
					circleStd->update(stageOffset, ANIMATION_OUT_3_DURATION * 0.0667, circleStdMaxSizeDest * 1.2159); // zoom in
				else if(stageOffset <= ANIMATION_OUT_3_DURATION * 0.2667)
					circleStd->update(stageOffset, ANIMATION_OUT_3_DURATION * 0.2, circleStdSizeDest); // zoom out
				bgWidth -= stageOffset / static_cast<float>(ANIMATION_OUT_3_DURATION * 0.2667) * bgWidth;
				trapeze->setX(bgX + bgWidth);
				trapeze->update(stageOffset, ANIMATION_OUT_3_DURATION * 0.2667, trapeze->getWidth());
				if(stageOffset >= ANIMATION_OUT_3_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_OUT_3_DURATION;
				}
			}
			if(stage == 4)
			{
				circleStd->update(stageOffset, ANIMATION_OUT_4_DURATION, circleStdMaxSizeDest); // zoom in
				if(stageOffset >= ANIMATION_OUT_4_DURATION)
				{
					stage++;
					stageOffset %= ANIMATION_OUT_4_DURATION;
				}
			}
			if(stage == 5)
				circleStd->update(stageOffset, ANIMATION_OUT_5_DURATION, 0); // zoom out
			break;
		}
		if(panelCurrentTime > PANEL_DURATION)
			active = false;
	}
}