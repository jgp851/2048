#pragma once
#include <string>
#include <vector>
#include <allegro5\allegro.h>

class Game2048Core;
class Circle;
class Trapeze;

enum TraxInfoPanelState
{
	ANIMATION_IN,
	MAIN,
	ORIGIN_ST,
	ANIMATION_OUT
};

class TraxInfoPanel
{
private:
	Game2048Core *gameCore;
	TraxInfoPanelState panelState;
	Circle *circleBg;
	Circle *circleStd;
	Trapeze *trapeze;
	bool active;
	bool originStFlag;
	
	float panelX;
	float panelY;

	float circleBgSizeDest;
	float circleStdSizeDest;
	float circleStdMaxSizeDest;
	
	unsigned char bgFrameTrans;
	unsigned char bgFrameTransDest;
	unsigned char bgFrameTransLight;
	unsigned char bgTrans;
	unsigned char bgTransDest;
	unsigned char bgTransLight;
	float bgX;
	float bgY;
	float bgFrameDownY;
	float bgFrameUpY;
	float bgMainY;
	float bgWidth;
	float bgHeight;
	float bgWidthDest;
	float bgMainHeight;
	float bgFrameHeight;

	float txtX;
	float txtAlbumY;
	float txtPerformerY;
	float txtTitleY;

	unsigned char txtTrans;
	
	int stage;
		int thirdStage;
	unsigned panelCurrentTime;
	unsigned stageOffset;
	unsigned stagePointOffset;
	unsigned stateTime;
	
	unsigned IN_DURATION;
		unsigned ANIMATION_IN_0_DURATION;
		unsigned ANIMATION_IN_1_DURATION;
		unsigned ANIMATION_IN_2_DURATION;
			unsigned ANIMATION_IN_2_ZOOM_IN_DURATION;
		unsigned ANIMATION_IN_3_DURATION;
			unsigned ANIMATION_IN_3_0_DURATION;
			unsigned ANIMATION_IN_3_1_DURATION;
		unsigned ANIMATION_IN_4_DURATION;
		unsigned ANIMATION_IN_5_DURATION;
		unsigned ANIMATION_IN_6_DURATION;
	unsigned MAIN_DURATION;
	unsigned ORIGIN_ST_DURATION;
		unsigned ANIMATION_ORIGIN_ST_0_DURATION;
		unsigned ANIMATION_ORIGIN_ST_1_DURATION;
		unsigned ANIMATION_ORIGIN_ST_2_DURATION;
		unsigned ANIMATION_ORIGIN_ST_3_DURATION;
		unsigned ANIMATION_ORIGIN_ST_4_DURATION;
		unsigned ANIMATION_ORIGIN_ST_5_DURATION;
		unsigned ANIMATION_ORIGIN_ST_6_DURATION;
		unsigned ANIMATION_ORIGIN_ST_7_DURATION;
		unsigned ANIMATION_ORIGIN_ST_8_DURATION;
	unsigned OUT_DURATION;
		unsigned ANIMATION_OUT_0_DURATION;
		unsigned ANIMATION_OUT_1_DURATION;
		unsigned ANIMATION_OUT_2_DURATION;
		unsigned ANIMATION_OUT_3_DURATION;
		unsigned ANIMATION_OUT_4_DURATION;
		unsigned ANIMATION_OUT_5_DURATION;
	unsigned PANEL_DURATION;
		unsigned PANEL_STD_DURATION;
		unsigned PANEL_WITH_ORIGIN_DURATION;

	std::string album;
	std::string origin;
	std::string performer;
	std::string title;
	
	void drawCircle();
	void drawHighlight();
	void drawText();
	void reset();
	void setState(TraxInfoPanelState panelState);
public:
	TraxInfoPanel(Game2048Core *gameCore);
	~TraxInfoPanel();
	
	void draw();
	void setData(std::string album, std::string origin, std::string performer, std::string title);
	void startDrawing();
	void update(unsigned elapsedTime);
};