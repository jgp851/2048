#include "Playlist.h"
#include "MusicPlayerCore.h"
#include "..\Game2048Core.h"
#include "..\..\..\..\..\jgp-std-math\jgp-std-math\JGPstdMath.h" // DLL jgp-std-math
#include "..\..\..\..\..\jgm-rdr\jgm-rdr\jgmFileReader.h" // DLL jgm-rdr
#include <allegro5\allegro_memfile.h>

Playlist::Playlist(Game2048Core *gameCore, MusicPlayerCore *playerCore, std::string filePath)
{
	this->gameCore = gameCore;
	this->playerCore = playerCore;
	this->filePath = filePath;
	getJGMfileInstance();
	percentLastList = 0.4;
	lastSongPlayedTotalCount = songCount * percentLastList;
	if(lastSongPlayedTotalCount == 0)
		lastSongPlayedTotalCount++;
}

Playlist::~Playlist()
{
	if(jgmFile)
		delete jgmFile;
}

ALLEGRO_AUDIO_STREAM* Playlist::createAudioStream(int id)
{
	const char *fileBuffer = jgmFile->getFileBuffer(id);
	unsigned int fileLength = jgmFile->getFileLength(id);
	const char *fileType = jgmFile->getFileType(id);

	ALLEGRO_FILE *alFile = al_open_memfile((void*)fileBuffer, fileLength, "rb");
	ALLEGRO_AUDIO_STREAM *songStream = al_load_audio_stream_f(alFile, fileType, 4, 2048);
	unsigned int duration = al_get_audio_stream_length_secs(songStream) * 1000;
	jgmFile->setDuration(id, duration);

	return songStream;
}

ALLEGRO_AUDIO_STREAM* Playlist::getAudioStream()
{
	return createAudioStream(playedSong);
}

ALLEGRO_AUDIO_STREAM* Playlist::getAudioStream(unsigned song)
{
	return createAudioStream(song);
}

const char* Playlist::getAlbum()
{
	return jgmFile->getAlbum(playedSong);
}

const char* Playlist::getOrigin()
{
	return jgmFile->getOrigin(playedSong);
}

const char* Playlist::getPerformer()
{
	return jgmFile->getPerformer(playedSong);
}

const char* Playlist::getPlaylistTitle()
{
	return jgmFile->getPlaylistTitle();
}

const char* Playlist::getTitle()
{
	return jgmFile->getTitle(playedSong);
}

const char* Playlist::getAlbumOf(unsigned song)
{
	return jgmFile->getAlbum(song);
}

const char* Playlist::getOriginOf(unsigned song)
{
	return jgmFile->getOrigin(song);
}

const char* Playlist::getPerformerOf(unsigned song)
{
	return jgmFile->getPerformer(song);
}

const char* Playlist::getTitleOf(unsigned song)
{
	return jgmFile->getTitle(song);
}

bool Playlist::haveAlbum()
{
	if(strlen(jgmFile->getAlbum(playedSong)))
		return true;
	return false;
}

bool Playlist::haveOrigin()
{
	if(strlen(jgmFile->getOrigin(playedSong)))
		return true;
	return false;
}

bool Playlist::havePerformer()
{
	if(strlen(jgmFile->getPerformer(playedSong)))
		return true;
	return false;
}

bool Playlist::songIsNotLast(unsigned id)
{
	for(unsigned i = 0; i < lastPlayed.size(); i++)
		if(lastPlayed[i] == id)
			return false;
	return true;
}

unsigned Playlist::getDuration()
{
	return jgmFile->getDuration(playedSong);
}

unsigned Playlist::getSongCount()
{
	return jgmFile->getSongCount();
}

unsigned Playlist::getSongID()
{
	return playedSong;
}

void Playlist::getJGMfileInstance()
{
	jgmFile = playerCore->getJGMfile(filePath.c_str());
	songCount = getSongCount();
}

void Playlist::nextSong()
{
	playedSong++;
	if(playedSong == songCount)
		playedSong = 0;
}

void Playlist::playSong(unsigned id)
{
	playedSong = id;
}

void Playlist::randSong()
{
	if(songCount > 1)
	{
		unsigned random = rand() % (songCount - lastPlayed.size());
		for(playedSong = 0; playedSong < songCount; playedSong++)
		{
			if(songIsNotLast(playedSong))
			{
				if(random == 0)
					break;
				random--;
			}
		}
		if(lastPlayed.size() < lastSongPlayedTotalCount)
			lastPlayed.push_back(playedSong);
		else
		{
			if(lastPlayedPnt == lastPlayed.size())
				lastPlayedPnt = 0;
			lastPlayed[lastPlayedPnt] = playedSong;
			lastPlayedPnt++;
		}
	}
	else
		playedSong = 0;
}

void Playlist::reset()
{
	lastPlayedPnt = 0;
	playedSong = -1;
	lastPlayed.clear();
}