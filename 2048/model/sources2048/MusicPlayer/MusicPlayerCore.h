#pragma once
#include <vector>
#include <string>
#undef UNICODE // LPCSTR dzia�a zar�wno w DEBUG i RELEASE, bez tego nie �adowa�o jgm-rdr.dll
#include <windows.h>
#include <allegro5\allegro_audio.h>

class Game2048Core;
class Playlist;
class TraxInfoPanel;
class JGMdllInterface;
class JGMfile;

class MusicPlayerCore
{
private:
	Game2048Core *gameCore;

	HINSTANCE hDLL;
	JGMdllInterface *jgmFileReader;
	
	ALLEGRO_AUDIO_STREAM *musicStream;
	TraxInfoPanel *traxInfoPanel;
	std::vector<Playlist*> playlists;
	std::vector<const char*> *JGMfiles;

	bool *isRandomized;
	bool isTurned;
	bool *gameMusicTurned;
	bool *menuMusicTurned;
	float *gameVolume;
	float *menuVolume;
	float *actualVolume;
	float pan;
	float speed;
	float maxVolume;
	float minVolume;
	unsigned currentTime;
	unsigned playlistCount;
	unsigned selectedPlaylist;
	
	bool isSongEnd();

	void checkGameTurned();
	void checkMenuTurned();
	void checkGameTurnedOff();
	void checkMenuTurnedOff();
	void checkGameTurnedOn();
	void checkMenuTurnedOn();
	void chooseNewSong();
	void nextSong();
	void playNewSong();
	void randomSong();
	void updateSongData();
	void updateTermsGameState();

	bool isAnyPlaylist;
	
	void createDLLinstance();
	void initPlaylist();
public:
	MusicPlayerCore(Game2048Core *gameCore);
	~MusicPlayerCore();

	JGMfile* getJGMfile(const char *filePath);
	
	const char* getAlbumOf(unsigned playlist, unsigned song);
	const char* getOriginOf(unsigned playlist, unsigned song);
	const char* getPerformerOf(unsigned playlist, unsigned song);
	const char* getTitleOf(unsigned playlist, unsigned song);
	const char* getTitleSelectedPlaylist();

	bool getGameTurned();
	bool getMenuTurned();

	double getMusicStreamLength(unsigned playlist, unsigned song);

	float* getGameVolumeAddress();
	float* getMenuVolumeAddress();
	float getGameVolumeValue();
	float getMenuVolumeValue();

	unsigned getCurrentTime();
	unsigned getDurationCurrentSong();
	unsigned getPlaylistCount();
	unsigned getPlaySong();
	unsigned getSelectedPlaylist();
	unsigned getTotalSongPlaylist(unsigned id);
	unsigned getTotalSongSelectedPlaylist();

	bool isAnyMusicPlaylist();
	void createPlaylist(std::string filePath);
	void draw();
	void playPlaylist();
	void playPlaylist(unsigned id);
	void playSong(unsigned song);
	void seekMusic(double time); // przesuni�cie w czasie wyra�ane w sekundach
	void setGameTurned(bool state);
	void setMenuTurned(bool state);
	void setGameVolume(float volume);
	void setMenuVolume(float volume);
	void playNextPlaylist();
	void playPreviousPlaylist();
	void setPlaylist(unsigned id);
	void stopPlaylist();
	void update(unsigned elapsedTime);
};