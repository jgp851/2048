#include "MusicPlayerCore.h"
#include "Playlist.h"
#include "TraxInfoPanel\TraxInfoPanel.h"
#include "..\Game2048Core.h"
#include "..\GameSettings.h"
#include "..\..\..\..\..\JGP.iostream\JGP.iostream\jgp.iostream.h" // DLL JGP.iostream
#include "..\..\..\..\..\jgm-rdr\jgm-rdr\jgmFileReader.h" // DLL jgm-rdr
#include <allegro5/allegro_native_dialog.h>

MusicPlayerCore::MusicPlayerCore(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->traxInfoPanel = new TraxInfoPanel(gameCore);
	this->isRandomized = &gameCore->gameSettings->musicPlayerRandomized;
	this->isTurned = false;
	this->playlistCount = 0;
	this->selectedPlaylist = 0;
	this->isAnyPlaylist = false;
	gameMusicTurned = &gameCore->gameSettings->gameMusicTurned;
	menuMusicTurned = &gameCore->gameSettings->menuMusicTurned;
	gameVolume = &gameCore->gameSettings->musicGameVolume;
	menuVolume = &gameCore->gameSettings->musicMenuVolume;
	actualVolume = menuVolume;
	pan = 0;
	speed = 1;
	maxVolume = 0.7;
	minVolume = 0;
	musicStream = NULL;
	currentTime = 0;
	
	createDLLinstance();
	initPlaylist();
	setPlaylist(gameCore->gameSettings->selectedPlaylist);
}

MusicPlayerCore::~MusicPlayerCore()
{
	if(musicStream)
		al_destroy_audio_stream(musicStream);
	for(std::vector<Playlist*>::iterator it = playlists.begin(); it != playlists.end(); it++)
		delete (*it);
	delete traxInfoPanel;
	if(hDLL)
		FreeLibrary(hDLL);
}

JGMfile* MusicPlayerCore::getJGMfile(const char *filePath)
{
	return jgmFileReader->loadFile(filePath);
}

const char* MusicPlayerCore::getAlbumOf(unsigned playlist, unsigned song)
{
	return playlists[playlist]->getAlbumOf(song);
}

const char* MusicPlayerCore::getOriginOf(unsigned playlist, unsigned song)
{
	return playlists[playlist]->getOriginOf(song);
}

const char* MusicPlayerCore::getPerformerOf(unsigned playlist, unsigned song)
{
	return playlists[playlist]->getPerformerOf(song);
}

const char* MusicPlayerCore::getTitleOf(unsigned playlist, unsigned song)
{
	return playlists[playlist]->getTitleOf(song);
}

const char* MusicPlayerCore::getTitleSelectedPlaylist()
{
	return playlists[selectedPlaylist]->getPlaylistTitle();
}

bool MusicPlayerCore::getGameTurned()
{
	return *gameMusicTurned;
}

bool MusicPlayerCore::getMenuTurned()
{
	return *menuMusicTurned;
}

bool MusicPlayerCore::isAnyMusicPlaylist()
{
	return isAnyPlaylist;
}

bool MusicPlayerCore::isSongEnd()
{
	if(currentTime >= playlists[selectedPlaylist]->getDuration())
		return true;
	return false;
}

double MusicPlayerCore::getMusicStreamLength(unsigned playlist, unsigned song)
{
	return al_get_audio_stream_length_secs(playlists[playlist]->getAudioStream(song));
}

float* MusicPlayerCore::getGameVolumeAddress()
{
	return gameVolume;
}

float* MusicPlayerCore::getMenuVolumeAddress()
{
	return menuVolume;
}

float MusicPlayerCore::getGameVolumeValue()
{
	return *gameVolume;
}

float MusicPlayerCore::getMenuVolumeValue()
{
	return *menuVolume;
}

unsigned MusicPlayerCore::getCurrentTime()
{
	return currentTime;
}

unsigned MusicPlayerCore::getDurationCurrentSong()
{
	return playlists[selectedPlaylist]->getDuration();
}

unsigned MusicPlayerCore::getPlaylistCount()
{
	return playlistCount;
}

unsigned MusicPlayerCore::getPlaySong()
{
	return playlists[selectedPlaylist]->getSongID();
}

unsigned MusicPlayerCore::getSelectedPlaylist()
{
	return selectedPlaylist;
}

unsigned MusicPlayerCore::getTotalSongPlaylist(unsigned id)
{
	return playlists[id]->getSongCount();
}

unsigned MusicPlayerCore::getTotalSongSelectedPlaylist()
{
	return playlists[selectedPlaylist]->getSongCount();
}

void MusicPlayerCore::checkGameTurned()
{
	checkGameTurnedOff();
	checkGameTurnedOn();
}

void MusicPlayerCore::checkMenuTurned()
{
	checkMenuTurnedOff();
	checkMenuTurnedOn();
}

void MusicPlayerCore::checkGameTurnedOff()
{
	switch(gameCore->getGameState())
	{
	case GAME: case GAME_PAUSE: case GAME_END: case GAME_WIN:
		if(isTurned && !*gameMusicTurned)
			stopPlaylist();
		break;
	}
}

void MusicPlayerCore::checkMenuTurnedOff()
{
	switch(gameCore->getGameState())
	{
	case MAIN_MENU: case NEW_GAME: case LOAD_GAME: case GAME_OPTIONS:
		if(isTurned && !*menuMusicTurned)
			stopPlaylist();
		break;
	}
}

void MusicPlayerCore::checkGameTurnedOn()
{
	switch(gameCore->getGameState())
	{
	case GAME: case GAME_PAUSE: case GAME_END: case GAME_WIN:
		if(!isTurned && *gameMusicTurned)
			playPlaylist();
		break;
	}
}

void MusicPlayerCore::checkMenuTurnedOn()
{
	switch(gameCore->getGameState())
	{
	case MAIN_MENU: case NEW_GAME: case LOAD_GAME: case GAME_OPTIONS:
		if(!isTurned && *menuMusicTurned)
			playPlaylist();
		break;
	}
}

void MusicPlayerCore::chooseNewSong()
{
	if(*isRandomized)
		randomSong();
	else
		nextSong();
	updateSongData();
}

void MusicPlayerCore::createDLLinstance()
{
	hDLL = LoadLibrary((LPCSTR)"jgm-rdr.dll");
	if(hDLL == NULL)
		al_show_native_message_box(gameCore->alManager->getDisplay(),
			"Error", "No DLL file!", "\"jgm-rdr.dll\"",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
	CREATE_JGM_INTERFACE pEntryFunction = (CREATE_JGM_INTERFACE)GetProcAddress(hDLL,"CreateJGMreaderObject");
	jgmFileReader = pEntryFunction();
}

void MusicPlayerCore::createPlaylist(std::string filePath)
{
	playlists.push_back(new Playlist(gameCore, this, filePath));
	playlistCount++;
}

void MusicPlayerCore::draw()
{
	if(isTurned)
		traxInfoPanel->draw();
}

void MusicPlayerCore::initPlaylist()
{
	JGMfiles = jgp_iostr_fileListing("music\\", true);
	if(!JGMfiles->empty())
	{
		for(unsigned i = 0; i < JGMfiles->size(); i++)
			createPlaylist((*JGMfiles)[i]);
		isAnyPlaylist = true;
	}
}

void MusicPlayerCore::nextSong()
{
	playlists[selectedPlaylist]->nextSong();
	playNewSong();
}

void MusicPlayerCore::playPlaylist()
{
	// Je�li mamy wczytan� min jedn� playlist�, z za�o�enia odtwarzacz jest uruchamiany.
	// Jednak poni�sze dwie funkcje sprawdzaj� czy rzeczywi�cie mo�e by� on uruchomiony w zale�no�ci od gameState.
	if(isAnyPlaylist)
	{
		isTurned = true;
		checkGameTurnedOff();
		checkMenuTurnedOff();
		if(isTurned)
		{
			currentTime = 0;
			playlists[selectedPlaylist]->reset();
			chooseNewSong();
		}
	}
}

void MusicPlayerCore::playPlaylist(unsigned id)
{
	if(isAnyPlaylist) // je�li istniej� jakie� wczytane playlisty wtedy mo�na je zmieni�
	{
		selectedPlaylist = id;
		playPlaylist();
	}
}

void MusicPlayerCore::playNewSong()
{
	if(musicStream)
		al_destroy_audio_stream(musicStream);
	musicStream = playlists[selectedPlaylist]->getAudioStream();
	al_set_audio_stream_speed(musicStream, speed);
	al_set_audio_stream_gain(musicStream, *actualVolume);
	al_set_audio_stream_pan(musicStream, pan);
	al_set_audio_stream_playmode(musicStream, ALLEGRO_PLAYMODE_ONCE);
	
	al_register_event_source(gameCore->alManager->getAllegroEventQueue(), al_get_audio_stream_event_source(musicStream));
	al_attach_audio_stream_to_mixer(musicStream, gameCore->alManager->getAllegroMixer());
}

void MusicPlayerCore::playNextPlaylist()
{
	selectedPlaylist++;
	if(selectedPlaylist == playlistCount)
		selectedPlaylist = 0;
	playPlaylist(selectedPlaylist);
}

void MusicPlayerCore::playPreviousPlaylist()
{
	if(selectedPlaylist == 0)
		selectedPlaylist = playlistCount - 1;
	else
		selectedPlaylist--;
	playPlaylist(selectedPlaylist);
}

void MusicPlayerCore::playSong(unsigned song)
{
	isTurned = true;
	checkGameTurnedOff();
	checkMenuTurnedOff();
	if(isTurned)
	{
		currentTime = 0;
		playlists[selectedPlaylist]->reset();
		playlists[selectedPlaylist]->playSong(song);
		playNewSong();
		updateSongData();
	}
}

void MusicPlayerCore::randomSong()
{
	playlists[selectedPlaylist]->randSong();
	playNewSong();
}

void MusicPlayerCore::seekMusic(double time)
{
	al_seek_audio_stream_secs(musicStream, time);
	currentTime = time * 1000;
}

void MusicPlayerCore::setGameTurned(bool state)
{
	*gameMusicTurned = state;
}

void MusicPlayerCore::setMenuTurned(bool state)
{
	*menuMusicTurned = state;
}

void MusicPlayerCore::setGameVolume(float volume)
{
	*gameVolume = volume;
}

void MusicPlayerCore::setMenuVolume(float volume)
{
	*menuVolume = volume;
}

void MusicPlayerCore::setPlaylist(unsigned id)
{
	if(getPlaylistCount() - 1 >= id)
		selectedPlaylist = id;
}

void MusicPlayerCore::stopPlaylist()
{
	if(musicStream)
		al_detach_audio_stream(musicStream);
	isTurned = false;
}

void MusicPlayerCore::update(unsigned elapsedTime)
{
	updateTermsGameState();
	if(isTurned)
	{
		currentTime += elapsedTime;
		traxInfoPanel->update(elapsedTime);
		if(isSongEnd())
		{
			currentTime %= playlists[selectedPlaylist]->getDuration();
			chooseNewSong();
		}
	}
}

void MusicPlayerCore::updateSongData()
{
	traxInfoPanel->setData(playlists[selectedPlaylist]->getAlbum(), playlists[selectedPlaylist]->getOrigin(), playlists[selectedPlaylist]->getPerformer(), playlists[selectedPlaylist]->getTitle());
	traxInfoPanel->startDrawing();
}

void MusicPlayerCore::updateTermsGameState()
{
	switch(gameCore->getGameState())
	{
	case MAIN_MENU: case NEW_GAME: case LOAD_GAME: case GAME_OPTIONS:
		checkMenuTurned();
		actualVolume = menuVolume;
		break;
	case GAME: case GAME_PAUSE: case GAME_END: case GAME_WIN:
		checkGameTurned();
		actualVolume = gameVolume;
		break;
	default: actualVolume = menuVolume; break;
	}
	if(musicStream)
		al_set_audio_stream_gain(musicStream, *actualVolume);
}