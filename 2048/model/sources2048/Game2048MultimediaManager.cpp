#include "Game2048MultimediaManager.h"
#include "Game2048Core.h"

Game2048MultimediaManager::Game2048MultimediaManager(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	loadMultimedia();
	createTexturesTemplates();
}
Game2048MultimediaManager::~Game2048MultimediaManager()
{
}

void Game2048MultimediaManager::createTexturesTemplates()
{
	//tekstury wbudowane
	texturesDefault.push_back(PNG_textDef1);
	texturesDefault.push_back(PNG_textDef2);
	texturesDefault.push_back(PNG_textDef3);
	texturesDefault.push_back(PNG_textDef4);
	texturesDefault.push_back(PNG_textDef5);
	texturesDefault.push_back(PNG_textDef6);
	texturesDefault.push_back(PNG_textDef7);
	texturesHexDefault.push_back(PNG_textHexDef1);
	texturesHexDefault.push_back(PNG_textHexDef2);
	texturesHexDefault.push_back(PNG_textHexDef3);
	texturesHexDefault.push_back(PNG_textHexDef4);
	texturesHexDefault.push_back(PNG_textHexDef5);

	//tekstury u�ytkownika
	char path[256];
	unsigned char path_len;

	ifstream fileTexturesLocations("customTextures.ini", ios::in | ios::binary);
	if(fileTexturesLocations)
	{
		while(fileTexturesLocations.read((char*)&path_len, 1))
		{
			fileTexturesLocations.read(path, path_len);
			path[path_len] = '\0';

			customTexturesDirectories.push_back(path); //zapisuje �cie�ki do plik�w w wektorze string
			texturesCustom.push_back(gameCore->bitmapLoader->loadBitmap(path)); //zapisuje bitmapy w wektorze bitmap
		}
	}
	fileTexturesLocations.close();
	ifstream fileTexturesHexLocations("customHexTextures.ini", ios::in | ios::binary);
	if(fileTexturesHexLocations)
	{
		while(fileTexturesHexLocations.read((char*)&path_len, 1))
		{
			fileTexturesHexLocations.read(path, path_len);
			path[path_len] = '\0';

			customTexturesHexDirectories.push_back(path); //zapisuje �cie�ki do plik�w w wektorze string
			texturesHexCustom.push_back(gameCore->bitmapLoader->loadBitmap(path)); //zapisuje bitmapy w wektorze bitmap
		}
	}
	fileTexturesHexLocations.close();
}

void Game2048MultimediaManager::loadMultimedia()
{
	PNG_buttonPlay = gameCore->bitmapLoader->loadBitmap("gfx/button-play.png");
	PNG_gameTitle = gameCore->bitmapLoader->loadBitmap("gfx/title.png");
	PNG_lightOff = gameCore->bitmapLoader->loadBitmap("gfx/light-off.png");
	PNG_lightOn = gameCore->bitmapLoader->loadBitmap("gfx/light-on.png");
	PNG_pauseBg = gameCore->bitmapLoader->loadBitmap("gfx/bg-pause.png");
	// Button
	PNG_buttonMainLeft = gameCore->bitmapLoader->loadBitmap("gfx/button-option-main-left.png");
	PNG_buttonMainMiddle = gameCore->bitmapLoader->loadBitmap("gfx/button-option-main-middle.png");
	PNG_buttonMainRight = gameCore->bitmapLoader->loadBitmap("gfx/button-option-main-right.png");
	PNG_buttonMainLeftPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-option-main-left-pressed.png");
	PNG_buttonMainMiddlePressed = gameCore->bitmapLoader->loadBitmap("gfx/button-option-main-middle-pressed.png");
	PNG_buttonMainRightPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-option-main-right-pressed.png");
	PNG_buttonLeft = gameCore->bitmapLoader->loadBitmap("gfx/button-option-left.png");
	PNG_buttonLeftPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-option-left-pressed.png");
	PNG_buttonLeftIcon = gameCore->bitmapLoader->loadBitmap("gfx/button-option-left-icon.png");
	PNG_buttonRight = gameCore->bitmapLoader->loadBitmap("gfx/button-option-right.png");
	PNG_buttonRightPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-option-right-pressed.png");
	PNG_buttonRightIcon = gameCore->bitmapLoader->loadBitmap("gfx/button-option-right-icon.png");
	// GameScrollingBackground
	PNG_scrollingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg.png");
	// MainMenuRotatingBackground
		// PNG_mainMenuRotatingBgBackground
		if(gameCore->appSettings->getScreenWidth() == 640)
			PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-640.png");
		else if(gameCore->appSettings->getScreenWidth() == 800)
			PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-800.png");
		else if(gameCore->appSettings->getScreenWidth() == 1024)
			PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1024.png");
		else if(gameCore->appSettings->getScreenWidth() == 1280)
		{
			if(gameCore->appSettings->getScreenHeight() == 800)
				PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1280-800.png");
			if(gameCore->appSettings->getScreenHeight() == 960)
				PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1280-960.png");
			if(gameCore->appSettings->getScreenHeight() == 1024)
				PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1280-1024.png");
		}
		else if (gameCore->appSettings->getScreenWidth() == 1366)
			PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1366.png");
		else if (gameCore->appSettings->getScreenWidth() == 1600)
			PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1600.png");
		else
			PNG_mainMenuRotatingBgBackground = gameCore->bitmapLoader->loadBitmap("gfx/bg-rotate-1920.png");
	// MusicPlayer
	PNG_playerBg = gameCore->bitmapLoader->loadBitmap("gfx/player-bg.png");
	// KeyboardButton
	PNG_buttonKbDelete = gameCore->bitmapLoader->loadBitmap("gfx/button-delete.png");
	PNG_buttonKbComma = gameCore->bitmapLoader->loadBitmap("gfx/button-comma.png");
	PNG_buttonKbFullstop = gameCore->bitmapLoader->loadBitmap("gfx/button-fullstop.png");
	PNG_buttonKbMinus = gameCore->bitmapLoader->loadBitmap("gfx/button-minus.png");
	PNG_buttonKbPlus = gameCore->bitmapLoader->loadBitmap("gfx/button-plus.png");
	PNG_buttonKbDeletePressed = gameCore->bitmapLoader->loadBitmap("gfx/button-delete-pressed.png");
	PNG_buttonKbCommaPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-comma-pressed.png");
	PNG_buttonKbFullstopPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-fullstop-pressed.png");
	PNG_buttonKbMinusPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-minus-pressed.png");
	PNG_buttonKbPlusPressed = gameCore->bitmapLoader->loadBitmap("gfx/button-plus-pressed.png");
	// ScrollBar
	PNG_scrollBar_bar = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-bar.png");
	PNG_scrollBar_barBottom = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-bar-bottom.png");
	PNG_scrollBar_barMiddle = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-bar-middle.png");
	PNG_scrollBar_barTop = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-bar-top.png");
	PNG_scrollBar_buttonArrowheadDown = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-button-arrowhead-down.png");
	PNG_scrollBar_buttonArrowheadUp = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-button-arrowhead-up.png");
	PNG_scrollBar_buttonDown = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-button-down.png");
	PNG_scrollBar_buttonUp = gameCore->bitmapLoader->loadBitmap("gfx/scrollbar-button-up.png");
	// TextBox
	PNG_textBgBottom = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-bottom.png");
	PNG_textBgLeft = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-left.png");
	PNG_textBgMiddle = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-middle.png");
	PNG_textBgRight = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-right.png");
	PNG_textBgTop = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-top.png");
	PNG_textBgCorner_ne = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-corner-ne.png");
	PNG_textBgCorner_nw = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-corner-nw.png");
	PNG_textBgCorner_se = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-corner-se.png");
	PNG_textBgCorner_sw = gameCore->bitmapLoader->loadBitmap("gfx/text-bg-corner-sw.png");
	// tekstury
	PNG_textDef1 = gameCore->bitmapLoader->loadBitmap("gfx/textures/abstraction-flower.png");
	PNG_textDef2 = gameCore->bitmapLoader->loadBitmap("gfx/textures/honeycomb.png");
	PNG_textDef3 = gameCore->bitmapLoader->loadBitmap("gfx/textures/military-material.png");
	PNG_textDef4 = gameCore->bitmapLoader->loadBitmap("gfx/textures/rock1.png");
	PNG_textDef5 = gameCore->bitmapLoader->loadBitmap("gfx/textures/rock2.png");
	PNG_textDef6 = gameCore->bitmapLoader->loadBitmap("gfx/textures/steel-surface.png");
	PNG_textDef7 = gameCore->bitmapLoader->loadBitmap("gfx/textures/textile.png");
	PNG_textHexDef1 = gameCore->bitmapLoader->loadBitmap("gfx/textures/textile-hex.png");
	PNG_textHexDef2 = gameCore->bitmapLoader->loadBitmap("gfx/textures/honeycomb-hex.png");
	PNG_textHexDef3 = gameCore->bitmapLoader->loadBitmap("gfx/textures/glass-hex.png");
	PNG_textHexDef4 = gameCore->bitmapLoader->loadBitmap("gfx/textures/water-hex.png");
	PNG_textHexDef5 = gameCore->bitmapLoader->loadBitmap("gfx/textures/metalic-bg-hex.png");
	PNG_textureHighlightHex = gameCore->bitmapLoader->loadBitmap("gfx/textureHighlightHex.png");
	// TraxInfoPanel
	PNG_traxPanelCircle = gameCore->bitmapLoader->loadBitmap("gfx/trax_panel_circle.png");

	SMP_goIn = gameCore->soundLoader->loadSound("gfx/go-in.ogg");
	SMP_goOut = gameCore->soundLoader->loadSound("gfx/go-out.ogg");
	SMP_switchButtons = gameCore->soundLoader->loadSound("gfx/switch-buttons.ogg");
	SMP_switchMenu = gameCore->soundLoader->loadSound("gfx/switch-menu.ogg");
}