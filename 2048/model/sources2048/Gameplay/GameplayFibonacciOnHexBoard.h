#pragma once
#include "HexagonBoard.h"

class Game2048Core;

class GameplayFibonacciOnHexBoard : public HexagonBoard
{
private:
	bool joinExistingSquares(int keyCode, bool haveAdd = true);
	void setSquareValue(int x, int y);
	void setWinningNumber();
public:
	GameplayFibonacciOnHexBoard(Game2048Core *gameCore);
	~GameplayFibonacciOnHexBoard();
};