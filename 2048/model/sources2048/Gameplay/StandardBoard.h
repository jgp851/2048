#pragma once
#include "GameplayCore.h"
#include <vector>

class Game2048Core;

class StandardBoard : public GameplayCore
{
private:
	bool isNoMovement();
	bool moveToYourDirection(int keyCode);
	void drawSquare(int x, int y);
	void setSquareFontSize();
	
	virtual bool joinExistingSquares(int keyCode, bool haveAdd = true) = 0;
	virtual void setSquareValue(int x, int y) = 0;
protected:
	bool connectExistingSquares(int keyCode, bool haveAdd = true);
	bool connectFibExistingSquares(int keyCode, bool haveAdd = true);
public:
	StandardBoard(Game2048Core *gameCore);
	virtual ~StandardBoard();
	
	Tile* loadCorrectTile(unsigned int x, unsigned int y);
	void createBoard(int size); // virtual
	void createBoard(int width, int height);
	void draw();
	void processInput(unsigned elapsedTime);
	void setTexture();
};