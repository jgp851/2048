#include "GameplayStandardOnHexBoard.h"
#include "Points\ScoresPower2.h"
#include "..\Game2048Core.h"

GameplayStandardOnHexBoard::GameplayStandardOnHexBoard(Game2048Core *gameCore) : HexagonBoard(gameCore)
{
	gameMode = GAME_MODE_STD;
	scores = new ScoresPower2(gameCore);
	continueGameAtEnd = true;
	initialNumber = 1;
	typeConnectValues = 0;
	setWinningNumber();
}
GameplayStandardOnHexBoard::~GameplayStandardOnHexBoard() {}

bool GameplayStandardOnHexBoard::joinExistingSquares(int keyCode, bool haveAdd)
{
	return HexagonBoard::connectExistingSquares(keyCode, haveAdd);
}

void GameplayStandardOnHexBoard::setSquareValue(int x, int y)
{
	GameplayCore::setSquareValuePower2(x, y);
}

void GameplayStandardOnHexBoard::setWinningNumber()
{
	winningNumber = gameCore->gameSettings->winningNumber;
}