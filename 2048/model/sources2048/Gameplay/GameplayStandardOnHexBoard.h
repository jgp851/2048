#pragma once
#include "HexagonBoard.h"

class Game2048Core;

class GameplayStandardOnHexBoard : public HexagonBoard
{
private:
	bool joinExistingSquares(int keyCode, bool haveAdd = true);
	void setSquareValue(int x, int y);
	void setWinningNumber();
public:
	GameplayStandardOnHexBoard(Game2048Core *gameCore);
	~GameplayStandardOnHexBoard();
};