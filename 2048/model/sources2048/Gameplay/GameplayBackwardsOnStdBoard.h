#pragma once
#include "StandardBoard.h"

class Game2048Core;

class GameplayBackwardsOnStdBoard : public StandardBoard
{
private:
	bool joinExistingSquares(int keyCode, bool haveAdd = true);
	void setSquareValue(int x, int y);
	void setWinningNumber();
public:
	GameplayBackwardsOnStdBoard(Game2048Core *gameCore);
	~GameplayBackwardsOnStdBoard();
};