#pragma once
#include <string>
#include <fstream>
#include <vector>
#include <allegro5\allegro_font.h>

#define POINTS_DRAW_TIME 1500
#define POINTS_SCROLL_IN_TIME 400
#define POINTS_SCROLL_SLOW_OUT_TIME 900
#define POINTS_SCROLL_OUT_TIME 200

class Game2048Core;
class Scores;

enum PointsState
{
	IN_SCROLLING,
	SLOW_OUT_SCROLLING,
	OUT_SCROLLING
};

class LastEarnedPoints
{
private:
	Game2048Core *gameCore;
	Scores *scores;
	PointsState pointsState;
	
	ALLEGRO_FONT *font;

	std::vector<unsigned> exponent;
	unsigned scoresNumber;
	std::string scoresText;
	
	float x;
	float y;
	unsigned drawTime;
	
	float scoresInOffset;
	float scrollInProgress;
	float startInPosition;
	float endInPosition;
	
	float scoresSlowOutOffset;
	float scrollSlowOutProgress;
	float startSlowOutPosition;
	float endSlowOutPosition;
	
	float scoresOutOffset;
	float scrollOutProgress;
	float startOutPosition;
	float endOutPosition;

	void setScoresString();
public:
	LastEarnedPoints(Game2048Core *gameCore, Scores *scores);
	~LastEarnedPoints();

	bool isActive;

	void draw();
	void load(std::ifstream &file);
	void reset();
	void resetScores();
	void save(std::ofstream &file);
	void setValues();
	void update(unsigned elapsedTime);

	friend class ScoresFibonacci;
	friend class ScoresPower2;
};