#include "Scores.h"
#include "LastEarnedPoints.h"
#include <sstream>

Scores::Scores()
{
	this->lastEarnedPoints = NULL;
	scoresNumber = 0;
}
Scores::Scores(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	lastEarnedPoints = new LastEarnedPoints(gameCore, this);
	scoresNumber = 0;
}
Scores::~Scores()
{
	delete lastEarnedPoints;
}

std::string Scores::getScoresString()
{
	std::stringstream scoresText;
	if(!exponent.empty())
	{
		for(unsigned i = 0; i < exponent.size(); i++)
		{
			scoresText << prefix << exponent[i] << suffix;
			if(i < exponent.size() - 1 || scoresNumber > 0)
				scoresText << " + ";
		}
	}
	if(exponent.empty() || (!exponent.empty() && scoresNumber > 0))
		scoresText << scoresNumber;

	return scoresText.str();
}

void Scores::draw()
{
	if(lastEarnedPoints->isActive)
		this->lastEarnedPoints->draw();
}

void Scores::load(std::ifstream &file)
{
	lastEarnedPoints->load(file);
}

void Scores::reset()
{
	lastEarnedPoints->reset();
	exponent.clear();
	scoresNumber = 0;
}

void Scores::resetLastPoints(bool state)
{
	if(state)
		lastEarnedPoints->resetScores();
}

void Scores::save(std::ofstream &file)
{
	lastEarnedPoints->save(file);
}

void Scores::setValues()
{
	lastEarnedPoints->setValues();
}

void Scores::update(unsigned elapsedTime)
{
	if(lastEarnedPoints->isActive)
		this->lastEarnedPoints->update(elapsedTime);
}