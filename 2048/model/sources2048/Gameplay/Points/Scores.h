#pragma once
#include <string>
#include <vector>

class Game2048Core;
class LastEarnedPoints;

class Scores
{
protected:
	Game2048Core *gameCore;
	LastEarnedPoints *lastEarnedPoints;
	std::vector<unsigned> exponent;
	std::string prefix;
	std::string suffix;
	unsigned scoresNumber;

	virtual void reorderExponents(std::vector<unsigned> *exponent) = 0;
public:
	Scores();
	Scores(Game2048Core *gameCore);
	~Scores();
	
	std::string getScoresString();
	void draw();
	void load(std::ifstream &file);
	void reset();
	void resetLastPoints(bool state);
	void save(std::ofstream &file);
	void setValues();
	void update(unsigned elapsedTime);
	virtual void addScores(unsigned number) = 0;

	friend class LastEarnedPoints;
	friend class Steps;
	friend class LoaderGame;
	friend class SaveGame;
};