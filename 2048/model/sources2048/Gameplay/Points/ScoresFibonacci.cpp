#include "ScoresFibonacci.h"
#include "LastEarnedPoints.h"
#include "..\..\Game2048Core.h"
#include <algorithm>

ScoresFibonacci::ScoresFibonacci() : Scores()
{
}
ScoresFibonacci::ScoresFibonacci(Game2048Core *gameCore) : Scores(gameCore)
{
	prefix = "fib(";
	suffix = ")";
}

void ScoresFibonacci::addScores(unsigned number)
{
	// sprawdzenie czy f(n) na planszy jest wi�ksze r�wne 48, je�li tak, naliczenie tylko f(n). Dla f(47) = 2971215073, f(48) = 4807526976
	if(number >= 48)
	{
		exponent.push_back(number);
		lastEarnedPoints->exponent.push_back(number);
	}
	else // w przeciwnym wypadku dodanie zwyk�ych punkt�w
	{
		// sprawdzenie czy po dodaniu punkt�w, suma b�dzie wi�ksza ni� 2^32-1
		unsigned tmp = scoresNumber + gameCore->fibonacci(number + 1);
		if(scoresNumber > tmp)
			exponent.push_back(48);
		tmp = lastEarnedPoints->scoresNumber + gameCore->fibonacci(number + 1);
		if(lastEarnedPoints->scoresNumber > tmp)
			lastEarnedPoints->exponent.push_back(48);
		// dodanie zwyk�ych punkt�w
		scoresNumber += gameCore->fibonacci(number + 1);
		lastEarnedPoints->scoresNumber += gameCore->fibonacci(number + 1);
	}

	if(!exponent.empty())
		reorderExponents(&exponent);
	if(!lastEarnedPoints->exponent.empty())
		reorderExponents(&lastEarnedPoints->exponent);
}

void ScoresFibonacci::reorderExponents(std::vector<unsigned> *exponent)
{
	// sortowanie
	std::sort(exponent->begin(), exponent->end());
	// sumowanie wyraz�w podobnych
	bool order = true;
	while(order)
	{
		order = false;
		for(unsigned i = 0; i < exponent->size() - 1; i++)
			if((*exponent)[i] == (*exponent)[i + 1] - 1)
			{
				(*exponent)[i]++;
				exponent->erase(exponent->begin() + i + 1);
				order = true;
			}
	}
}