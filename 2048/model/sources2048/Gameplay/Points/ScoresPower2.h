#pragma once
#include <string>
#include <vector>
#include "Scores.h"

class Game2048Core;

class ScoresPower2 : public Scores
{
private:
	void reorderExponents(std::vector<unsigned> *exponent);
public:
	ScoresPower2();
	ScoresPower2(Game2048Core *gameCore);
	~ScoresPower2();

	void addScores(unsigned number);
};