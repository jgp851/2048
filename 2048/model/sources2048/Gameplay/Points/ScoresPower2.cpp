#include "ScoresPower2.h"
#include "LastEarnedPoints.h"
#include "..\..\Game2048Core.h"
#include <algorithm>

ScoresPower2::ScoresPower2() : Scores()
{
}
ScoresPower2::ScoresPower2(Game2048Core *gameCore) : Scores(gameCore)
{
	prefix = "2^";
}

void ScoresPower2::addScores(unsigned number)
{
	// sprawdzenie czy wyk�adnich liczby na planszy jest wi�kszy r�wny 32, je�li tak, naliczenie tylko pot�gi
	if(number >= 32)
	{
		exponent.push_back(number);
		lastEarnedPoints->exponent.push_back(number);
	}
	else // w przeciwnym wypadku dodanie zwyk�ych punkt�w
	{
		// sprawdzenie czy po dodaniu punkt�w, suma b�dzie wi�ksza ni� 2^32-1
		unsigned tmp = scoresNumber + pow(2.0, static_cast<int>(number));
		if(scoresNumber > tmp)
			exponent.push_back(32);
		tmp = lastEarnedPoints->scoresNumber + pow(2.0, static_cast<int>(number));
		if(lastEarnedPoints->scoresNumber > tmp)
			lastEarnedPoints->exponent.push_back(32);
		// dodanie zwyk�ych punkt�w
		scoresNumber += pow(2.0, static_cast<int>(number));
		lastEarnedPoints->scoresNumber += pow(2.0, static_cast<int>(number));
	}

	if(!exponent.empty())
		reorderExponents(&exponent);
	if(!lastEarnedPoints->exponent.empty())
		reorderExponents(&lastEarnedPoints->exponent);
}

void ScoresPower2::reorderExponents(std::vector<unsigned> *exponent)
{
	// sortowanie
	std::sort(exponent->begin(), exponent->end());
	// sumowanie wyraz�w podobnych
	bool order = true;
	while(order)
	{
		order = false;
		for(unsigned i = 0; i < exponent->size() - 1; i++)
			if((*exponent)[i] == (*exponent)[i + 1])
			{
				(*exponent)[i]++;
				exponent->erase(exponent->begin() + i + 1);
				order = true;
			}
	}
}