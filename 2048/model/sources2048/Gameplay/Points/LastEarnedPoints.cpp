#include "LastEarnedPoints.h"
#include "Scores.h"
#include "..\BoardDrawing\BoardDrawing.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\GameplayCore.h"
#include "..\Steps.h"
#include <sstream>

LastEarnedPoints::LastEarnedPoints(Game2048Core *gameCore, Scores *scores)
{
	this->gameCore = gameCore;
	this->scores = scores;

	font = gameCore->fontManager->font_lastEarnedPoints->font;

	isActive = false;
	scoresNumber = 0;

	endInPosition = gameCore->appSettings->getScreenWidth() * 0.5;
	startSlowOutPosition = endInPosition;
	endSlowOutPosition = endInPosition + gameCore->appSettings->getScreenWidth() * 0.1;
}
LastEarnedPoints::~LastEarnedPoints()
{
}

void LastEarnedPoints::draw()
{
	al_draw_text(font,
		al_map_rgb(255, 255, 255),
		x,
		y,
		ALLEGRO_ALIGN_CENTER,
		scoresText.c_str());
}

void LastEarnedPoints::load(std::ifstream &file)
{
	reset();
	file.read((char*)&isActive, sizeof(bool));

	if(isActive)
	{
		float percent;

		file.read((char*)&drawTime, sizeof(unsigned));
		file.read((char*)&scoresNumber, sizeof(unsigned));
		unsigned vecSize;
			file.read((char*)&vecSize, sizeof(unsigned));
			for(unsigned i = 0; i < vecSize; i++)
			{
				unsigned number;
				file.read((char*)&number, sizeof(unsigned));
				exponent.push_back(number);
			}
		setScoresString();

		file.read((char*)&percent, sizeof(float));
		x = percent * gameCore->appSettings->getScreenWidth() / 100;
		y = gameCore->game->boardDrawing->getY() / 2 - al_get_font_line_height(font) / 2;

		startInPosition =  0 - (al_get_text_width(font, scoresText.c_str()) / 2);
		endOutPosition = gameCore->appSettings->getScreenWidth() + (-startInPosition);

		file.read((char*)&pointsState, sizeof(PointsState));
		switch(pointsState)
		{
		case IN_SCROLLING:
			file.read((char*)&percent, sizeof(float));
			scrollInProgress = percent * gameCore->appSettings->getScreenWidth() / 100;
			break;
		case SLOW_OUT_SCROLLING:
			file.read((char*)&percent, sizeof(float));
			scrollSlowOutProgress = percent * gameCore->appSettings->getScreenWidth() / 100;
			break;
		case OUT_SCROLLING:
			file.read((char*)&percent, sizeof(float));
			scrollOutProgress = percent * gameCore->appSettings->getScreenWidth() / 100;
			file.read((char*)&startOutPosition, sizeof(float));
			startOutPosition = percent * gameCore->appSettings->getScreenWidth() / 100;
			break;
		}
	}
}

void LastEarnedPoints::reset()
{
	isActive = false;
	resetScores();

	scrollInProgress = 0;
	scrollSlowOutProgress = 0;
	scrollOutProgress = 0;

	y = gameCore->game->boardDrawing->getY() / 2 - al_get_font_line_height(font) / 2;
}

void LastEarnedPoints::resetScores()
{
	scoresNumber = 0;
	scoresText.clear();
	exponent.clear();
}

void LastEarnedPoints::save(std::ofstream &file)
{
	file.write((char*)&isActive, sizeof(bool));

	if(isActive)
	{
		float percent;

		file.write((char*)&drawTime, sizeof(unsigned));
		file.write((char*)&scoresNumber, sizeof(unsigned));
		unsigned vecSize = exponent.size();
			file.write((char*)&vecSize, sizeof(unsigned));
			for(unsigned i = 0; i < exponent.size(); i++)
			{
				unsigned number = exponent[i];
				file.write((char*)&number, sizeof(unsigned));
			}
		percent = x * 100 / gameCore->appSettings->getScreenWidth();
		file.write((char*)&percent, sizeof(float));

		file.write((char*)&pointsState, sizeof(PointsState));
		switch(pointsState)
		{
		case IN_SCROLLING:
			percent = scrollInProgress * 100 / gameCore->appSettings->getScreenWidth();
			file.write((char*)&percent, sizeof(float));
			break;
		case SLOW_OUT_SCROLLING:
			percent = scrollSlowOutProgress * 100 / gameCore->appSettings->getScreenWidth();
			file.write((char*)&percent, sizeof(float));
			break;
		case OUT_SCROLLING:
			percent = scrollOutProgress * 100 / gameCore->appSettings->getScreenWidth();
			file.write((char*)&percent, sizeof(float));
			percent = startOutPosition * 100 / gameCore->appSettings->getScreenWidth();
			file.write((char*)&startOutPosition, sizeof(float));
			break;
		}
	}
}

void LastEarnedPoints::setScoresString()
{
	std::stringstream scoresTextStream;
	scoresTextStream << "+ ";
	if(!exponent.empty())
		for(unsigned i = 0; i < exponent.size(); i++)
		{
			scoresTextStream << scores->prefix << exponent[i] << scores->suffix;
			if(i < exponent.size() - 1 || scoresNumber > 0)
				scoresTextStream << " + ";
		}
	if(scoresNumber > 0)
		scoresTextStream << scoresNumber;
	scoresText = scoresTextStream.str();
}

void LastEarnedPoints::setValues()
{
	// punkty
	setScoresString();
	
	// warto�ci obiektu
	isActive = true;
	pointsState = IN_SCROLLING;

	drawTime = 0;

	scrollInProgress = 0;
	startInPosition = 0 - (al_get_text_width(font, scoresText.c_str()) / 2);

	scrollSlowOutProgress = 0;
	
	scrollOutProgress = 0;
	endOutPosition = gameCore->appSettings->getScreenWidth() + (-startInPosition);

	x = startInPosition;
}

void LastEarnedPoints::update(unsigned elapsedTime)
{
	drawTime += elapsedTime;

	if(drawTime >= POINTS_DRAW_TIME)
		isActive = false;

	switch(pointsState)
	{
	case IN_SCROLLING:
		scrollInProgress += elapsedTime;

		scoresInOffset = (endInPosition + startInPosition) * (scrollInProgress / static_cast<float>(POINTS_SCROLL_IN_TIME));
		if((-startInPosition) + scoresInOffset < endInPosition)
			x = (-startInPosition) + scoresInOffset;
		else
			x = endInPosition;

		if(scrollInProgress >= POINTS_SCROLL_IN_TIME)
			pointsState = SLOW_OUT_SCROLLING;
		break;
	case SLOW_OUT_SCROLLING:
		scrollSlowOutProgress += elapsedTime;

		scoresSlowOutOffset = (endSlowOutPosition - startSlowOutPosition) * (scrollSlowOutProgress / static_cast<float>(POINTS_SCROLL_SLOW_OUT_TIME));

		x = startSlowOutPosition + scoresSlowOutOffset;

		if(scrollSlowOutProgress >= POINTS_SCROLL_SLOW_OUT_TIME)
		{
			pointsState = OUT_SCROLLING;
			startOutPosition = x;
		}
		break;
	case OUT_SCROLLING:
		scrollOutProgress += elapsedTime;
		scoresOutOffset = (endOutPosition - startOutPosition) * (scrollOutProgress / static_cast<float>(POINTS_SCROLL_OUT_TIME));
		x = startOutPosition + scoresOutOffset;
		break;
	}
}