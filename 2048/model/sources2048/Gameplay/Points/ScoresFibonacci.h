#pragma once
#include <string>
#include <vector>
#include "Scores.h"

class Game2048Core;

class ScoresFibonacci : public Scores
{
private:
	Game2048Core *gameCore;
	void reorderExponents(std::vector<unsigned> *exponent);
public:
	ScoresFibonacci();
	ScoresFibonacci(Game2048Core *gameCore);
	~ScoresFibonacci();

	void addScores(unsigned number);
};