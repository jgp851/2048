#pragma once

class Game2048Core;

class MouseGestures
{
protected:
	Game2048Core *gameCore;
	
	bool isSliding;
	bool slide;
	int beginX;
	int beginY;
	int progressToGesture;
	
	virtual void checkSliding() = 0;
public:
	MouseGestures(Game2048Core *gameCore);
	~MouseGestures();
	
	virtual bool downSlide() = 0;
	virtual bool upSlide() = 0;
	virtual bool leftSlide() = 0;
	virtual bool rightSlide() = 0;
	virtual bool downLeftSlide() = 0;
	virtual bool downRightSlide() = 0;
	virtual bool upLeftSlide() = 0;
	virtual bool upRightSlide() = 0;
	virtual void reset() = 0;
	virtual void update() = 0;
};