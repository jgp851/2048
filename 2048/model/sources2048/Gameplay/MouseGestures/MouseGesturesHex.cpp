#include "MouseGesturesHex.h"
#include "..\..\Game2048Core.h"
#include <cmath>

MouseGesturesHex::MouseGesturesHex(Game2048Core *gameCore) : MouseGestures(gameCore)
{
}

MouseGesturesHex::~MouseGesturesHex()
{
}

bool MouseGesturesHex::isDown()
{
	if(abs(gameCore->mouseManager->getY() - beginY) >= progressToGesture && gameCore->mouseManager->getY() > beginY)
		return true;
	return false;
}

bool MouseGesturesHex::isUp()
{
	if(abs(gameCore->mouseManager->getY() - beginY) >= progressToGesture && gameCore->mouseManager->getY() < beginY)
		return true;
	return false;
}

bool MouseGesturesHex::isLeft()
{
	if(abs(gameCore->mouseManager->getX() - beginX) >= progressToGesture && gameCore->mouseManager->getX() < beginX)
		return true;
	return false;
}

bool MouseGesturesHex::isRight()
{
	if(abs(gameCore->mouseManager->getX() - beginX) >= progressToGesture && gameCore->mouseManager->getX() > beginX)
		return true;
	return false;
}

bool MouseGesturesHex::isDownLeft()
{
	if(isDown() && isLeft())
		return true;
	return false;
}

bool MouseGesturesHex::isDownRight()
{
	if(isDown() && isRight())
		return true;
	return false;
}

bool MouseGesturesHex::isUpLeft()
{
	if(isUp() && isLeft())
		return true;
	return false;
}

bool MouseGesturesHex::isUpRight()
{
	if(isUp() && isRight())
		return true;
	return false;
}

bool MouseGesturesHex::downSlide()
{
	return down;
}

bool MouseGesturesHex::upSlide()
{
	return up;
}

bool MouseGesturesHex::downLeftSlide()
{
	return downLeft;
}

bool MouseGesturesHex::downRightSlide()
{
	return downRight;
}

bool MouseGesturesHex::upLeftSlide()
{
	return upLeft;
}

bool MouseGesturesHex::upRightSlide()
{
	return upRight;
}

void MouseGesturesHex::checkSliding()
{
	if(isDown() && !isUp() && !isDownLeft() && !isDownRight() && !isUpLeft() && !isUpRight())
	{
		down = true;
		isSliding = false;
	}
	if(isUp() && !isDown() && !isDownLeft() && !isDownRight() && !isUpLeft() && !isUpRight())
	{
		up = true;
		isSliding = false;
	}
	if(isDownLeft() && !isUp() && !isDownRight() && !isUpLeft() && !isUpRight())
	{
		downLeft = true;
		isSliding = false;
	}
	if(isDownRight() && !isUp() && !isDownLeft() && !isUpLeft() && !isUpRight())
	{
		downRight = true;
		isSliding = false;
	}
	if(isUpLeft() && !isDown() && !isDownLeft() && !isDownRight() && !isUpRight())
	{
		upLeft = true;
		isSliding = false;
	}
	if(isUpRight() && !isDown() && !isDownLeft() && !isDownRight() && !isUpLeft())
	{
		upRight = true;
		isSliding = false;
	}
}

void MouseGesturesHex::reset()
{
	down = false;
	up = false;
	downLeft = false;
	downRight = false;
	upLeft = false;
	upRight = false;
}

void MouseGesturesHex::update()
{
	if(!slide && gameCore->mouseManager->isLeftPressed())
	{
		slide = true;
		isSliding = true;
		beginX = gameCore->mouseManager->getX();
		beginY = gameCore->mouseManager->getY();
	}
	if(slide && gameCore->mouseManager->isLeftPressed() && isSliding)
		checkSliding();
	if(slide && !gameCore->mouseManager->isLeftPressed())
	{
		reset();
		slide = false;
	}
}