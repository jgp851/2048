#include "MouseGesturesStd.h"
#include "..\..\Game2048Core.h"
#include <cmath>

MouseGesturesStd::MouseGesturesStd(Game2048Core *gameCore) : MouseGestures(gameCore)
{
}

MouseGesturesStd::~MouseGesturesStd()
{
}

bool MouseGesturesStd::isDown()
{
	if(abs(gameCore->mouseManager->getY() - beginY) >= progressToGesture && gameCore->mouseManager->getY() > beginY)
		return true;
	return false;
}

bool MouseGesturesStd::isUp()
{
	if(abs(gameCore->mouseManager->getY() - beginY) >= progressToGesture && gameCore->mouseManager->getY() < beginY)
		return true;
	return false;
}

bool MouseGesturesStd::isLeft()
{
	if(abs(gameCore->mouseManager->getX() - beginX) >= progressToGesture && gameCore->mouseManager->getX() < beginX)
		return true;
	return false;
}

bool MouseGesturesStd::isRight()
{
	if(abs(gameCore->mouseManager->getX() - beginX) >= progressToGesture && gameCore->mouseManager->getX() > beginX)
		return true;
	return false;
}

bool MouseGesturesStd::downSlide()
{
	return down;
}

bool MouseGesturesStd::upSlide()
{
	return up;
}

bool MouseGesturesStd::leftSlide()
{
	return left;
}

bool MouseGesturesStd::rightSlide()
{
	return right;
}

void MouseGesturesStd::checkSliding()
{
	if(isDown() && !isUp() && !isLeft() && !isRight())
	{
		down = true;
		isSliding = false;
	}
	if(isUp() && !isDown() && !isLeft() && !isRight())
	{
		up = true;
		isSliding = false;
	}
	if(isLeft() && !isDown() && !isUp() && !isRight())
	{
		left = true;
		isSliding = false;
	}
	if(isRight() && !isDown() && !isUp() && !isLeft())
	{
		right = true;
		isSliding = false;
	}
}

void MouseGesturesStd::reset()
{
	down = false;
	up = false;
	left = false;
	right = false;
}

void MouseGesturesStd::update()
{
	if(!slide && gameCore->mouseManager->isLeftPressed())
	{
		slide = true;
		isSliding = true;
		beginX = gameCore->mouseManager->getX();
		beginY = gameCore->mouseManager->getY();
	}
	if(slide && gameCore->mouseManager->isLeftPressed() && isSliding)
		checkSliding();
	if(slide && !gameCore->mouseManager->isLeftPressed())
	{
		reset();
		slide = false;
	}
}