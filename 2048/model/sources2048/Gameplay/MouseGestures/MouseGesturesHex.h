#pragma once
#include "MouseGestures.h"

class Game2048Core;

class MouseGesturesHex : public MouseGestures
{
private:
	bool down;
	bool up;
	bool downLeft;
	bool downRight;
	bool upLeft;
	bool upRight;
	
	bool isDown();
	bool isUp();
	bool isDownLeft();
	bool isDownRight();
	bool isUpLeft();
	bool isUpRight();
	
	bool isLeft();
	bool isRight();
	
	void checkSliding();
public:
	MouseGesturesHex(Game2048Core *gameCore);
	~MouseGesturesHex();

	bool downSlide();
	bool upSlide();
	bool downLeftSlide();
	bool downRightSlide();
	bool upLeftSlide();
	bool upRightSlide();
	bool leftSlide(){ return false; } // virtual
	bool rightSlide(){ return false; } // virtual

	void reset();
	void update();
};