#pragma once
#include "MouseGestures.h"

class Game2048Core;

class MouseGesturesStd : public MouseGestures
{
private:
	bool down;
	bool up;
	bool left;
	bool right;
	
	bool isDown();
	bool isUp();
	bool isLeft();
	bool isRight();
	
	void checkSliding();
public:
	MouseGesturesStd(Game2048Core *gameCore);
	~MouseGesturesStd();

	bool downSlide();
	bool upSlide();
	bool downLeftSlide(){ return false; } // virtual
	bool downRightSlide(){ return false; } // virtual
	bool upLeftSlide(){ return false; } // virtual
	bool upRightSlide(){ return false; } // virtual
	bool leftSlide();
	bool rightSlide();

	void reset();
	void update();
};