#include "GameplayStandardOnStdBoard.h"
#include "Points\ScoresPower2.h"
#include "..\Game2048Core.h"

GameplayStandardOnStdBoard::GameplayStandardOnStdBoard(Game2048Core *gameCore) : StandardBoard(gameCore)
{
	gameMode = GAME_MODE_STD;
	scores = new ScoresPower2(gameCore);
	continueGameAtEnd = true;
	initialNumber = 1;
	typeConnectValues = 0;
	setWinningNumber();
}

GameplayStandardOnStdBoard::~GameplayStandardOnStdBoard() {}

bool GameplayStandardOnStdBoard::joinExistingSquares(int keyCode, bool haveAdd)
{
	return StandardBoard::connectExistingSquares(keyCode, haveAdd);
}

void GameplayStandardOnStdBoard::setSquareValue(int x, int y)
{
	GameplayCore::setSquareValuePower2(x, y);
}

void GameplayStandardOnStdBoard::setWinningNumber()
{
	winningNumber = gameCore->gameSettings->winningNumber;
}