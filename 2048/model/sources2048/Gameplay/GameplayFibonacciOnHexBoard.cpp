#include "GameplayFibonacciOnHexBoard.h"
#include "Points\ScoresFibonacci.h"
#include "..\Game2048Core.h"

GameplayFibonacciOnHexBoard::GameplayFibonacciOnHexBoard(Game2048Core *gameCore) : HexagonBoard(gameCore)
{
	gameMode = GAME_MODE_FIB;
	scores = new ScoresFibonacci(gameCore);
	continueGameAtEnd = true;
	initialNumber = 1;
	typeConnectValues = 0;
	setWinningNumber();
}
GameplayFibonacciOnHexBoard::~GameplayFibonacciOnHexBoard() {}

bool GameplayFibonacciOnHexBoard::joinExistingSquares(int keyCode, bool haveAdd)
{
	return HexagonBoard::connectFibExistingSquares(keyCode, haveAdd);
}

void GameplayFibonacciOnHexBoard::setSquareValue(int x, int y)
{
	GameplayCore::setSquareValueFibonacci(x, y);
}

void GameplayFibonacciOnHexBoard::setWinningNumber()
{
	winningNumber = gameCore->gameSettings->winningNumber;
}