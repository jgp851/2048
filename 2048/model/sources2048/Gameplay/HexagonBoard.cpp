#include "HexagonBoard.h"
#include "BoardDrawing\BoardDrawingHexagon.h"
#include "MouseGestures\MouseGesturesHex.h"
#include "Points\LastEarnedPoints.h"
#include "Points\Scores.h"
#include "Tile\TileHexagonal.h"
#include "Steps.h"
#include "..\Game2048Core.h"
#include "..\Game2048FontManager.h"
#include "..\..\gamecore\TextBox\TextBox.h"

#define ALLEGRO_KEY_LEFT_UP 7
#define ALLEGRO_KEY_LEFT_DOWN 1
#define ALLEGRO_KEY_RIGHT_UP 9
#define ALLEGRO_KEY_RIGHT_DOWN 3

HexagonBoard::HexagonBoard(Game2048Core *gameCore) : GameplayCore(gameCore)
{
	this->boardDrawing = new BoardDrawingHexagon(gameCore);
	this->mouseGestures = new MouseGesturesHex(gameCore);

	squareTexture = NULL;
	textureBitmap = NULL;
	
	boardType = 1;
	screenshot = NULL;
	screenshot_tmp = NULL;
	changed = false;
	gameLoaded = false;
	boardTab = NULL;
}

HexagonBoard::~HexagonBoard()
{
	if(boardTab != NULL)
		delete boardTab;

	delete boardDrawing;
	al_destroy_bitmap(screenshot);
}

Tile* HexagonBoard::loadCorrectTile(unsigned int x, unsigned int y)
{
	if(selectedTextureType == 1 && showTexture < 0)
		return new TileHexagonal(gameCore, boardDrawing->getSquareResX(x), boardDrawing->getSquareResY(x, y), boardDrawing->getSquareWidth(), boardDrawing->getSquareHeight(), boardDrawing->getSquareLength(), boardDrawing->getArmWidth(), boardDrawing->getThickness(), boardDrawing->getSquareHalfHeight(), typeConnectValues);
	else if(selectedTextureType == 2 && !coloringCustomTextures)
		return new TileHexagonal(gameCore, boardDrawing->getSquareResX(x), boardDrawing->getSquareResY(x, y), boardDrawing->getSquareWidth(), boardDrawing->getSquareHeight(), boardDrawing->getSquareLength(), boardDrawing->getArmWidth(), boardDrawing->getThickness(), boardDrawing->getSquareHalfHeight(), typeConnectValues, textureBitmap, coloringCustomTextures);
	else
		return new TileHexagonal(gameCore, boardDrawing->getSquareResX(x), boardDrawing->getSquareResY(x, y), boardDrawing->getSquareWidth(), boardDrawing->getSquareHeight(), boardDrawing->getSquareLength(), boardDrawing->getArmWidth(), boardDrawing->getThickness(), boardDrawing->getSquareHalfHeight(), typeConnectValues, textureBitmap);
}

bool HexagonBoard::connectExistingSquares(int keyCode, bool haveAdd)
{
	bool added = false;
	switch(keyCode)
	{
	case ALLEGRO_KEY_UP:
		for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
			for(int y = 1; y < static_cast<int>((*boardTab)[x].size()); y++)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[x][y-1]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[x][y-1]->connectValues();
						addScores(!added, (*boardTab)[x][y-1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_DOWN:
		for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
			for(int y = static_cast<int>((*boardTab)[x].size()) - 2; y >= 0; y--)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[x][y+1]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[x][y+1]->connectValues();
						addScores(!added, (*boardTab)[x][y+1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_LEFT_UP:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = mainIteration + 1, y = (*boardTab)[x].size() - 1, dx, dy; i < localIterations; i++)
			{
				if(i <= localIterations / 2)
					y = (*boardTab)[x].size() - 1 - i;
				else
				{
					x++;
					if(x > boardTotalFields / 2)
						y--;
				}

				dx = x - 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y - 1;
					else if(dx > boardTotalFields / 2)
						dy--;
				}
				else
				{
					if(i == 0)
						dy = (*boardTab)[x].size() - 1;
					else
						dy--;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[dx][dy]->connectValues();
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
		}
		break;
	case ALLEGRO_KEY_LEFT_DOWN:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = mainIteration + 1, y = 0, dx, dy; i < localIterations; i++)
			{
				dx = x - 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y;
					else if(dx <= boardTotalFields / 2)
						dy++;
				}
				else
				{
					if(i == 0)
						dy = y + 1;
					else if(i <= localIterations / 2)
						dy++;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[dx][dy]->connectValues();
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				if(i < localIterations / 2)
					y++;
				else
				{
					x++;
					if(x <= boardTotalFields / 2)
						y++;
				}
			}
		}
		break;
	case ALLEGRO_KEY_RIGHT_UP:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = boardTotalFields - 2 - mainIteration, y = (*boardTab)[x].size() - 1, dx, dy; i < localIterations; i++)
			{
				if(i <= localIterations / 2)
					y = (*boardTab)[x].size() - 1 - i;
				else
				{
					x--;
					if(x < boardTotalFields / 2)
						y--;
				}

				dx = x + 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y - 1;
					else if(dx < boardTotalFields / 2)
						dy--;
				}
				else
				{
					if(i == 0)
						dy = (*boardTab)[x].size() - 1;
					else
						dy--;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[dx][dy]->connectValues();
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
		}
		break;
	case ALLEGRO_KEY_RIGHT_DOWN:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = boardTotalFields - 2 - mainIteration, y = 0, dx, dy; i < localIterations; i++)
			{
				dx = x + 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y;
					else if(dx >= boardTotalFields / 2)
						dy++;
				}
				else
				{
					if(i == 0)
						dy = y + 1;
					else if(i <= localIterations / 2)
						dy++;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[dx][dy]->connectValues();
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				if(i < localIterations / 2)
					y++;
				else
				{
					x--;
					if(x >= boardTotalFields / 2)
						y++;
				}
			}
		}
		break;
	}
	return added;
}

bool HexagonBoard::connectFibExistingSquares(int keyCode, bool haveAdd)
{
	bool added = false;
	switch(keyCode)
	{
	case ALLEGRO_KEY_UP:
		for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
			for(int y = 1; y < static_cast<int>((*boardTab)[x].size()); y++)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y-1]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[x][y-1]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[x][y-1]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[x][y-1]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[x][y-1]->getValue() == initialNumber)
							(*boardTab)[x][y-1]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[x][y-1]->getValue())
							(*boardTab)[x][y-1]->increment();
						else
							(*boardTab)[x][y-1]->increment(2);
						addScores(!added, (*boardTab)[x][y-1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_DOWN:
		for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
			for(int y = static_cast<int>((*boardTab)[x].size()) - 2; y >= 0; y--)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y+1]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[x][y+1]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[x][y+1]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[x][y+1]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[x][y+1]->getValue() == initialNumber)
							(*boardTab)[x][y+1]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[x][y+1]->getValue())
							(*boardTab)[x][y+1]->increment();
						else
							(*boardTab)[x][y+1]->increment(2);
						addScores(!added, (*boardTab)[x][y+1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_LEFT_UP:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = mainIteration + 1, y = (*boardTab)[x].size() - 1, dx, dy; i < localIterations; i++)
			{
				if(i <= localIterations / 2)
					y = (*boardTab)[x].size() - 1 - i;
				else
				{
					x++;
					if(x > boardTotalFields / 2)
						y--;
				}

				dx = x - 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y - 1;
					else if(dx > boardTotalFields / 2)
						dy--;
				}
				else
				{
					if(i == 0)
						dy = (*boardTab)[x].size() - 1;
					else
						dy--;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[dx][dy]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[dx][dy]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[dx][dy]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[dx][dy]->getValue() == initialNumber)
							(*boardTab)[dx][dy]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[dx][dy]->getValue())
							(*boardTab)[dx][dy]->increment();
						else
							(*boardTab)[dx][dy]->increment(2);
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
		}
		break;
	case ALLEGRO_KEY_LEFT_DOWN:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = mainIteration + 1, y = 0, dx, dy; i < localIterations; i++)
			{
				dx = x - 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y;
					else if(dx <= boardTotalFields / 2)
						dy++;
				}
				else
				{
					if(i == 0)
						dy = y + 1;
					else if(i <= localIterations / 2)
						dy++;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[dx][dy]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[dx][dy]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[dx][dy]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[dx][dy]->getValue() == initialNumber)
							(*boardTab)[dx][dy]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[dx][dy]->getValue())
							(*boardTab)[dx][dy]->increment();
						else
							(*boardTab)[dx][dy]->increment(2);
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				if(i < localIterations / 2)
					y++;
				else
				{
					x++;
					if(x <= boardTotalFields / 2)
						y++;
				}
			}
		}
		break;
	case ALLEGRO_KEY_RIGHT_UP:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = boardTotalFields - 2 - mainIteration, y = (*boardTab)[x].size() - 1, dx, dy; i < localIterations; i++)
			{
				if(i <= localIterations / 2)
					y = (*boardTab)[x].size() - 1 - i;
				else
				{
					x--;
					if(x < boardTotalFields / 2)
						y--;
				}

				dx = x + 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y - 1;
					else if(dx < boardTotalFields / 2)
						dy--;
				}
				else
				{
					if(i == 0)
						dy = (*boardTab)[x].size() - 1;
					else
						dy--;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[dx][dy]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[dx][dy]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[dx][dy]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[dx][dy]->getValue() == initialNumber)
							(*boardTab)[dx][dy]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[dx][dy]->getValue())
							(*boardTab)[dx][dy]->increment();
						else
							(*boardTab)[dx][dy]->increment(2);
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
		}
		break;
	case ALLEGRO_KEY_RIGHT_DOWN:
		for(int mainIteration = 0, localIterations; mainIteration < boardTotalFields - 1; mainIteration++)
		{
			if(mainIteration < (boardTotalFields - 1) / 2)
				localIterations = boardTotalFields;
			else
				localIterations -= 2;

			for(int i = 0, x = boardTotalFields - 2 - mainIteration, y = 0, dx, dy; i < localIterations; i++)
			{
				dx = x + 1;
				if(localIterations == boardTotalFields)
				{
					if(i <= localIterations / 2)
						dy = y;
					else if(dx >= boardTotalFields / 2)
						dy++;
				}
				else
				{
					if(i == 0)
						dy = y + 1;
					else if(i <= localIterations / 2)
						dy++;
				}
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[dx][dy]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[dx][dy]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[dx][dy]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[dx][dy]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[dx][dy]->getValue() == initialNumber)
							(*boardTab)[dx][dy]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[dx][dy]->getValue())
							(*boardTab)[dx][dy]->increment();
						else
							(*boardTab)[dx][dy]->increment(2);
						addScores(!added, (*boardTab)[dx][dy]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				if(i < localIterations / 2)
					y++;
				else
				{
					x--;
					if(x >= boardTotalFields / 2)
						y++;
				}
			}
		}
		break;
	}
	return added;
}

bool HexagonBoard::isNoMovement()
{
	if(!joinExistingSquares(ALLEGRO_KEY_UP, false) && !joinExistingSquares(ALLEGRO_KEY_DOWN, false) && !joinExistingSquares(ALLEGRO_KEY_LEFT_UP, false) && !joinExistingSquares(ALLEGRO_KEY_LEFT_DOWN, false) && !joinExistingSquares(ALLEGRO_KEY_RIGHT_UP, false) && !joinExistingSquares(ALLEGRO_KEY_RIGHT_DOWN, false))
		return true;
	else
		return false;
}

bool HexagonBoard::moveToYourDirection(int keyCode)
{
	bool moved = false;

	switch(keyCode)
	{
	case ALLEGRO_KEY_UP:
		for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
			for(int y = 1; y < static_cast<int>((*boardTab)[x].size()); y++)
				if((*boardTab)[x][y]->getValue() != 0)
					for(int zy = 0; zy < y; zy++)
						if((*boardTab)[x][zy]->getValue() == 0)
						{
							(*boardTab)[x][zy]->setValue((*boardTab)[x][y]->getValue());
							(*boardTab)[x][y]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						break;
	case ALLEGRO_KEY_DOWN:
		for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
			for(int y = static_cast<int>((*boardTab)[x].size()) - 2; y >= 0; y--)
				if((*boardTab)[x][y]->getValue() != 0)
					for(int zy = (*boardTab)[x].size() - 1; zy > y; zy--)
						if((*boardTab)[x][zy]->getValue() == 0)
						{
							(*boardTab)[x][zy]->setValue((*boardTab)[x][y]->getValue());
							(*boardTab)[x][y]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						break;
	case ALLEGRO_KEY_LEFT_UP:
		for(int i = 0, x = 1, y = (*boardTab)[x].size() - 1, zxLength = boardHexSize - 1; i < boardTotalFields; i++)
		{
			if(i <= boardTotalFields / 2)
				y = (*boardTab)[x].size() - 1 - i;
			else
			{
				x++;
				if(i < boardTotalFields - 1)
					y = 1;
				else
					y = 0;
			}

			for(int zi = 0, zx = x, zy = y; zi < zxLength; zi++)
			{
				if((*boardTab)[zx][zy]->getValue() != 0)
				{
					int ai = 0, ax = x - 1, ay = 0;
					if(i < boardHexSize)
						ay = y - 1;
					for(; ax < zx; ax++)
					{
						if((*boardTab)[ax][ay]->getValue() == 0)
						{
							(*boardTab)[ax][ay]->setValue((*boardTab)[zx][zy]->getValue());
							(*boardTab)[zx][zy]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						if(ax < boardTotalFields / 2)
							ay++;
					}
				}

				if(zx < boardTotalFields / 2)
					zy++;
				zx++;
			}
			if(i < boardTotalFields / 2)
				zxLength++;
			else
				zxLength--;
		}
		break;
	case ALLEGRO_KEY_LEFT_DOWN:
		for(int i = 0, x = 1, y = 0, zxLength = boardHexSize - 1; i < boardTotalFields; i++)
		{
			for(int zi = 0, zx = x, zy = y; zi < zxLength; zi++)
			{
				if((*boardTab)[zx][zy]->getValue() != 0)
				{
					int ai = 0, ax = x - 1, ay = y;
					if(i == boardTotalFields - 1)
						ay = y + 1;
					for(; ax < zx; ax++)
					{
						if((*boardTab)[ax][ay]->getValue() == 0)
						{
							(*boardTab)[ax][ay]->setValue((*boardTab)[zx][zy]->getValue());
							(*boardTab)[zx][zy]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						if(ax >= boardTotalFields / 2)
							ay--;
					}
				}

				zx++;
				if(zx > boardTotalFields / 2)
					zy--;
			}
			if(i < boardTotalFields / 2)
				y++;
			else
			{
				x++;
				if(x <= boardTotalFields / 2)
					y++;
			}
			if(i < boardTotalFields / 2)
				zxLength++;
			else
				zxLength--;
		}
		break;
	case ALLEGRO_KEY_RIGHT_UP:
		for(int i = 0, x = boardTotalFields - 2, y = (*boardTab)[x].size() - 1, zxLength = boardHexSize - 1; i < boardTotalFields; i++)
		{
			if(i <= boardTotalFields / 2)
				y = (*boardTab)[x].size() - 1 - i;
			else
			{
				x--;
				if(i < boardTotalFields - 1)
					y = 1;
				else
					y = 0;
			}

			for(int zi = 0, zx = x, zy = y; zi < zxLength; zi++)
			{
				if((*boardTab)[zx][zy]->getValue() != 0)
				{
					int ai = 0, ax = x + 1, ay = 0;
					if(i < boardHexSize)
						ay = y - 1;
					for(; ax > zx; ax--)
					{
						if((*boardTab)[ax][ay]->getValue() == 0)
						{
							(*boardTab)[ax][ay]->setValue((*boardTab)[zx][zy]->getValue());
							(*boardTab)[zx][zy]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						if(ax > boardTotalFields / 2)
							ay++;
					}
				}

				if(zx > boardTotalFields / 2)
					zy++;
				zx--;
			}
			if(i < boardTotalFields / 2)
				zxLength++;
			else
				zxLength--;
		}
		break;
	case ALLEGRO_KEY_RIGHT_DOWN:
		for(int i = 0, x = boardTotalFields - 2, y = 0, zxLength = boardHexSize - 1; i < boardTotalFields; i++)
		{
			for(int zi = 0, zx = x, zy = y; zi < zxLength; zi++)
			{
				if((*boardTab)[zx][zy]->getValue() != 0)
				{
					int ai = 0, ax = x + 1, ay = y;
					if(i == boardTotalFields - 1)
						ay = y + 1;
					for(; ax > zx; ax--)
					{
						if((*boardTab)[ax][ay]->getValue() == 0)
						{
							(*boardTab)[ax][ay]->setValue((*boardTab)[zx][zy]->getValue());
							(*boardTab)[zx][zy]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						if(ax <= boardTotalFields / 2)
							ay--;
					}
				}

				zx--;
				if(zx < boardTotalFields / 2)
					zy--;
			}
			if(i < boardTotalFields / 2)
				y++;
			else
			{
				if(x > boardTotalFields / 2)
					y++;
				x--;
			}
			if(i < boardTotalFields / 2)
				zxLength++;
			else
				zxLength--;
		}
		break;
	}
	return moved;
}

void HexagonBoard::createBoard(int size)
{
	boardDrawing->setBoard(size);
	boardTab = new std::vector<std::vector<Tile*>>();
	boardHexSize = size;
	boardTotalFields = boardHexSize * 2 - 1;
	squaresCount = 0;
	for(int i = size; i < boardTotalFields; i++)
		squaresCount += (i * 2);
	squaresCount += boardTotalFields;
	
	int total;
	for(int x = 0; x < boardTotalFields; x++)
	{
		if(x < boardTotalFields / 2)
			total = boardTotalFields - (boardTotalFields / 2 - x);
		else if(x > boardTotalFields / 2)
		{
			total = (x - boardTotalFields / 2) - boardTotalFields;
			total = -total;
		}
		else
			total = boardTotalFields;
		
		boardTab->push_back(std::vector<Tile*>());
		for(int y = 0; y < total; y++)
			(*boardTab)[x].push_back(loadCorrectTile(x, y));
	}
}

void HexagonBoard::createBoard(int width, int height){} // virtual

void HexagonBoard::draw()
{
	boardDrawing->drawBackground();
	for(int x = 0; x < static_cast<int>(boardTab->size()); x++)
		for(int y = 0; y < static_cast<int>((*boardTab)[x].size()); y++)
			if((*boardTab)[x][y]->getValue() > 0)
			{
				setSquareValue(x, y);
				(*boardTab)[x][y]->draw();
			}
	boardDrawing->drawBorder();
	scores->draw();
	gameHUD->draw();
}

void HexagonBoard::processInput(unsigned elapsedTime)
{	
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
	{
		screenshot_tmp = al_get_backbuffer(gameCore->alManager->getDisplay());
		al_save_bitmap("temp/screenshot.jpg", screenshot_tmp);
		screenshot = gameCore->bitmapLoader->loadBitmap("temp/screenshot.jpg");
		lastModificationTime = gameCore->getCurrentTime("%Y-%m-%d %H:%M:%S");

		gameCore->setGameState(GAME_PAUSE);
		gameCore->mouseManager->stopHiding();
	}

	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_Z, true))
			steps->undoStep();
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_Y, true))
			steps->nextStep();
	
	mouseGestures->update();
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true)) || mouseGestures->upLeftSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_LEFT_UP);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true)) || mouseGestures->downLeftSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_LEFT_DOWN);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true)) || mouseGestures->upRightSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_RIGHT_UP);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if((gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true)) || mouseGestures->downRightSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_RIGHT_DOWN);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true) || mouseGestures->upSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_UP);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true) || mouseGestures->downSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_DOWN);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
}

void HexagonBoard::setTexture()
{
	std::vector<ALLEGRO_BITMAP*> *textures;
	int selectedTexture;
	// ustawienie tekstury odpowiedniego typu - jeśli texturesCustom nie ma, ustawia tekstury domyślne
	if(gameCore->gameSettings->selectedTextureHexType == 2 && gameCore->mediaManager->texturesHexCustom.empty())
		gameCore->gameSettings->selectedTextureHexType = 1;
	selectedTextureType = gameCore->gameSettings->selectedTextureHexType;
	switch(selectedTextureType)
	{
	case 1:
		textures = &gameCore->mediaManager->texturesHexDefault;
		selectedTexture = gameCore->gameSettings->selectedTextureHexDefault;
		break;
	case 2:
		textures = &gameCore->mediaManager->texturesHexCustom;
		selectedTexture = gameCore->gameSettings->selectedTextureHexCustom;
		break;
	}
	GameplayCore::setTexture(textures, &gameCore->mediaManager->customTexturesHexDirectories, gameCore->gameSettings->coloringCustomTexturesHex, gameCore->gameSettings->randomTextureHex, selectedTexture);
}