#include "Steps.h"
#include "..\backgrounds\GameBackground\GameBackground.h"
#include "..\backgrounds\GameBackground\GameScrollingBackground.h"
#include "..\Game2048Core.h"
#include "GameplayCore.h"
#include "Points\ScoresFibonacci.h"
#include "Points\ScoresPower2.h"
#include "Tile\Tile.h"

Steps::Steps(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	actualStep = 0;
	tmp = NULL;
	tmpScores = NULL;
}

Steps::~Steps()
{
	for(std::vector<std::vector<std::vector<unsigned int>>*>::iterator it = stepsVector.begin(); it != stepsVector.end(); it++)
		delete *it;
	stepsVector.clear();
	
	for(std::vector<Scores*>::iterator it = scoresVector.begin(); it != scoresVector.end(); it++)
		delete *it;
	scoresVector.clear();
}

bool Steps::isDrawOnlyTotalSteps()
{
	return drawOnlyTotalSteps;
}

unsigned Steps::getActualStep()
{
	return actualStep;
}

void Steps::addStep()
{
	if(actualStep < stepsVector.size() - 1)
		eraseSteps();

	actualStep++;
	
	std::vector<std::vector<Tile*>> *boardTab = gameCore->game->boardTab;
	tmp = new std::vector<std::vector<unsigned int>>();
	
	for(unsigned x = 0; x < boardTab->size(); x++)
		tmp->push_back(std::vector<unsigned int>());

	for(unsigned x = 0; x < boardTab->size(); x++)
		for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
			(*tmp)[x].push_back(0);

	switch(gameCore->game->gameMode)
	{
	case GAME_MODE_STD: case GAME_MODE_BACK: tmpScores = new ScoresPower2(); break;
	case GAME_MODE_FIB: tmpScores = new ScoresFibonacci(); break;
	}

	copyBoard(boardTab, tmp);
	copyScores(gameCore->game->scores, tmpScores);

	stepsVector.push_back(tmp);
	scoresVector.push_back(tmpScores);
	
	drawOnlyTotalSteps = true;
	gameCore->game->reversingMovements = false;
	gameCore->gameBackground->nextStep();
}

void Steps::copyBoard(std::vector<std::vector<Tile*>> *source, std::vector<std::vector<unsigned int>> *destination)
{
	for(unsigned x = 0; x < source->size(); x++)
		for(unsigned y = 0; y < (*source)[x].size(); y++)
			(*destination)[x][y] = (*source)[x][y]->getValue();
}

void Steps::copyBoard(std::vector<std::vector<unsigned int>> *source, std::vector<std::vector<Tile*>> *destination)
{
	for(unsigned x = 0; x < source->size(); x++)
		for(unsigned y = 0; y < (*source)[x].size(); y++)
			(*destination)[x][y]->setValue((*source)[x][y]);
}

void Steps::copyScores(Scores *source, Scores *destination)
{
	if(!source->exponent.empty())
		destination->exponent = source->exponent;
	else
		destination->exponent.clear();
	destination->scoresNumber = source->scoresNumber;
}

void Steps::eraseSteps()
{
	for(std::vector<std::vector<std::vector<unsigned int>>*>::iterator it = stepsVector.begin() + (actualStep + 1); it != stepsVector.end(); it++)
		delete *it;
	stepsVector.erase(stepsVector.begin() + (actualStep + 1), stepsVector.end());
	
	for(std::vector<Scores*>::iterator it = scoresVector.begin() + (actualStep + 1); it != scoresVector.end(); it++)
		delete *it;
	scoresVector.erase(scoresVector.begin() + (actualStep + 1), scoresVector.end());
}

void Steps::initStep()
{
	std::vector<std::vector<Tile*>> *boardTab = gameCore->game->boardTab;
	tmp = new std::vector<std::vector<unsigned int>>();
	
	for(unsigned x = 0; x < boardTab->size(); x++)
		tmp->push_back(std::vector<unsigned int>());

	for(unsigned x = 0; x < boardTab->size(); x++)
		for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
			(*tmp)[x].push_back(0);
	
	switch(gameCore->game->gameMode)
	{
	case GAME_MODE_STD: case GAME_MODE_BACK: tmpScores = new ScoresPower2(); break;
	case GAME_MODE_FIB: tmpScores = new ScoresFibonacci(); break;
	}
	
	copyBoard(boardTab, tmp);
	copyScores(gameCore->game->scores, tmpScores);

	stepsVector.push_back(tmp);
	scoresVector.push_back(tmpScores);
	
	drawOnlyTotalSteps = true;
	gameCore->game->reversingMovements = false;
}

void Steps::nextStep()
{
	if(actualStep == stepsVector.size() - 1)
		return;

	actualStep++;
	
	copyBoard(stepsVector[actualStep], gameCore->game->boardTab);
	copyScores(scoresVector[actualStep], gameCore->game->scores);

	gameCore->game->activeSquares = gameCore->game->getActiveSquares();
	
	gameCore->gameBackground->nextStep();
}

void Steps::reset()
{
	for(std::vector<std::vector<std::vector<unsigned int>>*>::iterator it = stepsVector.begin(); it != stepsVector.end(); it++)
		delete *it;
	stepsVector.clear();
	for(std::vector<Scores*>::iterator it = scoresVector.begin(); it != scoresVector.end(); it++)
		delete *it;
	scoresVector.clear();
	
	drawOnlyTotalSteps = true;
	actualStep = 0;
	tmp = NULL;
	tmpScores = NULL;
}

void Steps::undoStep()
{
	if(actualStep == 0)
		return;

	actualStep--;

	copyBoard(stepsVector[actualStep], gameCore->game->boardTab);
	copyScores(scoresVector[actualStep], gameCore->game->scores);
	
	gameCore->game->activeSquares = gameCore->game->getActiveSquares();

	drawOnlyTotalSteps = false;
	gameCore->gameBackground->undoStep();
}