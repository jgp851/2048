#include "GameplayBackwardsOnHexBoard.h"
#include "Points\ScoresPower2.h"
#include "..\Game2048Core.h"

GameplayBackwardsOnHexBoard::GameplayBackwardsOnHexBoard(Game2048Core *gameCore) : HexagonBoard(gameCore)
{
	gameMode = GAME_MODE_BACK;
	scores = new ScoresPower2(gameCore);
	continueGameAtEnd = false;
	initialNumber = gameCore->gameSettings->backwardsNumber;
	typeConnectValues = 1;
	setWinningNumber();
}
GameplayBackwardsOnHexBoard::~GameplayBackwardsOnHexBoard() {}

bool GameplayBackwardsOnHexBoard::joinExistingSquares(int keyCode, bool haveAdd)
{
	return HexagonBoard::connectExistingSquares(keyCode, haveAdd);
}

void GameplayBackwardsOnHexBoard::setSquareValue(int x, int y)
{
	GameplayCore::setSquareValuePower2(x, y);
}

void GameplayBackwardsOnHexBoard::setWinningNumber()
{
	winningNumber = 1;
}