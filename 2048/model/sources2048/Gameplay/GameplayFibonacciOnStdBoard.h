#pragma once
#include "StandardBoard.h"

class Game2048Core;

class GameplayFibonacciOnStdBoard : public StandardBoard
{
private:
	bool joinExistingSquares(int keyCode, bool haveAdd = true);
	void setSquareValue(int x, int y);
	void setWinningNumber();
public:
	GameplayFibonacciOnStdBoard(Game2048Core *gameCore);
	~GameplayFibonacciOnStdBoard();
};