#pragma once
#include <string>
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>

#define CNN_ADD 0
#define CNN_SUB 1

class Game2048Core;

/*
 * char coloringTexture - okre�lna rodzaj tekstury: 0 - to zwyk�a, jednokolorowa figura; 1 - to tekstura z bitmap� kolorowan�; 2 - tekstura z bitmap� niekolorowan�
 */

class Tile
{
protected:
	Game2048Core *gameCore;
	ALLEGRO_BITMAP *textureBitmap;
	ALLEGRO_COLOR tileColor;
	ALLEGRO_COLOR valueColor;
	ALLEGRO_FONT *valueFont;
	std::string valueString;
	bool coloringTexture;
	char textureType;
	char typeConnectValues;
	float x;
	float y;
	float width;
	float height;
	int fontSize;
	unsigned int value;
	
	void drawTileWithTexture();
	void setTileColor();
	virtual void setTileFontSize(float tileLength) = 0;
public:
	Tile(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues); // kafelka wbudowana, standardowa
	Tile(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues, ALLEGRO_BITMAP *textureBitmap, bool coloringTexture = true); // kafelki wbudowane lub u�ytkownika
	~Tile();

	unsigned int getValue();

	void connectValues(unsigned int value = 1);
	void decrement(unsigned int value = 1);
	void increment(unsigned int value = 1);
	void setValue(unsigned int value);
	void setValueString(std::string valueString);
	virtual void draw() = 0;
};