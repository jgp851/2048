#pragma once
#include "Tile.h"

class Game2048Core;

class TileHexagonal : public Tile
{
private:
	float armWidth;
	float borderThickness;
	float halfHeight;
	float tileLength;
	void setTileFontSize(float tileLength);
public:
	TileHexagonal(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, float armWidth, float borderThickness, float halfHeight, char typeConnectValues);
	TileHexagonal(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, float armWidth, float borderThickness, float halfHeight, char typeConnectValues, ALLEGRO_BITMAP *textureBitmap, bool coloringTexture = true);
	~TileHexagonal();

	void draw();
};