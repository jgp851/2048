#include "TileHexagonal.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"

TileHexagonal::TileHexagonal(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, float armWidth, float borderThickness, float halfHeight, char typeConnectValues) : Tile(gameCore, x, y, width, height, tileLength, typeConnectValues)
{
	this->armWidth = armWidth;
	this->borderThickness = borderThickness;
	this->halfHeight = halfHeight;
	this->tileLength = tileLength;
	setTileFontSize(tileLength); //rozmiar fontu wzgl�dem rozmiaru kafelki
}
TileHexagonal::TileHexagonal(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, float armWidth, float borderThickness, float halfHeight, char typeConnectValues, ALLEGRO_BITMAP *textureBitmap, bool coloringTexture) : Tile(gameCore, x, y, width, height, tileLength, typeConnectValues, textureBitmap, coloringTexture)
{
	this->armWidth = armWidth;
	this->borderThickness = borderThickness;
	this->halfHeight = halfHeight;
	this->tileLength = tileLength;
	setTileFontSize(tileLength); //rozmiar fontu wzgl�dem rozmiaru kafelki
}

TileHexagonal::~TileHexagonal()
{
}

void TileHexagonal::draw()
{
	switch(textureType)
	{
	case 0:
		al_draw_filled_rectangle(x + armWidth, y, x + armWidth + tileLength, y + height, tileColor);
		al_draw_filled_triangle(x, y + halfHeight - borderThickness / 2,
			x + armWidth,
			y - borderThickness / 2,
			x + armWidth,
			y + height + borderThickness / 1.5,
			tileColor);
		al_draw_filled_triangle(x + width - borderThickness / 2,
			y + halfHeight - borderThickness / 2,
			x + armWidth + tileLength,
			y - borderThickness / 2,
			x + armWidth + tileLength,
			y + height + borderThickness / 1.5,
			tileColor);
		break;
	case 1: case 2: drawTileWithTexture(); break;
	}
	al_draw_text(valueFont, valueColor, x + armWidth + tileLength / 2, y + height / 2 - fontSize / 2, ALLEGRO_ALIGN_CENTER, valueString.c_str());
}

void TileHexagonal::setTileFontSize(float tileLength)
{
	if(tileLength < 26)
	{
		valueFont = gameCore->fontManager->font_number10->font;
		fontSize = 10;
	}
	else if(tileLength < 35)
	{
		valueFont = gameCore->fontManager->font_number12->font;
		fontSize = 12;
	}
	else if(tileLength < 50)
	{
		valueFont = gameCore->fontManager->font_number18->font;
		fontSize = 18;
	}
	else if(tileLength < 70)
	{
		valueFont = gameCore->fontManager->font_number25->font;
		fontSize = 25;
	}
	else if(tileLength < 100)
	{
		valueFont = gameCore->fontManager->font_number32->font;
		fontSize = 32;
	}
	else
	{
		valueFont = gameCore->fontManager->font_number36->font;
		fontSize = 36;
	}
}