#pragma once
#include "Tile.h"

class Game2048Core;

class TileSquare : public Tile
{
private:
	void setTileFontSize(float tileLength);
public:
	TileSquare(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues);
	TileSquare(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues, ALLEGRO_BITMAP *textureBitmap, bool coloringTexture = true);
	~TileSquare();

	void draw();
};