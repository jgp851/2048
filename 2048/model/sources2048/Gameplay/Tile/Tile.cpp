#include "Tile.h"
#include "..\..\Game2048Core.h"

Tile::Tile(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues)
{
	this->gameCore = gameCore;
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->textureBitmap = NULL;
	this->coloringTexture = true;
	this->typeConnectValues = typeConnectValues;
	this->value = 0;
	textureType = 0;
	valueColor = al_map_rgb(255, 255, 255);
}
Tile::Tile(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues, ALLEGRO_BITMAP *textureBitmap, bool coloringTexture)
{
	this->gameCore = gameCore;
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->textureBitmap = textureBitmap;
	this->coloringTexture = coloringTexture;
	this->typeConnectValues = typeConnectValues;
	this->value = 0;
	if(coloringTexture)
		textureType = 1;
	else
		textureType = 2;
	valueColor = al_map_rgb(255, 255, 255);
}

Tile::~Tile()
{
}

unsigned int Tile::getValue()
{
	return value;
}

void Tile::connectValues(unsigned int value)
{
	switch(typeConnectValues)
	{
	case CNN_ADD: increment(value); break;
	case CNN_SUB: decrement(value); break;
	}
}

void Tile::decrement(unsigned int value)
{
	if(this->value + 1 > value)
	{
		this->value -= value;
		setTileColor();
	}
}

void Tile::drawTileWithTexture()
{
	switch(textureType)
	{
	case 1: al_draw_tinted_scaled_bitmap(textureBitmap, tileColor, 0, 0, al_get_bitmap_width(textureBitmap), al_get_bitmap_height(textureBitmap), x, y, width, height, 0); break;
	case 2: al_draw_scaled_bitmap(textureBitmap, 0, 0, al_get_bitmap_width(textureBitmap), al_get_bitmap_height(textureBitmap), x, y, width, height, 0); break;
	}
}

void Tile::increment(unsigned int value)
{
	this->value += value;
	setTileColor();
}

void Tile::setTileColor()
{
	switch(value % 31)
	{
	case 1: tileColor = al_map_rgb(255, 0, 0); break;
	case 2: tileColor = al_map_rgb(255, 47, 0); break;
	case 3: tileColor = al_map_rgb(255, 94, 0); break;
	case 4: tileColor = al_map_rgb(255, 140, 0); break;
	case 5: tileColor = al_map_rgb(255, 187, 0); break;
	case 6: tileColor = al_map_rgb(255, 234, 0); break;
	case 7: tileColor = al_map_rgb(229, 255, 0); break;
	case 8: tileColor = al_map_rgb(183, 255, 0); break;
	case 9: tileColor = al_map_rgb(136, 255, 0); break;
	case 10: tileColor = al_map_rgb(89, 255, 0); break;
	case 11: tileColor = al_map_rgb(42, 255, 0); break;
	case 12: tileColor = al_map_rgb(0, 255, 4); break;
	case 13: tileColor = al_map_rgb(0, 255, 51); break;
	case 14: tileColor = al_map_rgb(0, 255, 98); break;
	case 15: tileColor = al_map_rgb(0, 255, 145); break;
	case 16: tileColor = al_map_rgb(0, 255, 191); break;
	case 17: tileColor = al_map_rgb(0, 255, 238); break;
	case 18: tileColor = al_map_rgb(0, 225, 255); break;
	case 19: tileColor = al_map_rgb(0, 178, 255); break;
	case 20: tileColor = al_map_rgb(0, 132, 255); break;
	case 21: tileColor = al_map_rgb(0, 85, 255); break;
	case 22: tileColor = al_map_rgb(0, 38, 255); break;
	case 23: tileColor = al_map_rgb(9, 0, 255); break;
	case 24: tileColor = al_map_rgb(55, 0, 255); break;
	case 25: tileColor = al_map_rgb(102, 0, 255); break;
	case 26: tileColor = al_map_rgb(149, 0, 255); break;
	case 27: tileColor = al_map_rgb(196, 0, 255); break;
	case 28: tileColor = al_map_rgb(242, 0, 255); break;
	case 29: tileColor = al_map_rgb(255, 0, 221); break;
	case 30: tileColor = al_map_rgb(255, 0, 174); break;
	case 0: tileColor = al_map_rgb(255, 0, 127); break;
	default: tileColor = al_map_rgb(0, 0, 0); break;
	}
}

void Tile::setValue(unsigned value)
{
	this->value = value;
	setTileColor();
}

void Tile::setValueString(std::string valueString)
{
	this->valueString = valueString;
}