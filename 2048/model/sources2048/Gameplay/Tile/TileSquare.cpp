#include "TileSquare.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"

TileSquare::TileSquare(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues) : Tile(gameCore, x, y, width, height, tileLength, typeConnectValues)
{
	setTileFontSize(tileLength); //rozmiar fontu wzgl�dem rozmiaru kafelki
}
TileSquare::TileSquare(Game2048Core *gameCore, float x, float y, float width, float height, float tileLength, char typeConnectValues, ALLEGRO_BITMAP *textureBitmap, bool coloringTexture) : Tile(gameCore, x, y, width, height, tileLength, typeConnectValues, textureBitmap, coloringTexture)
{
	setTileFontSize(tileLength); //rozmiar fontu wzgl�dem rozmiaru kafelki
}

TileSquare::~TileSquare()
{
}

void TileSquare::draw()
{
	switch(textureType)
	{
	case 0: al_draw_filled_rectangle(x, y, x + width, y + height, tileColor); break;
	case 1: case 2: drawTileWithTexture(); break;
	}
	al_draw_text(valueFont, valueColor, x + width / 2, y + height / 2 - fontSize / 2, ALLEGRO_ALIGN_CENTER, valueString.c_str());
}

void TileSquare::setTileFontSize(float tileLength)
{
	if(tileLength < 46) // 10
	{
		valueFont = gameCore->fontManager->font_number10->font;
		fontSize = 10;
	}
	else if(tileLength < 55) // 8, 9
	{
		valueFont = gameCore->fontManager->font_number12->font;
		fontSize = 12;
	}
	else if(tileLength < 70) // 7
	{
		valueFont = gameCore->fontManager->font_number16->font;
		fontSize = 16;
	}
	else if(tileLength < 90) // 5, 6
	{
		valueFont = gameCore->fontManager->font_number18->font;
		fontSize = 18;
	}
	else if(tileLength < 120) // 4
	{
		valueFont = gameCore->fontManager->font_number25->font;
		fontSize = 25;
	}
	else // 3
	{
		valueFont = gameCore->fontManager->font_number32->font;
		fontSize = 32;
	}
}