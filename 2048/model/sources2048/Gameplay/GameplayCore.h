#pragma once
#include <string>
#include <sstream>
#include <vector>
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>

#define BOX_HUD_FIELD_STEPS 0
#define BOX_HUD_FIELD_SCORES 1
#define BOX_HUD_FIELD_TIME 2

#define GAME_MODE_STD 0
#define GAME_MODE_BACK 1
#define GAME_MODE_FIB 2

class BoardDrawing;
class JGP_IOSTREAM_FileBuffer;
class Game2048ConfigFileManager;
class Game2048Core;
class Game2048FontManager;
class MouseGestures;
class Scores;
class Steps;
class Tile;

class TextBox;
class TextWritelnField;

class GameplayCore
{
protected:
	Game2048Core *gameCore;
	BoardDrawing *boardDrawing;
	Game2048FontManager *fontManager;
	Steps *steps;
	MouseGestures *mouseGestures;
	Scores *scores;
	
	TextBox *gameHUD;
	std::vector<TextWritelnField*> HUDFields;

	ALLEGRO_BITMAP *screenshot;
	ALLEGRO_BITMAP *screenshot_tmp;
	ALLEGRO_BITMAP *squareTexture;
	ALLEGRO_BITMAP *textureBitmap;
	JGP_IOSTREAM_FileBuffer* textureFileBuffer;
		
	bool changed;
	bool joined;

	bool continueGameAtEnd;
	bool won;
	bool gameLoaded;
	bool saveThisGame;
	bool reversingMovements;

	unsigned int timeToHideMouse;
	
	std::vector<std::vector<Tile*>> *boardTab;
	bool coloringCustomTextures;
	char typeConnectValues;
	int activeSquares;
	int boardType;
	int gameMode;
	int selectedTextureType;
	int showTexture;
	int squaresCount;
	int winningNumber;
	unsigned initialNumber;
	
	int boardHexSize;
	int boardSizeX;
	int boardSizeY;
	
	unsigned time;
	unsigned hour;
	unsigned minute;
	unsigned second;
	unsigned elapsedMilliseconds;
	std::stringstream timeText;
	
	std::string fileName;
	std::string textureFileType;
	std::string lastModificationTime;
	
	bool checkChanges();
	bool placedWinningNumber();
	void addNewSquare();
	void addScores(bool resetLastScores, unsigned int value);
	void boardMove(int keyCode);
	void resetStatesOfBoard();
	void resetTime();
	void setSquareValueFibonacci(int x, int y);
	void setSquareValuePower2(int x, int y);
	void setStringTime();
	void setTexture(std::vector<ALLEGRO_BITMAP*> *textures, std::vector<std::string> *customTexturesDirectories, bool coloringCustomTextures, bool randTexture, int selectedTexture);
	void updateGameHUD();
	void updateTime(unsigned elapsedTime);
	
	virtual bool isNoMovement() = 0;
	virtual bool joinExistingSquares(int keyCode, bool haveAdd = true) = 0; //funkcja s�u��ca do po��czenia (z regu�y sumowania warto�ci kwadrat�w - zale�nie od trybu rozgrywki) kwadrat�w po przesuni�ciu planszy if(haveAdd == true), oraz do sprawdzania czy s� jeszcze mo�liwe ruchy if(haveAdd == false).
	virtual bool moveToYourDirection(int keyCode) = 0;
public:
	GameplayCore(Game2048Core *gameCore);
	virtual ~GameplayCore();
	
	bool isAllowContinueGameAtEnd();

	int getActiveSquares();
	
	void initBoard();
	void reset();
	void setMouseHiding(); // ustawia mouseManager na ukrywanie kursora je�li nieaktywny
	void update(unsigned elapsedTime);
	
	virtual Tile* loadCorrectTile(unsigned int x, unsigned int y) = 0;
	virtual void createBoard(int boardSize) = 0;
	virtual void createBoard(int boardSizeX, int boardSizeY) = 0;
	virtual void draw() = 0;
	virtual void processInput(unsigned elapsedTime) = 0;
	virtual void setTexture() = 0;

	friend class BoardDrawingHexagon;
	friend class BoardDrawingStandard;
	friend class Game2048GameEnd;
	friend class Game2048GamePause;
	friend class Steps;
	
	friend class LoaderGame;
	friend class NewGameInitializer;
	friend class SaveGame;
	friend class SaveWindow;
	
	friend class LastEarnedPoints;
};