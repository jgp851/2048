#pragma once
#include "HexagonBoard.h"

class Game2048Core;

class GameplayBackwardsOnHexBoard : public HexagonBoard
{
private:
	bool joinExistingSquares(int keyCode, bool haveAdd = true);
	void setSquareValue(int x, int y);
	void setWinningNumber();
public:
	GameplayBackwardsOnHexBoard(Game2048Core *gameCore);
	~GameplayBackwardsOnHexBoard();
};