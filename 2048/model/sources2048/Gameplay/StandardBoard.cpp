#include "StandardBoard.h"
#include "Steps.h"
#include "BoardDrawing\BoardDrawingStandard.h"
#include "MouseGestures\MouseGesturesStd.h"
#include "Points\LastEarnedPoints.h"
#include "Points\Scores.h"
#include "Tile\TileSquare.h"
#include "..\Game2048Core.h"
#include "..\Game2048FontManager.h"
#include "..\..\gamecore\TextBox\TextBox.h"

StandardBoard::StandardBoard(Game2048Core *gameCore) : GameplayCore(gameCore)
{
	this->boardDrawing = new BoardDrawingStandard(gameCore);
	this->mouseGestures = new MouseGesturesStd(gameCore);

	squareTexture = NULL;
	textureBitmap = NULL;

	boardType = 0;
	screenshot = NULL;
	screenshot_tmp = NULL;
	changed = false;
	gameLoaded = false;
	boardTab = NULL;
}

StandardBoard::~StandardBoard()
{
	if(boardTab != NULL)
		delete boardTab;

	delete boardDrawing;
	al_destroy_bitmap(screenshot);
}

Tile* StandardBoard::loadCorrectTile(unsigned int x, unsigned int y)
{
	if(selectedTextureType == 1 && showTexture < 0)
		return new TileSquare(gameCore, boardDrawing->getSquareResX(x), boardDrawing->getSquareResY(y), boardDrawing->getSquareWidth(), boardDrawing->getSquareHeight(), boardDrawing->getSquareLength(), typeConnectValues);
	else if(selectedTextureType == 2 && !coloringCustomTextures)
		return new TileSquare(gameCore, boardDrawing->getSquareResX(x), boardDrawing->getSquareResY(y), boardDrawing->getSquareWidth(), boardDrawing->getSquareHeight(), boardDrawing->getSquareLength(), typeConnectValues, textureBitmap, coloringCustomTextures);
	else
		return new TileSquare(gameCore, boardDrawing->getSquareResX(x), boardDrawing->getSquareResY(y), boardDrawing->getSquareWidth(), boardDrawing->getSquareHeight(), boardDrawing->getSquareLength(), typeConnectValues, textureBitmap);
}

bool StandardBoard::connectExistingSquares(int keyCode, bool haveAdd)
{
	bool connected = false;
	switch(keyCode)
	{
	case ALLEGRO_KEY_UP:
		for(int x = 0; x < boardSizeX; x++)
			for(int y = 1; y < boardSizeY; y++)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[x][y-1]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[x][y-1]->connectValues();
						addScores(!connected, (*boardTab)[x][y-1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						connected = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_DOWN:
		for(int x = 0; x < boardSizeX; x++)
			for(int y = boardSizeY - 2; y >= 0; y--)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[x][y+1]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[x][y+1]->connectValues();
						addScores(!connected, (*boardTab)[x][y+1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						connected = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_LEFT:
		for(int y = 0; y < boardSizeY; y++)
			for(int x = 1; x < boardSizeX; x++)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[x-1][y]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[x-1][y]->connectValues();
						addScores(!connected, (*boardTab)[x-1][y]->getValue());
						(*boardTab)[x][y]->setValue(0);
						connected = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	case ALLEGRO_KEY_RIGHT:
		for(int y = 0; y < boardSizeY; y++)
			for(int x = boardSizeX - 2; x >= 0; x--)
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y]->getValue() == (*boardTab)[x+1][y]->getValue())
				{
					if(haveAdd)
					{
						(*boardTab)[x+1][y]->connectValues();
						addScores(!connected, (*boardTab)[x+1][y]->getValue());
						(*boardTab)[x][y]->setValue(0);
						connected = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
				break;
	}
	return connected;
}

bool StandardBoard::connectFibExistingSquares(int keyCode, bool haveAdd)
{
	bool added = false;
	switch(keyCode)
	{
	case ALLEGRO_KEY_UP:
		for(int x = 0; x < boardSizeX; x++)
			for(int y = 1; y < boardSizeY; y++)
			{
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y-1]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[x][y-1]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[x][y-1]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[x][y-1]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[x][y-1]->getValue() == initialNumber)
							(*boardTab)[x][y-1]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[x][y-1]->getValue())
							(*boardTab)[x][y-1]->increment();
						else
							(*boardTab)[x][y-1]->increment(2);
						addScores(!added, (*boardTab)[x][y-1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
				break;
	case ALLEGRO_KEY_DOWN:
		for(int x = 0; x < boardSizeX; x++)
			for(int y = boardSizeY - 2; y >= 0; y--)
			{
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x][y+1]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[x][y+1]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[x][y+1]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[x][y+1]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[x][y+1]->getValue() == initialNumber)
							(*boardTab)[x][y+1]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[x][y+1]->getValue())
							(*boardTab)[x][y+1]->increment();
						else
							(*boardTab)[x][y+1]->increment(2);
						addScores(!added, (*boardTab)[x][y+1]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
				break;
	case ALLEGRO_KEY_LEFT:
		for(int y = 0; y < boardSizeY; y++)
			for(int x = 1; x < boardSizeX; x++)
			{
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x-1][y]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[x-1][y]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[x-1][y]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[x-1][y]->getValue()))
				{
					if(haveAdd)
					{
						if((*boardTab)[x-1][y]->getValue() == initialNumber)
							(*boardTab)[x-1][y]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[x-1][y]->getValue())
							(*boardTab)[x-1][y]->increment();
						else
							(*boardTab)[x-1][y]->increment(2);
						addScores(!added, (*boardTab)[x-1][y]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
				break;
	case ALLEGRO_KEY_RIGHT:
		for(int y = 0; y < boardSizeY; y++)
			for(int x = boardSizeX - 2; x >= 0; x--)
			{
				if((*boardTab)[x][y]->getValue() != 0 && (*boardTab)[x+1][y]->getValue() != 0 &&
					(((*boardTab)[x][y]->getValue() == initialNumber && (*boardTab)[x+1][y]->getValue() == initialNumber) || (*boardTab)[x][y]->getValue() == (*boardTab)[x+1][y]->getValue() + 1 || (*boardTab)[x][y]->getValue() + 1 == (*boardTab)[x+1][y]->getValue()))
				{
					if(haveAdd)
					{						
						if((*boardTab)[x+1][y]->getValue() == initialNumber)
							(*boardTab)[x+1][y]->increment();
						else if((*boardTab)[x][y]->getValue() < (*boardTab)[x+1][y]->getValue())
							(*boardTab)[x+1][y]->increment();
						else
							(*boardTab)[x+1][y]->increment(2);
						addScores(!added, (*boardTab)[x+1][y]->getValue());
						(*boardTab)[x][y]->setValue(0);
						added = true;
						changed = true;
						activeSquares--;
					}
					else
						return true;
				}
			}
				break;
	}
	return added;
}

bool StandardBoard::isNoMovement()
{
	if(!joinExistingSquares(ALLEGRO_KEY_UP, false) && !joinExistingSquares(ALLEGRO_KEY_DOWN, false) && !joinExistingSquares(ALLEGRO_KEY_LEFT, false) && !joinExistingSquares(ALLEGRO_KEY_RIGHT, false))
		return true;
	else
		return false;
}

bool StandardBoard::moveToYourDirection(int keyCode)
{
	bool moved = false;

	switch(keyCode)
	{
	case ALLEGRO_KEY_UP:
		for(int x = 0; x < boardSizeX; x++)
			for(int y = 1; y < boardSizeY; y++)
				if((*boardTab)[x][y]->getValue() != 0)
					for(int z = 0; z < y; z++)
						if((*boardTab)[x][z]->getValue() == 0)
						{
							(*boardTab)[x][z]->setValue((*boardTab)[x][y]->getValue());
							(*boardTab)[x][y]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						break;
	case ALLEGRO_KEY_DOWN:
		for(int x = 0; x < boardSizeX; x++)
			for(int y = boardSizeY - 2; y >= 0; y--)
				if((*boardTab)[x][y]->getValue() != 0)
					for(int z = boardSizeY - 1; z > y; z--)
						if((*boardTab)[x][z]->getValue() == 0)
						{
							(*boardTab)[x][z]->setValue((*boardTab)[x][y]->getValue());
							(*boardTab)[x][y]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						break;
	case ALLEGRO_KEY_LEFT:
		for(int y = 0; y < boardSizeY; y++)
			for(int x = 1; x < boardSizeX; x++)
				if((*boardTab)[x][y]->getValue() != 0)
					for(int z = 0; z < x; z++)
						if((*boardTab)[z][y]->getValue() == 0)
						{
							(*boardTab)[z][y]->setValue((*boardTab)[x][y]->getValue());
							(*boardTab)[x][y]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						break;
	case ALLEGRO_KEY_RIGHT:
		for(int y = 0; y < boardSizeY; y++)
			for(int x = boardSizeX - 2; x >= 0; x--)
				if((*boardTab)[x][y]->getValue() != 0)
					for(int z = boardSizeX - 1; z > x; z--)
						if((*boardTab)[z][y]->getValue() == 0)
						{
							(*boardTab)[z][y]->setValue((*boardTab)[x][y]->getValue());
							(*boardTab)[x][y]->setValue(0);
							moved = true;
							changed = true;
							break;
						}
						break;
	}
	return moved;
}

void StandardBoard::createBoard(int size){} // virtual

void StandardBoard::createBoard(int width, int height)
{
	boardDrawing->setBoard(width, height);
	boardSizeX = width;
	boardSizeY = height;
	squaresCount = boardSizeX * boardSizeY;

	boardTab = new std::vector<std::vector<Tile*>>();
	for(int x = 0; x < width; x++)
		boardTab->push_back(std::vector<Tile*>());
	
	for(int x = 0; x < boardSizeX; x++)
		for(int y = 0; y < boardSizeY; y++)
			(*boardTab)[x].push_back(loadCorrectTile(x, y));
}

void StandardBoard::draw()
{
	boardDrawing->drawBackground();
	for(int x = 0; x < boardSizeX; x++)
		for(int y = 0; y < boardSizeY; y++)
			if((*boardTab)[x][y]->getValue() > 0)
			{
				setSquareValue(x, y);
				(*boardTab)[x][y]->draw();
			}
	boardDrawing->drawBorder();
	scores->draw();
	gameHUD->draw();
}

void StandardBoard::processInput(unsigned elapsedTime)
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
	{
		screenshot_tmp = al_get_backbuffer(gameCore->alManager->getDisplay());
		al_save_bitmap("temp/screenshot.jpg", screenshot_tmp);
		screenshot = gameCore->bitmapLoader->loadBitmap("temp/screenshot.jpg");
		lastModificationTime = gameCore->getCurrentTime("%Y-%m-%d %H:%M:%S");

		gameCore->setGameState(GAME_PAUSE);
		gameCore->mouseManager->stopHiding();
	}

	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_Z, true))
			steps->undoStep();
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL, false) && gameCore->keyManager->isPressed(ALLEGRO_KEY_Y, true))
			steps->nextStep();
	
	mouseGestures->update();
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true) || mouseGestures->upSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_UP);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true) || mouseGestures->downSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_DOWN);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_LEFT, true) || mouseGestures->leftSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_LEFT);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_RIGHT, true) || mouseGestures->rightSlide())
	{
		resetStatesOfBoard();
		mouseGestures->reset();

		boardMove(ALLEGRO_KEY_RIGHT);
		if(changed)
		{
			addNewSquare();
			steps->addStep();
		}
		if(joined && (won || (!won && !placedWinningNumber())))
			scores->setValues();
	}
}

void StandardBoard::setTexture()
{
	std::vector<ALLEGRO_BITMAP*> *textures;
	int selectedTexture;
	// ustawienie tekstury odpowiedniego typu - jeśli texturesCustom nie ma, ustawia tekstury domyślne
	if(gameCore->gameSettings->selectedTextureStdType == 2 && gameCore->mediaManager->texturesCustom.empty())
		gameCore->gameSettings->selectedTextureStdType = 1;
	selectedTextureType = gameCore->gameSettings->selectedTextureStdType;
	switch(selectedTextureType)
	{
	case 1:
		textures = &gameCore->mediaManager->texturesDefault;
		selectedTexture = gameCore->gameSettings->selectedTextureDefault;
		break;
	case 2:
		textures = &gameCore->mediaManager->texturesCustom;
		selectedTexture = gameCore->gameSettings->selectedTextureCustom;
		break;
	}
	GameplayCore::setTexture(textures, &gameCore->mediaManager->customTexturesDirectories, gameCore->gameSettings->coloringCustomTextures, gameCore->gameSettings->randomTexture, selectedTexture);
}