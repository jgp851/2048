#include <vector>
#include <allegro5\allegro.h>

class Game2048Core;
class Scores;
class Tile;

class Steps
{
private:
	Game2048Core *gameCore;
	
	Scores *tmpScores;
	bool drawOnlyTotalSteps;
	unsigned actualStep;
	std::vector<std::vector<unsigned int>> *tmp;
	void copyBoard(std::vector<std::vector<Tile*>> *source, std::vector<std::vector<unsigned int>> *destination);
	void copyBoard(std::vector<std::vector<unsigned int>> *source, std::vector<std::vector<Tile*>> *destination);
	void copyScores(Scores *source, Scores *destination);
	
	void eraseSteps();
public:
	Steps(Game2048Core *gameCore);
	~Steps();
	
	bool isDrawOnlyTotalSteps();

	unsigned getActualStep();

	void addStep();
	void initStep();
	void nextStep();
	void reset();
	void undoStep();
	
	std::vector<std::vector<std::vector<unsigned int>>*> stepsVector;
	std::vector<Scores*> scoresVector;

	friend class LoaderGame;
	friend class SaveGame;
};