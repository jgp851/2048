#pragma once
#include "BoardDrawing.h"

class Game2048Core;

class BoardDrawingStandard : public BoardDrawing
{
private:
	Game2048Core *gameCore;
	
	int width;
	int height;
	//poziom
	float horizontalXbegin;
	float horizontalXend;
	float horizontalY;
	//pion
	float verticalX;
	float verticalYbegin;
	float verticalYend;

	int boardLengthX;
	int boardLengthY;

	int boardCountX;
	int boardCountY;
public:
	BoardDrawingStandard(Game2048Core *gameCore);
	~BoardDrawingStandard();
	
	float getArmWidth(); // virtual
	float getSquareHalfHeight(); // virtual
	float getSquareWidth();
	float getSquareHeight();
	float getSquareLength();
	float getSquareResX(int x);
	float getSquareResY(int y);
	float getSquareResY(int blockX, int blockY); // virtual
	int getWidth();
	int getHeight();
	int getX();
	int getY();

	void drawBackground();
	void drawBorder();
	void setBoard(int size); // virtual
	void setBoard(int x, int y);
	void setTileFontSize();
};