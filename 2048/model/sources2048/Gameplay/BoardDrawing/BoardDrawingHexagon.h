#pragma once
#include <vector>
#include "BoardDrawing.h"

class Game2048Core;

class BoardDrawingHexagon : public BoardDrawing
{
private:
	Game2048Core *gameCore;
	
	std::vector<std::pair<int, float>> columns; // ilo�� element�w, wsp�rz�dna y kolumny
	int width;
	int height;
	int x;
	int y;

	int boardFieldsCount;
	int boardTotalFields;
	float lineLength;
	float hexWidth;
	float hexHeight;
	float hexArmWidth;
	float hexHalfHeight;

public:
	BoardDrawingHexagon(Game2048Core *gameCore);
	~BoardDrawingHexagon();
	
	float getArmWidth();
	float getSquareHalfHeight();
	float getSquareWidth();
	float getSquareHeight();
	float getSquareLength();
	float getSquareResX(int blockX);
	float getSquareResY(int blockY); // virtual
	float getSquareResY(int blockX, int blockY);
	int getWidth();
	int getHeight();
	int getX();
	int getY();
	
	void drawBackground();
	void drawBorder();
	void setBoard(int size);
	void setBoard(int x, int y); // virtual
};