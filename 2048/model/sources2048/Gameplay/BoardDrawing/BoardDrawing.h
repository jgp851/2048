#include <allegro5\allegro.h>
#include <allegro5\allegro_primitives.h>
#include <vector>

#define BOARD_PERCENT_LENTGH gameCore->screenWidth*0.57

class Game2048Core;

class BoardDrawing
{
protected:
	const int boardPercentLength;
	ALLEGRO_COLOR bgColor;
	ALLEGRO_COLOR borderColor;
	int thickness;
public:
	BoardDrawing(Game2048Core *gameCore);
	~BoardDrawing();

	virtual float getArmWidth() = 0;
	virtual float getSquareHalfHeight() = 0;
	virtual float getSquareWidth() = 0;
	virtual float getSquareHeight() = 0;
	virtual float getSquareLength() = 0;
	virtual float getSquareResX(int blockX) = 0;
	virtual float getSquareResY(int blockY) = 0;
	virtual float getSquareResY(int blockX, int blockY) = 0;
	virtual int getWidth() = 0;
	virtual int getHeight() = 0;
	virtual int getX() = 0;
	virtual int getY() = 0;
	int getThickness();

	virtual void drawBackground() = 0;
	virtual void drawBorder() = 0;
	virtual void setBoard(int size) = 0;
	virtual void setBoard(int x, int y) = 0;
};