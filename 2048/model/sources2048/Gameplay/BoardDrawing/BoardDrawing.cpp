#include "BoardDrawing.h"
#include "..\..\Game2048Core.h"

BoardDrawing::BoardDrawing(Game2048Core *gameCore):boardPercentLength(gameCore->appSettings->getScreenHeight() * 0.766)
{
	bgColor = al_map_rgb(162, 145, 120);
	borderColor = al_map_rgb(255, 255, 255);
	thickness = 5;
}

BoardDrawing::~BoardDrawing()
{
}

int BoardDrawing::getThickness()
{
	return thickness;
}