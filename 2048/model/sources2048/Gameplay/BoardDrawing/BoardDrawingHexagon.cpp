#include "BoardDrawingHexagon.h"
#include "..\..\Game2048Core.h"
#include "..\GameplayCore.h"

BoardDrawingHexagon::BoardDrawingHexagon(Game2048Core *gameCore) : BoardDrawing(gameCore)
{
	this->gameCore = gameCore;
}

BoardDrawingHexagon::~BoardDrawingHexagon()
{
}

float BoardDrawingHexagon::getArmWidth()
{
	return hexArmWidth;
}
float BoardDrawingHexagon::getSquareHalfHeight()
{
	return hexHalfHeight;
}
float BoardDrawingHexagon::getSquareWidth()
{
	return hexWidth;
}
float BoardDrawingHexagon::getSquareHeight()
{
	return hexHeight - thickness;
}
float BoardDrawingHexagon::getSquareLength()
{
	return lineLength - thickness / 2.0f;
}
float BoardDrawingHexagon::getSquareResX(int blockX)
{
	float value = x;

	for(int i = 0; i < blockX; i++)
		value += (lineLength * 1.4934);

	return value + thickness / 2;
}
float BoardDrawingHexagon::getSquareResY(int blockY) { return 0; }
float BoardDrawingHexagon::getSquareResY(int blockX, int blockY)
{
	float value;
	value = columns[blockX].second + (hexHeight * blockY);
	return value + thickness / 2.0f;
}

int BoardDrawingHexagon::getWidth()
{
	return width;
}

int BoardDrawingHexagon::getHeight()
{
	return height;
}

int BoardDrawingHexagon::getX()
{
	return x;
}

int BoardDrawingHexagon::getY()
{
	return y;
}

void BoardDrawingHexagon::drawBackground()
{
	for(int x = 0; x < static_cast<int>(columns.size()); x++)
		for(int y = 0; y < static_cast<int>(columns[x].first); y++)
		{
			al_draw_filled_rectangle(getSquareResX(x) + getArmWidth(),
				getSquareResY(x, y),
				getSquareResX(x) + getArmWidth() + getSquareLength(),
				getSquareResY(x, y) + getSquareHeight(),
				bgColor);
			al_draw_filled_triangle(
				getSquareResX(x),
				getSquareResY(x, y) + getSquareHalfHeight() - getThickness() / 2,
				getSquareResX(x) + getArmWidth(),
				getSquareResY(x, y) - getThickness() / 2,
				getSquareResX(x) + getArmWidth(),
				getSquareResY(x, y) + getSquareHeight() + getThickness() / 1.5,
				bgColor);
			al_draw_filled_triangle(
				getSquareResX(x) + getSquareWidth() - getThickness() / 2,
				getSquareResY(x, y) + getSquareHalfHeight() - getThickness() / 2,
				getSquareResX(x) + getArmWidth() + getSquareLength(),
				getSquareResY(x, y) - getThickness() / 2,
				getSquareResX(x) + getArmWidth() + getSquareLength(),
				getSquareResY(x, y) + getSquareHeight() + getThickness() / 1.5,
				bgColor);
		}
}

void BoardDrawingHexagon::drawBorder()
{
	float locX = x, locY;
	for(int i = 0; i < boardTotalFields; i++, locX += (lineLength * 1.4934))
	{
		locY = columns[i].second;
		for(int j = 0; j <= columns[i].first; j++, locY += hexHeight)
			al_draw_line(locX + hexArmWidth, locY, locX + hexArmWidth + lineLength, locY, borderColor, thickness);
		
		locY = columns[i].second;
		for(int j = 0; j < columns[i].first; j++, locY += hexHeight)
			al_draw_line(locX + hexArmWidth, locY, locX, locY + hexHalfHeight, borderColor, thickness);
		
		locY = columns[i].second;
		for(int j = 0; j < columns[i].first; j++, locY += hexHeight)
			al_draw_line(locX + hexArmWidth + lineLength, locY, locX + hexWidth, locY + hexHalfHeight, borderColor, thickness);
		
		if(i <= boardTotalFields / 2)
		{
			locY = columns[i].second;
			for(int j = 0; j < columns[i].first; j++, locY += hexHeight)
				if(i == 0 || j == columns[i].first - 1)
					al_draw_line(locX, locY + hexHalfHeight, locX + hexArmWidth, locY + hexHeight, borderColor, thickness);
		}
		if(i >= boardTotalFields / 2)
		{
			locY = columns[i].second;
			for(int j = 0; j < columns[i].first; j++, locY += hexHeight)
				if(i == boardTotalFields - 1 || j == columns[i].first - 1)
					al_draw_line(locX + hexWidth, locY + hexHalfHeight, locX + hexWidth - hexArmWidth, locY + hexHeight, borderColor, thickness);
		}
	}
}

void BoardDrawingHexagon::setBoard(int size)
{
	boardFieldsCount = size;
	boardTotalFields = size * 2 - 1;
	
	width = boardPercentLength * 0.9636;
	height = boardPercentLength;
	lineLength = height * 0.5814 / boardTotalFields;
	hexWidth = lineLength * 1.9867;
	hexHeight = lineLength * 1.72;
	hexArmWidth = lineLength * 0.5066;
	hexHalfHeight = lineLength * 0.8666;

	x = (gameCore->appSettings->getScreenWidth() - width) / 2;
	y = (gameCore->appSettings->getScreenHeight() - height) / 2;
	
	float columnY;
	int total;
	for(int i = 0; i < boardTotalFields; i++)
	{
		if(i < boardTotalFields / 2)
			total = boardTotalFields - (boardTotalFields / 2 - i);
		else if(i > boardTotalFields / 2)
		{
			total = (i - boardTotalFields / 2) - boardTotalFields;
			total = -total;
		}
		else
			total = boardTotalFields;

		columnY = y + (boardTotalFields - total) * hexHeight / 2;
		columns.push_back(std::pair<int, float>(total, columnY));
	}
}

void BoardDrawingHexagon::setBoard(int x, int y) {} // virtual