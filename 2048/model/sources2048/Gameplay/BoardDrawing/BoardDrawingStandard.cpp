#include "BoardDrawingStandard.h"
#include "..\..\Game2048Core.h"
#include "..\GameplayCore.h"

BoardDrawingStandard::BoardDrawingStandard(Game2048Core *gameCore) : BoardDrawing(gameCore)
{
	this->gameCore = gameCore;
}

BoardDrawingStandard::~BoardDrawingStandard()
{
}

float BoardDrawingStandard::getArmWidth() { return 0; } // virtual
float BoardDrawingStandard::getSquareHalfHeight() { return 0; } // virtual
float BoardDrawingStandard::getSquareWidth()
{
	return getSquareLength();
}
float BoardDrawingStandard::getSquareHeight()
{
	return getSquareLength();
}
float BoardDrawingStandard::getSquareLength()
{
	return boardLengthX / static_cast<float>(boardCountX) - thickness;
}
float BoardDrawingStandard::getSquareResX(int x)
{
	float value = verticalX + thickness / 2.0f;

	for(int j = 0; j < x; j++)
		value += (boardLengthX / static_cast<float>(boardCountX));

	return value;
}
float BoardDrawingStandard::getSquareResY(int y)
{
	float value = horizontalY + thickness / 2.0f;

	for(int i = 0; i < y; i++)
		value += (boardLengthY / static_cast<float>(boardCountY));
	
	return value;
}
float BoardDrawingStandard::getSquareResY(int blockX, int blockY) {	return 0; } // virtual

int BoardDrawingStandard::getWidth()
{
	return width;
}

int BoardDrawingStandard::getHeight()
{
	return height;
}

int BoardDrawingStandard::getX()
{
	return verticalX;
}

int BoardDrawingStandard::getY()
{
	return horizontalY;
}

void BoardDrawingStandard::drawBackground()
{
	al_draw_filled_rectangle(verticalX, verticalYbegin, horizontalXend, verticalYend, bgColor);
}

void BoardDrawingStandard::drawBorder()
{
	for(float i = verticalX, j = 0; j <= boardCountX; i += (boardLengthX / static_cast<float>(boardCountX)), j++)
		al_draw_line(i, verticalYbegin, i, verticalYend, borderColor, thickness);
	for(float i = horizontalY, j = 0; j <= boardCountY; i += (boardLengthY / static_cast<float>(boardCountY)), j++)
		al_draw_line(horizontalXbegin, i, horizontalXend, i, borderColor, thickness);
}

void BoardDrawingStandard::setBoard(int size) {} // virtual

void BoardDrawingStandard::setBoard(int x, int y)
{
	boardCountX = x;
	boardCountY = y;

	if(boardCountX > boardCountY)
	{
		boardLengthX =	boardPercentLength;
		boardLengthY = (boardPercentLength / static_cast<float>(boardCountX)) * boardCountY;
	}
	else
	{
		boardLengthX = (boardPercentLength / static_cast<float>(boardCountY)) * boardCountX;
		boardLengthY =	boardPercentLength;
	}

	verticalX = (gameCore->appSettings->getScreenWidth() - boardLengthX) / 2;
	verticalYbegin = (gameCore->appSettings->getScreenHeight() - boardLengthY) / 2;
	verticalYend = (gameCore->appSettings->getScreenHeight() - boardLengthY) / 2 + boardLengthY;

	horizontalY = (gameCore->appSettings->getScreenHeight() - boardLengthY) / 2;
	horizontalXbegin = (gameCore->appSettings->getScreenWidth() - boardLengthX) / 2;
	horizontalXend = (gameCore->appSettings->getScreenWidth() - boardLengthX) / 2 + boardLengthX;

	width = horizontalXend - horizontalXbegin;
	height = verticalYend - verticalYbegin;
}