#include "GameplayCore.h"
#include "Steps.h"
#include "MouseGestures\MouseGesturesHex.h"
#include "MouseGestures\MouseGesturesStd.h"
#include "Points\LastEarnedPoints.h"
#include "Points\Scores.h"
#include "Tile\Tile.h"
#include "..\Game2048Core.h"
#include "..\Game2048FontManager.h"
#include "..\Game2048GameEnd.h"
#include "..\..\gamecore\TextBox\TextBox.h"

GameplayCore::GameplayCore(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->fontManager = gameCore->fontManager;
	this->steps = new Steps(gameCore);
	timeToHideMouse = 3000;
	
	/**
	 * TextBox gameInfo
	 */
	// HUD_FIELD_STEPS liczba kroków
	HUDFields.push_back(new TextWritelnField(0, gameCore->fontManager->font_gameInfoBox,
		gameCore->appSettings->getScreenWidth() * 0.25,
		al_get_font_line_height(gameCore->fontManager->font_gameInfoBox->font),
		al_map_rgb(255, 255, 255), "steps", ALLEGRO_ALIGN_CENTER));
	// HUD_FIELD_SCORES punkty
	HUDFields.push_back(new TextWritelnField(1, gameCore->fontManager->font_gameInfoBox,
		gameCore->appSettings->getScreenWidth() * 0.25,
		al_get_font_line_height(gameCore->fontManager->font_gameInfoBox->font),
		al_map_rgb(255, 255, 255), "scores", ALLEGRO_ALIGN_CENTER));
	// HUD_FIELD_TIME czas gry
	HUDFields.push_back(new TextWritelnField(2, gameCore->fontManager->font_gameInfoBox,
		gameCore->appSettings->getScreenWidth() * 0.25,
		al_get_font_line_height(gameCore->fontManager->font_gameInfoBox->font),
		al_map_rgb(255, 255, 255), "time", ALLEGRO_ALIGN_CENTER));

	gameHUD = new TextBox(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, ALLEGRO_ALIGN_CENTER, &HUDFields, BOX_HORIZONTAL, BOX_STANDARD);
	/**********************************/
}
GameplayCore::~GameplayCore()
{
	delete steps;
	delete scores;
	delete gameHUD;
}

bool GameplayCore::checkChanges()
{
	if(changed)
		return true;
	else
		return false;
}

bool GameplayCore::isAllowContinueGameAtEnd()
{
	return continueGameAtEnd;
}

bool GameplayCore::placedWinningNumber()
{
	for(unsigned x = 0; x < boardTab->size(); x++)
		for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
			if((*boardTab)[x][y]->getValue() == winningNumber)
				return true;
	return false;
}

int GameplayCore::getActiveSquares()
{
	int activeSquares = 0;

	for(unsigned x = 0; x < boardTab->size(); x++)
		for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
			if((*boardTab)[x][y]->getValue() != 0)
				activeSquares++;

	return activeSquares;
}

void GameplayCore::addNewSquare()
{
	if(activeSquares < squaresCount)
	{
		//losowanie miejsca dla kwadratu na planszy
		int random = rand() % (squaresCount - activeSquares);

		bool stop = false;
		for(unsigned x = 0; x < boardTab->size() && !stop; x++)
			for(unsigned y = 0; y < (*boardTab)[x].size() && !stop; y++)
			{
				if((*boardTab)[x][y]->getValue() == 0 && random > 0)
					random--;
				else if((*boardTab)[x][y]->getValue() == 0 && random == 0)
				{
					(*boardTab)[x][y]->setValue(initialNumber);
					stop = true;
				}
			}
		activeSquares++;
	}
}

void GameplayCore::addScores(bool resetLastScores, unsigned int value)
{
	scores->resetLastPoints(resetLastScores);
	scores->addScores(value);
}

void GameplayCore::boardMove(int keyCode)
{
	moveToYourDirection(keyCode);
	joined = joinExistingSquares(keyCode, true);
	if(joined)
		moveToYourDirection(keyCode);
}

void GameplayCore::initBoard()
{
	unsigned random = rand() % squaresCount;
	for(unsigned x = 0; x < boardTab->size(); x++)
		for(unsigned y = 0; y < (*boardTab)[x].size(); y++, random--)
			if(random == 0)
				(*boardTab)[x][y]->setValue(initialNumber);
	activeSquares++;
	steps->initStep();
}

void GameplayCore::reset()
{
	textureFileBuffer = NULL;
	activeSquares = 0;
	gameLoaded = false;
	saveThisGame = false;
	reversingMovements = false;
	won = false;
	mouseGestures->reset();
	resetStatesOfBoard();
	resetTime();
	steps->reset();
	setMouseHiding();
}

void GameplayCore::resetStatesOfBoard()
{
	changed = false;
	joined = false;
}

void GameplayCore::resetTime()
{
	if(!gameLoaded)
	{
		time = 0;
		hour = 0;
		minute = 0;
		second = 0;
		elapsedMilliseconds = 0;
	}
	else
		setStringTime();
}

void GameplayCore::setMouseHiding()
{
	gameCore->mouseManager->setHidingTime(gameCore->alManager->getDisplay(), timeToHideMouse);
}

void GameplayCore::setSquareValueFibonacci(int x, int y)
{
	std::stringstream numberTextSS;
	if((*boardTab)[x][y]->getValue() < 51)
		numberTextSS << gameCore->fibonacci((*boardTab)[x][y]->getValue() + 1);
	else
		numberTextSS << "fib(" << (*boardTab)[x][y]->getValue() + 1 << ")";
	(*boardTab)[x][y]->setValueString(numberTextSS.str());
}

void GameplayCore::setSquareValuePower2(int x, int y)
{
	std::stringstream numberTextSS;
	if((*boardTab)[x][y]->getValue() < 20)
		numberTextSS << pow(2.0, static_cast<int>((*boardTab)[x][y]->getValue()));
	else
		numberTextSS << "2^" << (*boardTab)[x][y]->getValue();
	(*boardTab)[x][y]->setValueString(numberTextSS.str());
}

void GameplayCore::setStringTime()
{
	hour = 0, minute = 0, second = 0;
	std::stringstream timeText;

	if(time >= 3600)
	{
		hour = time / 3600;
		minute = (time - (hour * 3600)) / 60;
		second = (time - (hour * 3600)) - minute * 60;
	}
	else if(time >= 60)
	{
		minute = time / 60;
		second = (time - (minute * 60));
	}
	else
		second = time;
		
	timeText << (*gameCore->LNGmanager)[LNG_GAME_TIME];
	if(hour > 0)
		timeText << hour << ":";
	if(minute < 10)
		timeText << "0";
	timeText << minute << ":";
	if(second < 10)
		timeText << "0";
	timeText << second;

	HUDFields[2]->setText(timeText.str());
}

void GameplayCore::setTexture(std::vector<ALLEGRO_BITMAP*> *textures, std::vector<std::string> *customTexturesDirectories, bool coloringCustomTextures, bool randTexture, int selectedTexture)
{
	this->coloringCustomTextures = coloringCustomTextures;
	switch(selectedTextureType)
	{
	case 1:
		{
			if(selectedTexture == -1) //losowanie tekstury jeśli taka wybrana
			{
				int random = rand() % (textures->size() + 1);
				if(random == textures->size())
					showTexture = -2;
				else
					showTexture = random;
			}
			else
				showTexture = selectedTexture; // jeśli losowano teksturę wyświetlana będzie ta wylosowana w przeciwnym wypadku wybrana.
			
			if(showTexture >= 0)
				textureBitmap = (*textures)[showTexture];
		}
		break;
	case 2:
		{
			if(randTexture) // jeśli wybrano tekstury użytkownika oraz ich losowanie
				showTexture = rand() % textures->size();
			else
				showTexture = selectedTexture;
			
			textureBitmap = (*textures)[showTexture];
			textureFileType = (*customTexturesDirectories)[showTexture].substr((*customTexturesDirectories)[showTexture].rfind('.'));
		}
		break;
	}
}

void GameplayCore::update(unsigned elapsedTime)
{
	this->updateTime(elapsedTime);
	this->updateGameHUD();
	this->scores->update(elapsedTime);
	gameCore->mouseManager->update(elapsedTime);

	if(!won && placedWinningNumber())
	{
		won = true;

		screenshot_tmp = al_get_backbuffer(gameCore->alManager->getDisplay());
		al_save_bitmap("temp/screenshot.jpg", screenshot_tmp);
		screenshot = gameCore->bitmapLoader->loadBitmap("temp/screenshot.jpg");
		lastModificationTime = gameCore->getCurrentTime("%Y-%m-%d %H:%M:%S");

		gameCore->setGameState(GAME_WIN);
		gameCore->mouseManager->stopHiding();
	}

	if(activeSquares == squaresCount && !reversingMovements && isNoMovement())
	{
		gameCore->gameEnd->reset();
		gameCore->setGameState(GAME_END);
		gameCore->mouseManager->stopHiding();
	}
}

void GameplayCore::updateGameHUD()
{
	std::stringstream stepsText;
	if(steps->isDrawOnlyTotalSteps())
		stepsText << (*gameCore->LNGmanager)[LNG_STEPS_COUNT] << steps->stepsVector.size() - 1;
	else
		stepsText << (*gameCore->LNGmanager)[LNG_STEPS_COUNT] << steps->getActualStep() << "/" << steps->stepsVector.size() - 1;
	HUDFields[BOX_HUD_FIELD_STEPS]->setText(stepsText.str());

	HUDFields[BOX_HUD_FIELD_SCORES]->setText((*gameCore->LNGmanager)[LNG_POINTS] + scores->getScoresString());
}

void GameplayCore::updateTime(unsigned elapsedTime)
{
	if(elapsedMilliseconds >= 1000)
	{
		time++;

		if(time >= 3600)
		{
			hour = time / 3600;
			minute = (time - (hour * 3600)) / 60;
			second = (time - (hour * 3600)) - minute * 60;
		}
		else if(time >= 60)
		{
			minute = time / 60;
			second = (time - (minute * 60));
		}
		else
			second = time;

		elapsedMilliseconds %= 1000;
	}
	else
		elapsedMilliseconds += elapsedTime;
	
	timeText.str("");
	timeText << (*gameCore->LNGmanager)[LNG_GAME_TIME];
	if(hour > 0)
		timeText << hour << ":";
	if(minute < 10)
		timeText << "0";
	timeText << minute << ":";
	if(second < 10)
		timeText << "0";
	timeText << second;
	
	HUDFields[BOX_HUD_FIELD_TIME]->setText(timeText.str());
}