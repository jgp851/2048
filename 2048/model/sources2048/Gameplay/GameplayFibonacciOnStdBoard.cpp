#include "GameplayFibonacciOnStdBoard.h"
#include "Points\ScoresFibonacci.h"
#include "..\Game2048Core.h"

GameplayFibonacciOnStdBoard::GameplayFibonacciOnStdBoard(Game2048Core *gameCore) : StandardBoard(gameCore)
{
	gameMode = GAME_MODE_FIB;
	scores = new ScoresFibonacci(gameCore);
	continueGameAtEnd = true;
	initialNumber = 1;
	typeConnectValues = 0;
	setWinningNumber();
}
GameplayFibonacciOnStdBoard::~GameplayFibonacciOnStdBoard() {}

bool GameplayFibonacciOnStdBoard::joinExistingSquares(int keyCode, bool haveAdd)
{
	return StandardBoard::connectFibExistingSquares(keyCode, haveAdd);
}

void GameplayFibonacciOnStdBoard::setSquareValue(int x, int y)
{
	GameplayCore::setSquareValueFibonacci(x, y);
}

void GameplayFibonacciOnStdBoard::setWinningNumber()
{
	winningNumber = gameCore->gameSettings->winningNumber;
}