#pragma once
#include "StandardBoard.h"

class Game2048Core;

class GameplayStandardOnStdBoard : public StandardBoard
{
private:
	bool joinExistingSquares(int keyCode, bool haveAdd = true);
	void setSquareValue(int x, int y);
	void setWinningNumber();
public:
	GameplayStandardOnStdBoard(Game2048Core *gameCore);
	~GameplayStandardOnStdBoard();
};