#pragma once
#include "GameplayCore.h"
#include <vector>

class Game2048Core;

class HexagonBoard : public GameplayCore
{
private:
	int boardTotalFields;

	bool isNoMovement();
	bool moveToYourDirection(int keyCode);
	void drawSquare(int x, int y);
	void setSquareFontSize();
	
	virtual bool joinExistingSquares(int keyCode, bool haveAdd = true) = 0;
	virtual void setSquareValue(int x, int y) = 0;
protected:
	bool connectExistingSquares(int keyCode, bool haveAdd = true);
	bool connectFibExistingSquares(int keyCode, bool haveAdd = true);
public:
	HexagonBoard(Game2048Core *gameCore);
	virtual ~HexagonBoard();

	Tile* loadCorrectTile(unsigned int x, unsigned int y);
	void createBoard(int size);
	void createBoard(int width, int height); // virtual
	void draw();
	void processInput(unsigned elapsedTime);
	void setTexture();
};