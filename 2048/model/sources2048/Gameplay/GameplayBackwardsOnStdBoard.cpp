#include "GameplayBackwardsOnStdBoard.h"
#include "Points\ScoresPower2.h"
#include "..\Game2048Core.h"

GameplayBackwardsOnStdBoard::GameplayBackwardsOnStdBoard(Game2048Core *gameCore) : StandardBoard(gameCore)
{
	gameMode = GAME_MODE_BACK;
	scores = new ScoresPower2(gameCore);
	continueGameAtEnd = false;
	initialNumber = gameCore->gameSettings->backwardsNumber;
	typeConnectValues = 1;
	setWinningNumber();
}
GameplayBackwardsOnStdBoard::~GameplayBackwardsOnStdBoard() {}

bool GameplayBackwardsOnStdBoard::joinExistingSquares(int keyCode, bool haveAdd)
{
	return StandardBoard::connectExistingSquares(keyCode, haveAdd);
}

void GameplayBackwardsOnStdBoard::setSquareValue(int x, int y)
{
	GameplayCore::setSquareValuePower2(x, y);
}

void GameplayBackwardsOnStdBoard::setWinningNumber()
{
	winningNumber = 1;
}