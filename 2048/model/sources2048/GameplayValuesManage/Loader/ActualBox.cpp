#include "ActualBox.h"
#include "LoaderGame.h"
#include "LoaderWindow.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Gameplay\GameplayCore.h"
#include "..\..\Game2048MultimediaManager.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"

#define TXT_LAST_GAME 0
#define TXT_BOARD_TYPE 1
#define TXT_GAME_MODE 2
#define TXT_BOARD_SIZE 3
#define TXT_WINNING_NUMBER 4
#define TXT_TIME 5
#define TXT_GAMEPLAY_WIN 6
#define TXT_TEXTURE_TYPE 7
#define TXT_COLORING_TEXTURES 8

ActualBox::ActualBox(Game2048Core *gameCore, LoaderWindow *window)
{
	this->gameCore = gameCore;
	this->window = window;
	
	this->title = gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.04, 0);

	this->screenshot = NULL;
	this->textureBitmap = NULL;

	this->x = window->fieldX1;
	this->selectedTextureType = 0;
	
	gameDetailsFields.push_back(new TextWritelnField(0, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(1, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(2, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(3, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(4, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(5, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(6, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(7, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
	gameDetailsFields.push_back(new TextWritelnField(8, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));

	gameDetails = new TextBox(gameCore, x + 10, 0, ALLEGRO_ALIGN_LEFT, &gameDetailsFields, BOX_VERTICAL, BOX_GAME_INFO);
}

ActualBox::~ActualBox()
{
	if(screenshot != NULL)
		al_destroy_bitmap(screenshot);
	if(selectedTextureType == 2 && textureBitmap != NULL)
		al_destroy_bitmap(textureBitmap);
	delete gameDetails;
	al_destroy_font(title);
}

bool ActualBox::mouseIsOverScreenshot()
{
	if(gameCore->mouseManager->getX() >= scrX && gameCore->mouseManager->getX() < scrX + scrW && gameCore->mouseManager->getY() >= scrY && gameCore->mouseManager->getY() < scrY + scrH)
		return true;
	return false;
}

void ActualBox::draw()
{
	al_draw_filled_rounded_rectangle(x, y, x + width, y + height, 30, 30, al_map_rgba(20, 10, 110, 127));
	
	gameDetails->setHeightBetweenTwoPoints(y, y + height, BOX_MIDDLE);
	gameDetails->draw();
	
	al_draw_text(title, al_map_rgb(255, 255, 255), x + (scrSourceX - x) / 2, y + (gameDetails->getY() - y) / 2 - al_get_font_line_height(title) / 2, ALLEGRO_ALIGN_CENTER, fileName.c_str());
	
	drawTextureMiniature();
	drawScreenshotMiniature();
}

void ActualBox::drawTextureMiniature()
{
	textureSquareLength = height * 0.36;
	textureX = scrSourceX + scrSourceW / 2 - textureSquareLength / 2;
	textureY = y + height - (height / 2 - 10) / 2 - textureSquareLength / 2;

	if(selectedTextureType == 1) // tekstury standardowe
	{
		if(showTexture >= 0)
			drawTintedTexture();
		else if(showTexture == -2)
		{
			switch(boardType)
			{
			case 0: drawStdPrimitiveTexture(); break;
			case 1: drawHexPrimitiveTexture(); break;
			}
		}
	}
	else
	{
		if(coloringCustomTextures == true) // tak jak w SaveWindow - jeśli tekstura standardowa lub jeśli użytkownika i colorowana to rysuj kolorowaną; w przeciwnym wypadku zwykła
			drawTintedTexture();
		else
			drawNoTintedTexture();
	}
}

void ActualBox::drawNoTintedTexture()
{
	al_draw_scaled_bitmap(textureBitmap,
		0, 0,
		al_get_bitmap_width(textureBitmap),
		al_get_bitmap_height(textureBitmap),
		textureX,
		textureY,
		textureSquareLength,
		textureSquareLength,
		0);
}

void ActualBox::drawTintedTexture()
{
	al_draw_tinted_scaled_bitmap(textureBitmap,
		al_map_rgb(255, 0, 0),
		0, 0,
		al_get_bitmap_width(textureBitmap),
		al_get_bitmap_height(textureBitmap),
		textureX,
		textureY,
		textureSquareLength,
		textureSquareLength,
		0);
}

void ActualBox::drawHexPrimitiveTexture()
{
	float lineLength = textureSquareLength * 0.5033;
	float hexArmWidth = lineLength * 0.5066;
	float hexHalfHeight = textureSquareLength / 2;
	float hexHeight = textureSquareLength;
	float hexWidth = lineLength * 1.9867;
	al_draw_filled_rectangle(textureX + hexArmWidth,
		textureY,
		textureX + hexArmWidth + lineLength - 2.5,
		textureY + hexHeight,
		al_map_rgb(255, 0, 0));
	al_draw_filled_triangle(
		textureX,
		textureY + hexHalfHeight,
		textureX + hexArmWidth,
		textureY,
		textureX + hexArmWidth,
		textureY + hexHeight,
		al_map_rgb(255, 0, 0));
	al_draw_filled_triangle(
		textureX + hexWidth,
		textureY + hexHalfHeight,
		textureX + hexArmWidth + lineLength - 2.5,
		textureY,
		textureX + hexArmWidth + lineLength - 2.5,
		textureY + hexHeight,
		al_map_rgb(255, 0, 0));
	al_draw_line(textureX - 2.5, textureY + textureSquareLength / 2, textureX + hexArmWidth - 1.5, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
	al_draw_line(textureX + hexArmWidth - 2.5, textureY - 2.5, textureX + hexArmWidth + lineLength, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
	al_draw_line(textureX + hexArmWidth + lineLength - 2.5, textureY - 3.5, textureX + hexArmWidth * 2 + lineLength, textureY + textureSquareLength / 2, al_map_rgb(255, 255, 0), 5);
	al_draw_line(textureX + hexArmWidth * 2 + lineLength, textureY + textureSquareLength / 2, textureX + hexArmWidth + lineLength - 2.5, textureY + textureSquareLength + 2.5, al_map_rgb(255, 255, 0), 5);
	al_draw_line(textureX + hexArmWidth - 2.5, textureY + textureSquareLength + 2.5, textureX + hexArmWidth + lineLength, textureY + textureSquareLength + 2.5, al_map_rgb(255, 255, 0), 5);
	al_draw_line(textureX + hexArmWidth - 2.5, textureY + textureSquareLength + 2.5, textureX - 2.5, textureY + textureSquareLength / 2, al_map_rgb(255, 255, 0), 5);
	al_draw_text(gameCore->fontManager->font_selectedStandardHex->font, al_map_rgb(255, 255, 255),
		textureX + textureSquareLength / 2,
		(textureY + textureSquareLength / 2) - /*50% wysokości*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.5 - /*66% wysokości*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.66,
		ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
	al_draw_text(gameCore->fontManager->font_selectedStandardHex->font, al_map_rgb(255, 255, 255),
		textureX + textureSquareLength / 2,
		(textureY + textureSquareLength / 2) - /*50% wysokości*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.5 + /*66% wysokości*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.66,
		ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
}

void ActualBox::drawStdPrimitiveTexture()
{
	al_draw_rectangle(textureX - 2.5, textureY - 2.5, textureX + textureSquareLength + 2.5, textureY + textureSquareLength + 2.5, al_map_rgb(255, 255, 0), 5); // żółte zaznaczenie

	al_draw_filled_rectangle(textureX, textureY, textureX + textureSquareLength, textureY + textureSquareLength, al_map_rgb(255, 0, 0));
	al_draw_text(gameCore->fontManager->font_number25->font, al_map_rgb(255, 255, 255),
		textureX + textureSquareLength / 2,
		(textureY + textureSquareLength / 2) - /*wysokość czcionki*/25 / 2 - /*66% wielkości - 1*/25*0.66 * 1,
		ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
	al_draw_text(gameCore->fontManager->font_number25->font, al_map_rgb(255, 255, 255),
		textureX + textureSquareLength / 2,
		(textureY + textureSquareLength / 2) - /*wysokość czcionki*/25 / 2 + /*66% wielkości - 1*/25*0.66 * 1,
		ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
}

void ActualBox::drawScreenshotMiniature()
{
	al_draw_scaled_bitmap(screenshot,
		0, 0,
		al_get_bitmap_width(screenshot),
		al_get_bitmap_height(screenshot),
		scrX, scrY,
		scrW, scrH,
		0);
	if(mouseIsOverScreenshot() && screenOffset == 0)
		al_draw_filled_rectangle(scrX, scrY, scrX + scrW, scrY + scrH, al_map_rgba(60, 60, 60, 50));
}

void ActualBox::loadHeadFile()
{
	if(screenshot != NULL)
	{
		al_destroy_bitmap(screenshot);
		screenshot = NULL;
	}
	if(selectedTextureType == 2 && textureBitmap != NULL)
	{
		al_destroy_bitmap(textureBitmap);
		textureBitmap = NULL;
	}
	
	std::string path = "saves\\";
	path.append((*window->files)[window->fileSelectedItem]);
	ifstream file(path.c_str(), ios::in | ios::binary);
	char value[256];
	unsigned char value_len;
	
	file.read((char*)&boardType, sizeof(int));
	file.read((char*)&gameMode, sizeof(int));
	file.read((char*)&selectedTextureType, sizeof(int));

	switch(boardType)
	{
	case 0:
		switch(selectedTextureType)
			case 1: textures = &gameCore->mediaManager->texturesDefault; break;
		break;
	case 1:
		switch(selectedTextureType)
			case 1: textures = &gameCore->mediaManager->texturesHexDefault; break;
		break;
	}

	file.read((char*)&value_len, 1);
	file.read(value, value_len);
	value[value_len] = '\0';
	fileName = value;
	
	file.read((char*)&value_len, 1);
	file.read(value, value_len);
	value[value_len] = '\0';
	lastModificationTime = value;
	
	if(selectedTextureType == 1)
	{
		file.read((char*)&showTexture, sizeof(int));
		if(showTexture >= 0)
			textureBitmap = (*textures)[showTexture];
		else
			textureBitmap = NULL;
	}
	else
	{
		file.read((char*)&coloringCustomTextures, sizeof(bool));
		
		char str[256];
		unsigned char str_len;
		file.read((char*)&str_len, 1);
		file.read(str, str_len);
		str[str_len] = '\0';
		textureFileType = str;
		
		unsigned int fileLength;
		file.read((char*)&fileLength, sizeof(unsigned int));
		char *buffer = new char[fileLength];
		file.read(buffer, fileLength);
		
		textureBitmap = gameCore->bitmapLoader->loadBitmap_f(buffer, fileLength, textureFileType);
		delete buffer;
	}
	
	// odczyt screena
		unsigned fileLength;
		file.read((char*)&fileLength, sizeof(unsigned));
		char *buffer = new char[fileLength + 1];
		file.read(buffer, fileLength);

		screenshot = gameCore->bitmapLoader->loadBitmap_f(buffer, fileLength, ".jpg");
		delete buffer;
	
	file.read((char*)&initialNumber, sizeof(unsigned));
	file.read((char*)&winningNumber, sizeof(unsigned));
	file.read((char*)&won, sizeof(bool));

	file.read((char*)&time, sizeof(unsigned));

	switch(boardType)
	{
	case 0:
		file.read((char*)&boardSizeX, sizeof(int));
		file.read((char*)&boardSizeY, sizeof(int));
		break;
	case 1:
		file.read((char*)&boardHexSize, sizeof(int));
		break;
	}

	file.close();
}

void ActualBox::reset()
{
	screenOffset = 0;
	if(window->files != NULL && window->files->size() > 1)
		this->width = window->width - 16;
	else
		this->width = window->width;
}

void ActualBox::resetAnimation()
{
	screenOffset = 0;
	scrX = scrSourceX;
	scrY = scrSourceY;
	scrW = scrSourceW;
	scrH = scrSourceH;
}

void ActualBox::update()
{
	updateDimensionAndCoordinates();
	update_LAST_GAME_field();
	update_BOARD_TYPE_field();
	update_GAME_MODE_field();
	update_BOARD_SIZE_field();
	update_WINNING_NUMBER_field();
	update_TIME_field();
	update_GAMEPLAY_WIN_field();
	update_TEXTURE_TYPE_field();
	update_COLORING_TEXTURES_field();
}

void ActualBox::updateDimensionAndCoordinates()
{
	scrX = gameCore->appSettings->getScreenWidth() / 2 - 10;
	scrY = y + 25;
	scrW = width * 0.47;
	scrH = height * 0.47;
	
	scrSourceX = scrX;
	scrSourceY = scrY;
	scrSourceW = scrW;
	scrSourceH = scrH;
}

void ActualBox::update_LAST_GAME_field()
{
	std::stringstream lastModTimeText;
	lastModTimeText << (*gameCore->LNGmanager)[LNG_LAST_MOD_TIME] << lastModificationTime;
	gameDetailsFields[TXT_LAST_GAME]->setText(lastModTimeText.str().c_str());
}

void ActualBox::update_BOARD_TYPE_field()
{
	std::stringstream boardTypeText;
	boardTypeText << (*gameCore->LNGmanager)[LNG_BOARD_TYPE];
	switch(boardType)
	{
	case 0: boardTypeText << (*gameCore->LNGmanager)[LNG_BOARD_TYPE_STD]; break;
	case 1: boardTypeText << (*gameCore->LNGmanager)[LNG_BOARD_TYPE_HEX]; break;
	}
	gameDetailsFields[TXT_BOARD_TYPE]->setText(boardTypeText.str().c_str());
}

void ActualBox::update_GAME_MODE_field()
{
	std::stringstream gameModeText;
	gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE];
	switch(gameMode)
	{
	case GAME_MODE_STD: gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE_STD]; break;
	case GAME_MODE_BACK: gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE_BACK]; break;
	case GAME_MODE_FIB: gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE_FIB]; break;
	}
	gameDetailsFields[TXT_GAME_MODE]->setText(gameModeText.str().c_str());
}

void ActualBox::update_BOARD_SIZE_field()
{
	std::stringstream boardSizeText;
	boardSizeText << (*gameCore->LNGmanager)[LNG_BOARD_SIZE];
	switch(boardType)
	{
	case 0: boardSizeText << boardSizeX << "x" << boardSizeY; break;
	case 1: boardSizeText << boardHexSize; break;
	}
	gameDetailsFields[TXT_BOARD_SIZE]->setText(boardSizeText.str().c_str());
}

void ActualBox::update_WINNING_NUMBER_field()
{
	std::stringstream winningNumberText;
	switch(gameMode)
	{
	case GAME_MODE_STD: winningNumberText << (*gameCore->LNGmanager)[LNG_WINNING_NUMBER] << pow(2.0, winningNumber); break;
	case GAME_MODE_BACK: winningNumberText << (*gameCore->LNGmanager)[LNG_BEGIN_NUMBER] << pow(2.0, initialNumber); break;
	case GAME_MODE_FIB: winningNumberText << (*gameCore->LNGmanager)[LNG_WINNING_NUMBER] << gameCore->fibonacci(winningNumber + 1); break;
	}
	gameDetailsFields[TXT_WINNING_NUMBER]->setText(winningNumberText.str().c_str());
}

void ActualBox::update_TIME_field()
{
	unsigned hour = 0, minute = 0, second = 0;
	if(time >= 3600)
	{
		hour = time / 3600;
		minute = (time - (hour * 3600)) / 60;
		second = (time - (hour * 3600)) - minute * 60;
	}
	else if(time >= 60)
	{
		minute = time / 60;
		second = (time - (minute * 60));
	}
	else
		second = time;
	
	std::stringstream timeText;
	timeText << (*gameCore->LNGmanager)[LNG_GAME_TIME];
	if(hour > 0)
		timeText << hour << ":";
	if(minute < 10)
		timeText << "0";
	timeText << minute << ":";
	if(second < 10)
		timeText << "0";
	timeText << second;
	gameDetailsFields[TXT_TIME]->setText(timeText.str().c_str());
}

void ActualBox::update_GAMEPLAY_WIN_field()
{
	std::stringstream wonText;
	wonText << (*gameCore->LNGmanager)[LNG_GAMEPLAY_WON];
	if(won)
		wonText << (*gameCore->LNGmanager)[LNG_YES];
	else
		wonText << (*gameCore->LNGmanager)[LNG_NO];
	gameDetailsFields[TXT_GAMEPLAY_WIN]->setText(wonText.str().c_str());
}

void ActualBox::update_TEXTURE_TYPE_field()
{
	std::stringstream selectedTextureTypeText;
	selectedTextureTypeText << (*gameCore->LNGmanager)[LNG_TEXTURE_TYPE];
	if(selectedTextureType == 1)
		selectedTextureTypeText << (*gameCore->LNGmanager)[LNG_TEXTURE_DEFAULT];
	else
		selectedTextureTypeText << (*gameCore->LNGmanager)[LNG_TEXTURE_CUSTOM];
	gameDetailsFields[TXT_TEXTURE_TYPE]->setText(selectedTextureTypeText.str().c_str());
}

void ActualBox::update_COLORING_TEXTURES_field()
{
	if(selectedTextureType == 2)
	{
		gameDetails->setWritelnVisible(TXT_COLORING_TEXTURES, true);
		std::stringstream coloringCustomTexturesText;
		coloringCustomTexturesText << (*gameCore->LNGmanager)[LNG_COLORING_CUSTOM_TEXTURES];
		if(coloringCustomTextures)
			coloringCustomTexturesText << (*gameCore->LNGmanager)[LNG_YES];
		else
			coloringCustomTexturesText << (*gameCore->LNGmanager)[LNG_NO];
		gameDetailsFields[TXT_COLORING_TEXTURES]->setText(coloringCustomTexturesText.str().c_str());
	}
	else
		gameDetails->setWritelnVisible(TXT_COLORING_TEXTURES, false);
}

void ActualBox::updateAnimation(unsigned elapsedTime)
{
	screenOffset += elapsedTime;

	if(screenOffset >= SCREEN_ANIMATION_TIME)
	{
		gameCore->setGameState(GAME);
		if(screenshot != NULL)
		{
			al_destroy_bitmap(screenshot);
			screenshot = NULL;
		}
		if(selectedTextureType == 2 && textureBitmap != NULL)
		{
			al_destroy_bitmap(textureBitmap);
			textureBitmap = NULL;
		}
	}

	scrX = scrSourceX + (screenOffset / SCREEN_ANIMATION_TIME * (0 - scrSourceX));
	scrY = scrSourceY + (screenOffset / SCREEN_ANIMATION_TIME * (0 - scrSourceY));
	scrW = scrSourceW + (screenOffset / SCREEN_ANIMATION_TIME * (gameCore->appSettings->getScreenWidth() - scrSourceW));
	scrH = scrSourceH + (screenOffset / SCREEN_ANIMATION_TIME * (gameCore->appSettings->getScreenHeight() - scrSourceH));
}