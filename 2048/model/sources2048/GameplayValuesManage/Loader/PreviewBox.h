#pragma once
#include <string>
#include <allegro5\allegro_font.h>

class LoaderWindow;

struct PreviewBox
{
private:
	LoaderWindow *window;

	ALLEGRO_FONT *font;
public:
	PreviewBox(LoaderWindow *window);
	
	bool isDrawing;

	int x;
	int y;

	int width;
	int height;

	std::string text;
	
	void draw();
	void update(unsigned elapsedTime);
};