#pragma once
#include <vector>
#include <string>
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>

#define SCREEN_ANIMATION_TIME 1500

class Game2048Core;
class TextBox;
class TextWritelnField;
class LoaderWindow;

struct ActualBox
{
private:
	Game2048Core *gameCore;
	LoaderWindow *window;
	std::vector<ALLEGRO_BITMAP*> *textures;

	TextBox* gameDetails;
	std::vector<TextWritelnField*> gameDetailsFields;

	ALLEGRO_FONT *title;

	// dane z pliku
	ALLEGRO_BITMAP *screenshot;
	int scrX;
	int scrY;
	int scrW;
	int scrH;
	int scrSourceX;
	int scrSourceY;
	int scrSourceW;
	int scrSourceH;
	double screenOffset;
	ALLEGRO_BITMAP *textureBitmap;

	std::string fileName;
	std::string lastModificationTime;
	std::string textureFileType;
	
	bool coloringCustomTextures;
	bool won;

	int boardType;
	int boardHexSize;
	int boardSizeX;
	int boardSizeY;
	int gameMode;
	int initialNumber;
	int selectedTextureType;
	int showTexture;
	int winningNumber;
	unsigned time;

	int textureX;
	int textureY;
	int textureSquareLength;
	
	void drawTextureMiniature();
	void drawNoTintedTexture();
	void drawTintedTexture();
	void drawHexPrimitiveTexture();
	void drawStdPrimitiveTexture();

	void drawScreenshotMiniature();

	void updateDimensionAndCoordinates();
	void update_LAST_GAME_field();
	void update_BOARD_TYPE_field();
	void update_GAME_MODE_field();
	void update_BOARD_SIZE_field();
	void update_WINNING_NUMBER_field();
	void update_TIME_field();
	void update_GAMEPLAY_WIN_field();
	void update_TEXTURE_TYPE_field();
	void update_COLORING_TEXTURES_field();

public:
	ActualBox(Game2048Core *gameCore, LoaderWindow *window);
	~ActualBox();

	int x;
	int y;

	int width;
	int height;

	bool mouseIsOverScreenshot();
	void draw();
	void loadHeadFile();
	void reset();
	void resetAnimation();
	void update();
	void updateAnimation(unsigned elapsedTime);
};