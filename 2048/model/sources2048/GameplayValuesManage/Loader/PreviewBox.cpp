#include "PreviewBox.h"
#include "LoaderWindow.h"
#include "..\..\Game2048Core.h"

PreviewBox::PreviewBox(LoaderWindow *window)
{
	this->window = window;
	
	this->x = window->fieldX1;
	this->width = window->width - 16;
	this->font = al_load_ttf_font("gfx/arial.ttf", window->gameCore->appSettings->getScreenHeight() * 0.03, 0);
	this->height = al_get_font_line_height(font);
}

void PreviewBox::draw()
{
	if(this->isDrawing)
		al_draw_text(font, al_map_rgb(255, 255, 255), x + width / 2, y, ALLEGRO_ALIGN_CENTER, text.c_str());
}