#pragma once
#include <vector>
#include <string>
#include <allegro5\allegro.h>

class Game2048Core;
class LoaderWindow;

class LoaderGame
{
private:
	Game2048Core *gameCore;
	LoaderWindow *loaderWindow;
	std::vector<ALLEGRO_BITMAP*> *textures;
	int *selectedTexture;
public:
	LoaderGame(Game2048Core *gameCore);
	~LoaderGame();

	void draw();
	void load(std::string fileName);
	void reset();
	void update(unsigned elapsedTime);
};