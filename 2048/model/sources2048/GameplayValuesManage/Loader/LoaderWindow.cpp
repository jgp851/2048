#include "LoaderWindow.h"
#include "LoaderGame.h"
#include "ActualBox.h"
#include "PreviewBox.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Game2048MainMenu.h"
#include "..\..\..\gamecore\ScrollBar.h"
#include "..\..\..\gamecore\Button\Button.h"
#include "..\..\..\..\..\..\JGP.iostream\JGP.iostream\jgp.iostream.h" // DLL JGP.iostream

LoaderWindow::LoaderWindow(Game2048Core *gameCore, LoaderGame *loaderGame)
{
	this->gameCore = gameCore;
	this->loaderGame = loaderGame;
		
	noFile = new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() / 2, false, (*gameCore->LNGmanager)[LNG_NO_GAME_FILE], BTN_TYP_DEFAULT_BUTTON);
		
	// wartości interfejsu
		x = 0;
		y = 0;
		widthWindow = gameCore->appSettings->getScreenWidth();
		heightWindow = gameCore->appSettings->getScreenHeight();
		
		borderThickness = 16;
		borderX1 = x + borderThickness / 2;
		borderY1 = y + borderThickness / 2;
		borderX2 = x + widthWindow - borderThickness / 2;
		borderY2 = y + heightWindow - borderThickness / 2;

		fieldX1 = borderX1 + borderThickness / 2;
		fieldY1 = borderY1 + borderThickness / 2;
		fieldX2 = borderX2 - borderThickness / 2;
		fieldY2 = borderY2 - borderThickness / 2;
		width = fieldX2 - fieldX1;
		height = fieldY2 - fieldY1;
		
	actualBox = new ActualBox(gameCore, this);
	prevBox = new PreviewBox(this);
	nextBox = new PreviewBox(this);
	scrollBar = new ScrollBar(gameCore, fieldX1 + width - 16, fieldY1, height);
	reset();
}

LoaderWindow::~LoaderWindow()
{
	delete actualBox;
}

void LoaderWindow::draw()
{
	switch(windowState)
	{
	case SELECTING: case ANIMATION:
		prevBox->draw();
		nextBox->draw();

		// pasek przewijania
		if(files->size() > 1)
			scrollBar->draw();
		
		actualBox->draw();
		break;
	case NOFILE: noFile->draw(); break;
	}
}

void LoaderWindow::reset()
{
	files = jgp_iostr_fileListing("saves\\", false);
	
	fileSelectedItem = 0;
	lastFileSelectedItem = 0;

	if(!files->empty())
	{
		sortFiles();
		actualBox->reset();
		updateBoxes();
		setState(SELECTING);
	}
	else
		setState(NOFILE);
	scrollBar->setPositionVariable(&fileSelectedItem);
	scrollBar->setTotalElements(files->size());
}

void LoaderWindow::setState(LoaderWindowState newState)
{
	this->windowState = newState;
}

void LoaderWindow::sortFiles()
{
	std::vector<string> modTime;

	for(unsigned i = 0; i < files->size(); i++)
		if((*files)[i] == "last game.sav")
		{
			files[i] = files[0];
			(*files)[0] = "last game.sav";
			break;
		}
		
	for(unsigned i = 0; i < files->size(); i++)
	{
		std::string path = "saves\\";
		path.append((*files)[i]);
		ifstream file(path.c_str(), ios::in | ios::binary);
		char value[256];
		unsigned char value_len;
		unsigned pos = file.tellg(); 
		file.seekg(pos + sizeof(int) * 2);
		
		file.read((char*)&value_len, 1);
		pos = file.tellg(); 
		file.seekg(pos + value_len);
		
		file.read((char*)&value_len, 1);
		file.read(value, value_len);
		value[value_len] = '\0';

		modTime.push_back(value);
		file.close();
	}

	bool changed = true;
	while(changed)
	{
		changed = false;
		for(unsigned i = 1; i < modTime.size() - 1; i++)
			if(modTime[i] < modTime[i + 1])
			{
				swap(modTime[i], modTime[i + 1]);
				swap(files[i], files[i + 1]);
				changed = true;
			}
	}
}

void LoaderWindow::update(unsigned elapsedTime)
{
	switch(windowState)
	{
	case SELECTING:
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DOWN, true))
		{
			fileSelectedItem++;
			if(fileSelectedItem >= static_cast<int>(files->size()))
				fileSelectedItem = 0;
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_UP, true))
		{
			fileSelectedItem--;
			if(fileSelectedItem < 0)
				fileSelectedItem = static_cast<int>(files->size()) - 1;
		}
		
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_DELETE, true) && (*files)[fileSelectedItem] != "last game.sav")
		{
			std::string tmp = "saves\\";
			tmp.append((*files)[fileSelectedItem]);
			remove(tmp.c_str());
			files->erase(files->begin() + fileSelectedItem);

			if(!files->empty())
			{
				if(fileSelectedItem == files->size())
					fileSelectedItem--;

				if(files->size() == 1)
					actualBox->width = this->width;

				this->updateBoxes();
			}
			else
				this->setState(NOFILE);
			scrollBar->setTotalElements(files->size());
		}

		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true) ||
			(actualBox->mouseIsOverScreenshot() && gameCore->mouseManager->isLeftReleased() && !scrollBar->isSlideBar()))
		{
			loaderGame->load((*files)[fileSelectedItem]);
			actualBox->resetAnimation();
			gameCore->mainMenu->reset();
			setState(ANIMATION);
		}
		
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
		{
			gameCore->setGameState(MAIN_MENU);
			gameCore->mainMenu->setState(ANIMATION_IN);
			gameCore->soundPlayer->playGoOut();
		}

		scrollBar->update();
		if(lastFileSelectedItem != fileSelectedItem)
		{
			lastFileSelectedItem = fileSelectedItem;
			this->updateBoxes();
		}
		break;
	case ANIMATION: actualBox->updateAnimation(elapsedTime); break;
	case NOFILE:
		noFile->update();
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true) || gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true) || noFile->isLeftMousePressed())
		{
			gameCore->setGameState(MAIN_MENU);
			gameCore->mainMenu->setState(ANIMATION_IN);
			gameCore->soundPlayer->playGoIn();
		}
		break;
	}
}

void LoaderWindow::updateBoxes()
{
	// rozmieszczenie
		this->prevBox->isDrawing = false;
		this->nextBox->isDrawing = false;
		this->actualBox->height = this->height;
		this->actualBox->y = fieldY1;

		// prevBox
		if(fileSelectedItem > 0)
		{
			prevBox->isDrawing = true;
			prevBox->x = fieldX1;
			prevBox->y = fieldY1;

			actualBox->height -= prevBox->height;
		}
		// nextBox
		if(fileSelectedItem < static_cast<int>(files->size()) - 1)
		{
			nextBox->isDrawing = true;
			nextBox->x = fieldX1;
			
			actualBox->height -= nextBox->height;
			nextBox->y = fieldY1 + actualBox->height;
			if(prevBox->isDrawing)
				nextBox->y += prevBox->height;
		}

		// actualBox
		if(prevBox->isDrawing)
			actualBox->y = prevBox->y + prevBox->height;

	// zawartość
		if(prevBox->isDrawing)
		{
			prevBox->text = (*files)[fileSelectedItem - 1];
			prevBox->text.erase(prevBox->text.size() - 4, 4);
		}
		if(nextBox->isDrawing)
		{
			nextBox->text = (*files)[fileSelectedItem + 1];
			nextBox->text.erase(nextBox->text.size() - 4, 4);
		}

		actualBox->loadHeadFile();
		actualBox->update();
}