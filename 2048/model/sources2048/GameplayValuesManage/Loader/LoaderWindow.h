#pragma once
#include <vector>

class Game2048Core;
class LoaderGame;
class Button;
struct ActualBox;
struct PreviewBox;
struct ScrollBar;

enum LoaderWindowState
{
	SELECTING,
	NOFILE,
	ANIMATION
};

class LoaderWindow
{
private:
	Game2048Core *gameCore;
	LoaderGame *loaderGame;
	LoaderWindowState windowState;
	
	Button *noFile;

	ActualBox *actualBox;
	PreviewBox *prevBox;
	PreviewBox *nextBox;
	ScrollBar *scrollBar;
	
	// warto�ci interfejsu
		int x;
		int y;
		int widthWindow;
		int heightWindow;
		
		int borderThickness;
		int borderX1;
		int borderY1;
		int borderX2;
		int borderY2;

		int fieldX1;
		int fieldY1;
		int fieldX2;
		int fieldY2;
		int width;
		int height;

	std::vector<const char*> *files;
	int fileSelectedItem;
	int lastFileSelectedItem;
	void sortFiles();
public:
	LoaderWindow(Game2048Core *gameCore, LoaderGame *loaderGame);
	~LoaderWindow();

	void draw();
	void reset();
	void setState(LoaderWindowState newState);
	void update(unsigned elapsedTime);
	void updateBoxes();

	friend struct ActualBox;
	friend struct PreviewBox;
	friend struct ScrollBar;
};