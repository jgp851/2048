#include "LoaderGame.h"
#include "LoaderWindow.h"
#include "..\..\Game2048Core.h"
#include "..\..\GameSettings.h"
#include "..\..\backgrounds\GameBackground\GameBackground.h"
#include "..\..\backgrounds\GameBackground\GameScrollingBackground.h"
#include "..\..\backgrounds\ThingsBg\ThingsBackground.h"
#include "..\..\Gameplay\BoardDrawing\BoardDrawing.h"
#include "..\..\Gameplay\GameplayBackwardsOnHexBoard.h"
#include "..\..\Gameplay\GameplayBackwardsOnStdBoard.h"
#include "..\..\Gameplay\GameplayFibonacciOnHexBoard.h"
#include "..\..\Gameplay\GameplayFibonacciOnStdBoard.h"
#include "..\..\Gameplay\GameplayStandardOnHexBoard.h"
#include "..\..\Gameplay\GameplayStandardOnStdBoard.h"
#include "..\..\Gameplay\Steps.h"
#include "..\..\Gameplay\Points\LastEarnedPoints.h"
#include "..\..\Gameplay\Points\ScoresFibonacci.h"
#include "..\..\Gameplay\Points\ScoresPower2.h"
#include "..\..\Gameplay\Tile\Tile.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"
#include "..\..\..\..\..\..\JGP.iostream\JGP.iostream\jgp.iostream.h"

LoaderGame::LoaderGame(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->loaderWindow = new LoaderWindow(gameCore, this);
	this->textures = NULL;
	this->selectedTexture = NULL;
}

LoaderGame::~LoaderGame()
{
}

void LoaderGame::draw()
{
	loaderWindow->draw();
}

void LoaderGame::load(std::string fileName)
{
	std::string path = "saves\\";
	path += fileName;
	// za�adowanie pliku sav i przypisanie z niego warto�ci dla gry
	ifstream file(path.c_str(), ios::in | ios::binary);
	char value[256];
	unsigned char value_len;
		
		/*
		 * nag��wek
		 */
		int boardType, gameMode, selectedTextureType;
		file.read((char*)&boardType, sizeof(int));
		file.read((char*)&gameMode, sizeof(int));
		file.read((char*)&selectedTextureType, sizeof(int));
		
		gameCore->createNewGameInstance(boardType, gameMode);
		switch(boardType)
		{
		case 0:
			switch(selectedTextureType)
				case 1: textures = &gameCore->mediaManager->texturesDefault; break;
			break;
		case 1:
			switch(selectedTextureType)
				case 1: textures = &gameCore->mediaManager->texturesHexDefault; break;
			break;
		}
		gameCore->game->reset(); //reset gry
		gameCore->gameBackground->reset();

		file.read((char*)&value_len, 1);
		file.read(value, value_len);
		value[value_len] = '\0';
		gameCore->game->fileName = value;
		
		file.read((char*)&value_len, 1);
		file.read(value, value_len);
		value[value_len] = '\0';
		gameCore->game->lastModificationTime = value;

		gameCore->game->selectedTextureType = selectedTextureType;
		if(gameCore->game->selectedTextureType == 1)
		{
			file.read((char*)&gameCore->game->showTexture, sizeof(int));
			if(gameCore->game->showTexture >= 0)
				gameCore->game->textureBitmap = (*textures)[gameCore->game->showTexture];
			else
				gameCore->game->textureBitmap = NULL;
		}
		else
		{
			gameCore->game->showTexture = 0; // W teksturach wbudowanych identyfikuje j� spo�r�d nich. W tym wypadku oznacza jedynie, �e tekstur� jest bitmapa. Jest to warto�ci� umowna.
			file.read((char*)&gameCore->game->coloringCustomTextures, sizeof(bool));
			
			// utworzenie i odczytanie bufora z bitmap� z tekstury
			gameCore->game->textureFileBuffer = new JGP_IOSTREAM_FileBuffer(file);
			gameCore->game->textureBitmap = gameCore->bitmapLoader->loadBitmap_f(gameCore->game->textureFileBuffer->getFileBuffer(), gameCore->game->textureFileBuffer->getFileLength(), gameCore->game->textureFileBuffer->getFileType());
		}

		// omini�cie screena
			unsigned fileLength;
			file.read((char*)&fileLength, sizeof(unsigned));

			unsigned pos = file.tellg();
			file.seekg(pos + fileLength);
		
		file.read((char*)&gameCore->game->initialNumber, sizeof(unsigned));
		file.read((char*)&gameCore->game->winningNumber, sizeof(int));
		file.read((char*)&gameCore->game->won, sizeof(bool));

		file.read((char*)&gameCore->game->time, sizeof(unsigned));

		int boardSizeX, boardSizeY, boardHexSize;
		switch(boardType)
		{
		case 0:
			file.read((char*)&boardSizeX, sizeof(int));
			file.read((char*)&boardSizeY, sizeof(int));
			break;
		case 1:
			file.read((char*)&boardHexSize, sizeof(int));
			break;
		}
	
	/*
	 * pozosta�e
	 */
	switch(boardType)
	{
	case 0: gameCore->game->createBoard(boardSizeX, boardSizeY); break;
	case 1: gameCore->game->createBoard(boardHexSize); break;
	}
		
	file.read((char*)&gameCore->game->elapsedMilliseconds, sizeof(unsigned));
	
	gameCore->game->scores->load(file);
	
	std::vector<std::vector<Tile*>> *boardTab = gameCore->game->boardTab;
	// actual step
		file.read((char*)&gameCore->game->steps->actualStep, sizeof(unsigned));
		file.read((char*)&gameCore->game->steps->drawOnlyTotalSteps, sizeof(bool));
		file.read((char*)&gameCore->game->reversingMovements, sizeof(bool));
		gameCore->gameBackground->thingsBg->load(file);
		gameCore->gameBackground->scrollingBg->load(file);
		
		for(unsigned x = 0; x < boardTab->size(); x++)
			for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
			{
				unsigned value;
				file.read((char*)&value, sizeof(unsigned));
				(*boardTab)[x][y]->setValue(value);
			}
		
		file.read((char*)&gameCore->game->activeSquares, sizeof(int));
		
		// odczyt aktualnych punkt�w
			unsigned size;
			unsigned val;
			file.read((char*)&size, sizeof(unsigned));
			for(unsigned i = 0; i < size; i++)
			{
				file.read((char*)&val, sizeof(unsigned));
				gameCore->game->scores->exponent.push_back(val);
			}

			file.read((char*)&gameCore->game->scores->scoresNumber, sizeof(unsigned));
	
	// previous steps
		unsigned vecSize;
		std::vector<std::vector<unsigned int>> *tmp = NULL;
		Scores *tmpScores = NULL;

		// stepsVector
		file.read((char*)&vecSize, sizeof(unsigned));

		for(unsigned vecNO = 0; vecNO < vecSize; vecNO++)
		{
			tmp = new std::vector<std::vector<unsigned int>>();
			
			for(unsigned x = 0; x < boardTab->size(); x++)
				tmp->push_back(std::vector<unsigned int>());
			
			for(unsigned x = 0; x < boardTab->size(); x++)
				for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
					(*tmp)[x].push_back(0);
				
			for(unsigned x = 0; x < boardTab->size(); x++)
				for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
				{
					unsigned value;
					file.read((char*)&value, sizeof(unsigned));
					(*tmp)[x][y] = value;
				}
			gameCore->game->steps->stepsVector.push_back(tmp);
		}
				
		// scoresVector
		file.read((char*)&vecSize, sizeof(unsigned));

		for(unsigned vecNO = 0; vecNO < vecSize; vecNO++)
		{
			switch(gameMode)
			{
			case GAME_MODE_STD: case GAME_MODE_BACK: tmpScores = new ScoresPower2(); break;
			case GAME_MODE_FIB: tmpScores = new ScoresFibonacci(); break;
			}
			
			unsigned size;
			unsigned val;
			file.read((char*)&size, sizeof(unsigned));
			for(unsigned i = 0; i < size; i++)
			{
				file.read((char*)&val, sizeof(unsigned));
				tmpScores->exponent.push_back(val);
			}

			file.read((char*)&tmpScores->scoresNumber, sizeof(unsigned));

			gameCore->game->steps->scoresVector.push_back(tmpScores);
		}
		
	gameCore->game->gameLoaded = true;
	// ustawianie obiekt�w w odpowiednim miejscu na ekranie.
	gameCore->game->gameHUD->setHeightBetweenTwoPoints(gameCore->game->boardDrawing->getY() + gameCore->game->boardDrawing->getHeight(), gameCore->appSettings->getScreenHeight(), BOX_TOP); // ustawienie HUD pod plansz�.
	gameCore->game->setStringTime();
}

void LoaderGame::reset()
{
	loaderWindow->reset();
}

void LoaderGame::update(unsigned elapsedTime)
{
	loaderWindow->update(elapsedTime);
}