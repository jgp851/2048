#pragma once

class Game2048Core;
class Game2048FontManager;
class GalleryDrawer;

class ItemTextureDefault
{
private:
	Game2048Core *gameCore;
	Game2048FontManager *fontManager;

	GalleryDrawer *galleryDrawer;

	int y;

	int selectedTextureDefaultHere;
	int *selectedTextureDefaultSource;
	int *PNTselectedTextureType;
	int boardType;

	void selectLeftItem();
	void selectRightItem();
public:
	ItemTextureDefault(Game2048Core *gameCore);
	~ItemTextureDefault();
	
	void draw();
	void reset(int boardType);
	void setY(int y);
	void update(unsigned elapsedTime);
};