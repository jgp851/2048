#include <sstream>
#include "ItemTextureCustom.h"
#include "NewGameInitializer.h"
#include "..\..\GalleryDrawer\GalleryDrawerCustom.h"
#include "..\..\Gameplay\GameplayCore.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Game2048MultimediaManager.h"
#include "..\..\..\gamecore\Button\ButtonManager.h"
#include "..\..\..\gamecore\KeyboardButton\KeyboardButton.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"

ItemTextureCustom::ItemTextureCustom(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->fontManager = gameCore->fontManager;

	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_ADD_TEXTURE])); // ADD
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_SELECT_TEXTURE])); // SELECT
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // RANDOM
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // COLORING
	buttonManager = new ButtonManager(gameCore, buttons);

	buttonsInfo = new std::vector<Button*>();
	buttonsInfo->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_FAILED_PATH], BTN_TYP_INFO_BUTTON_BAD)); // FAILED_PATH
	buttonsInfo->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_ADDING_DONE], BTN_TYP_INFO_BUTTON_GOOD)); // ADDING_DONE
	buttonsInfo->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_NO_TEXTURE], BTN_TYP_INFO_BUTTON_BAD)); // NO_TEXTURE
	(*buttonsInfo)[BTN_FAILED_PATH]->setState(BTN_ST_SELECTED);
	(*buttonsInfo)[BTN_ADDING_DONE]->setState(BTN_ST_SELECTED);
	(*buttonsInfo)[BTN_NO_TEXTURE]->setState(BTN_ST_SELECTED);
	
	buttonMoveTypes.push_back(KB_BTN_TYP_COMMA);
	buttonMoveTypes.push_back(KB_BTN_TYP_FULLSTOP);
	buttonMove = new KeyboardButtonPanel(gameCore, buttonMoveTypes, 0, 0, KB_BTN_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_MOVE_TEXTURE], KB_BTN_TEXT_SIDE_RIGHT);
	buttonDeleteType.push_back(KB_BTN_TYP_DELETE);
	buttonDelete = new KeyboardButtonPanel(gameCore, buttonDeleteType, 0, 0, KB_BTN_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_DELETE_TEXTURE], KB_BTN_TEXT_SIDE_RIGHT);
	buttonPanels = new std::vector<KeyboardButtonPanel*>();
	buttonPanels->push_back(buttonMove);
	buttonPanels->push_back(buttonDelete);
	btnPanelManager = new KeyboardPanelManager(gameCore, buttonPanels, gameCore->appSettings->getScreenWidth() / 2, 0, ALLEGRO_ALIGN_CENTER);

	pathFileText.push_back(new TextWritelnField(0, gameCore->fontManager->font_pathTexture,
		gameCore->appSettings->getScreenWidth() * 0.4, 
		al_get_font_line_height(gameCore->fontManager->font_pathTexture->font),
		al_map_rgb(255, 255, 255),
		(*gameCore->LNGmanager)[LNG_WRITE_PATH_FILE],
		ALLEGRO_ALIGN_LEFT));
	pathFile.push_back(new TextReadlnField(1, gameCore->fontManager->font_pathTexture,
		0,
		al_get_font_line_height(gameCore->fontManager->font_pathTexture->font),
		29, 0, al_map_rgb(255, 255, 255)));
	pathFileBox = new TextBox(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, ALLEGRO_ALIGN_CENTER, &pathFileText, &pathFile, BOX_HORIZONTAL, BOX_STANDARD);

	infoAboutFileTypeField.push_back(new TextWritelnField(0, gameCore->fontManager->font_buttonsInfo,
		0,
		0,
		al_map_rgb(255, 255, 255), (*gameCore->LNGmanager)[LNG_INFO_ABOUT_FILE_TYPE_OF_TEXTURE], ALLEGRO_ALIGN_CENTER));
	infoAboutFileTypeField.push_back(new TextWritelnField(1, gameCore->fontManager->font_arial,
		0,
		0,
		al_map_rgb(255, 255, 255), (*gameCore->LNGmanager)[LNG_NOTE_ABOUT_HEXAGON_TEXTURE], ALLEGRO_ALIGN_CENTER));
	infoAboutFileType = new TextBox(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, ALLEGRO_ALIGN_CENTER, &infoAboutFileTypeField, BOX_VERTICAL, BOX_STANDARD);

	addedBitmap = NULL;
	PNTcoloringCustomTextures = NULL;
	PNTrandomCustomTextures = NULL;
	PNTselectedTextureType = NULL;
	PNTselectedTextureCustom = NULL;
	PNTtextureCustomFileNO = NULL;
	texturesCustom = NULL;
	customTexturesDirectories = NULL;

	this->galleryDrawer = new GalleryDrawerCustom(gameCore);
}

ItemTextureCustom::~ItemTextureCustom()
{
	delete buttonManager;
	delete galleryDrawer;
}

void ItemTextureCustom::bookmarkManagement()
{
	if(galleryDrawer->textures->size() > 1)
		randomOptionShow = true;
	else
		randomOptionShow = false;
	if(galleryDrawer->textures->size() == 1 || randomOptionShow)
		coloringOptionShow = true;
	else
		coloringOptionShow = false;
}

void ItemTextureCustom::deleteTexture()
{
	remove((*customTexturesDirectories)[selectedTextureCustomHere].c_str());
	
	al_destroy_bitmap((*texturesCustom)[selectedTextureCustomHere]);
	texturesCustom->erase(texturesCustom->begin() + selectedTextureCustomHere);
	customTexturesDirectories->erase(customTexturesDirectories->begin() + selectedTextureCustomHere);

	if(selectedTextureCustomHere == galleryDrawer->textures->size() && selectedTextureCustomHere > 0)
		selectedTextureCustomHere--;
	if(texturesCustom->empty())
	{
		// wyzerowanie tych zmiennych to takie zabezpieczenie, aby po ponownym dodaniu pierwszej tekstury, była ona właściwie wybrana. Zdarzało się, że pierwsza zmnienna miała wartość 1, a druga 0
		selectedTextureCustomHere = 0;
		texturesCustom->clear();
		customTexturesDirectories->clear();
	}
	*PNTselectedTextureCustom = selectedTextureCustomHere;
	saveTexturesCustomDirectories(customTexturesDirectories);
}

void ItemTextureCustom::draw()
{
	std::string pathText;
	
	switch(textureCustomState)
	{
	case ADD: draw_ADDstate(); break;
	case CHECK_ADDING: draw_CHECK_ADDINGstate(); break;
	case MENU: buttonManager->draw(); break;
	case SELECT: draw_SELECTstate(); break;
	}
}

void ItemTextureCustom::draw_ADDstate()
{
	pathFileBox->setHeight(y);
	pathFileBox->draw();

	y += gameCore->appSettings->getScreenHeight() * 0.085;

	if(showedFormatFileInfo)
	{
		infoAboutFileType->setHeight(y);
		infoAboutFileType->draw();
		y += 45;
	}
}

void ItemTextureCustom::draw_CHECK_ADDINGstate()
{
	// BUTTON FAILED_PATH
	if(!fileCustomTextureAdded)
		(*buttonsInfo)[BTN_FAILED_PATH]->draw();
	// BUTTON ADDING_DONE
	else
		(*buttonsInfo)[BTN_ADDING_DONE]->draw();
}

void ItemTextureCustom::draw_SELECTstate()
{
	if(!texturesCustom->empty())
	{			
		galleryDrawer->setY(y);
		galleryDrawer->draw();
		
		y += galleryDrawer->getHeight() + 18;
		btnPanelManager->setY(y);
		
		// interfejs
			if(galleryDrawer->textures->size() > 1)
				btnPanelManager->setDrawingState(true, 0);
			else
				btnPanelManager->setDrawingState(false, 0);
		btnPanelManager->draw();
	}
	else
	{
		(*buttonsInfo)[BTN_NO_TEXTURE]->setY(y);
		(*buttonsInfo)[BTN_NO_TEXTURE]->draw();
	}
}

void ItemTextureCustom::moveTextureToLeft()
{
	int destinationPlace = selectedTextureCustomHere - 1;
	ALLEGRO_BITMAP *tmp = NULL;
	std::string tmpDir;
	if(destinationPlace < 0)
	{
		destinationPlace = galleryDrawer->textures->size() - 1;
		tmp = (*texturesCustom)[selectedTextureCustomHere];
		tmpDir = (*customTexturesDirectories)[selectedTextureCustomHere];

		for(int i = 0; i < static_cast<signed>(galleryDrawer->textures->size() - 1); i++)
		{
			(*texturesCustom)[i] = (*texturesCustom)[i + 1];
			(*customTexturesDirectories)[i] = (*customTexturesDirectories)[i + 1];
		}
		(*texturesCustom)[destinationPlace] = tmp;
		(*customTexturesDirectories)[destinationPlace] = tmpDir;
	}
	else
	{
		tmp = (*texturesCustom)[destinationPlace];
		tmpDir = (*customTexturesDirectories)[destinationPlace];

		(*texturesCustom)[destinationPlace] = (*texturesCustom)[selectedTextureCustomHere];
		(*customTexturesDirectories)[destinationPlace] = (*customTexturesDirectories)[selectedTextureCustomHere];

		(*texturesCustom)[selectedTextureCustomHere] = tmp;
		(*customTexturesDirectories)[selectedTextureCustomHere] = tmpDir;
	}
	selectedTextureCustomHere = destinationPlace;
	*PNTselectedTextureCustom = selectedTextureCustomHere;
	saveTexturesCustomDirectories(customTexturesDirectories);
}

void ItemTextureCustom::moveTextureToRight()
{
	int destinationPlace = selectedTextureCustomHere + 1;
	ALLEGRO_BITMAP *tmp = NULL;
	std::string tmpDir;
	if(destinationPlace == galleryDrawer->textures->size())
	{
		destinationPlace = 0;
		tmp = (*texturesCustom)[selectedTextureCustomHere];
		tmpDir = (*customTexturesDirectories)[selectedTextureCustomHere];

		for(int i = static_cast<signed>(galleryDrawer->textures->size() - 2); i >= 0; i--)
		{
			(*texturesCustom)[i + 1] = (*texturesCustom)[i];
			(*customTexturesDirectories)[i + 1] = (*customTexturesDirectories)[i];
		}
		(*texturesCustom)[destinationPlace] = tmp;
		(*customTexturesDirectories)[destinationPlace] = tmpDir;
	}
	else
	{
		tmp = (*texturesCustom)[destinationPlace];
		tmpDir = (*customTexturesDirectories)[destinationPlace];

		(*texturesCustom)[destinationPlace] = (*texturesCustom)[selectedTextureCustomHere];
		(*customTexturesDirectories)[destinationPlace] = (*customTexturesDirectories)[selectedTextureCustomHere];

		(*texturesCustom)[selectedTextureCustomHere] = tmp;
		(*customTexturesDirectories)[selectedTextureCustomHere] = tmpDir;
	}
	selectedTextureCustomHere = destinationPlace;
	*PNTselectedTextureCustom = selectedTextureCustomHere;
	saveTexturesCustomDirectories(customTexturesDirectories);
}

void ItemTextureCustom::reset(int boardType)
{
	this->boardType = boardType;
	switch(boardType)
	{
	case 0:
		PNTcoloringCustomTextures = &gameCore->gameSettings->coloringCustomTextures;
		PNTrandomCustomTextures = &gameCore->gameSettings->randomTexture;
		PNTselectedTextureType = &gameCore->gameSettings->selectedTextureStdType;
		PNTselectedTextureCustom = &gameCore->gameSettings->selectedTextureCustom;
		PNTtextureCustomFileNO = &gameCore->gameSettings->textureStdCustomFileNO;
		selectedTextureCustomHere = gameCore->gameSettings->selectedTextureCustom;
		coloringCustomTexturesHere = gameCore->gameSettings->coloringCustomTextures;
		randomCustomTexturesHere = gameCore->gameSettings->randomTexture;
		textureCustomFileNO = gameCore->gameSettings->textureStdCustomFileNO;
		texturesCustom = &gameCore->mediaManager->texturesCustom;
		customTexturesDirectories = &gameCore->mediaManager->customTexturesDirectories;
		infoAboutFileType->setWritelnVisible(1, false);
		break;
	case 1:
		PNTselectedTextureType = &gameCore->gameSettings->selectedTextureHexType;
		PNTselectedTextureCustom = &gameCore->gameSettings->selectedTextureHexCustom;
		PNTcoloringCustomTextures = &gameCore->gameSettings->coloringCustomTexturesHex;
		PNTrandomCustomTextures = &gameCore->gameSettings->randomTextureHex;
		PNTtextureCustomFileNO = &gameCore->gameSettings->textureHexCustomFileNO;
		coloringCustomTexturesHere = gameCore->gameSettings->coloringCustomTexturesHex;
		randomCustomTexturesHere = gameCore->gameSettings->randomTextureHex;
		selectedTextureCustomHere = gameCore->gameSettings->selectedTextureHexCustom;
		textureCustomFileNO = gameCore->gameSettings->textureHexCustomFileNO;
		texturesCustom = &gameCore->mediaManager->texturesHexCustom;
		customTexturesDirectories = &gameCore->mediaManager->customTexturesHexDirectories;
		infoAboutFileType->setWritelnVisible(1, true);
		break;
	}
	galleryDrawer->reset(boardType, &selectedTextureCustomHere);
	setTextureCustomState(MENU);
	buttonManager->selectButton(0);
}

void ItemTextureCustom::resetAddition()
{
	pathFile[0]->reset();
	showedFormatFileInfo = true;
}

void ItemTextureCustom::saveTexturesCustomDirectories(const std::vector<std::string> *data)
{
	std::string path;
	switch(boardType)
	{
	case 0: path = "customTextures.ini"; break;
	case 1: path = "customHexTextures.ini"; break;
	}
	ofstream file(path.c_str(), ios::out | ios::binary | ios::trunc);
	unsigned char name_len;
	
	for(unsigned i = 0; i < data->size(); i++)
	{
		name_len = (*data)[i].size();
		file.write((char*)&name_len, 1);
		file.write((*data)[i].c_str(), name_len);
	}
}

void ItemTextureCustom::setTextureCustomState(TextureCustomState newState)
{
	textureCustomState = newState;
}

void ItemTextureCustom::setY(int y)
{
	this->y = y;
}

void ItemTextureCustom::refreshButtons()
{
	switch(textureCustomState)
	{
	case CHECK_ADDING: refresh_CHECK_ADDINGstate(); break;
	case MENU: refresh_MENUstate(); break;
	}
}

void ItemTextureCustom::refresh_CHECK_ADDINGstate()
{
	// BUTTON FAILED_PATH
	if(!fileCustomTextureAdded)
		(*buttonsInfo)[BTN_FAILED_PATH]->setY(y);
	// BUTTON ADDING_DONE
	else
		(*buttonsInfo)[BTN_ADDING_DONE]->setY(y);
	y += gameCore->appSettings->getScreenHeight() * 0.085;
}

void ItemTextureCustom::refresh_MENUstate()
{
	// BUTTON ADD
	(*buttons)[BTN_ADD]->setY(y);
	y += gameCore->appSettings->getScreenHeight() * 0.085;

	//BUTTON SELECT
	(*buttons)[BTN_SELECT]->setY(y);
	y += gameCore->appSettings->getScreenHeight() * 0.085;

	//BUTTON RANDOM
	if(randomOptionShow)
	{
		std::stringstream randomTexturesText;
		randomTexturesText << (*gameCore->LNGmanager)[LNG_RANDOM_TEXTURE];
		switch(randomCustomTexturesHere)
		{
		case false: randomTexturesText << (*gameCore->LNGmanager)[LNG_NO]; break;
		case true: randomTexturesText << (*gameCore->LNGmanager)[LNG_YES]; break;
		}
		(*buttons)[BTN_RANDOM]->setText(randomTexturesText.str());

		(*buttons)[BTN_RANDOM]->setY(y);
		y += gameCore->appSettings->getScreenHeight() * 0.085;
		(*buttons)[BTN_RANDOM]->setDrawing(true);
	}
	else
		(*buttons)[BTN_RANDOM]->setDrawing(false);
	//BUTTON COLORING
	if(coloringOptionShow)
	{
		std::stringstream coloringCustomTexturesText;
		coloringCustomTexturesText << (*gameCore->LNGmanager)[LNG_COLORING_CUSTOM_TEXTURES];
		switch(coloringCustomTexturesHere)
		{
		case false: coloringCustomTexturesText << (*gameCore->LNGmanager)[LNG_NO]; break;
		case true: coloringCustomTexturesText << (*gameCore->LNGmanager)[LNG_YES]; break;
		}
		(*buttons)[BTN_COLORING]->setText(coloringCustomTexturesText.str());
		
		(*buttons)[BTN_COLORING]->setY(y);
		y += gameCore->appSettings->getScreenHeight() * 0.085;
		(*buttons)[BTN_COLORING]->setDrawing(true);
	}
	else
		(*buttons)[BTN_COLORING]->setDrawing(false);
}

void ItemTextureCustom::selectLeftItem()
{
	selectedTextureCustomHere--;
	if(selectedTextureCustomHere < 0)
		selectedTextureCustomHere = galleryDrawer->textures->size() - 1;
}

void ItemTextureCustom::selectRightItem()
{
	selectedTextureCustomHere++;
	if(selectedTextureCustomHere > static_cast<signed>(galleryDrawer->textures->size() - 1))
		selectedTextureCustomHere = 0;
}

void ItemTextureCustom::update(unsigned elapsedTime)
{
	refreshButtons();
	
	switch(textureCustomState)
	{
	case ADD: update_ADDstate(elapsedTime); break;
	case CHECK_ADDING: update_CHECK_ADDINGstate(elapsedTime); break;
	case MENU: update_MENUstate(elapsedTime); break;
	case SELECT: update_SELECTstate(elapsedTime); break;
	}
}

void ItemTextureCustom::update_ADDstate(unsigned elapsedTime)
{
	switch(pathFile[0]->getState())
	{
	case READLN:
		if(!pathFile[0]->isFieldTextEmpty())
			showedFormatFileInfo = false;

		pathFile[0]->update(elapsedTime);
		break;
	case ENTER:
		{
		string format;
		string textField = pathFile[0]->getFieldText();
		string tmpPath;
		ifstream addedBitmapFile(textField.c_str(), ios::binary);

		//jeśli nie wczytano, sprawdzane są domyślne rozszerzenia plików graficznych, których użytkownik mógł nie podać
		if(!addedBitmapFile)
			for(int i = 0; i < 6; i++)
			{
				format.clear();
				tmpPath.clear();
				switch(i)
				{
				case 0:
					format = ".png";
					break;
				case 1:
					format = ".bmp";
					break;
				case 2:
					format = ".jpg";
					break;
				case 3:
					format = ".jpeg";
					break;
				case 4:
					format = ".pcx";
					break;
				case 5:
					format = ".tga";
					break;
				}

				tmpPath = textField + format;
				addedBitmapFile.open(tmpPath.c_str(), ios::binary);
				if(addedBitmapFile)
				{
					textField = tmpPath;
					break;
				}
			}

		if(addedBitmapFile)
		{
			format.clear();
			fileCustomTextureAdded = false;

			//uzyskanie nazwy pliku
			int nameBegin;
			string nameFile;
			for(int i = textField.size() - 1; textField[i] >= 0; i--)
				if(textField[i] == '\\')
				{
					nameBegin = i + 1;
					break;
				}
			nameFile.insert(0, textField, nameBegin, (textField.size() - nameBegin));
			
			//sprawdzenie czy plik jest odpowiedniego formatu
			int formatBegin;
			for(int i = 0; i < static_cast<signed>(nameFile.size()); i++)
				if(nameFile[i] == '.')
				{
					formatBegin = i + 1;
					break;
				}
			format.insert(0, nameFile, formatBegin, (nameFile.size() - formatBegin));
			if(format == "png" || format == "bmp" || format == "jpg" || format == "jpeg" || format == "pcx" || format == "tga")
				fileCustomTextureAdded = true;
			
			if(fileCustomTextureAdded)
			{
				fileCustomTextureAdded = false;

				//zmiana nazwy pliku na kolejny numer tekstury
				ostringstream ss;
				ss << textureCustomFileNO << "." << format;
				nameFile = ss.str();

				string pathFile = (boardType == 0?"custom textures\\":"custom hex textures\\") + nameFile;

				//kopiowanie pliku
					int destTextSize = 1024 * 0.3486; // określa maksymalną wielkość tekstury jaką jest sens używać w grze. 1024 - najwyższa wartość wysokości z dostępnych rozdzielczości.
					addedBitmap = gameCore->alManager->getResizedBitmap(tmpPath.c_str(), destTextSize, destTextSize); // zeskalowanie bitmapy do pożądanych, mniejszych rozmiarów, jeśli jest za duża
					al_save_bitmap(pathFile.c_str(), addedBitmap); // zapis zeskalowanej bitmapy do pliku. lokalnie w podkatalogu gry

				textField = pathFile;

				customTexturesDirectories->push_back(textField.c_str());
				
				texturesCustom->push_back(addedBitmap);
				fileCustomTextureAdded = true;
				
				*PNTselectedTextureType = 2;
				(*PNTtextureCustomFileNO)++;
				*PNTselectedTextureCustom = galleryDrawer->textures->size() - 1;
				saveTexturesCustomDirectories(customTexturesDirectories);
			}
			randomCustomTexturesHere = 0; // jeśli dodano teksturę - ustawienie opcji losowania na false
		}
		else
			fileCustomTextureAdded = false;
		setTextureCustomState(CHECK_ADDING);
		gameCore->soundPlayer->playGoIn();
		break;
		}
	case ESCAPE:
		setTextureCustomState(MENU);
		gameCore->soundPlayer->playGoOut();
		break;
	}
}

void ItemTextureCustom::update_CHECK_ADDINGstate(unsigned elapsedTime)
{
	(*buttonsInfo)[BTN_FAILED_PATH]->update();
	(*buttonsInfo)[BTN_ADDING_DONE]->update();
	if((*buttonsInfo)[BTN_FAILED_PATH]->isPressed() || (*buttonsInfo)[BTN_ADDING_DONE]->isPressed())
		setTextureCustomState(MENU);
}

void ItemTextureCustom::update_MENUstate(unsigned elapsedTime)
{
	bookmarkManagement();
	buttonManager->update();
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true))
	{
		switch(buttonManager->getSelectedButton())
		{
		case BTN_ADD:
			resetAddition();
			setTextureCustomState(ADD);
			break;
		case BTN_SELECT: setTextureCustomState(SELECT); break;
		default:
			*PNTselectedTextureType = 2;
			*PNTselectedTextureCustom = selectedTextureCustomHere;
			*PNTcoloringCustomTextures = coloringCustomTexturesHere;
			*PNTrandomCustomTextures = randomCustomTexturesHere;
			gameCore->newGameInitializer->setState(MAIN_STATE);
			break;
		}
		gameCore->soundPlayer->playGoIn();
	}
	else if(buttonManager->isLeftMousePressed())
	{
		switch(buttonManager->getSelectedButton())
		{
		case BTN_ADD:
			gameCore->soundPlayer->playGoIn();
			resetAddition();
			setTextureCustomState(ADD);
			break;
		case BTN_SELECT:
			gameCore->soundPlayer->playGoIn();
			setTextureCustomState(SELECT); break;
		}
	}
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
	{
		gameCore->newGameInitializer->setState(TEXTURE_ITEM);
		gameCore->soundPlayer->playGoOut();
	}

	switch(buttonManager->getSelectedButton())
	{
	case BTN_RANDOM:
		if((*buttons)[BTN_RANDOM]->isLeftPressed())
			randomCustomTexturesHere = !randomCustomTexturesHere;
		if((*buttons)[BTN_RANDOM]->isRightPressed())
			randomCustomTexturesHere = !randomCustomTexturesHere;
		break;
	case BTN_COLORING:
		if((*buttons)[BTN_COLORING]->isLeftPressed())
			coloringCustomTexturesHere = !coloringCustomTexturesHere;
		if((*buttons)[BTN_COLORING]->isRightPressed())
			coloringCustomTexturesHere = !coloringCustomTexturesHere;
		break;
	}
}

void ItemTextureCustom::update_SELECTstate(unsigned elapsedTime)
{
	if(!texturesCustom->empty())
	{
		galleryDrawer->update();
		btnPanelManager->update();
		if(galleryDrawer->buttonLeft->isPressed())
			selectLeftItem();
		if(gameCore->mouseManager->isWheelUP())
		{
			gameCore->soundPlayer->playSwitchMenu();
			selectLeftItem();
		}
		if(galleryDrawer->buttonRight->isPressed())
			selectRightItem();
		if(gameCore->mouseManager->isWheelDOWN())
		{
			gameCore->soundPlayer->playSwitchMenu();
			selectRightItem();
		}
		if(galleryDrawer->textureSelected())
		{
			*PNTselectedTextureType = 2;
			*PNTselectedTextureCustom = selectedTextureCustomHere;
			*PNTcoloringCustomTextures = coloringCustomTexturesHere;
			*PNTrandomCustomTextures = randomCustomTexturesHere;
			*PNTrandomCustomTextures = 0; // jeśli wybrano teksturę - ustawienie opcji losowania na false
			gameCore->newGameInitializer->setState(MAIN_STATE);
		}
		
		if(buttonMove->isPressed(0))
		{
			moveTextureToLeft();
			gameCore->soundPlayer->playSwitchMenu();
		}
		
		if(buttonMove->isPressed(1))
		{
			moveTextureToRight();
			gameCore->soundPlayer->playSwitchMenu();
		}
		
		if(buttonDelete->isPressed())
			deleteTexture();
		
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
		{
			buttonManager->selectButton(1);
			setTextureCustomState(MENU);
			gameCore->soundPlayer->playGoOut();
		}
	}
	else
	{
		(*buttonsInfo)[BTN_NO_TEXTURE]->update();
		if((*buttonsInfo)[BTN_NO_TEXTURE]->isPressed() || gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
			setTextureCustomState(MENU);
	}
}