#include "NewGameInitializer.h"
#include "ItemTextureCustom.h"
#include "ItemTextureDefault.h"
#include "..\..\..\gamecore\ApplicationSettings.h"
#include "..\..\..\gamecore\Button\ButtonManager.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"
#include "..\..\backgrounds\GameBackground\GameBackground.h"
#include "..\..\backgrounds\ThingsBg\ThingsBackground.h"
#include "..\..\Gameplay\BoardDrawing\BoardDrawing.h"
#include "..\..\Gameplay\GameplayCore.h"
#include "..\..\Gameplay\Points\LastEarnedPoints.h"
#include "..\..\Gameplay\Points\Scores.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Game2048GameWin.h"
#include "..\..\Game2048MultimediaManager.h"
#include "..\..\GameSettings.h"
#include <sstream>

#define BTN_NEW_GAME 0
#define BTN_BOARD_TYPE 1
#define BTN_GAME_MODE 2
#define BTN_BOARD_SIZE_X 3
#define BTN_BOARD_SIZE_Y 4
#define BTN_BOARD_HEX_SIZE 5
#define BTN_BACKWARDS_NUMBER 6
#define BTN_WINNING_NUMBER 7
#define BTN_TEXTURE 8
#define BTN_START_GAME 9

#define BTN_TEXTURES_DEFAULT 0
#define BTN_TEXTURES_CUSTOM 1

NewGameInitializer::NewGameInitializer(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	textureCustom = new ItemTextureCustom(gameCore);
	textureDefault = new ItemTextureDefault(gameCore);
	
	buttons = new std::vector<Button*>();
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.05, false, (*gameCore->LNGmanager)[LNG_NEW_GAME], BTN_TYP_INFO_BUTTON));
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // boardType
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // gameMode
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // boardSizeX
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // boardSizeY
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // boardHexSize
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // backwardsNumber
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, true)); // winningNumber
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_TEXTURE]));  // TEXTURE
	buttons->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.8, false, (*gameCore->LNGmanager)[LNG_START_GAME]));
	buttonManager = new ButtonManager(gameCore, buttons);
	
	buttonsTextureItem = new std::vector<Button*>();
	buttonsTextureItem->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_TEXTURES_DEFAULT])); // TEXTURES_DEFAULT
	buttonsTextureItem->push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, 0, false, (*gameCore->LNGmanager)[LNG_TEXTURES_CUSTOM])); // TEXTURES_CUSTOM
	buttonTextureItemManager = new ButtonManager(gameCore, buttonsTextureItem);

	backwardsNumber = &gameCore->gameSettings->backwardsNumber;
	boardHexSize = &gameCore->gameSettings->boardHexSize;
	boardSizeX = &gameCore->gameSettings->boardSizeX;
	boardSizeY = &gameCore->gameSettings->boardSizeY;
	boardType = &gameCore->gameSettings->boardType;
	gameMode = &gameCore->gameSettings->gameMode;
	winningNumber = &gameCore->gameSettings->winningNumber;

	setState(MAIN_STATE);
}

NewGameInitializer::~NewGameInitializer()
{
	delete buttonManager;
	delete buttonTextureItemManager;
}

void NewGameInitializer::initNewGame()
{
	gameCore->createNewGameInstance(*boardType, *gameMode);
	gameCore->gameWin->setToContinueGameAtEnd(gameCore->game->isAllowContinueGameAtEnd());
	gameCore->game->reset();
	gameCore->gameBackground->reset();
	gameCore->gameBackground->thingsBg->prepareBackground();

	// nadanie odpowiednich wartości dla nowej gry
	gameCore->game->setTexture();
	switch(*boardType)
	{
	case 0: gameCore->game->createBoard(gameCore->gameSettings->boardSizeX, gameCore->gameSettings->boardSizeY); break;
	case 1: gameCore->game->createBoard(gameCore->gameSettings->boardHexSize); break;
	}
	gameCore->game->initBoard();
	gameCore->game->scores->reset();
	
	gameCore->game->gameHUD->setHeightBetweenTwoPoints(gameCore->game->boardDrawing->getY() + gameCore->game->boardDrawing->getHeight(), gameCore->appSettings->getScreenHeight(), BOX_TOP); // ustawienie HUD pod planszą.
}

void NewGameInitializer::draw()
{
	buttonManager->draw();
	switch(initializerState)
	{
	case TEXTURE_ITEM: buttonTextureItemManager->draw(); break;
	case TEXTURE_TYPE_1: textureDefault->draw(); break;
	case TEXTURE_TYPE_2: textureCustom->draw(); break;
	}
}

void NewGameInitializer::refreshButtons()
{
	yText = gameCore->appSettings->getScreenHeight() * 0.05;
	
	// BUTTON BOARD_TYPE
	if(initializerState == MAIN_STATE)
		(*buttons)[BTN_BOARD_TYPE]->setType(BTN_TYP_AVAILABLE);
	else
		(*buttons)[BTN_BOARD_TYPE]->setType(BTN_TYP_UNAVAILABLE);
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	std::stringstream boardTypeText;
	boardTypeText << (*gameCore->LNGmanager)[LNG_BOARD_TYPE];
	switch(*boardType)
	{
	case 0: boardTypeText << (*gameCore->LNGmanager)[LNG_BOARD_TYPE_STD]; break;
	case 1: boardTypeText << (*gameCore->LNGmanager)[LNG_BOARD_TYPE_HEX]; break;
	}
	(*buttons)[BTN_BOARD_TYPE]->setText(boardTypeText.str());
	(*buttons)[BTN_BOARD_TYPE]->setY(yText);
	
	// BUTTON GAME_MODE
	if(initializerState == MAIN_STATE)
		(*buttons)[BTN_GAME_MODE]->setType(BTN_TYP_AVAILABLE);
	else
		(*buttons)[BTN_GAME_MODE]->setType(BTN_TYP_UNAVAILABLE);
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	std::stringstream gameModeText;
	gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE];
	switch(*gameMode)
	{
	case GAME_MODE_STD: gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE_STD]; break;
	case GAME_MODE_BACK: gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE_BACK]; break;
	case GAME_MODE_FIB: gameModeText << (*gameCore->LNGmanager)[LNG_GAME_MODE_FIB]; break;
	}
	(*buttons)[BTN_GAME_MODE]->setText(gameModeText.str());
	(*buttons)[BTN_GAME_MODE]->setY(yText);

	switch(*boardType)
	{
	case 0:
		{
			// BUTTON BOARD_SIZE_X
			if(initializerState == MAIN_STATE)
				(*buttons)[BTN_BOARD_SIZE_X]->setType(BTN_TYP_AVAILABLE);
			else
				(*buttons)[BTN_BOARD_SIZE_X]->setType(BTN_TYP_UNAVAILABLE);
			yText += gameCore->appSettings->getScreenHeight() * 0.085;
			std::stringstream boardSizeXText;
			boardSizeXText << (*gameCore->LNGmanager)[LNG_BOARD_SIZE_X] << *boardSizeX;
			(*buttons)[BTN_BOARD_SIZE_X]->setText(boardSizeXText.str());
			(*buttons)[BTN_BOARD_SIZE_X]->setY(yText);
			
			// BUTTON BOARD_SIZE_Y
			if(initializerState == MAIN_STATE)
				(*buttons)[BTN_BOARD_SIZE_Y]->setType(BTN_TYP_AVAILABLE);
			else
				(*buttons)[BTN_BOARD_SIZE_Y]->setType(BTN_TYP_UNAVAILABLE);
			yText += gameCore->appSettings->getScreenHeight() * 0.085;
			std::stringstream boardSizeYText;
			boardSizeYText << (*gameCore->LNGmanager)[LNG_BOARD_SIZE_Y] << *boardSizeY;
			(*buttons)[BTN_BOARD_SIZE_Y]->setText(boardSizeYText.str());
			(*buttons)[BTN_BOARD_SIZE_Y]->setY(yText);
		}
		break;
	case 1:
		{
			// BUTTON BOARD_HEX_SIZE
			if(initializerState == MAIN_STATE)
				(*buttons)[BTN_BOARD_HEX_SIZE]->setType(BTN_TYP_AVAILABLE);
			else
				(*buttons)[BTN_BOARD_HEX_SIZE]->setType(BTN_TYP_UNAVAILABLE);
			yText += gameCore->appSettings->getScreenHeight() * 0.085;
			std::stringstream boardHexSizeText;
			boardHexSizeText << (*gameCore->LNGmanager)[LNG_BOARD_SIZE] << *boardHexSize;
			(*buttons)[BTN_BOARD_HEX_SIZE]->setText(boardHexSizeText.str());
			(*buttons)[BTN_BOARD_HEX_SIZE]->setY(yText);
		}
		break;
	}
	
	switch(*gameMode)
	{
	case GAME_MODE_STD: case GAME_MODE_FIB:
		{
			// BUTTON WINNING_NUMBER
			if(initializerState == MAIN_STATE)
				(*buttons)[BTN_WINNING_NUMBER]->setType(BTN_TYP_AVAILABLE);
			else
				(*buttons)[BTN_WINNING_NUMBER]->setType(BTN_TYP_UNAVAILABLE);
			yText += gameCore->appSettings->getScreenHeight() * 0.085;
			std::stringstream winningNumberText;
			winningNumberText << (*gameCore->LNGmanager)[LNG_WINNING_NUMBER];
			switch(*gameMode)
			{
			case GAME_MODE_STD: winningNumberText << pow(2.0, *winningNumber); break;
			case GAME_MODE_FIB: winningNumberText << gameCore->fibonacci(*winningNumber + 1); break;
			}
			(*buttons)[BTN_WINNING_NUMBER]->setText(winningNumberText.str());
			(*buttons)[BTN_WINNING_NUMBER]->setY(yText);
		} break;
	case GAME_MODE_BACK:
		{
			// BUTTON BACKWARDS_NUMBER
			if(initializerState == MAIN_STATE)
				(*buttons)[BTN_BACKWARDS_NUMBER]->setType(BTN_TYP_AVAILABLE);
			else
				(*buttons)[BTN_BACKWARDS_NUMBER]->setType(BTN_TYP_UNAVAILABLE);
			yText += gameCore->appSettings->getScreenHeight() * 0.085;
			std::stringstream backwardsNumberText;
			backwardsNumberText << (*gameCore->LNGmanager)[LNG_BEGIN_NUMBER] << pow(2.0, *backwardsNumber);
			(*buttons)[BTN_BACKWARDS_NUMBER]->setText(backwardsNumberText.str());
			(*buttons)[BTN_BACKWARDS_NUMBER]->setY(yText);
		} break;
	}
	
	// BUTTON TEXTURE
	switch(initializerState)
	{
	case MAIN_STATE: (*buttons)[BTN_TEXTURE]->setType(BTN_TYP_AVAILABLE); break;
	case TEXTURE_ITEM: case TEXTURE_TYPE_1: case TEXTURE_TYPE_2: (*buttons)[BTN_TEXTURE]->setType(BTN_TYP_INFO_BUTTON); break;
	default: (*buttons)[BTN_TEXTURE]->setType(BTN_TYP_UNAVAILABLE); break;
	}
	yText += gameCore->appSettings->getScreenHeight() * 0.085;
	(*buttons)[BTN_TEXTURE]->setY(yText);
	
	switch(initializerState)
	{
	case MAIN_STATE:
		(*buttons)[BTN_START_GAME]->setDrawing(true);
		buttonTextureItemManager->setDrawing(false);
		break;
	case TEXTURE_ITEM:
		(*buttons)[BTN_START_GAME]->setDrawing(false);
		buttonTextureItemManager->setDrawing(true);
		yText += gameCore->appSettings->getScreenHeight() * 0.085;
		(*buttonsTextureItem)[BTN_TEXTURES_DEFAULT]->setY(yText);
		yText += gameCore->appSettings->getScreenHeight() * 0.085;
		(*buttonsTextureItem)[BTN_TEXTURES_CUSTOM]->setY(yText);
		break;
	case TEXTURE_TYPE_1: yText += gameCore->appSettings->getScreenHeight() * 0.085; textureDefault->setY(yText); break;
	case TEXTURE_TYPE_2: yText += gameCore->appSettings->getScreenHeight() * 0.085; textureCustom->setY(yText); break;
	}
}

void NewGameInitializer::reset()
{
	buttonManager->selectButton(BTN_START_GAME);
	switch(*boardType)
	{
	case 0:
		selectedTextureTypeHere = gameCore->gameSettings->selectedTextureStdType;
		textureDefault->reset(*boardType);
		break;
	case 1:
		selectedTextureTypeHere = gameCore->gameSettings->selectedTextureHexType;
		textureCustom->reset(*boardType);
		break;
	}
}

void NewGameInitializer::setDefaultSettings()
{
	*backwardsNumber = 11;
	*boardHexSize = 3;
	*boardSizeX = 4;
	*boardSizeY = 4;
	*boardType = 0;
	*gameMode = 0;
	*winningNumber = 11;
}

void NewGameInitializer::setState(InitializerState newState)
{
	initializerState = newState;
}

void NewGameInitializer::update(unsigned elapsedTime)
{
	refreshButtons();
	switch(initializerState)
	{
	case MAIN_STATE:
		switch(*boardType)
		{
		case 0:
			(*buttons)[BTN_BOARD_SIZE_X]->setDrawing(true);
			(*buttons)[BTN_BOARD_SIZE_Y]->setDrawing(true);
			(*buttons)[BTN_BOARD_HEX_SIZE]->setDrawing(false);
			break;
		case 1:
			(*buttons)[BTN_BOARD_SIZE_X]->setDrawing(false);
			(*buttons)[BTN_BOARD_SIZE_Y]->setDrawing(false);
			(*buttons)[BTN_BOARD_HEX_SIZE]->setDrawing(true);
			break;
		}
		switch(*gameMode)
		{
		case GAME_MODE_STD: case GAME_MODE_FIB:
			(*buttons)[BTN_BACKWARDS_NUMBER]->setDrawing(false);
			(*buttons)[BTN_WINNING_NUMBER]->setDrawing(true);
			break;
		case GAME_MODE_BACK:
			(*buttons)[BTN_BACKWARDS_NUMBER]->setDrawing(true);
			(*buttons)[BTN_WINNING_NUMBER]->setDrawing(false);
			break;
		}
		buttonManager->update();
		switch(buttonManager->getSelectedButton())
		{
		case BTN_BOARD_TYPE:
			{
				bool changed = false;

				if((*buttons)[BTN_BOARD_TYPE]->isLeftPressed())
				{
					changed = true;
					(*boardType)--;
					if(*boardType < 0)
						*boardType = 1;
				}
				if((*buttons)[BTN_BOARD_TYPE]->isRightPressed())
				{
					changed = true;
					(*boardType)--;
					if(*boardType < 0)
						*boardType = 1;
				}
				if(changed)
					switch(*boardType)
					{
					case 0:
						selectedTextureTypeHere = gameCore->gameSettings->selectedTextureStdType;
						break;
					case 1:
						selectedTextureTypeHere = gameCore->gameSettings->selectedTextureHexType;
						break;
					}
			}
			break;
		case BTN_GAME_MODE:
			if((*buttons)[BTN_GAME_MODE]->isLeftPressed())
			{
				(*gameMode)--;
				if(*gameMode < 0)
					*gameMode = 2;
			}
			if((*buttons)[BTN_GAME_MODE]->isRightPressed())
			{
				(*gameMode)++;
				if(*gameMode > 2)
					*gameMode = 0;
			}
			break;
		case BTN_BOARD_SIZE_X:
			if((*buttons)[BTN_BOARD_SIZE_X]->isLeftPressed())
			{
				(*boardSizeX)--;
				if(*boardSizeX < 3)
					*boardSizeX = 10;
				if(!gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL, false))
					*boardSizeY = *boardSizeX;
			}
			if((*buttons)[BTN_BOARD_SIZE_X]->isRightPressed())
			{
				(*boardSizeX)++;
				if(*boardSizeX > 10)
					*boardSizeX = 3;
				if(!gameCore->keyManager->isPressed(ALLEGRO_KEY_LCTRL, false))
					*boardSizeY = *boardSizeX;
			}
			break;
		case BTN_BOARD_SIZE_Y:
			if((*buttons)[BTN_BOARD_SIZE_Y]->isLeftPressed())
			{
				(*boardSizeY)--;
				if(*boardSizeY < 3)
					*boardSizeY = 10;
			}
			if((*buttons)[BTN_BOARD_SIZE_Y]->isRightPressed())
			{
				(*boardSizeY)++;
				if(*boardSizeY > 10)
					*boardSizeY = 3;
			}
			break;
		case BTN_BOARD_HEX_SIZE:
			if((*buttons)[BTN_BOARD_HEX_SIZE]->isLeftPressed())
			{
				(*boardHexSize)--;
				if(*boardHexSize < 2)
					*boardHexSize = 6;
			}
			if((*buttons)[BTN_BOARD_HEX_SIZE]->isRightPressed())
			{
				(*boardHexSize)++;
				if(*boardHexSize > 6)
					*boardHexSize = 2;
			}
			break;
		case BTN_BACKWARDS_NUMBER:
			if((*buttons)[BTN_BACKWARDS_NUMBER]->isLeftPressed())
			{
				(*backwardsNumber)--;
				if(*backwardsNumber < 9)
					*backwardsNumber = 19;
			}
			if((*buttons)[BTN_BACKWARDS_NUMBER]->isRightPressed())
			{
				(*backwardsNumber)++;
				if(*backwardsNumber > 19)
					*backwardsNumber = 9;
			}
			break;
		case BTN_WINNING_NUMBER:
			if((*buttons)[BTN_WINNING_NUMBER]->isLeftPressed())
			{
				(*winningNumber)--;
				if(*winningNumber < 9)
					*winningNumber = 19;
			}
			if((*buttons)[BTN_WINNING_NUMBER]->isRightPressed())
			{
				(*winningNumber)++;
				if(*winningNumber > 19)
					*winningNumber = 9;
			}
			break;
		}
		
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true))
		{
			switch(buttonManager->getSelectedButton())
			{
			case BTN_BOARD_TYPE: case BTN_GAME_MODE: case BTN_BOARD_SIZE_X: case BTN_BOARD_SIZE_Y: case BTN_BOARD_HEX_SIZE: case BTN_BACKWARDS_NUMBER: case BTN_WINNING_NUMBER:
				gameCore->soundPlayer->playGoIn();
				buttonManager->selectButton(BTN_START_GAME);
				break;
			case BTN_TEXTURE: 
				gameCore->soundPlayer->playGoIn();
				setState(TEXTURE_ITEM); break;
			case BTN_START_GAME:
				initNewGame();
				gameCore->setGameState(GAME);
				break;
			}
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_HOME, true)) // domyślne ustawienia
			setDefaultSettings();
		if(buttonManager->isLeftMousePressed())
			switch(buttonManager->getSelectedButton())
			{
			case BTN_TEXTURE: 
				gameCore->soundPlayer->playGoIn();
				setState(TEXTURE_ITEM); break;
			case BTN_START_GAME:
				initNewGame();
				gameCore->setGameState(GAME);
				break;
			}

		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
		{
			gameCore->setGameState(MAIN_MENU);
			gameCore->soundPlayer->playGoOut();
		}
		break;
	case TEXTURE_ITEM:
		buttonManager->updateWithoutInputProcess();
		buttonTextureItemManager->update();
		selectedTextureTypeHere = buttonTextureItemManager->getSelectedButton() + 1;

		if(buttonTextureItemManager->isPressed())
		{
			switch(buttonTextureItemManager->getSelectedButton())
			{
			case 0:
				textureDefault->reset(*boardType);
				setState(TEXTURE_TYPE_1);
				break;
			case 1: 
				textureCustom->reset(*boardType);
				setState(TEXTURE_TYPE_2);
				break;
			}
		}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
		{
			setState(MAIN_STATE);
			gameCore->soundPlayer->playGoOut();
		}
		break;
	case TEXTURE_TYPE_1: buttonManager->updateWithoutInputProcess(); textureDefault->update(elapsedTime); break;
	case TEXTURE_TYPE_2: buttonManager->updateWithoutInputProcess(); textureCustom->update(elapsedTime); break;
	}
}