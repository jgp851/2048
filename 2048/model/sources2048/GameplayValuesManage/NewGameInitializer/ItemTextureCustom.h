#pragma once
#include <allegro5\allegro.h>
#include <string>
#include <vector>
#include "..\..\..\gamecore\KeyboardButton\KeyboardPanelManager.h"

#define BTN_ADD 0
#define BTN_SELECT 1
#define BTN_RANDOM 2
#define BTN_COLORING 3

#define BTN_FAILED_PATH 0
#define BTN_ADDING_DONE 1
#define BTN_NO_TEXTURE 2

class Button;
class ButtonManager;
class KeyboardButtonPanel;
class Game2048Core;
class Game2048FontManager;
class GalleryDrawer;

class TextBox;
class TextReadlnField;
class TextWritelnField;

enum TextureCustomState
{
	MENU,
	ADD,
	CHECK_ADDING,
	SELECT
};

class ItemTextureCustom
{
private:
	Game2048Core *gameCore;
	Game2048FontManager *fontManager;
	GalleryDrawer *galleryDrawer;
	
	std::vector<Button*> *buttons;
	std::vector<Button*> *buttonsInfo;
	ButtonManager *buttonManager;

	KeyboardButtonPanel *buttonMove;
	std::vector<KbBtnType> buttonMoveTypes;
	KeyboardButtonPanel *buttonDelete;
	std::vector<KbBtnType> buttonDeleteType;
	std::vector<KeyboardButtonPanel*> *buttonPanels;
	KeyboardPanelManager *btnPanelManager;

	std::vector<ALLEGRO_BITMAP*> *texturesCustom;
	std::vector<std::string> *customTexturesDirectories;
	
	TextBox *pathFileBox;
	std::vector<TextWritelnField*> pathFileText;
	std::vector<TextReadlnField*> pathFile;

	TextBox *infoAboutFileType;
	std::vector<TextWritelnField*> infoAboutFileTypeField;

	TextureCustomState textureCustomState;
	
	ALLEGRO_BITMAP *addedBitmap;

	int y;

	bool fileCustomTextureAdded;
	bool showedFormatFileInfo;
	bool coloringCustomTexturesHere;
	bool randomCustomTexturesHere;
	int selectedBookmark;
	int selectedTextureCustomHere;
	unsigned textureCustomFileNO;
	bool *PNTcoloringCustomTextures;
	bool *PNTrandomCustomTextures;
	int *PNTselectedTextureType;
	int *PNTselectedTextureCustom;
	unsigned *PNTtextureCustomFileNO;
	
	int totalOptions;
	bool coloringOptionShow;
	bool randomOptionShow;
	int boardType;
	
	void bookmarkManagement();
	void deleteTexture();
	void moveTextureToLeft();
	void moveTextureToRight();
	void saveTexturesCustomDirectories(const std::vector<std::string> *data);
	void selectLeftItem();
	void selectRightItem();

	void draw_ADDstate();
	void draw_CHECK_ADDINGstate();
	void draw_SELECTstate();

	void refreshButtons();
	void refresh_CHECK_ADDINGstate();
	void refresh_MENUstate();
	
	void update_ADDstate(unsigned elapsedTime);
	void update_CHECK_ADDINGstate(unsigned elapsedTime);
	void update_MENUstate(unsigned elapsedTime);
	void update_SELECTstate(unsigned elapsedTime);
public:
	ItemTextureCustom(Game2048Core *gameCore);
	~ItemTextureCustom();
	
	void draw();
	void reset(int boardType);
	void resetAddition();
	void setTextureCustomState(TextureCustomState newState);
	void setY(int y);
	void update(unsigned elapsedTime);

	friend class GalleryDrawerCustom;
};