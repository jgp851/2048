#pragma once
#include <vector>
#include <allegro5\allegro.h>

class Game2048Core;
class Button;
class ButtonManager;
class ItemTextureCustom;
class ItemTextureDefault;

enum InitializerState
{
	MAIN_STATE,
	TEXTURE_ITEM,
	TEXTURE_TYPE_1,
	TEXTURE_TYPE_2
};

class NewGameInitializer
{
private:
	Game2048Core *gameCore;
	ItemTextureCustom *textureCustom;
	ItemTextureDefault *textureDefault;
	InitializerState initializerState;

	std::vector<Button*> *buttons;
	std::vector<Button*> *buttonsTextureItem;
	ButtonManager *buttonManager;
	ButtonManager *buttonTextureItemManager;
	
	int *backwardsNumber;
	int *boardHexSize;
	int *boardSizeX;
	int *boardSizeY;
	int *boardType;
	int *gameMode;
	int *winningNumber;
	int selectedTextureTypeHere;
	int yText;

	void refreshButtons();
	void setDefaultSettings();
public:
	NewGameInitializer(Game2048Core *gameCore);
	~NewGameInitializer();
	
	void initNewGame();

	void draw();
	void reset();
	void setState(InitializerState newState);
	void update(unsigned elapsedTime);
	
	friend class GalleryDrawerCustom;
};