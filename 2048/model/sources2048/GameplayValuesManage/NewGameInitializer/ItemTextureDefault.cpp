#include "ItemTextureDefault.h"
#include "NewGameInitializer.h"
#include "..\..\GalleryDrawer\GalleryDrawerDefault.h"
#include "..\..\Gameplay\GameplayCore.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Game2048MultimediaManager.h"

ItemTextureDefault::ItemTextureDefault(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->fontManager = gameCore->fontManager;
	this->galleryDrawer = new GalleryDrawerDefault(gameCore);
}

ItemTextureDefault::~ItemTextureDefault()
{
	delete galleryDrawer;
}

void ItemTextureDefault::draw()
{
	galleryDrawer->setY(y);
	galleryDrawer->draw();
}

void ItemTextureDefault::reset(int boardType)
{
	this->boardType = boardType;
	switch(boardType)
	{
	case 0:
		selectedTextureDefaultHere = gameCore->gameSettings->selectedTextureDefault;
		selectedTextureDefaultSource = &gameCore->gameSettings->selectedTextureDefault;
		PNTselectedTextureType = &gameCore->gameSettings->selectedTextureStdType;
		break;
	case 1:
		selectedTextureDefaultHere = gameCore->gameSettings->selectedTextureHexDefault;
		selectedTextureDefaultSource = &gameCore->gameSettings->selectedTextureHexDefault;
		PNTselectedTextureType = &gameCore->gameSettings->selectedTextureHexType;
		break;
	}
	galleryDrawer->reset(boardType, &selectedTextureDefaultHere);
}

void ItemTextureDefault::selectLeftItem()
{
	selectedTextureDefaultHere--;
	if(selectedTextureDefaultHere < -2)
		selectedTextureDefaultHere = galleryDrawer->textures->size() - 1;
}

void ItemTextureDefault::selectRightItem()
{
	selectedTextureDefaultHere++;
	if(selectedTextureDefaultHere > static_cast<signed>(galleryDrawer->textures->size() - 1))
		selectedTextureDefaultHere = -2;
}

void ItemTextureDefault::setY(int y)
{
	this->y = y;
}

void ItemTextureDefault::update(unsigned elapsedTime)
{
	galleryDrawer->update();
	if(galleryDrawer->buttonLeft->isPressed())
		selectLeftItem();
	if(gameCore->mouseManager->isWheelUP())
	{
		gameCore->soundPlayer->playSwitchMenu();
		selectLeftItem();
	}
	if(galleryDrawer->buttonRight->isPressed())
		selectRightItem();
	if(gameCore->mouseManager->isWheelDOWN())
	{
		gameCore->soundPlayer->playSwitchMenu();
		selectRightItem();
	}
	if(galleryDrawer->textureSelected())
	{
		*selectedTextureDefaultSource = selectedTextureDefaultHere;
		*PNTselectedTextureType = 1;
		gameCore->newGameInitializer->setState(MAIN_STATE);
	}

	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
	{
		gameCore->newGameInitializer->setState(TEXTURE_ITEM);
		gameCore->soundPlayer->playGoOut();
	}
}