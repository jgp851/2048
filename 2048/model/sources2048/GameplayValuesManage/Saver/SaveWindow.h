#pragma once
#include <vector>
#include <allegro5\allegro.h>

class Game2048Core;
class SaveGame;
class TextBox;
class TextReadlnField;
class TextWritelnField;

class SaveWindow
{
private:
	Game2048Core *gameCore;
	SaveGame *saveGame;
	ALLEGRO_BITMAP *textureBitmap;
	
	// warto�ci interfejsu
		int x;
		int y;
		int widthWindow;
		int heightWindow;
		
		int borderThickness;
		int borderX1;
		int borderY1;
		int borderX2;
		int borderY2;

		int width;
		int height;
		int fieldX1;
		int fieldY1;
		int fieldX2;
		int fieldY2;
	
	// obiekty okna
		TextBox *gameInfo;
		std::vector<TextWritelnField*> gameInfoFields;

		TextBox *path;
		std::vector<TextWritelnField*> pathTextField;
		std::vector<TextReadlnField*> pathField;

		int boardType;
		int selectedTextureType;
		int textureSquareLength;
public:
	SaveWindow(Game2048Core *gameCore, SaveGame *saveGame);
	~SaveWindow();

	std::string getDateInFileFormat();
	void draw();
	void reset(int boardType, int selectedTextureType);

	friend class SaveGame;
};