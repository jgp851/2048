#pragma once
#include <string>
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;
class SaveWindow;

enum SaveGameState
{
	READLN_NAME,
	QST_OVERWRITE,
	QST_THESAMEFILE
};

class SaveGame
{
private:
	Game2048Core *gameCore;
	SaveWindow *saveWindow;
	SaveGameState saveGameState;
	std::vector<Button*> buttonsOverWrite;
	std::vector<Button*> buttonsTheSameFile;
	ButtonManager *buttonOverWriteManager;
	ButtonManager *buttonTheSameFileManager;
	
	bool isUniqueName();
public:
	SaveGame(Game2048Core *gameCore);
	~SaveGame();
	
	void draw();
	void reset();
	void save(std::string fileName);
	void saveLastGame();
	void setSaveGameState(SaveGameState newState);
	void update(unsigned elapsedTime);
};