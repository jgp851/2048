#include "SaveGame.h"
#include "SaveWindow.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Game2048GamePause.h"
#include "..\..\GameSettings.h"
#include "..\..\backgrounds\GameBackground\GameBackground.h"
#include "..\..\backgrounds\GameBackground\GameScrollingBackground.h"
#include "..\..\backgrounds\ThingsBg\ThingsBackground.h"
#include "..\..\Gameplay\GameplayCore.h"
#include "..\..\Gameplay\Steps.h"
#include "..\..\Gameplay\Points\LastEarnedPoints.h"
#include "..\..\Gameplay\Points\Scores.h"
#include "..\..\Gameplay\Tile\Tile.h"
#include "..\..\..\gamecore\Button\ButtonManager.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"
#include "..\..\..\..\..\..\JGP.iostream\JGP.iostream\jgp.iostream.h" // DLL JGP.iostream

SaveGame::SaveGame(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->saveWindow = new SaveWindow(gameCore, this);

	buttonsOverWrite.push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.4, false, (*gameCore->LNGmanager)[LNG_OVERWRITE_FILE_QST], BTN_TYP_DEFAULT_BUTTON));
	buttonsOverWrite.push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.485, false, (*gameCore->LNGmanager)[LNG_OVERWRITE_FILE]));
	buttonsOverWrite.push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.57, false, (*gameCore->LNGmanager)[LNG_CREATE_NEW_FILE]));
	buttonOverWriteManager = new ButtonManager(gameCore, &buttonsOverWrite);

	buttonsTheSameFile.push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.4, false, (*gameCore->LNGmanager)[LNG_THE_SAME_FILE_QST], BTN_TYP_DEFAULT_BUTTON));
	buttonsTheSameFile.push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.485, false, (*gameCore->LNGmanager)[LNG_OVERWRITE_FILE]));
	buttonsTheSameFile.push_back(new Button(gameCore, gameCore->appSettings->getScreenWidth() / 2, gameCore->appSettings->getScreenHeight() * 0.57, false, (*gameCore->LNGmanager)[LNG_CHANGE_NAME]));
	buttonTheSameFileManager = new ButtonManager(gameCore, &buttonsTheSameFile);
}

SaveGame::~SaveGame()
{
	delete buttonOverWriteManager;
	delete buttonTheSameFileManager;
	delete saveWindow;
}

bool SaveGame::isUniqueName()
{
	std::vector<const char*> *files = jgp_iostr_fileListing("saves\\", false);
	
	for(unsigned i = 0; i < files->size(); i++)
		if((*files)[i] == this->saveWindow->getDateInFileFormat())
			return false;
	return true;
}

void SaveGame::draw()
{
	switch(saveGameState)
	{
	case READLN_NAME:
		saveWindow->draw();
		break;
	case QST_OVERWRITE: buttonOverWriteManager->draw(); break;
	case QST_THESAMEFILE: buttonTheSameFileManager->draw(); break;
	}
}

void SaveGame::reset()
{
	if(!gameCore->game->fileName.empty() && gameCore->game->fileName != "last game")
		setSaveGameState(QST_OVERWRITE);
	else
		setSaveGameState(READLN_NAME);
	
	buttonOverWriteManager->selectButton(1);
	buttonTheSameFileManager->selectButton(1);
	saveWindow->reset(gameCore->game->boardType, gameCore->game->selectedTextureType);
}

void SaveGame::save(std::string fileName)
{
	std::string path = "saves/" + fileName + ".sav";
	unsigned char value_len;

	ofstream file(path.c_str(), ios::out | ios::binary | ios::trunc);
	
		file.write((char*)&gameCore->game->boardType, sizeof(int));
		file.write((char*)&gameCore->game->gameMode, sizeof(int));
		file.write((char*)&gameCore->game->selectedTextureType, sizeof(int));

		value_len = fileName.size();
		file.write((char*)&value_len, 1);
		file.write(fileName.c_str(), value_len);

		value_len = gameCore->game->lastModificationTime.size();
		file.write((char*)&value_len, 1);
		file.write(gameCore->game->lastModificationTime.c_str(), value_len);

		if(gameCore->game->selectedTextureType == 1) // tekstura wbudowana - zapisujemy tylko jej id. Tekstury te są na stałe zintegrowane z grą
			file.write((char*)&gameCore->game->showTexture, sizeof(int));
		else // tekstura użytkownika - użytkownik może każdą teksturę usunąć, przenieść zmieniając id więc trzeba ten plik graficzny dołączyć do pliku SAV
		{
			// opcja kolorowania tekstur
			file.write((char*)&gameCore->game->coloringCustomTextures, sizeof(bool));

			// zapis pliku tekstury z pliku źródłowego jeśli gra nie jest grą wcześniej wczytaną (nowa gra) - tekstura jest świeżo wybrana z galerii i trzeba ją dołączyć do pliku
			if(!gameCore->game->gameLoaded)
			{
				ifstream sourceTextureFile;
				switch(gameCore->game->boardType)
				{
				case 0: sourceTextureFile.open(gameCore->mediaManager->customTexturesDirectories[gameCore->game->showTexture].c_str(), ios::binary); break;
				case 1: sourceTextureFile.open(gameCore->mediaManager->customTexturesHexDirectories[gameCore->game->showTexture].c_str(), ios::binary); break;
				}
				if(!sourceTextureFile)
					return;

				char *bufferTextFile = NULL;
				std::streamoff fileLength;
				
				// odczytanie pliku graficznego
				sourceTextureFile.seekg(0, ios::end);
				fileLength = sourceTextureFile.tellg();
				bufferTextFile = new char[fileLength];
				sourceTextureFile.seekg(0, ios::beg);
				sourceTextureFile.read(bufferTextFile, fileLength);
				sourceTextureFile.close();
				
				// zapis typu pliku graficznego (jego rozszerzenie wraz z kropką)
				value_len = gameCore->game->textureFileType.size();
				file.write((char*)&value_len, 1);
				file.write(gameCore->game->textureFileType.c_str(), value_len);
				
				// zapis z bufora do pliku sav
				file.write((char*)&fileLength, sizeof(unsigned int));
				file.write(bufferTextFile, fileLength);
				
				delete[] bufferTextFile;
			}
			else // w przeciwnym wypadku zapisuję plik z bufora GameplayCore::textureFileBuffer
				gameCore->game->textureFileBuffer->writeFile(file);
		}

		// zapis pliku screen'a
			ifstream sourceScreenFile;
				sourceScreenFile.open("temp/screenshot.jpg", ios::binary);

			if(!sourceScreenFile)
				return;
			
			// odczyt z pliku do bufora
			sourceScreenFile.seekg(0, ios::end);
			unsigned fileLength = sourceScreenFile.tellg();
			char *buffer = new char[fileLength + 1];
			sourceScreenFile.seekg(0, ios::beg);
			sourceScreenFile.read(buffer, fileLength);
			buffer[fileLength] = '\0';
			sourceScreenFile.close();
						
			// zapis z bufora do pliku sav
			file.write((char*)&fileLength, sizeof(unsigned));
			file.write(buffer, fileLength);
			
			delete buffer;


		file.write((char*)&gameCore->game->initialNumber, sizeof(unsigned));
		file.write((char*)&gameCore->game->winningNumber, sizeof(int));
		file.write((char*)&gameCore->game->won, sizeof(bool));
		
		file.write((char*)&gameCore->game->time, sizeof(unsigned));

		switch(gameCore->game->boardType)
		{
		case 0:
			file.write((char*)&gameCore->game->boardSizeX, sizeof(int));
			file.write((char*)&gameCore->game->boardSizeY, sizeof(int));
			break;
		case 1:
			file.write((char*)&gameCore->game->boardHexSize, sizeof(int));
			break;
		}
	
	file.write((char*)&gameCore->game->elapsedMilliseconds, sizeof(unsigned));
	
	gameCore->game->scores->save(file);
	
	std::vector<std::vector<Tile*>> *boardTab = gameCore->game->boardTab;
	// actual step
	file.write((char*)&gameCore->game->steps->actualStep, sizeof(unsigned));
	file.write((char*)&gameCore->game->steps->drawOnlyTotalSteps, sizeof(bool));
	file.write((char*)&gameCore->game->reversingMovements, sizeof(bool));
	gameCore->gameBackground->thingsBg->save(file);
	gameCore->gameBackground->scrollingBg->save(file);

	for(unsigned x = 0; x < boardTab->size(); x++)
		for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
		{
			unsigned value = (*boardTab)[x][y]->getValue();
			file.write((char*)&value, sizeof(unsigned));
		}
	
	file.write((char*)&gameCore->game->activeSquares, sizeof(int));
	
	// zapis aktualnych punktów
		unsigned size = gameCore->game->scores->exponent.size();
		file.write((char*)&size, sizeof(unsigned));
		for(unsigned i = 0; i < size; i++)
			file.write((char*)&gameCore->game->scores->exponent[i], sizeof(unsigned));
		
		file.write((char*)&gameCore->game->scores->scoresNumber, sizeof(unsigned));
			
	// previous steps
	unsigned vecSize;
	vecSize = gameCore->game->steps->stepsVector.size();
	file.write((char*)&vecSize, sizeof(unsigned));
	for(unsigned i = 0; i < vecSize; i++)
		for(unsigned x = 0; x < boardTab->size(); x++)
			for(unsigned y = 0; y < (*boardTab)[x].size(); y++)
			{
				std::vector<std::vector<unsigned int>> *boardStep = gameCore->game->steps->stepsVector[i];
				unsigned value = (*boardStep)[x][y];
				file.write((char*)&value, sizeof(unsigned));
			}
		
	vecSize = gameCore->game->steps->scoresVector.size();
	file.write((char*)&vecSize, sizeof(unsigned));
	for(unsigned i = 0; i < vecSize; i++)
	{
		unsigned size = gameCore->game->steps->scoresVector[i]->exponent.size();
		file.write((char*)&size, sizeof(unsigned));
		for(unsigned j = 0; j < size; j++)
			file.write((char*)&gameCore->game->steps->scoresVector[i]->exponent[j], sizeof(unsigned));
		
		file.write((char*)&gameCore->game->steps->scoresVector[i]->scoresNumber, sizeof(unsigned));
	}
}

void SaveGame::saveLastGame()
{
	if(!gameCore->game->saveThisGame)
		save("last game");
}

void SaveGame::setSaveGameState(SaveGameState newState)
{
	saveGameState = newState;
}

void SaveGame::update(unsigned elapsedTime)
{
	switch(saveGameState)
	{
	case READLN_NAME:
		switch(saveWindow->pathField[0]->getState())
		{
		case READLN: saveWindow->pathField[0]->update(elapsedTime); break;
		case ENTER:
			if(isUniqueName())
			{
				gameCore->game->saveThisGame = true;
				save(this->saveWindow->getDateInFileFormat());
				gameCore->gamePause->setGamePauseState(PAUSE_MENU);
			}
			else
				setSaveGameState(QST_THESAMEFILE);
			break;
		case ESCAPE: gameCore->gamePause->setGamePauseState(PAUSE_MENU); break;
		}
		break;
	case QST_OVERWRITE:
		buttonOverWriteManager->update();
		if(buttonOverWriteManager->isPressed())
			switch(buttonOverWriteManager->getSelectedButton())
			{
			case 1:
				save(gameCore->game->fileName);
				gameCore->gamePause->setGamePauseState(PAUSE_MENU);
				break;
			case 2:
				gameCore->saveGame->reset();
				setSaveGameState(READLN_NAME);
				break;
			}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
			gameCore->gamePause->setGamePauseState(PAUSE_MENU);
		break;
	case QST_THESAMEFILE:
		buttonTheSameFileManager->update();
		if(buttonTheSameFileManager->isPressed())
			switch(buttonTheSameFileManager->getSelectedButton())
			{
			case 1:
				gameCore->game->saveThisGame = true;
				save(this->saveWindow->getDateInFileFormat());
				gameCore->gamePause->setGamePauseState(PAUSE_MENU);
				break;
			case 2:
				gameCore->saveGame->reset();
				setSaveGameState(READLN_NAME);
				break;
			}
		if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ESCAPE, true))
			gameCore->gamePause->setGamePauseState(PAUSE_MENU);
		break;
	}
}