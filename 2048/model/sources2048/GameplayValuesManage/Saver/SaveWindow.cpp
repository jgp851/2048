#include "SaveWindow.h"
#include "SaveGame.h"
#include "..\..\Game2048Core.h"
#include "..\..\Game2048FontManager.h"
#include "..\..\Gameplay\GameplayCore.h"
#include "..\..\..\gamecore\ApplicationSettings.h"
#include "..\..\..\gamecore\TextBox\TextBox.h"

SaveWindow::SaveWindow(Game2048Core *gameCore, SaveGame *saveGame)
{
	this->gameCore = gameCore;
	this->saveGame = saveGame;
	this->textureBitmap = NULL;
	
	// warto�ci interfejsu
		x = (gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.86) / 2;
		y = (gameCore->appSettings->getScreenHeight() - gameCore->appSettings->getScreenHeight() * 0.86) / 2;
		widthWindow = gameCore->appSettings->getScreenWidth() * 0.86;
		heightWindow = gameCore->appSettings->getScreenHeight() * 0.86;
		
		borderThickness = 16;
		borderX1 = x + borderThickness / 2;
		borderY1 = y + borderThickness / 2;
		borderX2 = x + widthWindow - borderThickness / 2;
		borderY2 = y + heightWindow - borderThickness / 2;

		fieldX1 = borderX1 + borderThickness / 2;
		fieldY1 = borderY1 + borderThickness / 2;
		fieldX2 = borderX2 - borderThickness / 2;
		fieldY2 = borderY2 - borderThickness / 2;
		width = fieldX2 - fieldX1;
		height = fieldY2 - fieldY1;

	// obiekty okna
		gameInfoFields.push_back(new TextWritelnField(0, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
		gameInfoFields.push_back(new TextWritelnField(1, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
		gameInfoFields.push_back(new TextWritelnField(2, gameCore->fontManager->font_gameDetails, 0, 0, al_map_rgb(255, 255, 255), "", ALLEGRO_ALIGN_LEFT));
		gameInfo = new TextBox(gameCore, fieldX1 + 20, y + height * 0.5, ALLEGRO_ALIGN_LEFT, &gameInfoFields, BOX_VERTICAL, BOX_GAME_INFO);
		
		gameInfo->setHeightBetweenTwoPoints(fieldY1 + height * 0.25, fieldY2 - 170, BOX_MIDDLE);
		
		pathTextField.push_back(new TextWritelnField(0, gameCore->fontManager->font_saveWindowPathTitle, 0, al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font), al_map_rgb(255, 255, 255), (*gameCore->LNGmanager)[LNG_WRITE_FILE_NAME], ALLEGRO_ALIGN_LEFT));
		pathField.push_back(new TextReadlnField(1, gameCore->fontManager->font_saveWindowPath,
			0,
			al_get_font_line_height(gameCore->fontManager->font_saveWindowPath->font),
			23, 23, al_map_rgb(255, 255, 255)));
		path = new TextBox(gameCore, fieldX1 + 20, fieldY1 + 20, ALLEGRO_ALIGN_LEFT, &pathTextField, &pathField, BOX_VERTICAL, BOX_GAME_INFO);

		textureSquareLength = gameCore->appSettings->getScreenHeight() * 0.25;
}
SaveWindow::~SaveWindow()
{
	delete gameInfo;
	delete path;
}

std::string SaveWindow::getDateInFileFormat()
{
	std::string date = pathField[0]->getFieldText();
	for(unsigned int i = 0; i < pathField[0]->getFieldText().size(); i++)
		if(date[i] == ':')
			date[i] = '-';
	return date;
}

void SaveWindow::draw()
{
	// background
	al_draw_rectangle(borderX1, borderY1, borderX2, borderY2, al_map_rgb(232, 109, 0), borderThickness);
	al_draw_filled_rectangle(fieldX1, fieldY1, fieldX2, fieldY2, al_map_rgb(255, 0, 0));
	
	// obiekty
	path->draw();
	gameInfo->draw();

	// screenshot
	al_draw_scaled_bitmap(gameCore->game->screenshot,
		0, 0,
		al_get_bitmap_width(gameCore->game->screenshot),
		al_get_bitmap_height(gameCore->game->screenshot),
		gameCore->appSettings->getScreenWidth() / 2 - 20,
		fieldY1 + height * 0.25,
		width / 2,
		height / 2,
		0);

	// tekstura
	float textureX = fieldX1 + 20;
	float textureY = fieldY2 - textureSquareLength - 20;
	if(gameCore->game->showTexture >= 0)
	{
		if(gameCore->game->selectedTextureType == 1 || gameCore->game->coloringCustomTextures == true) // tak jak w ActualBox od Loader'a - je�li tekstura standardowa lub je�li u�ytkownika i colorowana to rysuj kolorowan�; w przeciwnym wypadku zwyk�a
			al_draw_tinted_scaled_bitmap(textureBitmap,
				al_map_rgb(255, 0, 0),
				0, 0, al_get_bitmap_width(textureBitmap), al_get_bitmap_height(textureBitmap),
				textureX, textureY,
				textureSquareLength, textureSquareLength, 0);
		else
			al_draw_scaled_bitmap(textureBitmap,
				0, 0, al_get_bitmap_width(textureBitmap), al_get_bitmap_height(textureBitmap),
				textureX, textureY,
				textureSquareLength, textureSquareLength, 0);
	}
	else
	{
		float lineLength = textureSquareLength * 0.5033;
		float hexArmWidth = lineLength * 0.5066;
		float hexHalfHeight = textureSquareLength / 2;
		float hexHeight = textureSquareLength;
		float hexWidth = lineLength * 1.9867;
		switch(gameCore->game->showTexture)
		{
		case -2:
			switch(boardType)
			{
			case 0:
				al_draw_rectangle(fieldX1 + 20 - 2.5, textureY - 2.5, fieldX1 + 20 + textureSquareLength + 2.5, textureY + textureSquareLength + 2.5, al_map_rgb(255, 255, 0), 5);
				al_draw_text(gameCore->fontManager->font_selectedStandard->font, al_map_rgb(255, 255, 255),
					textureX + textureSquareLength / 2,
					(textureY + textureSquareLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_selectedStandard->font, al_map_rgb(255, 255, 255),
					textureX + textureSquareLength / 2,
					(textureY + textureSquareLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_line(textureX - 2.5, textureY + textureSquareLength / 2, textureX + hexArmWidth - 1.5, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
				al_draw_line(textureX + hexArmWidth - 2.5, textureY - 2.5, textureX + hexArmWidth + lineLength, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
				al_draw_line(textureX + hexArmWidth + lineLength - 2.5, textureY - 3.5, textureX + hexArmWidth * 2 + lineLength, textureY + textureSquareLength / 2, al_map_rgb(255, 255, 0), 5);
				al_draw_line(textureX + hexArmWidth * 2 + lineLength, textureY + textureSquareLength / 2, textureX + hexArmWidth + lineLength - 2.5, textureY + textureSquareLength + 2.5, al_map_rgb(255, 255, 0), 5);
				al_draw_line(textureX + hexArmWidth - 2.5, textureY + textureSquareLength + 2.5, textureX + hexArmWidth + lineLength, textureY + textureSquareLength + 2.5, al_map_rgb(255, 255, 0), 5);
				al_draw_line(textureX + hexArmWidth - 2.5, textureY + textureSquareLength + 2.5, textureX - 2.5, textureY + textureSquareLength / 2, al_map_rgb(255, 255, 0), 5);
				al_draw_text(gameCore->fontManager->font_selectedStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + textureSquareLength / 2,
					(textureY + textureSquareLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_selectedStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + textureSquareLength / 2,
					(textureY + textureSquareLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			}
			break;
		}
	}
}

void SaveWindow::reset(int boardType, int selectedTextureType)
{
	this->boardType = boardType;
	this->selectedTextureType = selectedTextureType;
	textureBitmap = gameCore->game->textureBitmap;

	pathField[0]->reset();
	pathField[0]->setText(gameCore->game->lastModificationTime);
	pathField[0]->setText(getDateInFileFormat()); // pole z nazw� pliku musi mie� dat� w odpowiednim formacie, bez dwukropka
	pathField[0]->selectAll();

	gameInfoFields[0]->setText(gameCore->game->HUDFields[0]->getText());
	gameInfoFields[1]->setText(gameCore->game->HUDFields[1]->getText());
	gameInfoFields[2]->setText(gameCore->game->HUDFields[2]->getText());
}