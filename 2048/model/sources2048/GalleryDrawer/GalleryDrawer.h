#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <vector>
#include "..\..\gamecore\Button\ButtonSideLeft.h"
#include "..\..\gamecore\Button\ButtonSideRight.h"

enum MiniatureType
{
	MNTR_FURTHER,
	MNTR_NEXT,
	MNTR_SELECTED
};

class Game2048Core;

class GalleryDrawer
{
protected:
	Game2048Core *gameCore;
		
	std::vector<ALLEGRO_BITMAP*> *textures;
	ButtonSideLeft *buttonLeft;
	ButtonSideRight *buttonRight;
	ALLEGRO_BITMAP *textureHighlight;

	int boardType;
	int textureType;

	bool textureWasChanged;
	int currentDrawing;
	int distance;
	int spaceBetween;
	int furtherMiniatureLength;
	int nextMiniatureLength;
	int selectedMiniatureLength;
	int changedTextID;
	int selectedTextID;
	int selectedTextX;
	int selectedTextY;
	int textureX;
	int textureY;

	bool isTextureSelected;
	int *selectedTexture;
	int showTextures;

	int width;
	int height;
	int x;
	int y;
	
	bool mouseIsOverTexture(MiniatureType miniatureType, int x, int y);
	bool mouseIsOverSelectedTexture();
	void changeTextureByMouse();
	void highlightTexture(MiniatureType miniatureType, int x, int y);

	virtual void drawFurtherMiniature(int id) = 0;
	virtual void drawNextMiniature(int id) = 0;
	virtual void drawSelectedMiniature() = 0;
	virtual void setStartDrawing() = 0;
public:
	GalleryDrawer(Game2048Core *gameCore);
	
	bool textureSelected();
	int getHeight();
	void setY(int y);

	virtual void draw() = 0;
	virtual void reset(int boardType, int *selText) = 0;
	virtual void update() = 0;

	friend class ItemTextureCustom;
	friend class ItemTextureDefault;
};