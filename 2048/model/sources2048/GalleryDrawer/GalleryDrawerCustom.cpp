#include "GalleryDrawerCustom.h"
#include "..\Game2048Core.h"
#include "..\Game2048MultimediaManager.h"
#include "..\GameplayValuesManage\NewGameInitializer\ItemTextureCustom.h"
#include "..\GameplayValuesManage\NewGameInitializer\NewGameInitializer.h"

GalleryDrawerCustom::GalleryDrawerCustom(Game2048Core *gameCore) : GalleryDrawer(gameCore)
{
}
GalleryDrawerCustom::~GalleryDrawerCustom()
{
}

void GalleryDrawerCustom::draw()
{
	textureX = spaceBetween * (textures->size() > 4 ? 4 : (textures->size() - 1));
	setStartDrawing();
	
	for(int i = 0; static_cast<signed>(textures->size()) < 5 ? i < static_cast<signed>(textures->size()) : i < 5; i++)
	{
		distance = *selectedTexture - currentDrawing;
		if(distance > 1 || distance < -1)
			textureX += furtherMiniatureLength;
		else if(distance == 1 || distance == -1)
			textureX += nextMiniatureLength;
		else
			textureX += selectedMiniatureLength;
		currentDrawing++;
	}
	width = textureX;
	textureX = (gameCore->appSettings->getScreenWidth() - textureX) / 2;
	x = textureX;
	
	if(textures->size() > 1)
	{
		buttonLeft->setCoordinates(gameCore->appSettings->getScreenWidth() * 0.02, y + height / 2 - buttonLeft->getSize() / 2);
		buttonLeft->draw();
		buttonRight->setCoordinates(gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.02 - buttonLeft->getSize(), y + height / 2 - buttonLeft->getSize() / 2);
		buttonRight->draw();
	}

	setStartDrawing();
	showTextures = static_cast<signed>(textures->size()) < 5 ? static_cast<signed>(textures->size()) : 5;
	for(int i = 0; i < showTextures; i++)
	{
		textureY = y;
		if(currentDrawing < *selectedTexture - 1 || currentDrawing > *selectedTexture + 1)
			drawFurtherMiniature(i);
		else if(currentDrawing == *selectedTexture - 1 || currentDrawing == *selectedTexture + 1)
			drawNextMiniature(i);
		else
		{
			selectedTextID = i;
			drawSelectedMiniature();
		}
		currentDrawing++;
	}
	if(textureWasChanged)
		changeTextureByMouse();
}

void GalleryDrawerCustom::drawFurtherMiniature(int id)
{
	textureY += (selectedMiniatureLength - furtherMiniatureLength) / 2;
	int thisTextX = textureX;
	int thisTextY = textureY;
	if(!gameCore->newGameInitializer->textureCustom->coloringCustomTexturesHere)
		al_draw_scaled_bitmap((*textures)[currentDrawing],
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY, furtherMiniatureLength, furtherMiniatureLength, 0);
	else
		al_draw_tinted_scaled_bitmap((*textures)[currentDrawing],
			al_map_rgb(255, 128, 128),
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY, furtherMiniatureLength, furtherMiniatureLength, 0);	
	if(mouseIsOverTexture(MNTR_FURTHER, thisTextX, thisTextY))
	{
		highlightTexture(MNTR_FURTHER, thisTextX, thisTextY);
		if(gameCore->mouseManager->isLeftReleased())
		{
			textureWasChanged = true;
			changedTextID = id;
		}
	}		
	textureX += furtherMiniatureLength + spaceBetween;
}

void GalleryDrawerCustom::drawNextMiniature(int id)
{
	textureY += (selectedMiniatureLength - nextMiniatureLength) / 2;
	int thisTextX = textureX;
	int thisTextY = textureY;
	if(!gameCore->newGameInitializer->textureCustom->coloringCustomTexturesHere)
		al_draw_scaled_bitmap((*textures)[currentDrawing],
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY, nextMiniatureLength, nextMiniatureLength, 0);
	else
		al_draw_tinted_scaled_bitmap((*textures)[currentDrawing],
			al_map_rgb(255, 128, 128),
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY, nextMiniatureLength, nextMiniatureLength, 0);
	if(mouseIsOverTexture(MNTR_NEXT, thisTextX, thisTextY))
	{
		highlightTexture(MNTR_NEXT, thisTextX, thisTextY);
		if(gameCore->mouseManager->isLeftReleased())
		{
			textureWasChanged = true;
			changedTextID = id;
		}
	}	
	textureX += nextMiniatureLength + spaceBetween;
}

void GalleryDrawerCustom::drawSelectedMiniature()
{
	selectedTextX = textureX;
	selectedTextY = textureY;
	if(!gameCore->newGameInitializer->textureCustom->coloringCustomTexturesHere)
		al_draw_scaled_bitmap((*textures)[currentDrawing],
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY,
			selectedMiniatureLength, selectedMiniatureLength, 0);
	else
		al_draw_tinted_scaled_bitmap((*textures)[currentDrawing],
			al_map_rgb(255, 128, 128),
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY,
			selectedMiniatureLength, selectedMiniatureLength, 0);
	if(mouseIsOverTexture(MNTR_SELECTED, selectedTextX, selectedTextY))
		highlightTexture(MNTR_SELECTED, selectedTextX, selectedTextY);
	 //��te zaznaczenie
	switch(boardType)
	{
	case 0:
		al_draw_rectangle(textureX - 2.5, textureY - 2.5, textureX + selectedMiniatureLength + 2.5, textureY + selectedMiniatureLength + 2.5, al_map_rgb(255, 255, 0), 5);
		break;
	case 1:
		{
			float lineLength = selectedMiniatureLength * 0.5033;
			float hexArmWidth = lineLength * 0.5066;
			float hexHalfHeight = selectedMiniatureLength / 2;
			float hexHeight = selectedMiniatureLength;
			float hexWidth = lineLength * 1.9867;
			al_draw_line(textureX - 2.5, textureY + selectedMiniatureLength / 2, textureX + hexArmWidth - 1.5, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth - 2.5, textureY - 2.5, textureX + hexArmWidth + lineLength, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth + lineLength - 2.5, textureY - 3.5, textureX + hexArmWidth * 2 + lineLength, textureY + selectedMiniatureLength / 2, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth * 2 + lineLength, textureY + selectedMiniatureLength / 2, textureX + hexArmWidth + lineLength - 2.5, textureY + selectedMiniatureLength + 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth - 2.5, textureY + selectedMiniatureLength + 2.5, textureX + hexArmWidth + lineLength, textureY + selectedMiniatureLength + 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth - 2.5, textureY + selectedMiniatureLength + 2.5, textureX - 2.5, textureY + selectedMiniatureLength / 2, al_map_rgb(255, 255, 0), 5);
		}
		break;
	}
	textureX += selectedMiniatureLength + spaceBetween;
}

void GalleryDrawerCustom::reset(int boardType, int *selText)
{
	this->boardType = boardType;
	this->selectedTexture = selText;
	this->isTextureSelected = false;
	switch(boardType)
	{
	case 0: textures = &gameCore->mediaManager->texturesCustom; break;
	case 1: textures = &gameCore->mediaManager->texturesHexCustom; break;
	}
}

void GalleryDrawerCustom::setStartDrawing()
{
	textureWasChanged = false;
	if(textures->size() > 5)
	{
		if(*selectedTexture == 0 || *selectedTexture == 1)
			currentDrawing = 0;
		else if(*selectedTexture == textures->size() - 2)
			currentDrawing = *selectedTexture - 3;
		else if(*selectedTexture == textures->size() - 1)
			currentDrawing = *selectedTexture - 4;
		else
			currentDrawing = *selectedTexture - 2;
	}
	else
		currentDrawing = 0;
}

void GalleryDrawerCustom::update()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true) || (mouseIsOverSelectedTexture() && gameCore->mouseManager->isLeftReleased()))
	{
		isTextureSelected = true;
		gameCore->soundPlayer->playGoIn();
	}
	buttonLeft->update();
	buttonRight->update();
}