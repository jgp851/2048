#include "GalleryDrawerDefault.h"
#include "..\Game2048Core.h"
#include "..\Game2048FontManager.h"
#include "..\Game2048MultimediaManager.h"
#include "..\GameplayValuesManage\NewGameInitializer\ItemTextureDefault.h"
#include "..\GameplayValuesManage\NewGameInitializer\NewGameInitializer.h"

GalleryDrawerDefault::GalleryDrawerDefault(Game2048Core *gameCore) : GalleryDrawer(gameCore)
{
	showTextures = 5;
}
GalleryDrawerDefault::~GalleryDrawerDefault()
{
	delete buttonLeft;
	delete buttonRight;
}

void GalleryDrawerDefault::draw()
{
	textureX = spaceBetween * 4;
	setStartDrawing();
	
	for(int i = 0; i < 5; i++)
	{
		if(*selectedTexture == -2 && currentDrawing == -2)
			distance = 0;
		else if(*selectedTexture == -2 && currentDrawing == -1 || *selectedTexture == -1 && currentDrawing == -2)
			distance = 1;
		else
			distance = *selectedTexture - currentDrawing;

		if(distance > 1 || distance < -1)
			textureX += furtherMiniatureLength;
		else if(distance == 1 || distance == -1)
			textureX += nextMiniatureLength;
		else
			textureX += selectedMiniatureLength;

		currentDrawing++;
	}
	width = textureX;
	textureX = (gameCore->appSettings->getScreenWidth() - textureX) / 2;
	x = textureX;

	buttonLeft->setCoordinates(gameCore->appSettings->getScreenWidth() * 0.02, y + height / 2 - buttonLeft->getSize() / 2);
	buttonLeft->draw();
	buttonRight->setCoordinates(gameCore->appSettings->getScreenWidth() - gameCore->appSettings->getScreenWidth() * 0.02 - buttonLeft->getSize(), y + height / 2 - buttonLeft->getSize() / 2);
	buttonRight->draw();

	setStartDrawing();
	for(int i = 0; i < showTextures; i++)
	{
		textureY = y;
		if(currentDrawing < *selectedTexture - 1 || currentDrawing > *selectedTexture + 1)
			drawFurtherMiniature(i);
		else if(currentDrawing == *selectedTexture - 1 || currentDrawing == *selectedTexture + 1)
			drawNextMiniature(i);
		else
		{
			selectedTextID = i;
			drawSelectedMiniature();
		}
		currentDrawing++;
	}
	if(textureWasChanged)
		changeTextureByMouse();
}

void GalleryDrawerDefault::drawFurtherMiniature(int id)
{
	textureY += (selectedMiniatureLength - furtherMiniatureLength) / 2;
	int thisTextX = textureX;
	int thisTextY = textureY;
	if(currentDrawing > -1)
		al_draw_tinted_scaled_bitmap((*textures)[currentDrawing],
			al_map_rgb(255, 128, 128),
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY, furtherMiniatureLength, furtherMiniatureLength, 0);
	else
	{
		float lineLength = furtherMiniatureLength * 0.5033;
		float hexArmWidth = lineLength * 0.5066;
		float hexHalfHeight = furtherMiniatureLength / 2;
		float hexHeight = furtherMiniatureLength;
		float hexWidth = lineLength * 1.9867;
		switch(currentDrawing)
		{
		case -1:
			switch(boardType)
			{
			case 0:
				al_draw_filled_rectangle(textureX, textureY, textureX + furtherMiniatureLength, textureY + furtherMiniatureLength, al_map_rgb(0, 0, 0));
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				break;
			}
			al_draw_text(gameCore->fontManager->font_furtherRandom->font, al_map_rgb(255, 255, 255),
				textureX + furtherMiniatureLength / 2,
				(textureY + furtherMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherRandom->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherRandom->font) * 0.66,
				ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_RANDOM1]);
			al_draw_text(gameCore->fontManager->font_furtherRandom->font, al_map_rgb(255, 255, 255),
				textureX + furtherMiniatureLength / 2,
				(textureY + furtherMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherRandom->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherRandom->font) * 0.66,
				ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_RANDOM2]);
			break;
		case -2:
			switch(boardType)
			{
			case 0:
				al_draw_filled_rectangle(textureX, textureY, textureX + furtherMiniatureLength, textureY + furtherMiniatureLength, al_map_rgb(127, 127, 127));
				al_draw_text(gameCore->fontManager->font_furtherStandard->font, al_map_rgb(255, 255, 255),
					textureX + furtherMiniatureLength / 2,
					(textureY + furtherMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandard->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_furtherStandard->font, al_map_rgb(255, 255, 255),
					textureX + furtherMiniatureLength / 2,
					(textureY + furtherMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandard->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(127, 127, 127));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(127, 127, 127));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(127, 127, 127));
				al_draw_text(gameCore->fontManager->font_furtherStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + furtherMiniatureLength / 2,
					(textureY + furtherMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandardHex->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_furtherStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + furtherMiniatureLength / 2,
					(textureY + furtherMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandardHex->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_furtherStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			}
			break;
		}
	}
	if(mouseIsOverTexture(MNTR_FURTHER, thisTextX, thisTextY))
	{
		highlightTexture(MNTR_FURTHER, thisTextX, thisTextY);
		if(gameCore->mouseManager->isLeftReleased())
		{
			textureWasChanged = true;
			changedTextID = id;
		}
	}
	
	textureX += furtherMiniatureLength + spaceBetween;
}

void GalleryDrawerDefault::drawNextMiniature(int id)
{
	textureY += (selectedMiniatureLength - nextMiniatureLength) / 2;
	int thisTextX = textureX;
	int thisTextY = textureY;
	if(currentDrawing > -1)
		al_draw_tinted_scaled_bitmap((*textures)[currentDrawing],
			al_map_rgb(255, 128, 128),
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY, nextMiniatureLength, nextMiniatureLength, 0);
	else
	{
		float lineLength = nextMiniatureLength * 0.5033;
		float hexArmWidth = lineLength * 0.5066;
		float hexHalfHeight = nextMiniatureLength / 2;
		float hexHeight = nextMiniatureLength;
		float hexWidth = lineLength * 1.9867;
		switch(currentDrawing)
		{
		case -1:
			switch(boardType)
			{
			case 0:
				al_draw_filled_rectangle(textureX, textureY, textureX + nextMiniatureLength, textureY + nextMiniatureLength, al_map_rgb(0, 0, 0));
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				break;
			}
			al_draw_text(gameCore->fontManager->font_nextRandom->font, al_map_rgb(255, 255, 255),
				textureX + nextMiniatureLength / 2,
				(textureY + nextMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextRandom->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextRandom->font) * 0.66,
				ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_RANDOM1]);
			al_draw_text(gameCore->fontManager->font_nextRandom->font, al_map_rgb(255, 255, 255),
				textureX + nextMiniatureLength / 2,
				(textureY + nextMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextRandom->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextRandom->font) * 0.66,
				ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_RANDOM2]);
			break;
		case -2:
			switch(boardType)
			{
			case 0:
				al_draw_filled_rectangle(textureX, textureY, textureX + nextMiniatureLength, textureY + nextMiniatureLength, al_map_rgb(255, 0, 0));
				al_draw_text(gameCore->fontManager->font_nextStandard->font, al_map_rgb(255, 255, 255),
					textureX + nextMiniatureLength / 2,
					(textureY + nextMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandard->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_nextStandard->font, al_map_rgb(255, 255, 255),
					textureX + nextMiniatureLength / 2,
					(textureY + nextMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandard->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_text(gameCore->fontManager->font_nextStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + nextMiniatureLength / 2,
					(textureY + nextMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandardHex->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_nextStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + nextMiniatureLength / 2,
					(textureY + nextMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandardHex->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_nextStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			}
			break;
		}
	}
	if(mouseIsOverTexture(MNTR_NEXT, thisTextX, thisTextY))
	{
		highlightTexture(MNTR_NEXT, thisTextX, thisTextY);
		if(gameCore->mouseManager->isLeftReleased())
		{
			textureWasChanged = true;
			changedTextID = id;
		}
	}
	
	textureX += nextMiniatureLength + spaceBetween;
}

void GalleryDrawerDefault::drawSelectedMiniature()
{
	selectedTextX = textureX;
	selectedTextY = textureY;
	if(currentDrawing > -1)
		al_draw_tinted_scaled_bitmap((*textures)[currentDrawing],
			al_map_rgb(255, 128, 128),
			0, 0, al_get_bitmap_width((*textures)[currentDrawing]), al_get_bitmap_height((*textures)[currentDrawing]),
			textureX, textureY,
			selectedMiniatureLength, selectedMiniatureLength, 0);
	else
	{
		float lineLength = selectedMiniatureLength * 0.5033;
		float hexArmWidth = lineLength * 0.5066;
		float hexHalfHeight = selectedMiniatureLength / 2;
		float hexHeight = selectedMiniatureLength;
		float hexWidth = lineLength * 1.9867;
		switch(currentDrawing)
		{
		case -1:
			switch(boardType)
			{
			case 0:
				al_draw_filled_rectangle(textureX, textureY, textureX + selectedMiniatureLength, textureY + selectedMiniatureLength, al_map_rgb(0, 0, 0));
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(0, 0, 0));
				break;
			}
			al_draw_text(gameCore->fontManager->font_selectedRandom->font, al_map_rgb(255, 255, 255),
				textureX + selectedMiniatureLength / 2,
				(textureY + selectedMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedRandom->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedRandom->font) * 0.66,
				ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_RANDOM1]);
			al_draw_text(gameCore->fontManager->font_selectedRandom->font, al_map_rgb(255, 255, 255),
				textureX + selectedMiniatureLength / 2,
				(textureY + selectedMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedRandom->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedRandom->font) * 0.66,
				ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_RANDOM2]);
			break;
		case -2:
			switch(boardType)
			{
			case 0:
				al_draw_filled_rectangle(textureX, textureY, textureX + selectedMiniatureLength, textureY + selectedMiniatureLength, al_map_rgb(255, 0, 0));
				al_draw_text(gameCore->fontManager->font_selectedStandard->font, al_map_rgb(255, 255, 255),
					textureX + selectedMiniatureLength / 2,
					(textureY + selectedMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_selectedStandard->font, al_map_rgb(255, 255, 255),
					textureX + selectedMiniatureLength / 2,
					(textureY + selectedMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandard->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			case 1:
				al_draw_filled_rectangle(textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_filled_triangle(
					textureX,
					textureY + hexHalfHeight,
					textureX + hexArmWidth,
					textureY,
					textureX + hexArmWidth,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_filled_triangle(
					textureX + hexWidth,
					textureY + hexHalfHeight,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY,
					textureX + hexArmWidth + lineLength - 2.5,
					textureY + hexHeight,
					al_map_rgb(255, 0, 0));
				al_draw_text(gameCore->fontManager->font_selectedStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + selectedMiniatureLength / 2,
					(textureY + selectedMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.5 - /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD1]);
				al_draw_text(gameCore->fontManager->font_selectedStandardHex->font, al_map_rgb(255, 255, 255),
					textureX + selectedMiniatureLength / 2,
					(textureY + selectedMiniatureLength / 2) - /*50% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.5 + /*66% wysoko�ci*/al_get_font_line_height(gameCore->fontManager->font_selectedStandardHex->font) * 0.66,
					ALLEGRO_ALIGN_CENTER, (*gameCore->LNGmanager)[LNG_TEXTURE_STD2]);
				break;
			}
			break;
		}
	}
	if(mouseIsOverTexture(MNTR_SELECTED, selectedTextX, selectedTextY))
		highlightTexture(MNTR_SELECTED, selectedTextX, selectedTextY);
	 //��te zaznaczenie
	switch(boardType)
	{
	case 0:
		al_draw_rectangle(textureX - 2.5, textureY - 2.5, textureX + selectedMiniatureLength + 2.5, textureY + selectedMiniatureLength + 2.5, al_map_rgb(255, 255, 0), 5);
		break;
	case 1:
		{
			float lineLength = selectedMiniatureLength * 0.5033;
			float hexArmWidth = lineLength * 0.5066;
			float hexHalfHeight = selectedMiniatureLength / 2;
			float hexHeight = selectedMiniatureLength;
			float hexWidth = lineLength * 1.9867;
			al_draw_line(textureX - 2.5, textureY + selectedMiniatureLength / 2, textureX + hexArmWidth - 1.5, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth - 2.5, textureY - 2.5, textureX + hexArmWidth + lineLength, textureY - 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth + lineLength - 2.5, textureY - 3.5, textureX + hexArmWidth * 2 + lineLength, textureY + selectedMiniatureLength / 2, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth * 2 + lineLength, textureY + selectedMiniatureLength / 2, textureX + hexArmWidth + lineLength - 2.5, textureY + selectedMiniatureLength + 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth - 2.5, textureY + selectedMiniatureLength + 2.5, textureX + hexArmWidth + lineLength, textureY + selectedMiniatureLength + 2.5, al_map_rgb(255, 255, 0), 5);
			al_draw_line(textureX + hexArmWidth - 2.5, textureY + selectedMiniatureLength + 2.5, textureX - 2.5, textureY + selectedMiniatureLength / 2, al_map_rgb(255, 255, 0), 5);
		}
		break;
	}
	textureX += selectedMiniatureLength + spaceBetween;
}

void GalleryDrawerDefault::reset(int boardType, int *selText)
{
	this->boardType = boardType;
	this->selectedTexture = selText;
	this->isTextureSelected = false;
	switch(boardType)
	{
	case 0: textures = &gameCore->mediaManager->texturesDefault; break;
	case 1: textures = &gameCore->mediaManager->texturesHexDefault; break;
	}
}

void GalleryDrawerDefault::setStartDrawing()
{
	textureWasChanged = false;
	if(*selectedTexture == -2 || *selectedTexture == -1)
		currentDrawing = -2;
	else if(*selectedTexture == textures->size() - 2)
		currentDrawing = *selectedTexture - 3;
	else if(*selectedTexture == textures->size() - 1)
		currentDrawing = *selectedTexture - 4;
	else
		currentDrawing = *selectedTexture - 2;
}

void GalleryDrawerDefault::update()
{
	if(gameCore->keyManager->isPressed(ALLEGRO_KEY_ENTER, true) || (mouseIsOverSelectedTexture() && gameCore->mouseManager->isLeftReleased()))
	{
		isTextureSelected = true;
		gameCore->soundPlayer->playGoIn();
	}
	buttonLeft->update();
	buttonRight->update();
}