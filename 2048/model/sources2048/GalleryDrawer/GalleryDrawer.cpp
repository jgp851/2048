#include "GalleryDrawer.h"
#include "..\Game2048Core.h"
#include "..\Gameplay\GameplayCore.h"
#include "..\..\gamecore\Button\ButtonSideLeft.h"
#include "..\..\gamecore\Button\ButtonSideRight.h"

GalleryDrawer::GalleryDrawer(Game2048Core *gameCore)
{
	this->gameCore = gameCore;

	height = gameCore->appSettings->getScreenWidth() * 0.1875;
	spaceBetween = gameCore->appSettings->getScreenWidth() * 0.0625;
	furtherMiniatureLength = gameCore->appSettings->getScreenWidth() * 0.0625;
	nextMiniatureLength = gameCore->appSettings->getScreenWidth() * 0.125;
	selectedMiniatureLength = gameCore->appSettings->getScreenWidth() * 0.1875;
	buttonLeft = new ButtonSideLeft(gameCore, gameCore->appSettings->getScreenWidth() * 0.05);
	buttonRight = new ButtonSideRight(gameCore, gameCore->appSettings->getScreenWidth() * 0.05);
	textureHighlight = gameCore->mediaManager->PNG_textureHighlightHex;
}

bool GalleryDrawer::mouseIsOverTexture(MiniatureType miniatureType, int x, int y)
{
	int thisMiniatureLength;
	switch(miniatureType)
	{
	case MNTR_FURTHER: thisMiniatureLength = furtherMiniatureLength; break;
	case MNTR_NEXT: thisMiniatureLength = nextMiniatureLength; break;
	case MNTR_SELECTED: thisMiniatureLength = selectedMiniatureLength; break;
	}
	if(gameCore->mouseManager->getX() >= x && gameCore->mouseManager->getX() < x + thisMiniatureLength &&
		gameCore->mouseManager->getY() >= y && gameCore->mouseManager->getY() < y + thisMiniatureLength)
		return true;
	else
		return false;
}

bool GalleryDrawer::mouseIsOverSelectedTexture()
{
	if(gameCore->mouseManager->getX() >= selectedTextX && gameCore->mouseManager->getX() < selectedTextX + selectedMiniatureLength &&
		gameCore->mouseManager->getY() >= selectedTextY && gameCore->mouseManager->getY() < selectedTextY + selectedMiniatureLength)
		return true;
	else
		return false;
}

bool GalleryDrawer::textureSelected()
{
	return isTextureSelected;
}

int GalleryDrawer::getHeight()
{
	return height;
}

void GalleryDrawer::changeTextureByMouse()
{
	if(changedTextID < selectedTextID)
		*selectedTexture -= (selectedTextID - changedTextID);
	else
		*selectedTexture += (changedTextID - selectedTextID);
	gameCore->soundPlayer->playSwitchMenu();
}

void GalleryDrawer::highlightTexture(MiniatureType miniatureType, int x, int y)
{
	int thisMiniatureLength;
	switch(miniatureType)
	{
	case MNTR_FURTHER: thisMiniatureLength = furtherMiniatureLength; break;
	case MNTR_NEXT: thisMiniatureLength = nextMiniatureLength; break;
	case MNTR_SELECTED: thisMiniatureLength = selectedMiniatureLength; break;
	}
	switch(boardType)
	{
	case 0: al_draw_filled_rectangle(x, y, x + thisMiniatureLength, y + thisMiniatureLength, al_map_rgba(60, 60, 60, 50)); break;
	case 1: al_draw_scaled_bitmap(textureHighlight, 0, 0, al_get_bitmap_width(textureHighlight), al_get_bitmap_height(textureHighlight), x, y, thisMiniatureLength, thisMiniatureLength, 0); break;
	}
}

void GalleryDrawer::setY(int y)
{
	this->y = y;
}