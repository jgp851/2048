#pragma once
#include "GalleryDrawer.h"

class Game2048Core;

class GalleryDrawerCustom : public GalleryDrawer
{
private:
	void drawFurtherMiniature(int id);
	void drawNextMiniature(int id);
	void drawSelectedMiniature();
	void setStartDrawing();
public:
	GalleryDrawerCustom(Game2048Core *gameCore);
	~GalleryDrawerCustom();

	void draw();
	void reset(int boardType, int *selText);
	void update();
};