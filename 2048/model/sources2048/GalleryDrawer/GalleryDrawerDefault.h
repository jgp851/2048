#pragma once
#include "GalleryDrawer.h"

class Game2048Core;

class GalleryDrawerDefault : public GalleryDrawer
{
private:
	void drawFurtherMiniature(int id);
	void drawNextMiniature(int id);
	void drawSelectedMiniature();
	void setStartDrawing();
public:
	GalleryDrawerDefault(Game2048Core *gameCore);
	~GalleryDrawerDefault();
	
	void draw();
	void reset(int boardType, int *selText);
	void update();
};