#include "Game2048SoundPlayer.h"
#include "Game2048Core.h"
#include "GameSettings.h"

Game2048SoundPlayer::Game2048SoundPlayer(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	this->soundEffectsTurned = &gameCore->gameSettings->soundEffectsTurned;
	goIn = gameCore->mediaManager->SMP_goIn;
	goOut = gameCore->mediaManager->SMP_goOut;
	switchButtons = gameCore->mediaManager->SMP_switchButtons;
	switchMenu = gameCore->mediaManager->SMP_switchMenu;
	gain = &gameCore->gameSettings->soundEffectsVolume;
	pan = 0;
	speed = 1;
	maxVolume = 1;
	minVolume = 0;
}
Game2048SoundPlayer::~Game2048SoundPlayer()
{
}

bool Game2048SoundPlayer::getSoundEffectsTurned()
{
	return *soundEffectsTurned;
}

float* Game2048SoundPlayer::getVolumeAddress()
{
	return gain;
}

float Game2048SoundPlayer::getMaxVolume()
{
	return maxVolume;
}

float Game2048SoundPlayer::getMinVolume()
{
	return minVolume;
}

float Game2048SoundPlayer::getVolumeValue()
{
	return *gain;
}

void Game2048SoundPlayer::playGoIn(bool forcedVoice)
{
	if(forcedVoice)
		al_play_sample(goIn, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
	else if(*soundEffectsTurned)
		al_play_sample(goIn, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playGoOut(bool forcedVoice)
{
	if(forcedVoice)
		al_play_sample(goOut, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
	else if(*soundEffectsTurned)
		al_play_sample(goOut, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playSwitchButtons(bool forcedVoice)
{
	if(forcedVoice)
		al_play_sample(switchButtons, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
	else if(*soundEffectsTurned)
		al_play_sample(switchButtons, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playSwitchMenu(bool forcedVoice)
{
	if(forcedVoice)
		al_play_sample(switchMenu, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
	else if(*soundEffectsTurned)
		al_play_sample(switchMenu, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playSample(ALLEGRO_SAMPLE *sample)
{
	if(*soundEffectsTurned)
		al_play_sample(sample, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playSample(ALLEGRO_SAMPLE *sample, float volume)
{
	if(*soundEffectsTurned)
		al_play_sample(sample, volume, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playSampleForced(ALLEGRO_SAMPLE *sample)
{
	al_play_sample(sample, *gain, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::playSampleForced(ALLEGRO_SAMPLE *sample, float volume)
{
	al_play_sample(sample, volume, pan, speed, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void Game2048SoundPlayer::setTurned(bool state)
{
	*soundEffectsTurned = state;
}

void Game2048SoundPlayer::setVolume(float volume)
{
	*gain = volume;
}