#pragma once
#include <fstream>
#include <string>

using namespace std;

class Game2048Core;

class GameSettings
{
private:
	Game2048Core *gameCore;
	string pathConfigFile;
	
	bool coloringCustomTextures;
	bool coloringCustomTexturesHex;
	bool gameMusicTurned;
	bool menuMusicTurned;
	bool musicPlayerRandomized;
	bool randomTexture;
	bool randomTextureHex;
	bool soundEffectsTurned;
	float musicGameVolume;
	float musicMenuVolume;
	float soundEffectsVolume;
	int backwardsNumber;
	int boardHexSize;
	int boardSizeX;
	int boardSizeY;
	int boardType;
	int gameMode;
	int selectedTextureCustom;
	int selectedTextureDefault;
	int selectedTextureHexCustom;
	int selectedTextureHexDefault;
	int selectedTextureStdType;
	int selectedTextureHexType;
	int winningNumber;
	unsigned short int languageCode;
	unsigned int selectedPlaylist;
	unsigned int textureStdCustomFileNO;
	unsigned int textureHexCustomFileNO;

	void read(const string& path);
	void write(const string& path);
public:
	GameSettings(Game2048Core *gameCore);
	~GameSettings();

	void saveSettings();

	friend class GameplayBackwardsOnHexBoard;
	friend class GameplayBackwardsOnStdBoard;
	friend class GameplayFibonacciOnHexBoard;
	friend class GameplayFibonacciOnStdBoard;
	friend class GameplayStandardOnHexBoard;
	friend class GameplayStandardOnStdBoard;
	friend class HexagonBoard;
	friend class StandardBoard;
	friend class Game2048SoundPlayer;
	friend class GameOptions;
	friend class GameOptionsGraphics;
	friend class GameOptionsSound;
	friend class GameOptionsTrax;
	friend class ItemTextureCustom;
	friend class ItemTextureDefault;

	friend class NewGameInitializer;
	friend class MusicPlayerCore;
};