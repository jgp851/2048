#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <vector>

class Button;
class ButtonManager;
class Game2048Core;

class Game2048GameWin
{
private:
	Game2048Core *gameCore;

	std::vector<Button*> *buttons;
	ButtonManager *buttonManager;
	ALLEGRO_BITMAP *pauseBg;
	ALLEGRO_BITMAP *title;

	bool continueGameAtEnd;
	bool textBg;

	void reset();
public:
	Game2048GameWin(Game2048Core *gameCore);
	~Game2048GameWin();

	void draw();
	void setToContinueGameAtEnd(bool state);
	void update(unsigned elapsedTime);
};