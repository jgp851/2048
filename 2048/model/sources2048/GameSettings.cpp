#include "GameSettings.h"
#include "Game2048Core.h"
#include "MusicPlayer\MusicPlayerCore.h"

GameSettings::GameSettings(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	pathConfigFile = "config.ini";
	read(pathConfigFile);
	gameCore->LNGmanager->setLanguageFileByCode(languageCode);
}

GameSettings::~GameSettings()
{
}

void GameSettings::read(const string& path)
{
	ifstream file(path.c_str(), ios::in | ios::binary);
	file.read((char*)&coloringCustomTextures, sizeof(bool));
	file.read((char*)&coloringCustomTexturesHex, sizeof(bool));
	file.read((char*)&gameMusicTurned, sizeof(bool));
	file.read((char*)&menuMusicTurned, sizeof(bool));
	file.read((char*)&musicPlayerRandomized, sizeof(bool));
	file.read((char*)&randomTexture, sizeof(bool));
	file.read((char*)&randomTextureHex, sizeof(bool));
	file.read((char*)&soundEffectsTurned, sizeof(bool));
	file.read((char*)&musicGameVolume, sizeof(float));
	file.read((char*)&musicMenuVolume, sizeof(float));
	file.read((char*)&soundEffectsVolume, sizeof(float));
	file.read((char*)&backwardsNumber, sizeof(int));
	file.read((char*)&boardHexSize, sizeof(int));
	file.read((char*)&boardSizeX, sizeof(int));
	file.read((char*)&boardSizeY, sizeof(int));
	file.read((char*)&boardType, sizeof(int));
	file.read((char*)&gameMode, sizeof(int));
	file.read((char*)&selectedTextureCustom, sizeof(int));
	file.read((char*)&selectedTextureDefault, sizeof(int));
	file.read((char*)&selectedTextureHexCustom, sizeof(int));
	file.read((char*)&selectedTextureHexDefault, sizeof(int));
	file.read((char*)&selectedTextureStdType, sizeof(int));
	file.read((char*)&selectedTextureHexType, sizeof(int));
	file.read((char*)&winningNumber, sizeof(int));
	file.read((char*)&languageCode, sizeof(unsigned short int));
	file.read((char*)&selectedPlaylist, sizeof(unsigned int));
	file.read((char*)&textureStdCustomFileNO, sizeof(unsigned int));
	file.read((char*)&textureHexCustomFileNO, sizeof(unsigned int));
	file.close();
}

void GameSettings::write(const string& path)
{
	ofstream file(path.c_str(), ios::out | ios::binary | ios::trunc);
	file.write((char*)&coloringCustomTextures, sizeof(bool));
	file.write((char*)&coloringCustomTexturesHex, sizeof(bool));
	file.write((char*)&gameMusicTurned, sizeof(bool));
	file.write((char*)&menuMusicTurned, sizeof(bool));
	file.write((char*)&musicPlayerRandomized, sizeof(bool));
	file.write((char*)&randomTexture, sizeof(bool));
	file.write((char*)&randomTextureHex, sizeof(bool));
	file.write((char*)&soundEffectsTurned, sizeof(bool));
	file.write((char*)&musicGameVolume, sizeof(float));
	file.write((char*)&musicMenuVolume, sizeof(float));
	file.write((char*)&soundEffectsVolume, sizeof(float));
	file.write((char*)&backwardsNumber, sizeof(int));
	file.write((char*)&boardHexSize, sizeof(int));
	file.write((char*)&boardSizeX, sizeof(int));
	file.write((char*)&boardSizeY, sizeof(int));
	file.write((char*)&boardType, sizeof(int));
	file.write((char*)&gameMode, sizeof(int));
	file.write((char*)&selectedTextureCustom, sizeof(int));
	file.write((char*)&selectedTextureDefault, sizeof(int));
	file.write((char*)&selectedTextureHexCustom, sizeof(int));
	file.write((char*)&selectedTextureHexDefault, sizeof(int));
	file.write((char*)&selectedTextureStdType, sizeof(int));
	file.write((char*)&selectedTextureHexType, sizeof(int));
	file.write((char*)&winningNumber, sizeof(int));
	file.write((char*)&languageCode, sizeof(unsigned short int));
	file.write((char*)&selectedPlaylist, sizeof(unsigned int));
	file.write((char*)&textureStdCustomFileNO, sizeof(unsigned int));
	file.write((char*)&textureHexCustomFileNO, sizeof(unsigned int));
	file.close();
}

void GameSettings::saveSettings()
{
	write(pathConfigFile);
}