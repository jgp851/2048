#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <string>

class Game2048Core;
class Game2048StringBackground;

struct Fonts
{
	Fonts(ALLEGRO_FONT *font, ALLEGRO_COLOR color, int align)
	{
		this->font = font;
		this->color = color;
		this->align = align;
	}

	ALLEGRO_COLOR color;
	ALLEGRO_FONT *font;
	int align;
	int ID;
};

class Game2048FontManager
{
private:
	Game2048Core *gameCore;
	Game2048StringBackground *stringBackground;
public:
	Game2048FontManager(Game2048Core *gameCore);
	~Game2048FontManager();
	
	Fonts *font_number8;
	Fonts *font_number10;
	Fonts *font_number12;
	Fonts *font_number16;
	Fonts *font_number18;
	Fonts *font_number25;
	Fonts *font_number28;
	Fonts *font_number32;
	Fonts *font_number36;
	
	Fonts *font_arial;
	Fonts *font_buttonsInfo;
	Fonts *font_fallingSkyCondObl_performer;
	Fonts *font_fallingSkyCondObl_albumAndTitle;
	Fonts *font_gameDetails;
	Fonts *font_gameInfoBox;
	Fonts *font_gameOver;
	Fonts *font_infoAboutRebooting;
	Fonts *font_lastEarnedPoints;
	Fonts *font_mainMenu;
	Fonts *font_options;
	Fonts *font_pathTexture;
	Fonts *font_pressAnyKey;
	Fonts *font_scoresFont;
	
	Fonts *font_furtherRandom;
	Fonts *font_nextRandom;
	Fonts *font_selectedRandom;

	Fonts *font_furtherStandard;
	Fonts *font_furtherStandardHex;
	Fonts *font_nextStandard;
	Fonts *font_nextStandardHex;
	Fonts *font_selectedStandard;
	Fonts *font_selectedStandardHex;

	Fonts *font_saveWindowPath;
	Fonts *font_saveWindowPathTitle;
};