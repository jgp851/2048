#pragma once
#include <allegro5\allegro.h>

class Game2048Core;
class GameScrollingBackground;
class ThingsBackground;

class GameBackground
{
private:
	Game2048Core *gameCore;
public:
	GameBackground(Game2048Core *gameCore);
	~GameBackground();
	
	GameScrollingBackground *scrollingBg;
	ThingsBackground *thingsBg;

	void draw();
	void nextStep();
	void reset();
	void undoStep();
	void update(unsigned elapsedTime);
};