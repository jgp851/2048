#include "GameScrollingBackground.h"
#include "GameBackground.h"
#include "..\ThingsBg\ThingsBackground.h"
#include "..\..\Game2048Core.h"
#include "..\..\Gameplay\GameplayCore.h"

GameScrollingBackground::GameScrollingBackground(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	background = gameCore->mediaManager->PNG_scrollingBgBackground;
	reset();
}

GameScrollingBackground::~GameScrollingBackground()
{
}

void GameScrollingBackground::draw()
{
	for(int i = 0; i < gameCore->appSettings->getScreenWidth(); i += al_get_bitmap_width(background))
		for(int j = -al_get_bitmap_height(background); j < gameCore->appSettings->getScreenHeight(); j += al_get_bitmap_height(background))
			al_draw_tinted_bitmap(background, color, i, j + (bgScrollOffset / static_cast<float>(SCROLLING_BACKGROUND_SCROLL_TIME)) * al_get_bitmap_height(background), 0);
}

void GameScrollingBackground::load(std::ifstream &file)
{
	file.read((char*)&color, sizeof(ALLEGRO_COLOR));
	file.read((char*)&bgScrollOffset, sizeof(int));
	file.read((char*)&step, sizeof(unsigned));
}

void GameScrollingBackground::nextStep()
{
	switch(step)
	{
	case 1:
		if(color.g < 1.0f)
			color.g += 1.0f/255.0f;
		else
			step++;
		break;
	case 2:
		if(0 < color.r)
			color.r -= 1.0f/255.0f;
		else
			step++;
		break;
	case 3:
		if(color.b < 1.0f)
			color.b += 1.0f/255.0f;
		else
			step++;
		break;
	case 4:
		if(0 < color.g)
			color.g -= 1.0f/255.0f;
		else
			step++;
		break;
	case 5:
		if(color.r < 1.0f)
			color.r += 1.0f/255.0f;
		else
			step++;
		break;
	case 6:
		if(0 < color.b)
			color.b -= 1.0f/255.0f;
		else
		{
			gameCore->gameBackground->thingsBg->setNextThing();
			step = 1;
		}
		break;
	}
}

void GameScrollingBackground::reset()
{
	bgScrollOffset = 0;
	step = 1;
	color = al_map_rgb(255, 0, 0);
}

void GameScrollingBackground::save(std::ofstream &file)
{
	file.write((char*)&color, sizeof(ALLEGRO_COLOR));
	file.write((char*)&bgScrollOffset, sizeof(int));
	file.write((char*)&step, sizeof(unsigned));
}

void GameScrollingBackground::undoStep()
{
	switch(step)
	{
	case 1:
		if(0 < color.g)
			color.g -= 1.0f/255.0f;
		else
		{
			gameCore->gameBackground->thingsBg->setPreviousThing();
			step = 6;
		}
		break;
	case 2:
		if(color.r < 1.0f)
			color.r += 1.0f/255.0f;
		else
			step--;
		break;
	case 3:
		if(0 < color.b)
			color.b -= 1.0f/255.0f;
		else
			step--;
		break;
	case 4:
		if(color.g < 1.0f)
			color.g += 1.0f/255.0f;
		else
			step--;
		break;
	case 5:
		if(0 < color.r)
			color.r -= 1.0f/255.0f;
		else
			step--;
		break;
	case 6:
		if(color.b < 1.0f)
			color.b += 1.0f/255.0f;
		else
			step--;
		break;
	}
}

void GameScrollingBackground::update(unsigned elapsedTime)
{
	bgScrollOffset += elapsedTime;
	if(bgScrollOffset > SCROLLING_BACKGROUND_SCROLL_TIME)
		bgScrollOffset = bgScrollOffset % SCROLLING_BACKGROUND_SCROLL_TIME;
}