#include "GameBackground.h"
#include "GameScrollingBackground.h"
#include "..\ThingsBg\ThingsBackground.h"
#include "..\..\Game2048Core.h"

GameBackground::GameBackground(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	scrollingBg = new GameScrollingBackground(gameCore);
	thingsBg = new ThingsBackground(gameCore, 0, 20);
}

GameBackground::~GameBackground()
{
}

void GameBackground::draw()
{
	scrollingBg->draw();
	thingsBg->draw();
}

void GameBackground::nextStep()
{
	scrollingBg->nextStep();
}

void GameBackground::reset()
{
	scrollingBg->reset();
	thingsBg->randomType();
	thingsBg->reset();
}

void GameBackground::undoStep()
{
	scrollingBg->undoStep();
}

void GameBackground::update(unsigned elapsedTime)
{
	scrollingBg->update(elapsedTime);
	thingsBg->update(elapsedTime);
}