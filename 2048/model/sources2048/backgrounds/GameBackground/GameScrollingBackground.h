#pragma once
#include <allegro5\allegro.h>
#include <fstream>

#define SCROLLING_BACKGROUND_SCROLL_TIME 4000

class Game2048Core;

class GameScrollingBackground
{
private:
	Game2048Core *gameCore;
	ALLEGRO_BITMAP *background;
	ALLEGRO_COLOR color;
	int bgScrollOffset;
	unsigned step;
public:
	GameScrollingBackground(Game2048Core *gameCore);
	~GameScrollingBackground();

	void draw();
	void load(std::ifstream &file);
	void nextStep();
	void reset();
	void save(std::ofstream &file);
	void undoStep();
	void update(unsigned elapsedTime);
};