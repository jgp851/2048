#pragma once

class Game2048Core;
class MainMenuRotatingBackground;
class ThingsBackground;

class MainMenuBackground
{
private:
	Game2048Core *gameCore;
	
	ThingsBackground *thingsBackground;
	MainMenuRotatingBackground *rotatingBackground;
public:
	MainMenuBackground(Game2048Core *gameCore);
	~MainMenuBackground();

	void draw();
	void update(unsigned elapsedTime);
};