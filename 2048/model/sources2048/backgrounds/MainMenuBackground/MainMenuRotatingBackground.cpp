#include "MainMenuRotatingBackground.h"
#include "..\..\Game2048Core.h"

MainMenuRotatingBackground::MainMenuRotatingBackground(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	background = gameCore->mediaManager->PNG_mainMenuRotatingBgBackground;
	bgRotateOffset = 0.0;
	cW = al_get_bitmap_width(background) / 2;
	cH = al_get_bitmap_height(background) / 2;
	dW = gameCore->appSettings->getScreenWidth() / 2;
	dH = gameCore->appSettings->getScreenHeight() / 2;
}

MainMenuRotatingBackground::~MainMenuRotatingBackground()
{
}

void MainMenuRotatingBackground::draw()
{
	al_draw_rotated_bitmap(background, cW, cH, dW, dH, bgRotateOffset, 0);
}

void MainMenuRotatingBackground::update(unsigned elapsedTime)
{
	bgRotateOffset += static_cast<float>(elapsedTime) / ROTATING_BACKGROUND_ROTATE_TIME;
	if(bgRotateOffset > ROTATING_BACKGROUND_ROTATE_TIME)
		bgRotateOffset = bgRotateOffset - ROTATING_BACKGROUND_ROTATE_TIME;
}