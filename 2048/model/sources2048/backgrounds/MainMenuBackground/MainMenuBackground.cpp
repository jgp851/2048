#include "MainMenuBackground.h"
#include "MainMenuRotatingBackground.h"
#include "..\ThingsBg\ThingsBackground.h"
#include "..\..\Game2048Core.h"

MainMenuBackground::MainMenuBackground(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
	
	thingsBackground = new ThingsBackground(gameCore, 0, 20);
	thingsBackground->setType(BG_BLINK);
	rotatingBackground = new MainMenuRotatingBackground(gameCore);
}

MainMenuBackground::~MainMenuBackground()
{
	delete thingsBackground;
	delete rotatingBackground;
}

void MainMenuBackground::draw()
{
	rotatingBackground->draw();
	thingsBackground->draw();
}

void MainMenuBackground::update(unsigned elapsedTime)
{
	rotatingBackground->update(elapsedTime);
	thingsBackground->update(elapsedTime);
}