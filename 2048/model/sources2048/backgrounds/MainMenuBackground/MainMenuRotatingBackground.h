#pragma once
#include <allegro5\allegro.h>

#define ROTATING_BACKGROUND_ROTATE_TIME 4000.0

class Game2048Core;

class MainMenuRotatingBackground
{
private:
	Game2048Core *gameCore;
	ALLEGRO_BITMAP *background;
	float bgRotateOffset;
	int cW;
	int cH;
	int dW;
	int dH;
public:
	MainMenuRotatingBackground(Game2048Core *gameCore);
	~MainMenuRotatingBackground();

	void draw();
	void update(unsigned elapsedTime);
};