#pragma once
#include <allegro5\allegro.h>
#include <map>
#include <vector>

#define DRAWING_THING_TIME 2000

class Game2048Core;
class Sprite;
class TBGfile;
class ThingOfBackground;

enum ThingsBackgroundType
{
	BG_BLINK,
	BG_FALL,
	BG_ROLL
};

class ThingsBackground
{
private:
	Game2048Core *gameCore;
	ThingsBackgroundType bgType;
	std::vector<ThingOfBackground*> things;

	std::map<unsigned, std::vector<Sprite*>*> spriteLibrary;
	std::vector<const char*> *files;

	unsigned initialSelectedGroup;
	unsigned numberOfThings;
	unsigned setID;
	unsigned totalSets;
public:
	ThingsBackground(Game2048Core *gameCore, unsigned selectedThing, unsigned numberOfThings);
	~ThingsBackground();

	ThingsBackgroundType getBgType();

	void draw();
	void load(std::ifstream &file);
	void loadSpriteSet(unsigned setID);
	void prepareBackground();
	void randomType();
	void reset();
	void save(std::ofstream &file);
	void setNextThing();
	void setPreviousThing();
	void setType(ThingsBackgroundType newType);
	void update(unsigned elapsedTime);

	friend class ThingOfBackground;
};