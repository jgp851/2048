#include "ThingsBackground.h"
#include "ThingOfBackground.h"
#include "TBGfile\TBGfile.h"
#include "..\..\..\gamecore\Sprite.h"
#include "..\..\Game2048Core.h"
#include "..\..\..\..\..\..\JGP.iostream\JGP.iostream\jgp.iostream.h" // DLL JGP.iostream

#define DRAWING_TRANSPARENCY_TIME 600
#define STD_FRAME_TIME (DRAWING_THING_TIME - 2 * DRAWING_TRANSPARENCY_TIME)
#define SPRITE_POS (spriteLibrary.size() - 1)

ThingsBackground::ThingsBackground(Game2048Core *gameCore, unsigned setID, unsigned numberOfThings)
{
	files = jgp_iostr_fileListing("gfx\\thingsBg\\", true);
	this->totalSets = files->size();
	this->gameCore = gameCore;
	this->initialSelectedGroup = setID;
	this->numberOfThings = numberOfThings;
	this->setID = setID;
	this->setType(BG_BLINK);
	this->loadSpriteSet(setID);

	for(unsigned i = 0; i < numberOfThings; i++)
		this->things.push_back(new ThingOfBackground(gameCore, this, spriteLibrary[setID], setID));
}

ThingsBackground::~ThingsBackground()
{
	for(std::map<unsigned, std::vector<Sprite*>*>::iterator it = spriteLibrary.begin(); it != spriteLibrary.end(); it++)
		delete it->second;
	spriteLibrary.clear();
	for(unsigned i = 0; i < numberOfThings; i++)
		delete things[i];
}

ThingsBackgroundType ThingsBackground::getBgType()
{
	return bgType;
}

void ThingsBackground::draw()
{
	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->draw();
}

void ThingsBackground::load(std::ifstream &file)
{
	unsigned char bgTypeTmp;
	file.read((char*)&bgTypeTmp, sizeof(unsigned char));
	bgType = static_cast<ThingsBackgroundType>(bgTypeTmp);
	file.read((char*)&numberOfThings, sizeof(unsigned));
	file.read((char*)&setID, sizeof(unsigned));
	
	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->load(file, spriteLibrary);
}

/*
 * funkcja �aduj�ca zestaw Sprite'�w do spriteLibrary. Nowy Sprite tworzy tylko wtedy gdy nie jest zawarty w spriteLibrary.
 */
void ThingsBackground::loadSpriteSet(unsigned setID)
{
	if(this->spriteLibrary.count(setID) == 0)
	{
		std::vector<Sprite*> *spriteSet = new std::vector<Sprite*>();
		TBGfile *tbgFile = new TBGfile((*files)[setID]);

		switch(bgType)
		{
		case BG_BLINK:
			for(unsigned char i = 0; i < tbgFile->getBitmapCount(); i++)
			{
				spriteSet->push_back(new Sprite(tbgFile->bitmapIsTinted(i)));
					Animation *spriteAnim = new Animation();
					spriteAnim->addFrame(gameCore->bitmapLoader->loadBitmap_f(tbgFile->getFileData(i), tbgFile->getFileLength(i), tbgFile->getFileType(i)), STD_FRAME_TIME);
					spriteAnim->setRatio(gameCore->appSettings->getScreenWidth() * 0.125);					
					(*spriteSet)[i]->addAnimation(spriteAnim);
					(*spriteSet)[i]->setDuration(DRAWING_THING_TIME);
					(*spriteSet)[i]->setTransparencyInDuration(DRAWING_TRANSPARENCY_TIME);
					(*spriteSet)[i]->setTransparencyOutDuration(DRAWING_TRANSPARENCY_TIME);
			}
			break;
		case BG_FALL: case BG_ROLL:
			for(unsigned char i = 0; i < tbgFile->getBitmapCount(); i++)
			{
				spriteSet->push_back(new Sprite(tbgFile->bitmapIsTinted(i)));
					Animation *spriteAnim = new Animation();
					spriteAnim->addFrame(gameCore->bitmapLoader->loadBitmap_f(tbgFile->getFileData(i), tbgFile->getFileLength(i), tbgFile->getFileType(i)), DRAWING_THING_TIME);
					spriteAnim->setRatio(gameCore->appSettings->getScreenWidth() * 0.125);
					(*spriteSet)[i]->addAnimation(spriteAnim);
			}
			break;
		}
		spriteLibrary[setID] = spriteSet;
		delete tbgFile;
	}
}

void ThingsBackground::prepareBackground()
{
	setID = initialSelectedGroup;
	loadSpriteSet(setID);

	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->prepareBackground(spriteLibrary[setID]);
}

void ThingsBackground::randomType()
{
	bgType = static_cast<ThingsBackgroundType>(rand() % 3);
}

void ThingsBackground::reset()
{
	for(std::map<unsigned, std::vector<Sprite*>*>::iterator it = spriteLibrary.begin(); it != spriteLibrary.end(); it++)
		delete it->second;
	spriteLibrary.clear();

	setID = initialSelectedGroup;

	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->reset();
}

void ThingsBackground::save(std::ofstream &file)
{
	unsigned char bgTypeTmp = static_cast<unsigned char>(bgType);
	file.write((char*)&bgTypeTmp, sizeof(unsigned char));
	file.write((char*)&numberOfThings, sizeof(unsigned));
	file.write((char*)&setID, sizeof(unsigned));

	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->save(file);
}

void ThingsBackground::setNextThing()
{
	setID++;
	if(setID == totalSets)
		setID = 0;
	
	loadSpriteSet(setID);
	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->prepareNewThing(setID, spriteLibrary[setID]);
}

void ThingsBackground::setPreviousThing()
{
	if(setID == 0)
		setID = totalSets;
	else
		setID--;
	
	loadSpriteSet(setID);
	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->prepareNewThing(setID, spriteLibrary[setID]);
}

void ThingsBackground::setType(ThingsBackgroundType newType)
{
	bgType = newType;
}

void ThingsBackground::update(unsigned elapsedTime)
{
	for(unsigned i = 0; i < numberOfThings; i++)
		things[i]->update(elapsedTime);
}