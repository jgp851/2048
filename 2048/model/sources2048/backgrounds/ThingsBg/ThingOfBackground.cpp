#include "ThingOfBackground.h"
#include "ThingsBackground.h"
#include "..\..\..\gamecore\Sprite.h"
#include "..\..\Game2048Core.h"

ThingOfBackground::ThingOfBackground(Game2048Core *gameCore, ThingsBackground *thingsBg, std::vector<Sprite*> *newSet, unsigned setID)
{
	this->gameCore = gameCore;
	this->thingsBg = thingsBg;
	this->localSprite = new Sprite();

	this->currentSet = NULL;
	this->preparedSet = NULL;
	this->isPreparedSet = false;
	this->currentTime = 0;
	this->setID = setID;

	prepareBackground(newSet);
}

ThingOfBackground::~ThingOfBackground()
{
	delete localSprite;
}

void ThingOfBackground::detectEntranceOn()
{
	if(localSprite->getX() >= 0 &&
		localSprite->getY() >= 0 &&
		localSprite->getX() + localSprite->getScaledWidth() < gameCore->appSettings->getScreenWidth() &&
		localSprite->getY() + localSprite->getScaledHeight() < gameCore->appSettings->getScreenHeight()
		)
		spriteIsOnScreen = true;
}

void ThingOfBackground::detectWallCollisions()
{
	if(localSprite->getX() < 0)
	{
		localSprite->setX(-localSprite->getX());
		localSprite->setDX(-localSprite->getDX());
	}
	if(localSprite->getX() + localSprite->getScaledWidth() >= gameCore->appSettings->getScreenWidth())
	{
		localSprite->setX(localSprite->getX() - (localSprite->getX() + localSprite->getScaledWidth() - gameCore->appSettings->getScreenWidth()));
		localSprite->setDX(-localSprite->getDX());
	}
	if(localSprite->getY() < 0)
	{
		localSprite->setY(-localSprite->getY());
		localSprite->setDY(-localSprite->getDY());
	}
	if(localSprite->getY() + localSprite->getScaledHeight() >= gameCore->appSettings->getScreenHeight())
	{
		localSprite->setY(localSprite->getY() - (localSprite->getY() + localSprite->getScaledHeight() - gameCore->appSettings->getScreenHeight()));
		localSprite->setDY(-localSprite->getDY());
	}
}

void ThingOfBackground::detectWallCrossOver(unsigned elapsedTime)
{
	if((localSprite->getX() + localSprite->getScaledWidth() < 0 ||
		localSprite->getX() >= gameCore->appSettings->getScreenWidth()) &&
		(localSprite->getY() + localSprite->getScaledHeight() < 0 ||
		localSprite->getY() >= gameCore->appSettings->getScreenHeight()
		))
		setLocalSpriteStartProperties(elapsedTime);
}

void ThingOfBackground::draw()
{
	localSprite->draw();
}

void ThingOfBackground::load(std::ifstream &file, std::map<unsigned, std::vector<Sprite*>*> &spriteLibrary)
{
	int transOffset;
	unsigned transInProgress;
	unsigned transOutProgress;
	ALLEGRO_COLOR color, colorDest;
	file.read((char*)&color, sizeof(ALLEGRO_COLOR));
	file.read((char*)&colorDest, sizeof(ALLEGRO_COLOR));
	
	float x, y, px, py, spriteAccelerationX, spriteAccelerationY;
	file.read((char*)&x, sizeof(float));
	file.read((char*)&y, sizeof(float));
		px = x * gameCore->appSettings->getScreenWidth() / 100;
		py = y * gameCore->appSettings->getScreenHeight() / 100;
	switch(thingsBg->getBgType())
	{
	case BG_BLINK:
		{
			file.read((char*)&transOffset, sizeof(int));
			file.read((char*)&transInProgress, sizeof(unsigned));
			file.read((char*)&transOutProgress, sizeof(unsigned));
		}
		break;
	case BG_FALL:
		file.read((char*)&spriteAccelerationY, sizeof(float));
		break;
	case BG_ROLL:
		file.read((char*)&spriteAccelerationX, sizeof(float));
		file.read((char*)&spriteAccelerationY, sizeof(float));
		file.read((char*)&spriteIsOnScreen, sizeof(bool));
		break;
	}
	
	file.read((char*)&currentTime, sizeof(unsigned));
	
	file.read((char*)&isPreparedSet, sizeof(bool));
	file.read((char*)&bitmapNO, sizeof(unsigned));
	file.read((char*)&setID, sizeof(unsigned));
	if(isPreparedSet)
		file.read((char*)&preparedSetID, sizeof(unsigned));
	
	// ustawienie localSprite
		if(isPreparedSet)
		{
			this->thingsBg->loadSpriteSet(setID);
			this->thingsBg->loadSpriteSet(preparedSetID);
			this->currentSet = this->thingsBg->spriteLibrary[setID];
			this->preparedSet = this->thingsBg->spriteLibrary[preparedSetID];
		}
		else
		{
			thingsBg->loadSpriteSet(setID);
			this->currentSet = this->thingsBg->spriteLibrary[setID];
		}

		this->localSprite->copySprite((*currentSet)[bitmapNO]);
			this->localSprite->setCurrentTime(currentTime);
			this->localSprite->setTintColor(color);
			this->localSprite->setTintColorDest(colorDest);
			this->localSprite->setX(px);
			this->localSprite->setY(py);

		switch(thingsBg->getBgType())
		{
		case BG_BLINK:
			localSprite->setTransOffset(transOffset);
			localSprite->setTransInProgress(transInProgress);
			localSprite->setTransOutProgress(transOutProgress);
			localSprite->setSpriteType(SPRITE_TRANSPARENCY_LOOPED);
			break;
		case BG_FALL:
			localSprite->setDY(spriteAccelerationY);
			localSprite->setSpriteType(SPRITE_MANUAL);
			break;
		case BG_ROLL:
			localSprite->setDX(spriteAccelerationX);
			localSprite->setDY(spriteAccelerationY);
			localSprite->setSpriteType(SPRITE_MANUAL);
			break;
		}
}

// przygotowanie obiektu do nowej gry. Zalecany uprzedni reset.
void ThingOfBackground::prepareBackground(std::vector<Sprite*> *currentSet)
{
	this->currentSet = currentSet;
	setNewThing();

	setLocalSpriteStartProperties(0);
	setLocalSpriteRandomStartProperties();
}

// Ustawienie grupy things. Zapisanie nowych danych do p�l tymczasowych. Je�li jeszcze jest wy�wietlana stara bitmapa to nowa b�dzie wy�wietlona dopiero po wyga�ni�ciu starej.
void ThingOfBackground::prepareNewThing(unsigned preparedSetID,  std::vector<Sprite*> *preparedSet)
{
	this->preparedSet = preparedSet;
	this->preparedSetID = preparedSetID;
	isPreparedSet = true;
}

void ThingOfBackground::reset()
{
	isPreparedSet = false;
	currentSet = NULL;
	preparedSet = NULL;
}

void ThingOfBackground::save(std::ofstream &file)
{
	ALLEGRO_COLOR color = localSprite->getTintColor();
	file.write((char*)&color, sizeof(ALLEGRO_COLOR));
	ALLEGRO_COLOR colorDest = localSprite->getTintColorDest();
	file.write((char*)&colorDest, sizeof(ALLEGRO_COLOR));
	
	float x = localSprite->getX() * 100 / gameCore->appSettings->getScreenWidth();
	float y = localSprite->getY() * 100 / gameCore->appSettings->getScreenHeight();
	file.write((char*)&x, sizeof(float));
	file.write((char*)&y, sizeof(float));
	switch(thingsBg->getBgType())
	{
	case BG_BLINK:
		{
			int transOffset = localSprite->getTransOffset();
			unsigned transInProgress = localSprite->getTransInProgress();
			unsigned transOutProgress = localSprite->getTransOutProgress();
			file.write((char*)&transOffset, sizeof(int));
			file.write((char*)&transInProgress, sizeof(unsigned));
			file.write((char*)&transOutProgress, sizeof(unsigned));
		}
		break;
	case BG_FALL:
		{
			float spriteAcceleration = localSprite->getDY();
			file.write((char*)&spriteAcceleration, sizeof(float));
		}
		break;
	case BG_ROLL:
		{
			float spriteAccelerationX = localSprite->getDX();
			float spriteAccelerationY = localSprite->getDY();
			file.write((char*)&spriteAccelerationX, sizeof(float));
			file.write((char*)&spriteAccelerationY, sizeof(float));
			file.write((char*)&spriteIsOnScreen, sizeof(bool));
		}
		break;
	}
	
	file.write((char*)&currentTime, sizeof(unsigned));
	
	file.write((char*)&isPreparedSet, sizeof(bool));
	file.write((char*)&bitmapNO, sizeof(unsigned));
	file.write((char*)&setID, sizeof(unsigned));
	if(isPreparedSet)
		file.write((char*)&preparedSetID, sizeof(unsigned));
}

void ThingOfBackground::setLocalSpriteRandomStartProperties()
{
	float x = 0, y = 0;

	switch(thingsBg->getBgType())
	{
	case BG_BLINK:
		currentTime = rand() % localSprite->getDuration();
		localSprite->setCurrentTime(currentTime);
		break;
	case BG_FALL:
		y = rand() % gameCore->appSettings->getScreenHeight() - localSprite->getScaledHeight();
		localSprite->setY(y);
		break;
	case BG_ROLL:
		{
			localSprite->setSpriteType(SPRITE_MANUAL);
			spriteIsOnScreen = true;
			float spriteAccelerationX = rand() % 15 / 100.0 + 0.05;
			float spriteAccelerationY = rand() % 15 / 100.0 + 0.05;
			x = rand() % (gameCore->appSettings->getScreenWidth() - localSprite->getScaledWidth());
			y = rand() % (gameCore->appSettings->getScreenHeight() - localSprite->getScaledHeight());
			localSprite->setX(x);
			localSprite->setY(y);
			unsigned place = rand() & 2;
			unsigned direction = rand() % 2;
			
			if(x < gameCore->appSettings->getScreenWidth() / 2)
				localSprite->setDX(spriteAccelerationX);
			else
				localSprite->setDX(-spriteAccelerationX);
			if(y < gameCore->appSettings->getScreenHeight() / 2)
				localSprite->setDY(spriteAccelerationY);
			else
				localSprite->setDY(-spriteAccelerationY);
		}
		break;
	}
}

void ThingOfBackground::setLocalSpriteStartProperties(unsigned elapsedTime)
{
	float x = 0, y = 0, spriteAcceleration = 0;

	setNewThing();
	switch(thingsBg->getBgType())
	{
	case BG_BLINK:
		currentTime %= localSprite->getDuration();
		localSprite->setCurrentTime(currentTime);
		localSprite->setSpriteType(SPRITE_TRANSPARENCY_LOOPED);
		x = rand() % (gameCore->appSettings->getScreenWidth() - localSprite->getScaledWidth());
		y = rand() % (gameCore->appSettings->getScreenHeight() - localSprite->getScaledHeight());
		break;
	case BG_FALL:
		localSprite->setSpriteType(SPRITE_MANUAL);
		x = rand() % (gameCore->appSettings->getScreenWidth() - localSprite->getScaledWidth());
		y = 0 - localSprite->getScaledHeight();
		spriteAcceleration = rand() % 12 / 100.0 + 0.05;
		localSprite->setDY(spriteAcceleration);
		break;
	case BG_ROLL:
		{
			localSprite->setSpriteType(SPRITE_MANUAL);
			spriteIsOnScreen = false;
			
			float spriteAccelerationX = rand() % 15 / 100.0 + 0.05;
			float spriteAccelerationY = rand() % 15 / 100.0 + 0.05;
			unsigned place = rand() % 2;
			unsigned direction = rand() % 2;

			switch(place)
			{
			case 0: // po lewej lub prawej stronie planszy
				switch(direction)
				{
				case 0: // lewa strona
					x = 0 - localSprite->getScaledWidth();
					localSprite->setDX(spriteAccelerationX);
					break;
				case 1: // prawa strona
					x = gameCore->appSettings->getScreenWidth();
					localSprite->setDX(-spriteAccelerationX);
					break;
				}

				y = rand() % (gameCore->appSettings->getScreenHeight() + localSprite->getScaledHeight() * 2) - localSprite->getScaledHeight();
				if(y < gameCore->appSettings->getScreenHeight() / 2)
					localSprite->setDY(spriteAccelerationY);
				else
					localSprite->setDY(-spriteAccelerationY);
				break;
			case 1: // nad lub pod plansz�
				switch(direction)
				{
				case 0: // nad
					y = 0 - localSprite->getScaledHeight();
					localSprite->setDY(spriteAccelerationY);
					break;
				case 1: // pod
					y = gameCore->appSettings->getScreenHeight();
					localSprite->setDY(-spriteAccelerationY);
					break;
				}

				x = rand() % (gameCore->appSettings->getScreenWidth() + localSprite->getScaledWidth() * 2) - localSprite->getScaledWidth();
				if(x < gameCore->appSettings->getScreenWidth() / 2)
					localSprite->setDX(spriteAccelerationX);
				else
					localSprite->setDX(-spriteAccelerationX);
				break;
			}
		}
		break;
	}
	localSprite->setX(x);
	localSprite->setY(y);
	localSprite->randomTintColor();
}

void ThingOfBackground::setNewThing()
{
	if(isPreparedSet)
	{
		currentSet = preparedSet;
		setID = preparedSetID;
		isPreparedSet = false;
	}
	bitmapNO = rand() % currentSet->size();
	localSprite->copySprite((*currentSet)[bitmapNO]);
}

void ThingOfBackground::update(unsigned elapsedTime)
{
	currentTime += elapsedTime;
	switch(thingsBg->getBgType())
	{
	case BG_BLINK:
		if(currentTime > localSprite->getDuration())
			setLocalSpriteStartProperties(elapsedTime);
		break;
	case BG_FALL:
		if(localSprite->getY() > gameCore->appSettings->getScreenHeight())
			setLocalSpriteStartProperties(elapsedTime);
		break;
	case BG_ROLL:
		if(!isPreparedSet)
		{
			if(spriteIsOnScreen)
				detectWallCollisions();
			else
				detectEntranceOn();
		}
		else
			detectWallCrossOver(elapsedTime);
		break;
	}
	localSprite->update(elapsedTime);
}