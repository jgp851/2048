#pragma once
#include <allegro5\allegro.h>
#include <map>
#include <vector>

class Game2048Core;
class Sprite;
class ThingsBackground;

/** ThingOfBackground - klasa reprezentuj�ca widzialny obiekt na tle ThingsBackground
 *
 * - unsigned r, g, b, int a - losowane kolory i kana� przezroczysto�ci, je�li bitmapa jest koloryzowana.
 * - int x, y                - procentowe wsp�rz�dne na ekranie.
 * - int dx, dy              - obliczone wsp�rz�dne w pikselach.
 */

class ThingOfBackground
{
private:
	Game2048Core *gameCore;
	ThingsBackground *thingsBg;
	Sprite *localSprite;

	bool isPreparedSet;
	bool spriteIsOnScreen;
	unsigned r;
	unsigned g;
	unsigned b;

	unsigned currentTime;
	
	std::vector<Sprite*> *currentSet;
	std::vector<Sprite*> *preparedSet;
	unsigned bitmapNO;
	unsigned setID;
	unsigned preparedSetID;
	
	void detectEntranceOn();
	void detectWallCollisions();
	void detectWallCrossOver(unsigned elapsedTime);
	void prepareNewThing(unsigned preparedSetID, std::vector<Sprite*> *preparedSet);
	void setLocalSpriteStartProperties(unsigned elapsedTime);
	void setLocalSpriteRandomStartProperties();
	void setNewThing();
public:
	ThingOfBackground(Game2048Core *gameCore, ThingsBackground *thingsBg, std::vector<Sprite*> *newSet, unsigned setID);
	~ThingOfBackground();
	
	void draw();
	void load(std::ifstream &file, std::map<unsigned, std::vector<Sprite*>*> &spriteLibrary);
	void prepareBackground(std::vector<Sprite*> *preparedSet);
	void reset();
	void save(std::ofstream &file);
	void update(unsigned elapsedTime);

	friend class ThingsBackground;
};