#include "TBGfile.h"
#include "Thing.h"
#include <fstream>
#include <string>

TBGfile::TBGfile(std::string filePath)
{
	readFile(filePath);
}
TBGfile::~TBGfile()
{
	for(unsigned char i = 0; i < bitmapCount; i++)
		delete fileList[i];
}

bool TBGfile::bitmapIsTinted(unsigned char id)
{
	return fileList[id]->isTinted();
}

unsigned char TBGfile::getBitmapCount()
{
	return fileList.size();
}

char* TBGfile::getFileData(unsigned char id)
{
	return fileList[id]->getFileData();
}

int TBGfile::getFileLength(unsigned char id)
{
	return fileList[id]->getFileLength();
}

std::string TBGfile::getFileType(unsigned char id)
{
	return fileList[id]->getFileType();
}

void TBGfile::readFile(std::string filePath)
{
	std::ifstream tbgFile(filePath.c_str(), std::ios::in | std::ios::binary);
	tbgFile.read((char*)&bitmapCount, sizeof(char));
	for(unsigned char i = 0; i < bitmapCount; i++)
	{
		Thing *thing = new Thing();
		thing->readFile(tbgFile);
		fileList.push_back(thing);
	}
	tbgFile.close();
}