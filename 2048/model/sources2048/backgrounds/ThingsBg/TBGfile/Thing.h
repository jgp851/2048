#pragma once
#include <string>

class Thing
{
private:
	bool coloring;
	char *dataFile;
	int fileLength;
	std::string fileType;
public:
	Thing();
	~Thing();
	bool isTinted();
	char* getFileData();
	int getFileLength();
	std::string getFileType();
	void readFile(std::ifstream &tbgFile);
};