#pragma once
#include <vector>

class Thing;

class TBGfile
{
private:
	unsigned char bitmapCount;
	std::vector<Thing*> fileList;
	void readFile(std::string filePath);
public:
	TBGfile(std::string filePath);
	~TBGfile();

	bool bitmapIsTinted(unsigned char id);
	unsigned char getBitmapCount();
	char* getFileData(unsigned char id);
	int getFileLength(unsigned char id);
	std::string getFileType(unsigned char id);
};