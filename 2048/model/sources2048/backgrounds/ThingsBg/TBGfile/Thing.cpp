#include "Thing.h"
#include <fstream>

Thing::Thing()
{
	dataFile = NULL;
}
Thing::~Thing()
{
	if(dataFile)
		delete[] dataFile;
}

bool Thing::isTinted()
{
	return coloring;
}

char* Thing::getFileData()
{
	return dataFile;
}

int Thing::getFileLength()
{
	return fileLength;
}

std::string Thing::getFileType()
{
	return fileType;
}

void Thing::readFile(std::ifstream &tbgFile)
{
	tbgFile.read((char*)&coloring, sizeof(bool));
	
	unsigned char strLen;
	tbgFile.read((char*)&strLen, sizeof(char));
	char* str = new char[strLen + 1];
	tbgFile.read(str, strLen);
	str[strLen] = '\0';
	fileType = str;
	delete[] str;
	
	tbgFile.read((char*)&fileLength, sizeof(int));
	dataFile = new char[fileLength];
	tbgFile.read(dataFile, fileLength);
}