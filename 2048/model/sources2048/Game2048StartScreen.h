#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_font.h>
#include <string>

#define START_SCREEN_TITLE_SCROLL_TIME 300
#define COLOR_TRANS_OFFSET 160

class Button;
class Game2048Core;

class CpyRightPanel;

class Game2048StartScreen
{
private:
	Game2048Core *gameCore;
	CpyRightPanel *cpyRightPanel;
	
	Button *buttonAnyKey;
	ALLEGRO_BITMAP *title;
	int logoEndPosition;
	int logoScrollProgress;
	int logoStartPosition;
public:
	Game2048StartScreen(Game2048Core *gameCore);
	void draw();
	void update(unsigned elapsedTime);
};

class CpyRightPanel
{
private:
	Game2048Core *gameCore;
	
	ALLEGRO_COLOR cpyColor;
	bool colorToUp;
	int colorUpSection;
	int cpyColorValue;
	
	int destTimeToPlayMusic;
	int elapsedTimeToPlayMusic;
	
	std::string copyright;
	
	void randCpyColorUpSection();
	void updateCopyrightColor(unsigned elapsedTime);
public:
	CpyRightPanel(Game2048Core *gameCore);
	
	bool wasStartedMusic;

	bool isTimeToStartMusic();
	bool isMusicStarted();
	void draw();
	void update(unsigned elapsedTime);
};
