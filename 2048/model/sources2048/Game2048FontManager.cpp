#include "Game2048FontManager.h"
#include "Game2048Core.h"

#define FONT_PATH "gfx/arial.ttf"

Game2048FontManager::Game2048FontManager(Game2048Core *gameCore)
{
	this->gameCore = gameCore;
		
	font_number8 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 8, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number10 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 10, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number12 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 12, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number16 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 16, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number18 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 18, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number25 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 25, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number28 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 28, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number32 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 32, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_number36 = new Fonts(gameCore->fontLoader->loadFont(FONT_PATH, 36, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	
	font_arial = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.02, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_LEFT);
	font_buttonsInfo = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", gameCore->appSettings->getScreenWidth() * 0.0225, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_fallingSkyCondObl_albumAndTitle = new Fonts(gameCore->fontLoader->loadFont("gfx/FallingSkyCondObl.otf", gameCore->appSettings->getScreenHeight() * 0.033, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_LEFT);
	font_fallingSkyCondObl_performer = new Fonts(gameCore->fontLoader->loadFont("gfx/FallingSkyCondObl.otf", gameCore->appSettings->getScreenHeight() * 0.038, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_LEFT);
	font_gameDetails = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.025, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_LEFT);
	font_gameInfoBox = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenHeight() * 0.0267, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_LEFT); // 16
	font_gameOver = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", 32, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_infoAboutRebooting = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", 16, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_lastEarnedPoints = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", 32, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_mainMenu = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", 32, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_options = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", 32, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_pathTexture = new Fonts(gameCore->fontLoader->loadFont("gfx/cour.ttf", gameCore->appSettings->getScreenWidth() * 0.0225, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_pressAnyKey = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", 32, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_scoresFont = new Fonts(gameCore->fontLoader->loadFont("gfx/kirsty.ttf", 32, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	
	font_furtherRandom = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.0125, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_nextRandom = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.0225, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_selectedRandom = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.035, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);

	font_furtherStandard = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.01, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_furtherStandardHex = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.008, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_nextStandard = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.02, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_nextStandardHex = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.016, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_selectedStandard = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.03125, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);
	font_selectedStandardHex = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.027, 0), al_map_rgb(255, 255, 255), ALLEGRO_ALIGN_CENTER);

	font_saveWindowPath = new Fonts(gameCore->fontLoader->loadFont("gfx/cour.ttf", gameCore->appSettings->getScreenWidth() * 0.025, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
	font_saveWindowPathTitle = new Fonts(gameCore->fontLoader->loadFont("gfx/arial.ttf", gameCore->appSettings->getScreenWidth() * 0.025, 0), al_map_rgb(0, 0, 0), ALLEGRO_ALIGN_CENTER);
}

Game2048FontManager::~Game2048FontManager()
{
}