#pragma once
#include <allegro5\allegro_audio.h>

class Game2048Core;

class Game2048SoundPlayer
{
private:
	Game2048Core *gameCore;

	ALLEGRO_SAMPLE *goIn;
	ALLEGRO_SAMPLE *goOut;
	ALLEGRO_SAMPLE *switchButtons;
	ALLEGRO_SAMPLE *switchMenu;
	
	bool *soundEffectsTurned;
	float *gain;
	float pan;
	float speed;
	float maxVolume;
	float minVolume;
public:
	Game2048SoundPlayer(Game2048Core *gameCore);
	~Game2048SoundPlayer();

	bool getSoundEffectsTurned();

	float* getVolumeAddress();
	float getMaxVolume();
	float getMinVolume();
	float getVolumeValue();

	void playGoIn(bool forcedVoice = false);
	void playGoOut(bool forcedVoice = false);
	void playSwitchButtons(bool forcedVoice = false);
	void playSwitchMenu(bool forcedVoice = false);
	void playSample(ALLEGRO_SAMPLE *sample);
	void playSample(ALLEGRO_SAMPLE *sample, float volume);
	void playSampleForced(ALLEGRO_SAMPLE *sample);
	void playSampleForced(ALLEGRO_SAMPLE *sample, float volume);
	void setTurned(bool state);
	void setVolume(float volume);
};