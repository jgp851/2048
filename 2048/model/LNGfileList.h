// LNGfileList.h - C++ header created for LNGfileManage
// 2015 Copyright (c) Jerzy Gubala. All rights reserved.

#pragma once

#define LNG_NO 0 // NIE
#define LNG_YES 1 // TAK
#define LNG_PRESS_ANY_KEY 2 // Naciśnij dowolny klawisz
#define LNG_NEW_GAME 3 // Nowa gra
#define LNG_LOAD_GAME 4 // Wczytaj grę
#define LNG_OPTIONS 5 // Opcje
#define LNG_EXIT 6 // Wyjście
#define LNG_INFO_ABOUT_RESTART 7 // Zmieniłeś wartości opcji, które wymagają ponownego uruchomienia gry.
#define LNG_INFO_ABOUT_REBOOTING 8 // Po zmianie tej wartości, konieczny będzie restart gry.
#define LNG_CONFIRM_CHANGES 9 // Zatwierdź zmiany
#define LNG_GRAPHICS 10 // Grafika
#define LNG_RESOLUTION 11 // Rozdzielczość: 
#define LNG_FULL_SCREEN 12 // Full Screen: 
#define LNG_LANGUAGE 13 // Język: 
#define LNG_SOUND 14 // Dźwięk
#define LNG_GAME_MUSIC_VOLUME 15 // Głośność muzyki w trakcie rozgrywki
#define LNG_MENU_MUSIC_VOLUME 16 // Głośność muzyki w menu
#define LNG_SOUND_EFFECTS_VOLUME 17 // Głośność efektów dźwiękowych
#define LNG_RANDOMIZED 18 // Losowo
#define LNG_SEQUENTIAL 19 // Po kolei
#define LNG_NO_GAME_FILE 20 // Nie masz żadnej zapisanej gry!
#define LNG_TEXTURE_STD1 21 // Tekstura
#define LNG_TEXTURE_STD2 22 // standardowa
#define LNG_TEXTURE_RANDOM1 23 // Losowa
#define LNG_TEXTURE_RANDOM2 24 // tekstura
#define LNG_LAST_MOD_TIME 25 // Ostatnio grano: 
#define LNG_BOARD_TYPE 26 // Rodzaj planszy: 
#define LNG_BOARD_TYPE_STD 27 // standardowa
#define LNG_BOARD_TYPE_HEX 28 // heksagonalna
#define LNG_BOARD_SIZE_X 29 // Rozmiar planszy w poziomie: 
#define LNG_BOARD_SIZE_Y 30 // Rozmiar planszy w pionie: 
#define LNG_BOARD_SIZE 31 // Rozmiar planszy: 
#define LNG_GAME_MODE 32 // Tryb rozgrywki: 
#define LNG_GAME_MODE_STD 33 // standardowy
#define LNG_GAME_MODE_BACK 34 // backwards
#define LNG_GAME_MODE_FIB 35 // fibonacci
#define LNG_WINNING_NUMBER 36 // Liczba wygrywająca: 
#define LNG_BEGIN_NUMBER 37 // Liczba początkowa: 
#define LNG_GAME_TIME 38 // Czas gry: 
#define LNG_GAMEPLAY_WON 39 // Rozgrywka wygrana: 
#define LNG_TEXTURE_TYPE 40 // Rodzaj tekstury: 
#define LNG_TEXTURE_DEFAULT 41 // wbudowana
#define LNG_TEXTURE_CUSTOM 42 // użytkownika
#define LNG_COLORING_CUSTOM_TEXTURES 43 // Kolorowanie tekstury: 
#define LNG_START_GAME 44 // Rozpocznij grę
#define LNG_ADD_TEXTURE 45 // Dodaj teksturę
#define LNG_SELECT_TEXTURE 46 // Wybierz teksturę
#define LNG_RANDOM_TEXTURE 47 // Losuj teksturę: 
#define LNG_WRITE_FILE_NAME 48 // Podaj nazwę pliku
#define LNG_WRITE_PATH_FILE 49 // Podaj lokalizacje pliku:
#define LNG_FAILED_PATH 50 // Nieodpowiedni format pliku, jego brak lub błędna ścieżka!
#define LNG_ADDING_DONE 51 // Teksturę dodano pomyślnie!
#define LNG_NO_TEXTURE 52 // Nie masz żadnej tekstury! Dodaj teksturę.
#define LNG_MOVE_TEXTURE 53 // Przesuń teksturę
#define LNG_DELETE_TEXTURE 54 // Usuń teksturę
#define LNG_INFO_ABOUT_FILE_TYPE_OF_TEXTURE 55 // Obsługiwane pliki z rozszerzeniami: *.png, *.bmp, *.jpg, *.jpeg, *.pcx lub *.tga
#define LNG_NOTE_ABOUT_HEXAGON_TEXTURE 56 // Pamiętaj, że sam musisz zadbać o to, aby tekstura miała odpowiednio przezroczyste boki na kształt sześciokąta!
#define LNG_OVERWRITE_FILE_QST 57 // Czy nadpisać plik?
#define LNG_OVERWRITE_FILE 58 // Nadpisz
#define LNG_CREATE_NEW_FILE 59 // Stwórz nowy
#define LNG_THE_SAME_FILE_QST 60 // Plik o podanej nazwie już istnieje. Nadpisać istniejący plik?
#define LNG_CHANGE_NAME 61 // Zmień nazwę
#define LNG_STEPS_COUNT 62 // Liczba kroków: 
#define LNG_POINTS 63 // Punkty: 
#define LNG_YOU_WIN 64 // Wygrałeś!
#define LNG_GAME_CONTINUE 65 // Graj dalej
#define LNG_BACK_TO_MAIN_MENU 66 // Powrót do menu głównego
#define LNG_BACK_TO_GAME 67 // Powrót do gry
#define LNG_SAVE_GAME 68 // Zapisz grę
#define LNG_NO_MOVES_YOU_LOSE 69 // Brak ruchów, przegrałeś!
#define LNG_NO_MOVES_BACK 70 // Wróć i wycofaj ruchy
#define LNG_NO_MOVES_EXIT 71 // Zakończ rozgrywkę
#define LNG_TEXTURE 72 // Tekstura
#define LNG_TEXTURES_DEFAULT 73 // Tekstury wbudowane
#define LNG_TEXTURES_CUSTOM 74 // Tekstury użytkownika