#include "model/sources2048/Game2048Core.h"
#undef UNICODE  // LPCSTR dzia�a zar�wno w DEBUG i RELEASE
#include <windows.h>

int main(int argc, char **argv)
{
	GameCore *game = new Game2048Core();
	bool initSuccess = game->init();
	if (!initSuccess)
		return 1;
	game->mainLoop();

	if(!game->getRestartAppSetting())
		return 0;
	else
		return (ShellExecute(NULL, NULL, (LPCSTR)"2048.exe", NULL, NULL, SW_SHOWNORMAL) || 0);
}